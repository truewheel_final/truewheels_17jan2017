﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrueWheels.BEL;


namespace TrueWheels.DAL
{
  public  class BookingDAL
    {
        string connectionString = ConfigurationManager.ConnectionStrings["conStr"].ConnectionString;
      //  Transaction transaction = new Transaction();
        public BookingResult BookParkingArea(BookingBEL bookingBel)
      {
          BookingResult bookingResult = new BookingResult();
          //SqlConnection conn = new SqlConnection();
          Int32 BookingId =0;
          DataSet ds = null;
          try
          {              
              SqlParameter[] SqlParms = new SqlParameter[16];

              SqlParms[0] = new SqlParameter("@User_Id", SqlDbType.Int);
              SqlParms[0].Direction = ParameterDirection.Input;
              SqlParms[0].Value = bookingBel.userId;

              SqlParms[1] = new SqlParameter("@Parking_Id", SqlDbType.VarChar);
              SqlParms[1].Direction = ParameterDirection.Input;
              SqlParms[1].Value = bookingBel.parkingId;

              SqlParms[2] = new SqlParameter("@Vehical_no", SqlDbType.VarChar);
              SqlParms[2].Direction = ParameterDirection.Input;
              SqlParms[2].Value = bookingBel.vehicalNo;

              SqlParms[3] = new SqlParameter("@Mobile_No", SqlDbType.VarChar);
              SqlParms[3].Direction = ParameterDirection.Input;
              SqlParms[3].Value = bookingBel.mobileNo;

              SqlParms[4] = new SqlParameter("@OTP", SqlDbType.VarChar);
              SqlParms[4].Direction = ParameterDirection.Input;
              SqlParms[4].Value = bookingBel.OTP;

               SqlParms[5] = new SqlParameter("@BookedInTime", SqlDbType.VarChar);
              SqlParms[5].Direction = ParameterDirection.Input;
              SqlParms[5].Value = bookingBel.bookedInTime;

               SqlParms[6] = new SqlParameter("@BookedOutdatetime", SqlDbType.VarChar);
              SqlParms[6].Direction = ParameterDirection.Input;
              SqlParms[6].Value = bookingBel.bookedOutDatetime;

              SqlParms[7] = new SqlParameter("@CheckedInDateTime", SqlDbType.VarChar);
              SqlParms[7].Direction = ParameterDirection.Input;
              SqlParms[7].Value = bookingBel.checkedInDateTime;

              SqlParms[8] = new SqlParameter("@AllotedSeat", SqlDbType.VarChar);
              SqlParms[8].Direction = ParameterDirection.Input;
              SqlParms[8].Value = bookingBel.allotedSeat;

              SqlParms[9] = new SqlParameter("@VehicleWheels", SqlDbType.Int );
              SqlParms[9].Direction = ParameterDirection.Input;
              SqlParms[9].Value = bookingBel.vehicleWheels;

              SqlParms[10] = new SqlParameter("@MonthlySubscription", SqlDbType.VarChar);
              SqlParms[10].Direction = ParameterDirection.Input;
              SqlParms[10].Value = bookingBel.MonthlySubscription;

              SqlParms[11] = new SqlParameter("@AutoSubscrip", SqlDbType.VarChar);
              SqlParms[11].Direction = ParameterDirection.Input;
              SqlParms[11].Value = bookingBel.AutoSubscrip;

              SqlParms[12] = new SqlParameter("@Helmet", SqlDbType.VarChar);
              SqlParms[12].Direction = ParameterDirection.Input;
              SqlParms[12].Value = bookingBel.Helmet;
              
               SqlParms[13] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
              SqlParms[13].Direction = ParameterDirection.Output;

              SqlParms[14] = new SqlParameter("@Booked_Id", SqlDbType.NVarChar, 20);
              SqlParms[14].Direction = ParameterDirection.Output;

              SqlParms[15] = new SqlParameter("@MonthlyChargePaid", SqlDbType.Char);
              SqlParms[15].Direction = ParameterDirection.Input;
              SqlParms[15].Value = bookingBel.MonthlyChargePaid;

              ds= SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Insert_BookedParking", SqlParms);

              if ((SqlParms[13].Value == null || SqlParms[13].Value == "" || SqlParms[13].Value == "0") && SqlParms[14].Value != "0")
              {

                  bookingResult.parkingId = ds.Tables[0].Rows[0]["Parking_Id"].ToString();
                  bookingResult.vehicalNo = ds.Tables[0].Rows[0]["VehicalNumber"].ToString();
                  bookingResult.mobileNo = ds.Tables[0].Rows[0]["MobileNo"].ToString();
                  bookingResult.checkedInDateTime = ds.Tables[0].Rows[0]["CheckedIndateTime"].ToString();
                  bookingResult.bookingID = Convert.ToInt64(ds.Tables[0].Rows[0]["booked_id"]);
                  bookingResult.parkingaddress = ds.Tables[0].Rows[0]["parking_address"].ToString();
              }
              else
              {
                  bookingResult.ErrorMessage = Convert.ToString(SqlParms[13].Value);
                  bookingResult.bookingID = 0;
              }
            }
            catch (Exception ex)
            {
                bookingResult.ErrorMessage = ex.Message.ToString();
               // bookingResult.bookingID = 0;
            }
          return bookingResult;
                       
          }

      public BookingResult CheckOutNGenerateBill(BookingBEL bookingBel)
      {
          BookingResult bookingResult = new BookingResult();
          //SqlConnection conn = new SqlConnection();
          Int32 BookingId = 0;
          DataSet ds = null;
          try
          {
              SqlParameter[] SqlParms = new SqlParameter[4];

              

              SqlParms[0] = new SqlParameter("@Parking_Id", SqlDbType.VarChar);
              SqlParms[0].Direction = ParameterDirection.Input;
              SqlParms[0].Value = bookingBel.parkingId;
              
              //SqlParms[1] = new SqlParameter("@Mobile_No", SqlDbType.VarChar);
              //SqlParms[1].Direction = ParameterDirection.Input;
              //SqlParms[1].Value = bookingBel.mobileNo;

              SqlParms[1] = new SqlParameter("@OTP", SqlDbType.VarChar);
              SqlParms[1].Direction = ParameterDirection.Input;
              SqlParms[1].Value = bookingBel.OTP;
              
              SqlParms[2] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
              SqlParms[2].Direction = ParameterDirection.Output;

              SqlParms[3] = new SqlParameter("@Booked_Id", SqlDbType.VarChar, 20);
              SqlParms[3].Direction = ParameterDirection.Output;


             ds= SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "CheckOutGenerateBill", SqlParms);

             if (SqlParms[2].Value != null && Convert.ToString(SqlParms[2].Value) != "0")
             {
                 bookingResult.bookingID = 0;
                 bookingResult.ErrorMessage = Convert.ToString(SqlParms[2].Value);

             }
              else if(ds!=null && ds.Tables[0].Rows.Count>0)
             {
                 bookingResult.TotalAmount = Convert.ToString(ds.Tables[0].Rows[0]["TotalAmount"]);
                 bookingResult.parkingId = Convert.ToString(ds.Tables[0].Rows[0]["Parking_Id"]);
                 bookingResult.vehicalNo = Convert.ToString(ds.Tables[0].Rows[0]["VehicalNumber"]);
                 bookingResult.mobileNo = Convert.ToString(ds.Tables[0].Rows[0]["MobileNo"]);
                 bookingResult.checkedInDateTime = Convert.ToString(ds.Tables[0].Rows[0]["CheckedIndateTime"]);
                 bookingResult.checkedOutDateTime = Convert.ToString(ds.Tables[0].Rows[0]["CheckedOutdateTime"]);
                 bookingResult.bookingID = Convert.ToInt64(ds.Tables[0].Rows[0]["booked_id"]);
                 bookingResult.TotalHours = Convert.ToString(ds.Tables[0].Rows[0]["TotalHours"]);
                 bookingResult.TotalNights = Convert.ToString(ds.Tables[0].Rows[0]["TotalNights"]);
                 bookingResult.MonthlySubscription = Convert.ToString(ds.Tables[0].Rows[0]["monthlysubscription"]);
                 

             }
              //if (SqlParms[10].Value != null && SqlParms[10].Value != "0")
              //{
              //    BookingId = Convert.ToInt32(SqlParms[10].Value);

              //}
          }
          catch (Exception ex)
          {

          }
          return bookingResult;

      }


      public string GetOTPFromDB(string vehicleNo, string Parking_Id, out string mobileNo, out string errormessage)
      {
          BookingResult bookingResult = new BookingResult();
          //SqlConnection conn = new SqlConnection();
          string otp="0";
          mobileNo = "";
          errormessage = string.Empty;
          
          DataSet ds = null;
          try
          {
              SqlParameter[] SqlParms = new SqlParameter[5];



              SqlParms[0] = new SqlParameter("@vehicleNo", SqlDbType.VarChar);
              SqlParms[0].Direction = ParameterDirection.Input;
              SqlParms[0].Value = vehicleNo;

              SqlParms[1] = new SqlParameter("@Parking_Id", SqlDbType.VarChar);
              SqlParms[1].Direction = ParameterDirection.Input;
              SqlParms[1].Value = Parking_Id;              

              SqlParms[2] = new SqlParameter("@OTP", SqlDbType.VarChar, 6);
              SqlParms[2].Direction = ParameterDirection.Output;

              SqlParms[3] = new SqlParameter("@MobileNo", SqlDbType.VarChar, 10);
              SqlParms[3].Direction = ParameterDirection.Output;

              SqlParms[4] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
              SqlParms[4].Direction = ParameterDirection.Output;



              ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_LatestOTPforvehicleNo", SqlParms);

              if (SqlParms[4].Value != null && Convert.ToString(SqlParms[4].Value) != "0" && Convert.ToString(SqlParms[4].Value) != "")
              {
                  bookingResult.bookingID = 0;
                  errormessage = Convert.ToString(SqlParms[4].Value);

              }
              else if (SqlParms[2].Value != null && Convert.ToString(SqlParms[2].Value) != "0" && Convert.ToString(SqlParms[2].Value) != "")
              {
                  otp = Convert.ToString(SqlParms[2].Value);
              }

              if (SqlParms[3].Value != null && Convert.ToString(SqlParms[3].Value) != "0" && Convert.ToString(SqlParms[3].Value) != "")
              {
                  mobileNo = Convert.ToString(SqlParms[3].Value);

              }
          }
          catch (Exception ex)
          {

          }
          return otp;

      }

      public string SavePayment(PaymentBEL paymentBEL)
      {
          BookingResult bookingResult = new BookingResult();
          //SqlConnection conn = new SqlConnection();
          string result = "FALSE";

          DataSet ds = null;
          try
          {
              SqlParameter[] SqlParms = new SqlParameter[4];



              SqlParms[0] = new SqlParameter("@Booking_ID", SqlDbType.Int);
              SqlParms[0].Direction = ParameterDirection.Input;
              SqlParms[0].Value = paymentBEL.bookingID;

              SqlParms[1] = new SqlParameter("@payingAmount", SqlDbType.Int);
              SqlParms[1].Direction = ParameterDirection.Input;
              SqlParms[1].Value = paymentBEL.paidAmount;

              SqlParms[2] = new SqlParameter("@FullandFinal", SqlDbType.VarChar);
              SqlParms[2].Direction = ParameterDirection.Input;
              SqlParms[2].Value = paymentBEL.fullAndFinal;

              SqlParms[3] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
              SqlParms[3].Direction = ParameterDirection.Output;



              ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "[Save_Payment]", SqlParms);

              if (SqlParms[3].Value == null || Convert.ToString(SqlParms[3].Value) == "0")
              {
                 // bookingResult.bookingID = 0; 
                  result = "TRUE";
              }
              else 
              {
                  result = Convert.ToString(SqlParms[3].Value);
                  
              }
             
          }
          catch (Exception ex)
          {

          }
          return result;

      }

      public BookingResult GetBookingDetails(BookingBEL bookingBel)
      {
          BookingResult bookingResult = new BookingResult();
          //SqlConnection conn = new SqlConnection();
          Int32 BookingId = 0;
          DataSet ds = null;
          try
          {
              SqlParameter[] SqlParms = new SqlParameter[4];



              SqlParms[0] = new SqlParameter("@Parking_Id", SqlDbType.VarChar);
              SqlParms[0].Direction = ParameterDirection.Input;
              SqlParms[0].Value = bookingBel.parkingId;

              //SqlParms[1] = new SqlParameter("@Mobile_No", SqlDbType.VarChar);
              //SqlParms[1].Direction = ParameterDirection.Input;
              //SqlParms[1].Value = bookingBel.mobileNo;

              SqlParms[1] = new SqlParameter("@OTP", SqlDbType.VarChar);
              SqlParms[1].Direction = ParameterDirection.Input;
              SqlParms[1].Value = bookingBel.OTP;

              SqlParms[2] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
              SqlParms[2].Direction = ParameterDirection.Output;

              SqlParms[3] = new SqlParameter("@Booked_Id", SqlDbType.VarChar, 20);
              SqlParms[3].Direction = ParameterDirection.Output;


              ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "GetBookingDetailUsingOTP", SqlParms);

              if (SqlParms[2].Value != null && Convert.ToString(SqlParms[2].Value) != "0")
              {
                  bookingResult.bookingID = 0;
                  bookingResult.ErrorMessage = Convert.ToString(SqlParms[2].Value);

              }
              else if (ds != null && ds.Tables[0].Rows.Count > 0)
              {
                  bookingResult.MonthlySubscription = Convert.ToString(ds.Tables[0].Rows[0]["MonthlySubscription"]);
                  bookingResult.vehiclewheels= Convert.ToString(ds.Tables[0].Rows[0]["vehiclewheels"]);
                  bookingResult.checkedInDateTime = Convert.ToString(ds.Tables[0].Rows[0]["CheckedInDatetime"]);
                  bookingResult.checkedOutDateTime = Convert.ToString(ds.Tables[0].Rows[0]["CheckedOutDateTime"]);
                  bookingResult.mobileNo = Convert.ToString(ds.Tables[0].Rows[0]["MobileNo"]);
                  bookingResult.vehicalNo = Convert.ToString(ds.Tables[0].Rows[0]["VehicalNumber"]);
                  bookingResult.bookingID = Convert.ToInt64(ds.Tables[0].Rows[0]["Booked_ID"]);
                  bookingResult.MonthlyDailyCheckIN = Convert.ToString(ds.Tables[0].Rows[0]["MonthlyDailyCheckIN"]);
                  bookingResult.AutoSuscription = ds.Tables[0].Rows[0]["Autosubscribed"].ToString();


              }
              //if (SqlParms[10].Value != null && SqlParms[10].Value != "0")
              //{
              //    BookingId = Convert.ToInt32(SqlParms[10].Value);

              //}
          }
          catch (Exception ex)
          {

          }
          return bookingResult;

      }

      public BookingResult DailyMonthlyCheckIn(BookingBEL bookingBel)
      {
          BookingResult bookingResult = new BookingResult();
          //SqlConnection conn = new SqlConnection();
          
          Int32 BookingId = 0;
          DataSet ds = null;
          try
          {
              SqlParameter[] SqlParms = new SqlParameter[6];



              SqlParms[0] = new SqlParameter("@Parking_Id", SqlDbType.VarChar);
              SqlParms[0].Direction = ParameterDirection.Input;
              SqlParms[0].Value = bookingBel.parkingId;

              SqlParms[1] = new SqlParameter("@OTP", SqlDbType.VarChar);
              SqlParms[1].Direction = ParameterDirection.Input;
              SqlParms[1].Value = bookingBel.OTP;

              SqlParms[2] = new SqlParameter("@Mobile_No", SqlDbType.VarChar);
              SqlParms[2].Direction = ParameterDirection.Input;
              SqlParms[2].Value = bookingBel.mobileNo;

              SqlParms[3] = new SqlParameter("@NewOTP", SqlDbType.VarChar);
              SqlParms[3].Direction = ParameterDirection.Input;
              SqlParms[3].Value = bookingBel.NewOTP;

              SqlParms[4] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
              SqlParms[4].Direction = ParameterDirection.Output;

              SqlParms[5] = new SqlParameter("@Booked_Id", SqlDbType.VarChar, 20);
              SqlParms[5].Direction = ParameterDirection.Output;


              ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "MonthlySubscribedCheckIn", SqlParms);

              if (SqlParms[4].Value != null && Convert.ToString(SqlParms[4].Value) != "0")
              {
                  bookingResult.bookingID = 0;
                  bookingResult.ErrorMessage = Convert.ToString(SqlParms[4].Value);

              }
              else if (ds != null && ds.Tables[0].Rows.Count > 0)
              {
                  
                  bookingResult.parkingId = Convert.ToString(ds.Tables[0].Rows[0]["Parking_Id"]);
                  bookingResult.MonthlySubscription = Convert.ToString(ds.Tables[0].Rows[0]["MonthlySubscription"]);
                  bookingResult.bookingID = Convert.ToInt64(ds.Tables[0].Rows[0]["Booked_ID"]);
                  bookingResult.checkedInDateTime = Convert.ToString(ds.Tables[0].Rows[0]["CheckedInDatetime"]);
                  bookingResult.parkingaddress = Convert.ToString(ds.Tables[0].Rows[0]["Parking_address"]);
                  bookingResult.mobileNo = Convert.ToString(ds.Tables[0].Rows[0]["MobileNo"]);
                  bookingResult.vehicalNo = Convert.ToString(ds.Tables[0].Rows[0]["VehicalNumber"]);
                  bookingResult.vehiclewheels = Convert.ToString(ds.Tables[0].Rows[0]["vehiclewheels"]);                                    
                  bookingResult.MonthlyDailyCheckIN = Convert.ToString(ds.Tables[0].Rows[0]["MonthlyDailyCheckIN"]);
                  bookingResult.Isnumberchanged = Convert.ToString(ds.Tables[0].Rows[0]["IsNumberChanged"]);

                  


              }
              //if (SqlParms[10].Value != null && SqlParms[10].Value != "0")
              //{
              //    BookingId = Convert.ToInt32(SqlParms[10].Value);

              //}
          }
          catch (Exception ex)
          {

          }
          return bookingResult;

      }


      public BookingResult TerminateOrChangeSubscription(BookingBEL bookingBel)
      {
          BookingResult bookingResult = new BookingResult();
          //SqlConnection conn = new SqlConnection();
          Int32 BookingId = 0;
          DataSet ds = null;
          try
          {
              SqlParameter[] SqlParms = new SqlParameter[5];
         

              SqlParms[0] = new SqlParameter("@Parking_Id", SqlDbType.VarChar);
              SqlParms[0].Direction = ParameterDirection.Input;
              SqlParms[0].Value = bookingBel.parkingId;

              SqlParms[1] = new SqlParameter("@OTP", SqlDbType.VarChar);
              SqlParms[1].Direction = ParameterDirection.Input;
              SqlParms[1].Value = bookingBel.OTP;

              SqlParms[2] = new SqlParameter("@Booked_Id", SqlDbType.VarChar);
              SqlParms[2].Direction = ParameterDirection.Input;
              SqlParms[2].Value = bookingBel.bookingID;

              SqlParms[3] = new SqlParameter("@ChangeOrTerminate", SqlDbType.VarChar);
              SqlParms[3].Direction = ParameterDirection.Input;
              SqlParms[3].Value = bookingBel.changeOrTurminateflag;

              SqlParms[4] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
              SqlParms[4].Direction = ParameterDirection.Output;

              //SqlParms[3] = new SqlParameter("@Booked_Id", SqlDbType.VarChar, 20);
              //SqlParms[3].Direction = ParameterDirection.Output;


              ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "TerminateOrChangeSubscription", SqlParms);

              if (SqlParms[4].Value != null && Convert.ToString(SqlParms[4].Value) != "0")
              {
                  //bookingResult.bookingID = 0;
                  bookingResult.ErrorMessage = Convert.ToString(SqlParms[4].Value);

              }
              else if (ds != null && ds.Tables[0].Rows.Count > 0)
              {
                  bookingResult.parkingId = Convert.ToString(ds.Tables[0].Rows[0]["Parking_Id"]);
                  bookingResult.vehicalNo = Convert.ToString(ds.Tables[0].Rows[0]["VehicalNumber"]);
                  bookingResult.mobileNo = Convert.ToString(ds.Tables[0].Rows[0]["MobileNo"]);
                  //bookingResult.checkedInDateTime = ds.Tables[0].Rows[0]["CheckedIndateTime"].ToString();
                  bookingResult.bookingID = Convert.ToInt64(ds.Tables[0].Rows[0]["booked_id"]);
                  bookingResult.parkingaddress = Convert.ToString(ds.Tables[0].Rows[0]["parking_address"]);
                  bookingResult.parkingaddress = Convert.ToString(ds.Tables[0].Rows[0]["MonthlySubscription"]);
                  bookingResult.UnSuscriptionDate = Convert.ToString(ds.Tables[0].Rows[0]["UnsubscriptionDate"]);

              }
              //if (SqlParms[10].Value != null && SqlParms[10].Value != "0")
              //{
              //    BookingId = Convert.ToInt32(SqlParms[10].Value);

              //}
          }
          catch (Exception ex)
          {

          }
          return bookingResult;

      }


      public List<BookingResult> GetBookingDetailsByVehicle(BookingBEL bookingBel, out string Errormessage)
      {
          BookingResult bookingResult = new BookingResult();
          List<BookingResult> ListbookingResult = new List<BookingResult>();
          Int32 BookingId = 0;
          DataSet ds = null;
          try
          {
              SqlParameter[] SqlParms = new SqlParameter[4];

              SqlParms[0] = new SqlParameter("@Parking_Id", SqlDbType.VarChar);
              SqlParms[0].Direction = ParameterDirection.Input;
              SqlParms[0].Value = bookingBel.parkingId;

              SqlParms[1] = new SqlParameter("@VehicleNumber", SqlDbType.VarChar);
              SqlParms[1].Direction = ParameterDirection.Input;
              SqlParms[1].Value = bookingBel.vehicalNo;

              SqlParms[2] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
              SqlParms[2].Direction = ParameterDirection.Output;

              SqlParms[3] = new SqlParameter("@Booked_Id", SqlDbType.VarChar, 20);
              SqlParms[3].Direction = ParameterDirection.Output;


              ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "GetBookingDetailByVehicle", SqlParms);
              Errormessage = string.Empty;
              if (SqlParms[2].Value != null && Convert.ToString(SqlParms[2].Value) != "0" && Convert.ToString(SqlParms[2].Value) != "")
              {
                  bookingResult.bookingID = 0;
                  bookingResult.ErrorMessage = Convert.ToString(SqlParms[2].Value);
                  Errormessage = bookingResult.ErrorMessage;

              }
              else if (ds != null && ds.Tables[0].Rows.Count > 0)
              {
                  Errormessage = string.Empty;
                  ListbookingResult = (from DataRow BookingResults in ds.Tables[0].Rows
                                       select new BookingResult
                                       {
                                           //parking_address = parkingrow["Parking_Name"].ToString(),
                                           //Parking_id = parkingrow["ParkingId"].ToString()

                                           MonthlySubscription = Convert.ToString(BookingResults["MonthlySubscription"]),
                                           vehiclewheels = Convert.ToString(BookingResults["vehiclewheels"]),
                                           checkedInDateTime = Convert.ToString(BookingResults["CheckedInDatetime"]),
                                           checkedOutDateTime = Convert.ToString(BookingResults["CheckedOutDateTime"]),
                                           mobileNo = Convert.ToString(BookingResults["MobileNo"]),
                                           vehicalNo = Convert.ToString(BookingResults["VehicalNumber"]),
                                           bookingID = Convert.ToInt64(BookingResults["Booked_ID"]),
                                           MonthlyDailyCheckIN = Convert.ToString(BookingResults["MonthlyDailyCheckIN"]),
                                           AutoSuscription = BookingResults["Autosubscribed"].ToString(),
                                           OTP = Convert.ToString(BookingResults["otp"])


                                       }).ToList();

              }
              else
              {
                  Errormessage = "No Data found";
              }
            
          }
          catch (Exception ex)
          {
              Errormessage = ex.Message.ToString();
              throw ex;
          }
          return ListbookingResult;

      }
  }
    
}
