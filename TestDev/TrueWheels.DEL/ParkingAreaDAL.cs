using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrueWheels.BEL;
using TrueWheels.DAL;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Reflection;
using TrueWheels.DEL;


namespace TrueWheels.DAL
{
    public class ParkingAreaDAL
    {
        private string connectionString = string.Empty;
        private static string className = "ParkingAreaDAL";
        public ParkingAreaDAL()
        {
            connectionString = ConfigurationManager.ConnectionStrings["conStr"].ToString();
            // later a different project will be build where  encrypted connection will maintained.
        }
        Transaction transaction = new Transaction();

        #region  Get available parking
        public List<AvailableParkingAreaResult> GetAvailableParking(ParkingAreaBEL ParkingBEL)
        {
            List<AvailableParkingAreaResult> AvailableParkings = new List<AvailableParkingAreaResult>();
            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[9];

                SqlParms[0] = new SqlParameter("@Latitude", SqlDbType.Decimal, 10);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = ParkingBEL.Main_Latitude;

                SqlParms[1] = new SqlParameter("@Longitude", SqlDbType.Decimal, 10);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = ParkingBEL.Main_Longitude;

                SqlParms[2] = new SqlParameter("@distance", SqlDbType.Int);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = ParkingBEL.Distance;

                SqlParms[3] = new SqlParameter("@FromDateTime", SqlDbType.VarChar, 19);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = ParkingBEL.FromDateTime;

                SqlParms[4] = new SqlParameter("@ToDateTime", SqlDbType.VarChar, 19);
                SqlParms[4].Direction = ParameterDirection.Input;
                SqlParms[4].Value = ParkingBEL.ToDateTime;

                SqlParms[5] = new SqlParameter("@ParkingClass", SqlDbType.VarChar, 19);
                SqlParms[5].Direction = ParameterDirection.Input;
                SqlParms[5].Value = ParkingBEL.ParkingClass;

                SqlParms[6] = new SqlParameter("@OrderBy", SqlDbType.VarChar, 40);
                SqlParms[6].Direction = ParameterDirection.Input;
                SqlParms[6].Value = ParkingBEL.OrderBy;
                //Popular Parking check
                SqlParms[7] = new SqlParameter("@PopularParking", SqlDbType.Bit);
                SqlParms[7].Direction = ParameterDirection.Input;
                SqlParms[7].Value = ParkingBEL.PopularParking;

                SqlParms[8] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[8].Direction = ParameterDirection.Output;

                //SqlParms[7] = new SqlParameter("@UserId", SqlDbType.NVarChar, 20);
                //SqlParms[7].Direction = ParameterDirection.Output;



                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "SP_GetAvaiolableParking", SqlParms);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0 && Convert.ToString(SqlParms[7].Value) != "0")
                    {
                        AvailableParkings = (from DataRow parkingrow in ds.Tables[0].Rows
                                             select new AvailableParkingAreaResult
                                             {

                                                 Parking_id = Convert.ToString(parkingrow["Parking_id"]),

                                                 parking_address = (parkingrow["parking_address"] == null ? "" : Convert.ToString(parkingrow["parking_address"])),

                                                 street = (parkingrow["street"] == null ? "" : Convert.ToString(parkingrow["street"])),

                                                 city = (parkingrow["city"] == null ? "" : Convert.ToString(parkingrow["city"])),

                                                 state = (parkingrow["state"] == null ? "" : Convert.ToString(parkingrow["state"])),

                                                 lattitude = (parkingrow["lattitude"] == null ? "" : Convert.ToString(parkingrow["lattitude"])),

                                                 longitude = (parkingrow["longitude"] == null ? "" : Convert.ToString(parkingrow["longitude"])),

                                                 GeoLoc = (parkingrow["GeoLoc"] == null ? "" : Convert.ToString(parkingrow["GeoLoc"])),

                                                 DateTimeTo = (parkingrow["DateTimeTo"] == null ? "" : Convert.ToString(parkingrow["DateTimeTo"])),

                                                 DateTimeFrom = (parkingrow["DateTimeFrom"] == null ? "" : Convert.ToString(parkingrow["DateTimeFrom"])),

                                                 No_Of_Space_Avaiable = (parkingrow["No_Of_Space_Avaiable"] == null ? "" : Convert.ToString(parkingrow["No_Of_Space_Avaiable"])),

                                                 Detail_ID = (parkingrow["Detail_ID"] == null ? "" : Convert.ToString(parkingrow["Detail_ID"])),

                                                 BasicCharge = (parkingrow["BasicCharge"] == null ? 0 : Convert.ToInt32(parkingrow["BasicCharge"])),

                                                 ParkingClass = (parkingrow["ParkingClass"] == null ? "" : Convert.ToString(parkingrow["ParkingClass"])),

                                                 Rating = (parkingrow["ParkingRating"] == System.DBNull.Value ? 0 : Convert.ToInt32(parkingrow["ParkingRating"])),

                                                 Distance = (parkingrow["Distance"] == null ? "" : Convert.ToString(parkingrow["Distance"])),

                                                 Facilities = (parkingrow["FacilityGroupId"] == null ? "" : Convert.ToString(parkingrow["FacilityGroupId"])),
                                                 SpaceType = (parkingrow["Space_Type"] == null ? "" : Convert.ToString(parkingrow["Space_Type"])),
                                                 PropertyType = (parkingrow["Property_Type"] == null ? "" : Convert.ToString(parkingrow["Property_Type"])),
                                                 Success = true,
                                             }
                                        ).ToList<AvailableParkingAreaResult>();

                        return AvailableParkings;

                    }
                }
                else
                {
                    AvailableParkings.Add(new AvailableParkingAreaResult() { Success = false, ErrorMessage = Convert.ToString(SqlParms[7].Value) });
                    return AvailableParkings;
                }

            }
            catch (Exception ex)
            {
                AvailableParkings.Add(new AvailableParkingAreaResult() { Success = false, ErrorMessage = Convert.ToString(ex.Message.ToString()) });
                ErrorLog.Log(this.GetType().Name, MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), HttpContext.Current.Session["userPhoneNo/Email"] != null ? HttpContext.Current.Session["userPhoneNo/Email"].ToString() : "");
                return AvailableParkings;
            }

            return AvailableParkings;
        }
        #endregion

        //#region  Register parking area
        //public RegsiterParkingAreaResult RegisterParkingArea(RegisterParkingAreaBEL ParkingBEL)
        //{
        //    RegsiterParkingAreaResult ParkingDetail = new RegsiterParkingAreaResult();
        //    try
        //    {

        //        DataSet ds = null;

        //        SqlParameter[] SqlParms = new SqlParameter[17];

        //        SqlParms[1] = new SqlParameter("@owner_id", SqlDbType.Int);
        //        SqlParms[1].Direction = ParameterDirection.Input;
        //        SqlParms[1].Value = ParkingBEL.owner_id;

        //        SqlParms[2] = new SqlParameter("@Space_type", SqlDbType.VarChar, 10);
        //        SqlParms[2].Direction = ParameterDirection.Input;
        //        SqlParms[2].Value = ParkingBEL.Space_type;

        //        SqlParms[3] = new SqlParameter("@Property_type", SqlDbType.VarChar, 10);
        //        SqlParms[3].Direction = ParameterDirection.Input;
        //        SqlParms[3].Value = ParkingBEL.Property_type;

        //        SqlParms[4] = new SqlParameter("@No_of_space", SqlDbType.Int);
        //        SqlParms[4].Direction = ParameterDirection.Input;
        //        SqlParms[4].Value = ParkingBEL.No_of_space;

        //        SqlParms[5] = new SqlParameter("@VechileType", SqlDbType.VarChar, 10);
        //        SqlParms[5].Direction = ParameterDirection.Input;
        //        SqlParms[5].Value = ParkingBEL.VechileType;

        //        SqlParms[6] = new SqlParameter("@PropertyVerifiedStatus", SqlDbType.Char, 1);
        //        SqlParms[6].Direction = ParameterDirection.Input;
        //        SqlParms[6].Value = ParkingBEL.PropertyVerifiedStatus;

        //        SqlParms[7] = new SqlParameter("@FacilityGroupId", SqlDbType.VarChar, 5);
        //        SqlParms[7].Direction = ParameterDirection.Input;
        //        SqlParms[7].Value = ParkingBEL.FacilityGroupId;

        //        SqlParms[8] = new SqlParameter("@PropertyAddress", SqlDbType.VarChar, 200);
        //        SqlParms[8].Direction = ParameterDirection.Input;
        //        SqlParms[8].Value = ParkingBEL.PropertyAddress;

        //        SqlParms[9] = new SqlParameter("@PropertyPinCode", SqlDbType.Int);
        //        SqlParms[9].Direction = ParameterDirection.Input;
        //        SqlParms[9].Value = ParkingBEL.PropertyPinCode;

        //        SqlParms[10] = new SqlParameter("@PropertyZone", SqlDbType.VarChar, 100);
        //        SqlParms[10].Direction = ParameterDirection.Input;
        //        SqlParms[10].Value = ParkingBEL.PropertyZone;

        //        SqlParms[11] = new SqlParameter("@PropertyLandMark", SqlDbType.VarChar, 100);
        //        SqlParms[11].Direction = ParameterDirection.Input;
        //        SqlParms[11].Value = ParkingBEL.PropertyLandMark;

        //        SqlParms[12] = new SqlParameter("@OwnerComments", SqlDbType.VarChar, 19);
        //        SqlParms[12].Direction = ParameterDirection.Input;
        //        SqlParms[12].Value = ParkingBEL.OwnerComments;

        //        SqlParms[13] = new SqlParameter("@lattitude", SqlDbType.Decimal, 19);
        //        SqlParms[13].Direction = ParameterDirection.Input;
        //        SqlParms[13].Value = ParkingBEL.lattitude;

        //        SqlParms[14] = new SqlParameter("@longitude", SqlDbType.Decimal, 19);
        //        SqlParms[14].Direction = ParameterDirection.Input;
        //        SqlParms[14].Value = ParkingBEL.longitude;

        //        SqlParms[15] = new SqlParameter("@Error", SqlDbType.VarChar, 300);
        //        SqlParms[15].Direction = ParameterDirection.Output;

        //        SqlParms[16] = new SqlParameter("@Parking_id", SqlDbType.NVarChar, 150);
        //        SqlParms[16].Direction = ParameterDirection.Output;





        //        SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "Insert_RegParkingArea", SqlParms);

        //        if (Convert.ToString(SqlParms[16].Value) != "0" && Convert.ToString(SqlParms[15].Value) == "")
        //        {
        //            ParkingDetail.Parking_Id = Convert.ToInt32(SqlParms[16].Value);
        //            ParkingDetail.Success = true;
        //            ParkingDetail.ErrorMessage = "";

        //            return ParkingDetail;

        //        }
        //        else
        //        {
        //            ParkingDetail.Parking_Id = 0;
        //            ParkingDetail.Success = false;
        //            ParkingDetail.ErrorMessage = Convert.ToString(SqlParms[15].Value);
        //            return ParkingDetail;
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        ParkingDetail.Parking_Id = 0;
        //        ParkingDetail.Success = false;
        //        ParkingDetail.ErrorMessage = ex.Message.ToString();
        //        ErrorLog.Log(this.GetType().Name, MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), HttpContext.Current.Session["userPhoneNo/Email"] != null ? HttpContext.Current.Session["userPhoneNo/Email"].ToString() : "");
        //        return ParkingDetail;
        //    }

        //    return ParkingDetail;
        //}
        //#endregion

        #region  Get All Location for search
        public List<LocationsIDName> GetAllLocation(string Term)
        {
            // List<string> Locations = new List<string>();
            List<LocationsIDName> locationsIDName = new List<LocationsIDName>();
            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[2];

                SqlParms[0] = new SqlParameter("@LocationTerm", SqlDbType.VarChar, 100);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = Term;

                SqlParms[1] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;


                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "SP_GetAllLocationName", SqlParms);

                if (ds.Tables[0].Rows.Count > 0 && Convert.ToString(SqlParms[1].Value) != "0")
                {
                    ////return Locations =
                    ////    (from DataRow parkingrow in ds.Tables[0].Rows
                    ////     where Convert.ToString(parkingrow["PropertyAddress"].ToString().ToUpper()).Contains(Term.ToUpper())
                    ////     select parkingrow["PropertyAddress"].ToString()
                    ////                ).ToList();

                    locationsIDName = (from DataRow parkingrow in ds.Tables[0].Rows
                                       where Convert.ToString(parkingrow["PropertyAddress"].ToString().ToUpper()).Contains(Term.ToUpper())
                                       select new LocationsIDName
                                       {
                                           parking_address = parkingrow["PropertyAddress"].ToString(),
                                           Parking_id = parkingrow["Parking_Id"].ToString()

                                       }).ToList();
                    return locationsIDName;

                }
                else
                {
                    //AvailableParkings.Add(new AvailableParkingAreaResult() { Success = false, ErrorMessage = Convert.ToString(SqlParms[7].Value) });
                    //return AvailableParkings;
                }

            }
            catch (Exception ex)
            {
                //AvailableParkings.Add(new AvailableParkingAreaResult() { Success = false, ErrorMessage = Convert.ToString(ex.Message.ToString()) });
                //return AvailableParkings;
            }

            return locationsIDName;
        }
        #endregion

        public AvailableParkingAreaResult GetParkingDetails(string parkId)
        {
            AvailableParkingAreaResult objAvailableParkingAreaResult = new AvailableParkingAreaResult();

            DataSet ds = null;

            SqlParameter[] SqlParms = new SqlParameter[2];

            SqlParms[0] = new SqlParameter("@ParingId", SqlDbType.Int);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = parkId;

            SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
            SqlParms[1].Direction = ParameterDirection.Output;

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "GetParkingDetails", SqlParms);
            objAvailableParkingAreaResult = (from DataRow parkingrow in ds.Tables[0].Rows
                                             select new AvailableParkingAreaResult
                                             {
                                                 Description = Convert.ToString(parkingrow["Description"]),
                                                 Rating = (parkingrow["ParkingRating"] == System.DBNull.Value ? 0 : Convert.ToInt32(parkingrow["ParkingRating"]))

                                             }).FirstOrDefault();
            return objAvailableParkingAreaResult;
        }

        public List<UserRating> GetParkingRating(string parkId)
        {
            List<UserRating> userRating = new List<UserRating>();

            DataSet ds = null;

            SqlParameter[] SqlParms = new SqlParameter[2];

            SqlParms[0] = new SqlParameter("@ParkingId", SqlDbType.Int);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = parkId;

            SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
            SqlParms[1].Direction = ParameterDirection.Output;

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "GetParkingRating", SqlParms);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    userRating = (from DataRow ratingrow in ds.Tables[0].Rows
                                  select new UserRating
                                  {
                                      Parking_id = (ratingrow["ParkingId"] == null ? "" : Convert.ToString(ratingrow["ParkingId"])),
                                      Comments = (ratingrow["Comments"] == null ? "" : Convert.ToString(ratingrow["Comments"])),
                                      Rating = (ratingrow["Rating"] == null ? "" : Convert.ToString(ratingrow["Rating"])),
                                      DateTime = (ratingrow["DateTime"] == null ? "" : Convert.ToString(ratingrow["DateTime"])),
                                      FirstName = (ratingrow["First_Name"] == null ? "" : Convert.ToString(ratingrow["First_Name"])),
                                      LastName = (ratingrow["Last_Name"] == null ? "" : Convert.ToString(ratingrow["Last_Name"])),
                                      UserId = (ratingrow["User_id"] == null ? "" : Convert.ToString(ratingrow["User_id"])),
                                      Id = (ratingrow["Id"] == null ? "" : Convert.ToString(ratingrow["Id"])),
                                  }).ToList<UserRating>();
                }
            }
            return userRating;
        }

        public UserRating InserUpdateUserComments(string userComments, string userId, string parkingId)
        {
            UserRating userRating = new UserRating();
            SqlParameter[] SqlParms = new SqlParameter[5];

            DataSet ds = null;
            SqlParms[0] = new SqlParameter("@ParkingId", SqlDbType.VarChar, 40);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = parkingId;

            SqlParms[1] = new SqlParameter("@userComments", SqlDbType.VarChar, 4000);
            SqlParms[1].Direction = ParameterDirection.Input;
            SqlParms[1].Value = userComments;

            SqlParms[2] = new SqlParameter("@userId", SqlDbType.VarChar, 150);
            SqlParms[2].Direction = ParameterDirection.Input;
            SqlParms[2].Value = userId;


            SqlParms[3] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
            SqlParms[3].Direction = ParameterDirection.Output;

            SqlParms[4] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
            SqlParms[4].Direction = ParameterDirection.Output;

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "InserUpdateUserComments", SqlParms);

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0 && Convert.ToString(SqlParms[4].Value) == "true")
                    userRating.success = "true";
                else
                    userRating.success = "false";
            }
            return userRating;
        }

        public UserRating InserUpdateUserRating(string parkingId, string rating, string userId)
        {
            UserRating userRating = new UserRating();
            SqlParameter[] SqlParms = new SqlParameter[5];

            DataSet ds = null;
            SqlParms[0] = new SqlParameter("@ParkingId", SqlDbType.VarChar, 40);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = parkingId;

            SqlParms[1] = new SqlParameter("@Rating", SqlDbType.VarChar, 4000);
            SqlParms[1].Direction = ParameterDirection.Input;
            SqlParms[1].Value = rating;

            SqlParms[2] = new SqlParameter("@userId", SqlDbType.VarChar, 150);
            SqlParms[2].Direction = ParameterDirection.Input;
            SqlParms[2].Value = userId;


            SqlParms[3] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
            SqlParms[3].Direction = ParameterDirection.Output;

            SqlParms[4] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
            SqlParms[4].Direction = ParameterDirection.Output;

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "InserUpdateUserRating", SqlParms);

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0 && Convert.ToString(SqlParms[4].Value) == "true")
                {
                }
            }
            return userRating;
        }
        public List<PBDetailsBEL> GetParkingWithId()
        {
            List<PBDetailsBEL> pBDetailsBEL = new List<PBDetailsBEL>();
            PBDetailsBEL objpBDetailsBEL = new PBDetailsBEL();
            DataSet ds = null;
            SqlParameter[] SqlParms = new SqlParameter[2];

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "GetParkingWithId", SqlParms);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    pBDetailsBEL = (from DataRow row in ds.Tables[0].Rows
                                    select new PBDetailsBEL
                                    {
                                        ParkinIds = (row["Parking_id"] == null ? "" : Convert.ToString(row["Parking_id"])),
                                        ParkingName = (row["parking_address"] == null ? "" : Convert.ToString(row["parking_address"]))
                                    }).ToList();
                }
            }

            return pBDetailsBEL;
        }

        //Changes for Insert SYS details for new entry
        #region  Register parking area

        public RegsiterParkingAreaResult RegisterParkingArea(RegisterParkingAreaBEL ParkingBEL)
        {
            RegsiterParkingAreaResult ParkingDetail = new RegsiterParkingAreaResult();
            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[33];

                SqlParms[0] = new SqlParameter("@owner_id", SqlDbType.Int);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = ParkingBEL.owner_id;

                SqlParms[1] = new SqlParameter("@Space_type", SqlDbType.VarChar, 50);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = ParkingBEL.Space_type;

                SqlParms[2] = new SqlParameter("@Property_type", SqlDbType.VarChar, 50);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = ParkingBEL.Property_type;

                SqlParms[3] = new SqlParameter("@No_of_space", SqlDbType.Int);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = ParkingBEL.No_of_space;

                //SqlParms[4] = new SqlParameter("@VechileType", SqlDbType.VarChar, 50);
                //SqlParms[4].Direction = ParameterDirection.Input;
                //SqlParms[4].Value = ParkingBEL.VechileType;

                //SqlParms[5] = new SqlParameter("@PropertyVerifiedStatus", SqlDbType.Char, 1);
                //SqlParms[5].Direction = ParameterDirection.Input;
                //SqlParms[5].Value = ParkingBEL.PropertyVerifiedStatus;

                //SqlParms[6] = new SqlParameter("@FacilityGroupId", SqlDbType.VarChar, 5);
                //SqlParms[6].Direction = ParameterDirection.Input;
                //SqlParms[6].Value = ParkingBEL.FacilityGroupId;

                SqlParms[4] = new SqlParameter("@PropertyAddress", SqlDbType.VarChar, 200);
                SqlParms[4].Direction = ParameterDirection.Input;
                SqlParms[4].Value = ParkingBEL.PropertyAddress;

                SqlParms[5] = new SqlParameter("@PropertyPinCode", SqlDbType.Int);
                SqlParms[5].Direction = ParameterDirection.Input;
                SqlParms[5].Value = ParkingBEL.PropertyPinCode;

                //SqlParms[6] = new SqlParameter("@PropertyZone", SqlDbType.VarChar, 100);
                //SqlParms[6].Direction = ParameterDirection.Input;
                //SqlParms[6].Value = ParkingBEL.PropertyZone;

                SqlParms[6] = new SqlParameter("@PropertyLandMark", SqlDbType.VarChar, 100);
                SqlParms[6].Direction = ParameterDirection.Input;
                SqlParms[6].Value = ParkingBEL.PropertyLandMark;

                SqlParms[7] = new SqlParameter("@OwnerComments", SqlDbType.VarChar, 500);
                SqlParms[7].Direction = ParameterDirection.Input;
                SqlParms[7].Value = ParkingBEL.OwnerComments;

                SqlParms[8] = new SqlParameter("@lattitude", SqlDbType.Decimal, 19);
                SqlParms[8].Direction = ParameterDirection.Input;
                SqlParms[8].Value = ParkingBEL.lattitude;

                SqlParms[9] = new SqlParameter("@longitude", SqlDbType.Decimal, 19);
                SqlParms[9].Direction = ParameterDirection.Input;
                SqlParms[9].Value = ParkingBEL.longitude;

                SqlParms[10] = new SqlParameter("@Park_Space_Name", SqlDbType.VarChar, 100);
                SqlParms[10].Direction = ParameterDirection.Input;
                SqlParms[10].Value = ParkingBEL.parkingSpaceName;

                SqlParms[11] = new SqlParameter("@Status", SqlDbType.Char, 2);
                SqlParms[11].Direction = ParameterDirection.Input;
                SqlParms[11].Value = ParkingBEL.Status;

                //SqlParms[16] = new SqlParameter("@parkingAvailable", SqlDbType.VarChar, 100);
                //SqlParms[16].Direction = ParameterDirection.Input;
                //SqlParms[16].Value = ParkingBEL.parkingAvailable;

                SqlParms[12] = new SqlParameter("@TimeFrom", SqlDbType.VarChar, 50);
                SqlParms[12].Direction = ParameterDirection.Input;
                SqlParms[12].Value = ParkingBEL.TimeFrom;

                SqlParms[13] = new SqlParameter("@TimeTo", SqlDbType.VarChar, 50);
                SqlParms[13].Direction = ParameterDirection.Input;
                SqlParms[13].Value = ParkingBEL.TimeTo;

                SqlParms[14] = new SqlParameter("@parkingChargesBike", SqlDbType.VarChar, 50);
                SqlParms[14].Direction = ParameterDirection.Input;
                SqlParms[14].Value = ParkingBEL.BikeBasicCharge;

                SqlParms[15] = new SqlParameter("@parkingChargesCar", SqlDbType.VarChar, 50);
                SqlParms[15].Direction = ParameterDirection.Input;
                SqlParms[15].Value = ParkingBEL.CarBasicCharge;

                SqlParms[16] = new SqlParameter("@open24hours", SqlDbType.Char, 1);
                SqlParms[16].Direction = ParameterDirection.Input;
                SqlParms[16].Value = ParkingBEL.open24hours;

                //SqlParms[22] = new SqlParameter("@description", SqlDbType.VarChar, 300);
                //SqlParms[22].Direction = ParameterDirection.Input;
                //SqlParms[22].Value = ParkingBEL.description;


                SqlParms[17] = new SqlParameter("@lstSelectedDays", SqlDbType.VarChar, 300);
                SqlParms[17].Direction = ParameterDirection.Input;
                SqlParms[17].Value = ParkingBEL.daysSelected;

                SqlParms[18] = new SqlParameter("@lstDetailDescription", SqlDbType.VarChar, 300);
                SqlParms[18].Direction = ParameterDirection.Input;
                SqlParms[18].Value = ParkingBEL.descriptionSelected;

                SqlParms[19] = new SqlParameter("@FacilityType", SqlDbType.VarChar, 300);
                SqlParms[19].Direction = ParameterDirection.Input;
                SqlParms[19].Value = ParkingBEL.facilitySelected;


                SqlParms[20] = new SqlParameter("@Error", SqlDbType.VarChar, 300);
                SqlParms[20].Direction = ParameterDirection.Output;

                SqlParms[21] = new SqlParameter("@Parking_id", SqlDbType.NVarChar, 150);
                SqlParms[21].Direction = ParameterDirection.Output;

                //**changes by neeraj to add monthly, daily and weekly price for car and bike

                //SqlParms[28] = new SqlParameter("@CarDailyCharge", SqlDbType.VarChar ,50);
                //SqlParms[28].Direction = ParameterDirection.Input;
                //SqlParms[28].Value = ParkingBEL.CarDailyCharge;

                //SqlParms[29] = new SqlParameter("@bikedailycharge", SqlDbType.VarChar ,50);
                //SqlParms[29].Direction = ParameterDirection.Input;
                //SqlParms[29].Value = ParkingBEL.bikedailycharge;

                //SqlParms[30] = new SqlParameter("@carweeklycharge", SqlDbType.VarChar ,50);
                //SqlParms[30].Direction = ParameterDirection.Input;
                //SqlParms[30].Value = ParkingBEL.carweeklycharge;

                //SqlParms[31] = new SqlParameter("@bikeweeklycharge", SqlDbType.VarChar ,50);
                //SqlParms[31].Direction = ParameterDirection.Input;
                //SqlParms[31].Value = ParkingBEL.bikeweeklycharge;

                SqlParms[22] = new SqlParameter("@carmonthlycharge", SqlDbType.VarChar, 50);
                SqlParms[22].Direction = ParameterDirection.Input;
                SqlParms[22].Value = ParkingBEL.carmonthlycharge;

                SqlParms[23] = new SqlParameter("@bikemonthycharge", SqlDbType.VarChar, 50);
                SqlParms[23].Direction = ParameterDirection.Input;
                SqlParms[23].Value = ParkingBEL.bikemonthycharge;

                SqlParms[24] = new SqlParameter("@CarDailyCharge", SqlDbType.VarChar, 50);
                SqlParms[24].Direction = ParameterDirection.Input;
                SqlParms[24].Value = ParkingBEL.CarDailyCharge;

                SqlParms[25] = new SqlParameter("@bikedailycharge", SqlDbType.VarChar, 50);
                SqlParms[25].Direction = ParameterDirection.Input;
                SqlParms[25].Value = ParkingBEL.bikedailycharge;

                SqlParms[26] = new SqlParameter("@Street", SqlDbType.VarChar, 50);
                SqlParms[26].Direction = ParameterDirection.Input;
                SqlParms[26].Value = ParkingBEL.street;

                SqlParms[27] = new SqlParameter("@City", SqlDbType.VarChar, 50);
                SqlParms[27].Direction = ParameterDirection.Input;
                SqlParms[27].Value = ParkingBEL.city;

                SqlParms[28] = new SqlParameter("@No_of_space_Bike", SqlDbType.VarChar, 50);
                SqlParms[28].Direction = ParameterDirection.Input;
                SqlParms[28].Value = ParkingBEL.No_Of_Space_Avaiable_bike;

                SqlParms[29] = new SqlParameter("@ParkingAvailableDate", SqlDbType.VarChar, 50);
                SqlParms[29].Direction = ParameterDirection.Input;
                SqlParms[29].Value = ParkingBEL.Parking_Available_Date;

                SqlParms[30] = new SqlParameter("@Hourly_Pricing", SqlDbType.VarChar, 50);
                SqlParms[30].Direction = ParameterDirection.Input;
                SqlParms[30].Value = ParkingBEL.Hourly_Pricing;

                SqlParms[31] = new SqlParameter("@State", SqlDbType.NVarChar, 50);
                SqlParms[31].Direction = ParameterDirection.Input;
                SqlParms[31].Value = ParkingBEL.state;

                SqlParms[32] = new SqlParameter("@Parking_support_no", SqlDbType.NVarChar, 50);
                SqlParms[32].Direction = ParameterDirection.Input;
                SqlParms[32].Value = ParkingBEL.Parking_support_no;





                //** changes done


                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "Insert_RYS_RegParkingArea", SqlParms);

                if (Convert.ToString(SqlParms[21].Value) != "0" && Convert.ToString(SqlParms[20].Value) == "")
                {
                    ParkingDetail.Parking_Id = Convert.ToInt32(SqlParms[21].Value);
                    ParkingDetail.Success = true;
                    ParkingDetail.ErrorMessage = "";

                    return ParkingDetail;

                }
                else
                {
                    ParkingDetail.Parking_Id = 0;
                    ParkingDetail.Success = false;
                    ParkingDetail.ErrorMessage = Convert.ToString(SqlParms[20].Value);
                    return ParkingDetail;
                }


            }
            catch (Exception ex)
            {
                ParkingDetail.Parking_Id = 0;
                ParkingDetail.Success = false;
                ParkingDetail.ErrorMessage = ex.Message.ToString();
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
                return ParkingDetail;
            }

            //return ParkingDetail;
        }
        #endregion


        //Changes to Deactivate existing parking Space

        public RegsiterParkingAreaResult Deactivate_RentSpace(string parkingId, string ownerId)
        {
            DataSet ds = null;
            RegsiterParkingAreaResult regsiterParkingAreaResult = new RegsiterParkingAreaResult();
            SqlParameter[] SqlParms = new SqlParameter[5];
            try
            {
                SqlParms[0] = new SqlParameter("@ParkingId", SqlDbType.VarChar, 40);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = parkingId;

                SqlParms[1] = new SqlParameter("@OwnerId", SqlDbType.VarChar, 4000);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = ownerId;


                SqlParms[2] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[2].Direction = ParameterDirection.Output;

                SqlParms[3] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
                SqlParms[3].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Deactivate_UserRentSpace", SqlParms);

                if (ds != null)
                {
                    if (Convert.ToString(SqlParms[2].Value) == "" && string.IsNullOrWhiteSpace(Convert.ToString(SqlParms[2].Value)))
                    {
                        regsiterParkingAreaResult.Success = true;
                        // regsiterParkingAreaResult.ErrorMessage = "Parking Deactivated Successfully.";
                    }
                    else
                        regsiterParkingAreaResult.ErrorMessage = Convert.ToString(SqlParms[2].Value);
                }
                return regsiterParkingAreaResult;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
            }
            return regsiterParkingAreaResult;
        }


        // Changes to Activate existing space

        public RegsiterParkingAreaResult Activate_RentSpace(string parkingId, string ownerId)
        {
            DataSet ds = null;
            RegsiterParkingAreaResult regsiterParkingAreaResult = new RegsiterParkingAreaResult();
            SqlParameter[] SqlParms = new SqlParameter[5];
            try
            {
                SqlParms[0] = new SqlParameter("@ParkingId", SqlDbType.VarChar, 40);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = parkingId;

                SqlParms[1] = new SqlParameter("@OwnerId", SqlDbType.VarChar, 4000);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = ownerId;


                SqlParms[2] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[2].Direction = ParameterDirection.Output;

                SqlParms[3] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
                SqlParms[3].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Activate_UserRentSpace", SqlParms);

                if (ds != null)
                {
                    if (Convert.ToString(SqlParms[2].Value) == "" && string.IsNullOrWhiteSpace(Convert.ToString(SqlParms[2].Value)))
                    {
                        regsiterParkingAreaResult.Success = true;
                        // regsiterParkingAreaResult.ErrorMessage = "Parking Deactivated Successfully.";
                    }
                    else
                        regsiterParkingAreaResult.ErrorMessage = Convert.ToString(SqlParms[2].Value);
                }
                return regsiterParkingAreaResult;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
            }
            return regsiterParkingAreaResult;
        }


        // Changes to edit/update the existing space

        #region  Update parking area
        public RegsiterParkingAreaResult UpdateSYSRegisterParkingArea(RegisterParkingAreaBEL ParkingBEL)
        {
            RegsiterParkingAreaResult ParkingDetail = new RegsiterParkingAreaResult();
            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[33];

                SqlParms[0] = new SqlParameter("@owner_id", SqlDbType.Int);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = ParkingBEL.owner_id;

                SqlParms[1] = new SqlParameter("@Space_type", SqlDbType.VarChar, 50);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = ParkingBEL.Space_type;

                SqlParms[2] = new SqlParameter("@Property_type", SqlDbType.VarChar, 50);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = ParkingBEL.Property_type;

                SqlParms[3] = new SqlParameter("@No_of_space", SqlDbType.Int);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = ParkingBEL.No_of_space;

                //SqlParms[4] = new SqlParameter("@VechileType", SqlDbType.VarChar, 50);
                //SqlParms[4].Direction = ParameterDirection.Input;
                //SqlParms[4].Value = ParkingBEL.VechileType;

                //SqlParms[5] = new SqlParameter("@PropertyVerifiedStatus", SqlDbType.Char, 1);
                //SqlParms[5].Direction = ParameterDirection.Input;
                //SqlParms[5].Value = ParkingBEL.PropertyVerifiedStatus;

                //SqlParms[6] = new SqlParameter("@FacilityGroupId", SqlDbType.VarChar, 5);
                //SqlParms[6].Direction = ParameterDirection.Input;
                //SqlParms[6].Value = ParkingBEL.FacilityGroupId;

                SqlParms[4] = new SqlParameter("@PropertyAddress", SqlDbType.VarChar, 200);
                SqlParms[4].Direction = ParameterDirection.Input;
                SqlParms[4].Value = ParkingBEL.PropertyAddress;

                SqlParms[5] = new SqlParameter("@PropertyPinCode", SqlDbType.Int);
                SqlParms[5].Direction = ParameterDirection.Input;
                SqlParms[5].Value = ParkingBEL.PropertyPinCode;

                //SqlParms[9] = new SqlParameter("@PropertyZone", SqlDbType.VarChar, 100);
                //SqlParms[9].Direction = ParameterDirection.Input;
                //SqlParms[9].Value = ParkingBEL.PropertyZone;

                SqlParms[6] = new SqlParameter("@PropertyLandMark", SqlDbType.VarChar, 100);
                SqlParms[6].Direction = ParameterDirection.Input;
                SqlParms[6].Value = ParkingBEL.PropertyLandMark;

                SqlParms[7] = new SqlParameter("@OwnerComments", SqlDbType.VarChar, 500);
                SqlParms[7].Direction = ParameterDirection.Input;
                SqlParms[7].Value = ParkingBEL.OwnerComments;

                //SqlParms[12] = new SqlParameter("@lattitude", SqlDbType.Decimal, 19);
                //SqlParms[12].Direction = ParameterDirection.Input;
                //SqlParms[12].Value = ParkingBEL.lattitude;

                //SqlParms[13] = new SqlParameter("@longitude", SqlDbType.Decimal, 19);
                //SqlParms[13].Direction = ParameterDirection.Input;
                //SqlParms[13].Value = ParkingBEL.longitude;

                SqlParms[8] = new SqlParameter("@Park_Space_Name", SqlDbType.VarChar, 100);
                SqlParms[8].Direction = ParameterDirection.Input;
                SqlParms[8].Value = ParkingBEL.parkingSpaceName;

                SqlParms[9] = new SqlParameter("@Status", SqlDbType.Char, 2);
                SqlParms[9].Direction = ParameterDirection.Input;
                SqlParms[9].Value = 'Y';

                //SqlParms[16] = new SqlParameter("@parkingAvailable", SqlDbType.VarChar, 100);
                //SqlParms[16].Direction = ParameterDirection.Input;
                //SqlParms[16].Value = ParkingBEL.parkingAvailable;

                SqlParms[10] = new SqlParameter("@TimeFrom", SqlDbType.VarChar, 100);
                SqlParms[10].Direction = ParameterDirection.Input;
                SqlParms[10].Value = ParkingBEL.TimeFrom;

                SqlParms[11] = new SqlParameter("@TimeTo", SqlDbType.VarChar, 100);
                SqlParms[11].Direction = ParameterDirection.Input;
                SqlParms[11].Value = ParkingBEL.TimeTo;

                SqlParms[12] = new SqlParameter("@parkingChargesBike", SqlDbType.VarChar, 50);
                SqlParms[12].Direction = ParameterDirection.Input;
                SqlParms[12].Value = ParkingBEL.BikeBasicCharge;

                SqlParms[13] = new SqlParameter("@parkingChargesCar", SqlDbType.VarChar, 50);
                SqlParms[13].Direction = ParameterDirection.Input;
                SqlParms[13].Value = ParkingBEL.CarBasicCharge;

                SqlParms[14] = new SqlParameter("@open24hours", SqlDbType.VarChar, 50);
                SqlParms[14].Direction = ParameterDirection.Input;
                SqlParms[14].Value = ParkingBEL.open24hours;

                //SqlParms[22] = new SqlParameter("@description", SqlDbType.VarChar, 300);
                //SqlParms[22].Direction = ParameterDirection.Input;
                //SqlParms[22].Value = ParkingBEL.description;


                SqlParms[15] = new SqlParameter("@lstSelectedDays", SqlDbType.VarChar, 300);
                SqlParms[15].Direction = ParameterDirection.Input;
                SqlParms[15].Value = ParkingBEL.daysSelected;

                SqlParms[16] = new SqlParameter("@lstDetailDescription", SqlDbType.VarChar, 300);
                SqlParms[16].Direction = ParameterDirection.Input;
                SqlParms[16].Value = ParkingBEL.descriptionSelected;

                SqlParms[17] = new SqlParameter("@FacilityType", SqlDbType.VarChar, 300);
                SqlParms[17].Direction = ParameterDirection.Input;
                SqlParms[17].Value = ParkingBEL.facilitySelected;


                SqlParms[18] = new SqlParameter("@Error", SqlDbType.VarChar, 300);
                SqlParms[18].Direction = ParameterDirection.Output;

                SqlParms[19] = new SqlParameter("@Parking_id", SqlDbType.NVarChar, 150);
                SqlParms[19].Direction = ParameterDirection.Input;
                SqlParms[19].Value = ParkingBEL.parking_Id;

                //*******changes by neeraj to update daily monthly and weekly price for car and bike



                //SqlParms[30] = new SqlParameter("@carweeklycharge", SqlDbType.VarChar, 500);
                //SqlParms[30].Direction = ParameterDirection.Input;
                //SqlParms[30].Value = ParkingBEL.carweeklycharge;

                //SqlParms[31] = new SqlParameter("@bikeweeklycharge", SqlDbType.VarChar, 500);
                //SqlParms[31].Direction = ParameterDirection.Input;
                //SqlParms[31].Value = ParkingBEL.bikeweeklycharge;

                SqlParms[20] = new SqlParameter("@carmonthlycharge", SqlDbType.VarChar, 500);
                SqlParms[20].Direction = ParameterDirection.Input;
                SqlParms[20].Value = ParkingBEL.carmonthlycharge;

                SqlParms[21] = new SqlParameter("@bikemonthycharge", SqlDbType.VarChar, 500);
                SqlParms[21].Direction = ParameterDirection.Input;
                SqlParms[21].Value = ParkingBEL.bikemonthycharge;

                SqlParms[22] = new SqlParameter("@Street", SqlDbType.VarChar, 50);
                SqlParms[22].Direction = ParameterDirection.Input;
                SqlParms[22].Value = ParkingBEL.street;

                SqlParms[23] = new SqlParameter("@City", SqlDbType.VarChar, 50);
                SqlParms[23].Direction = ParameterDirection.Input;
                SqlParms[23].Value = ParkingBEL.city;

                SqlParms[24] = new SqlParameter("@No_of_space_Bike", SqlDbType.VarChar, 50);
                SqlParms[24].Direction = ParameterDirection.Input;
                SqlParms[24].Value = ParkingBEL.No_Of_Space_Avaiable_bike;

                SqlParms[25] = new SqlParameter("@ParkingAvailableDate", SqlDbType.VarChar, 50);
                SqlParms[25].Direction = ParameterDirection.Input;
                SqlParms[25].Value = ParkingBEL.Parking_Available_Date;

                SqlParms[26] = new SqlParameter("@Hourly_Pricing", SqlDbType.Char, 1);
                SqlParms[26].Direction = ParameterDirection.Input;
                SqlParms[26].Value = ParkingBEL.Hourly_Pricing;

                SqlParms[27] = new SqlParameter("@lattitude", SqlDbType.Decimal, 19);
                SqlParms[27].Direction = ParameterDirection.Input;
                SqlParms[27].Value = ParkingBEL.lattitude;

                SqlParms[28] = new SqlParameter("@longitude", SqlDbType.Decimal, 19);
                SqlParms[28].Direction = ParameterDirection.Input;
                SqlParms[28].Value = ParkingBEL.longitude;


                SqlParms[29] = new SqlParameter("@State", SqlDbType.VarChar, 50);
                SqlParms[29].Direction = ParameterDirection.Input;
                SqlParms[29].Value = ParkingBEL.state;

                SqlParms[30] = new SqlParameter("@CarDailyCharge", SqlDbType.VarChar, 500);
                SqlParms[30].Direction = ParameterDirection.Input;
                SqlParms[30].Value = ParkingBEL.CarDailyCharge;

                SqlParms[31] = new SqlParameter("@bikedailycharge", SqlDbType.VarChar, 500);
                SqlParms[31].Direction = ParameterDirection.Input;
                SqlParms[31].Value = ParkingBEL.bikedailycharge;

                SqlParms[32] = new SqlParameter("@Parking_support_no", SqlDbType.VarChar, 500);
                SqlParms[32].Direction = ParameterDirection.Input;
                SqlParms[32].Value = ParkingBEL.Parking_support_no;

                //*******Changes done

                SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Update_RYS_RegParkingArea", SqlParms);

                if (Convert.ToString(SqlParms[19].Value) != "0" && Convert.ToString(SqlParms[18].Value) == "")
                {
                    ParkingDetail.Parking_Id = Convert.ToInt32(SqlParms[19].Value);
                    ParkingDetail.Success = true;
                    ParkingDetail.ErrorMessage = "";

                    return ParkingDetail;

                }
                else
                {
                    ParkingDetail.Parking_Id = 0;
                    ParkingDetail.Success = false;
                    ParkingDetail.ErrorMessage = Convert.ToString(SqlParms[18].Value);
                    return ParkingDetail;
                }


            }
            catch (Exception ex)
            {
                ParkingDetail.Parking_Id = 0;
                ParkingDetail.Success = false;
                ParkingDetail.ErrorMessage = ex.Message.ToString();
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
                return ParkingDetail;
            }

            //return ParkingDetail;
        }

        public List<RegisterSYSParkingAreaResult> Select_RentSpace(string ownerId, out string errormessage)
        {
            DataSet ds = null;
            errormessage = string.Empty;
            SqlParameter[] SqlParms = new SqlParameter[3];



            SqlParms[0] = new SqlParameter("@owner_id", SqlDbType.VarChar, 4000);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = ownerId;


            SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
            SqlParms[1].Direction = ParameterDirection.Output;

            SqlParms[2] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
            SqlParms[2].Direction = ParameterDirection.Output;

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_Select_RentSpace_RegParkingArea", SqlParms);
            List<RegisterSYSParkingAreaResult> listRegisterSYSParkingAreaResult = new List<RegisterSYSParkingAreaResult>();
            if (ds != null)
            {
                RegisterSYSParkingAreaResult registerSYSParkingAreaResult;
                //if (ds.Tables.Count <= 0 && (SqlParms[1].Value == null || SqlParms[1].Value == "" || SqlParms[1].Value == "0"))
                //{
                //    errormessage = string.Empty;
                //}
                
                if ((SqlParms[1].Value == null || SqlParms[1].Value == "" || SqlParms[1].Value == "0"))
                {
                   

                    foreach (DataRow drow in ds.Tables[0].Rows)
                    {
                        registerSYSParkingAreaResult = new RegisterSYSParkingAreaResult();

                        registerSYSParkingAreaResult.Parking_Space_Name = (drow["Parking_Space_Name"]) == DBNull.Value ? "" : Convert.ToString(drow["Parking_Space_Name"]);// Convert.ToString(drow["Parking_Space_Name"]);
                        registerSYSParkingAreaResult.PropertyAddress = (drow["PropertyAddress"]) == DBNull.Value ? "" : Convert.ToString(drow["PropertyAddress"]); //Convert.ToString(drow["PropertyAddress"]);
                        registerSYSParkingAreaResult.Parking_Owner_Comment = (drow["Parking_Owner_Comment"]) == DBNull.Value ? "" : Convert.ToString(drow["Parking_Owner_Comment"]); //Convert.ToString(drow["Parking_Owner_Comment"]);
                        registerSYSParkingAreaResult.parking_address = (drow["parking_address"]) == DBNull.Value ? "" : Convert.ToString(drow["parking_address"]);// Convert.ToString(drow["parking_address"]);
                        registerSYSParkingAreaResult.street = (drow["street"]) == DBNull.Value ? "" : Convert.ToString(drow["street"]); //Convert.ToString(drow["street"]);
                        registerSYSParkingAreaResult.city = (drow["city"]) == DBNull.Value ? "" : Convert.ToString(drow["city"]); //Convert.ToString(drow["city"]);
                        registerSYSParkingAreaResult.state = (drow["state"]) == DBNull.Value ? "" : Convert.ToString(drow["state"]); //Convert.ToString(drow["state"]);
                        registerSYSParkingAreaResult.No_Of_Space_Avaiable = (drow["No_Of_Space_Avaiable"]) == DBNull.Value ? "" : Convert.ToString(drow["No_Of_Space_Avaiable"]); //Convert.ToString(drow["No_Of_Space_Avaiable"]);
                        registerSYSParkingAreaResult.CarBasicCharge = (drow["CarBasicCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["CarBasicCharge"]); //Convert.ToString(drow["CarBasicCharge"]);
                        registerSYSParkingAreaResult.BikeBasicCharge = (drow["BikeBasicCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["BikeBasicCharge"]);// Convert.ToString(drow["BikeBasicCharge"]);
                        registerSYSParkingAreaResult.CarDailyCharge = (drow["CarDailyCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["CarDailyCharge"]); //Convert.ToString(drow["CarDailyCharge"]);
                        registerSYSParkingAreaResult.bikedailycharge = (drow["bikedailycharge"]) == DBNull.Value ? "" : Convert.ToString(drow["bikedailycharge"]); //Convert.ToString(drow["bikedailycharge"]);
                        //registerSYSParkingAreaResult.carweeklycharge = Convert.ToString(drow["carweeklycharge"]);
                        //registerSYSParkingAreaResult.bikeweeklycharge = Convert.ToString(drow["bikeweeklycharge"]);
                        registerSYSParkingAreaResult.carmonthlycharge = (drow["carmonthlycharge"]) == DBNull.Value ? "" : Convert.ToString(drow["carmonthlycharge"]); //Convert.ToString(drow["carmonthlycharge"]);
                        registerSYSParkingAreaResult.bikemonthycharge = (drow["bikemonthycharge"]) == DBNull.Value ? "" : Convert.ToString(drow["bikemonthycharge"]); //Convert.ToString(drow["bikemonthycharge"]);
                        // registerSYSParkingAreaResult.vechileType = Convert.ToString(drow["vechileType"]);
                        registerSYSParkingAreaResult.Status = (drow["Status"]) == DBNull.Value ? "" : Convert.ToString(drow["Status"]); //Convert.ToString(drow["Status"]);
                        registerSYSParkingAreaResult.owner_id = (drow["owner_id"]) == DBNull.Value ? "" : Convert.ToString(drow["owner_id"]); //Convert.ToString(drow["owner_id"]);
                        registerSYSParkingAreaResult.Space_Type = (drow["Space_Type"]) == DBNull.Value ? "" : Convert.ToString(drow["Space_Type"]); //Convert.ToString(drow["Space_Type"]);
                        registerSYSParkingAreaResult.Property_Type = (drow["Property_Type"]) == DBNull.Value ? "" : Convert.ToString(drow["Property_Type"]); //Convert.ToString(drow["Property_Type"]);
                        //registerSYSParkingAreaResult.PropertyVerifiedStatus = (drow["PropertyVerifiedStatus"]) == DBNull.Value ? "" : Convert.ToString(drow["PropertyVerifiedStatus"]); //Convert.ToString(drow["PropertyVerifiedStatus"]);
                        registerSYSParkingAreaResult.Pin_Code = (drow["Pin_Code"]) == DBNull.Value ? "" : Convert.ToString(drow["Pin_Code"]); //Convert.ToString(drow["Pin_Code"]);
                        registerSYSParkingAreaResult.Landmark = (drow["Landmark"]) == DBNull.Value ? "" : Convert.ToString(drow["Landmark"]); //Convert.ToString(drow["Landmark"]);
                        registerSYSParkingAreaResult.lattitude = (drow["lattitude"]) == DBNull.Value ? "" : Convert.ToString(drow["lattitude"]); //Convert.ToString(drow["lattitude"]);
                        registerSYSParkingAreaResult.longitude = (drow["longitude"]) == DBNull.Value ? "" : Convert.ToString(drow["longitude"]); //Convert.ToString(drow["longitude"]);
                       // registerSYSParkingAreaResult.ParkingRating = (drow["ParkingRating"]) == DBNull.Value ? "" : Convert.ToString(drow["ParkingRating"]); //Convert.ToString(drow["ParkingRating"]);
                        //registerSYSParkingAreaResult.Description = (drow["Description"]) == DBNull.Value ? "" : Convert.ToString(drow["Description"]); //Convert.ToString(drow["Description"]);
                        registerSYSParkingAreaResult.TimeTo = (drow["TimeTo"]) == DBNull.Value ? "" : Convert.ToString(drow["TimeTo"]); //Convert.ToString(drow["TimeTo"]);
                        registerSYSParkingAreaResult.TimeFrom = (drow["TimeFrom"]) == DBNull.Value ? "" : Convert.ToString(drow["TimeFrom"]); //Convert.ToString(drow["TimeFrom"]);
                        // **Changes by neeraj 20-Nov
                        registerSYSParkingAreaResult.Active = (drow["Active"]) == DBNull.Value ? "" : Convert.ToString(drow["Active"]); //Convert.ToString(drow["Active"]);
                        //registerSYSParkingAreaResult.Detail_ID = Convert.ToString(drow["Detail_ID"]);
                        //registerSYSParkingAreaResult.ParkingClass = Convert.ToString(drow["ParkingClass"]);
                        //registerSYSParkingAreaResult.PopularParking = (drow["PopularParking"]) == DBNull.Value ? "" : Convert.ToString(drow["PopularParking"]); //Convert.ToString(drow["PopularParking"]);
                        registerSYSParkingAreaResult.FacilitySelected = (drow["FacilitySelected"]) == DBNull.Value ? "" : Convert.ToString(drow["FacilitySelected"]); //Convert.ToString(drow["FacilitySelected"]);
                        registerSYSParkingAreaResult.No_Of_Space_Avaiable_bike = (drow["No_Of_Space_Avaiable_bike"]) == DBNull.Value ? "" : Convert.ToString(drow["No_Of_Space_Avaiable_bike"]); //Convert.ToString(drow["No_Of_Space_Avaiable_bike"]);
                        registerSYSParkingAreaResult.Open24Hour = (drow["Open24Hour"]) == DBNull.Value ? "" : Convert.ToString(drow["Open24Hour"]); //Convert.ToString(drow["Open24Hour"]);
                        registerSYSParkingAreaResult.DescriptionSelected = (drow["DescriptionSelected"]) == DBNull.Value ? "" : Convert.ToString(drow["DescriptionSelected"]); //Convert.ToString(drow["DescriptionSelected"]);
                        registerSYSParkingAreaResult.DaysSelected = (drow["DaysSelected"]) == DBNull.Value ? "" : Convert.ToString(drow["DaysSelected"]); //Convert.ToString(drow["DaysSelected"]);
                        //registerSYSParkingAreaResult.Hourly_Pricing = (drow["Hourly_Pricing"]) == DBNull.Value ? "" : Convert.ToString(drow["Hourly_Pricing"]); //Convert.ToString(drow["Hourly_Pricing"]);
                        registerSYSParkingAreaResult.Parking_Available_Date = (drow["Parking_Available_Date"]) == DBNull.Value ? "" : Convert.ToString(drow["Parking_Available_Date"]); //Convert.ToString(drow["Parking_Available_Date"]);

                        //** Changes done
                        //registerSYSParkingAreaResult.bikemonthycharge = Convert.ToString(drow["bikemonthycharge"]);
                        registerSYSParkingAreaResult.Parking_id = (drow["Parking_id"]) == DBNull.Value ? 0 : Convert.ToInt64(drow["Parking_id"]); //Convert.ToInt64(drow["Parking_id"]);
                        registerSYSParkingAreaResult.GeoLoc = (drow["GeoLoc"]) == DBNull.Value ? "" : Convert.ToString(drow["GeoLoc"]); //Convert.ToString(drow["GeoLoc"]);
                        registerSYSParkingAreaResult.Parking_support_no = (drow["Parking_support_no"]) == DBNull.Value ? "" : Convert.ToString(drow["Parking_support_no"]); //Convert.ToInt64(drow["TWAttendantId"]);

                        registerSYSParkingAreaResult.Success = true;


                        listRegisterSYSParkingAreaResult.Add(registerSYSParkingAreaResult);

                       
                    }

                }

                else
                {
                    errormessage = Convert.ToString(SqlParms[1].Value);
                    // return;
                    //registerSYSParkingAreaResult.Parking_id = 0; 
                }
            }
            return listRegisterSYSParkingAreaResult;

        }


        #endregion

        // Added by Neeraj To get Booking Recieved

        public List<BookingRecievedSYS> Booking_Recieved(string ownerId, out string errormessage)
        {
            DataSet ds = null;
            errormessage = string.Empty;
            SqlParameter[] SqlParms = new SqlParameter[3];



            SqlParms[0] = new SqlParameter("@owner_id", SqlDbType.VarChar, 4000);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = ownerId;


            SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
            SqlParms[1].Direction = ParameterDirection.Output;

            SqlParms[2] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
            SqlParms[2].Direction = ParameterDirection.Output;

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_BookingRecievedRYS", SqlParms);
            List<BookingRecievedSYS> listBookingRecievedSYS = new List<BookingRecievedSYS>();
            if (ds != null)
            {
                BookingRecievedSYS bookingRecievedSYS;
                if ((SqlParms[1].Value == null || SqlParms[1].Value == "" || SqlParms[1].Value == "0"))
                {

                    foreach (DataRow drow in ds.Tables[0].Rows)
                    {
                        bookingRecievedSYS = new BookingRecievedSYS();
                        bookingRecievedSYS.Booked_Id = (drow["Booked_Id"]) == DBNull.Value ? 0 : Convert.ToInt64(drow["Booked_Id"]);
                        bookingRecievedSYS.BookingDateTime = (drow["BookingDateTime"]) == DBNull.Value ? "" : Convert.ToString(drow["BookingDateTime"]);
                        bookingRecievedSYS.BookedIntime = (drow["BookedIntime"]) == DBNull.Value ? "" : Convert.ToString(drow["BookedIntime"]);
                        bookingRecievedSYS.BookedOutTime = (drow["BookedOutTime"]) == DBNull.Value ? "" : Convert.ToString(drow["BookedOutTime"]);
                        bookingRecievedSYS.CheckedOutDateTime = (drow["CheckedOutDateTime"]) == DBNull.Value ? "" : Convert.ToString(drow["CheckedOutDateTime"]);
                        bookingRecievedSYS.BookingStatus = (drow["BookingStatus"]) == DBNull.Value ? "" : Convert.ToString(drow["BookingStatus"]);
                        bookingRecievedSYS.Base_Amount = (drow["Base_Amount"]) == DBNull.Value ? "" : Convert.ToString(drow["Base_Amount"]);
                        bookingRecievedSYS.Full_Name = (drow["Full_Name"]) == DBNull.Value ? "" : Convert.ToString(drow["Full_Name"]);
                        bookingRecievedSYS.VehicalNumber = (drow["VehicalNumber"]) == DBNull.Value ? "" : Convert.ToString(drow["VehicalNumber"]);
                        bookingRecievedSYS.VehicleType = (drow["VehicleType"]) == DBNull.Value ? "" : Convert.ToString(drow["VehicleType"]);
                        bookingRecievedSYS.MonthlySubscription = (drow["MonthlySubscription"]) == DBNull.Value ? "" : Convert.ToString(drow["MonthlySubscription"]);
                        bookingRecievedSYS.Total_Days = (drow["Total_Days"]) == DBNull.Value ? "" : Convert.ToString(drow["Total_Days"]);
                        bookingRecievedSYS.Total_Hour = (drow["Total_Hour"]) == DBNull.Value ? "" : Convert.ToString(drow["Total_Hour"]);
                        bookingRecievedSYS.Total_Min = (drow["Total_Min"]) == DBNull.Value ? "" : Convert.ToString(drow["Total_Min"]);
                        bookingRecievedSYS.Total_month = (drow["BookingDateTime"]) == DBNull.Value ? "" : Convert.ToString(drow["Total_month"]);
                        bookingRecievedSYS.Phone_No1 = (drow["Phone_No1"]) == DBNull.Value ? 0 : Convert.ToInt64(drow["Phone_No1"]);
                        bookingRecievedSYS.Parking_Space_Name = (drow["Parking_Space_Name"]) == DBNull.Value ? "" : Convert.ToString(drow["Parking_Space_Name"]);
                        bookingRecievedSYS.parking_address = (drow["parking_address"]) == DBNull.Value ? "" : Convert.ToString(drow["parking_address"]);


                        bookingRecievedSYS.Success = true;


                        listBookingRecievedSYS.Add(bookingRecievedSYS);
                    }

                }

                else
                {
                    errormessage = Convert.ToString(SqlParms[1].Value);
                    // return;
                    //registerSYSParkingAreaResult.Parking_id = 0; 
                }
            }
            return listBookingRecievedSYS;

        }


        // Added by Neeraj To get Booking Made

        public List<BookingMadeSYS> Booking_Made(string ownerId, out string errormessage)
        {
            DataSet ds = null;
            errormessage = string.Empty;
            SqlParameter[] SqlParms = new SqlParameter[3];



            SqlParms[0] = new SqlParameter("@owner_id", SqlDbType.VarChar, 4000);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = ownerId;


            SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
            SqlParms[1].Direction = ParameterDirection.Output;

            SqlParms[2] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
            SqlParms[2].Direction = ParameterDirection.Output;

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_BookingMadeRYS", SqlParms);
            List<BookingMadeSYS> listBookingMadeSYS = new List<BookingMadeSYS>();
            if (ds != null)
            {
                BookingMadeSYS bookingMadeSYS;
                if ((SqlParms[1].Value == null || SqlParms[1].Value == "" || SqlParms[1].Value == "0"))
                {

                    foreach (DataRow drow in ds.Tables[0].Rows)
                    {
                        bookingMadeSYS = new BookingMadeSYS();

                        bookingMadeSYS.Booked_Id = (drow["Booked_Id"]) == DBNull.Value ? 0 : Convert.ToInt64(drow["Booked_Id"]);
                        bookingMadeSYS.BookingDateTime = (drow["BookingDateTime"]) == DBNull.Value ? "" : Convert.ToString(drow["BookingDateTime"]);
                        bookingMadeSYS.BookedIntime = (drow["BookedIntime"]) == DBNull.Value ? "" : Convert.ToString(drow["BookedIntime"]);
                        bookingMadeSYS.BookedOutTime = (drow["BookedOutTime"]) == DBNull.Value ? "" : Convert.ToString(drow["BookedOutTime"]);
                        bookingMadeSYS.Base_Amount = (drow["Base_Amount"]) == DBNull.Value ? "" : Convert.ToString(drow["Base_Amount"]);
                        bookingMadeSYS.CheckedOutDateTime = (drow["CheckedOutDateTime"]) == DBNull.Value ? "" : Convert.ToString(drow["CheckedOutDateTime"]);
                        bookingMadeSYS.Total_Days = (drow["Total_Days"]) == DBNull.Value ? "" : Convert.ToString(drow["Total_Days"]);
                        bookingMadeSYS.Total_Hour = (drow["Total_Hour"]) == DBNull.Value ? "" : Convert.ToString(drow["Total_Hour"]);
                        bookingMadeSYS.Total_Min = (drow["Total_Min"]) == DBNull.Value ? "" : Convert.ToString(drow["Total_Min"]);
                        bookingMadeSYS.Total_month = (drow["Total_month"]) == DBNull.Value ? "" : Convert.ToString(drow["Total_month"]);
                        bookingMadeSYS.Full_Name = (drow["Full_Name"]) == DBNull.Value ? "" : Convert.ToString(drow["Full_Name"]);
                        bookingMadeSYS.Phone_No1 = (drow["Phone_No1"]) == DBNull.Value ? 0 : Convert.ToInt64(drow["Phone_No1"]);
                        bookingMadeSYS.VehicalNumber = (drow["VehicalNumber"]) == DBNull.Value ? "" : Convert.ToString(drow["VehicalNumber"]);
                        bookingMadeSYS.VehicleType = (drow["VehicleType"]) == DBNull.Value ? "" : Convert.ToString(drow["VehicleType"]);
                        bookingMadeSYS.BookingStatus = (drow["BookingStatus"]) == DBNull.Value ? "" : Convert.ToString(drow["BookingStatus"]);
                        bookingMadeSYS.MonthlySubscription = (drow["MonthlySubscription"]) == DBNull.Value ? "" : Convert.ToString(drow["MonthlySubscription"]);
                        bookingMadeSYS.parking_address = (drow["parking_address"]) == DBNull.Value ? "" : Convert.ToString(drow["parking_address"]);
                        bookingMadeSYS.Parking_Space_Name = (drow["Parking_Space_Name"]) == DBNull.Value ? "" : Convert.ToString(drow["Parking_Space_Name"]);

                        bookingMadeSYS.Success = true;


                        listBookingMadeSYS.Add(bookingMadeSYS);
                    }

                }


                else
                {
                    errormessage = Convert.ToString(SqlParms[1].Value);
                    // return;
                    //registerSYSParkingAreaResult.Parking_id = 0; 
                }
            }
            return listBookingMadeSYS;

        }


        public BookSYSParkingResult BookParking(BookSYSParking BookSYSParking)
        {
            DataSet ds = null;
            //Error = string.Empty;
            BookSYSParkingResult bookSYSParkingResult = new BookSYSParkingResult();
            SqlParameter[] SqlParms = new SqlParameter[20];
            SqlParms[0] = new SqlParameter("@ParkingId", SqlDbType.VarChar, 400);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = BookSYSParking.parkingId;

            SqlParms[1] = new SqlParameter("@OwnerId", SqlDbType.VarChar, 100);
            SqlParms[1].Direction = ParameterDirection.Input;
            SqlParms[1].Value = BookSYSParking.owner_userId;

            SqlParms[2] = new SqlParameter("@UserId", SqlDbType.VarChar, 100);
            SqlParms[2].Direction = ParameterDirection.Input;
            SqlParms[2].Value = BookSYSParking.login_userId;

            SqlParms[3] = new SqlParameter("@BookedInTime", SqlDbType.DateTime);
            SqlParms[3].Direction = ParameterDirection.Input;
            SqlParms[3].Value = BookSYSParking.bookedInTime;

            SqlParms[4] = new SqlParameter("@BookedOutDatetime", SqlDbType.DateTime);
            SqlParms[4].Direction = ParameterDirection.Input;
            SqlParms[4].Value = BookSYSParking.bookedOutDatetime;

            SqlParms[5] = new SqlParameter("@VehicalNo", SqlDbType.VarChar, 100);
            SqlParms[5].Direction = ParameterDirection.Input;
            SqlParms[5].Value = BookSYSParking.vehicalNo;

            SqlParms[6] = new SqlParameter("@VehicleType", SqlDbType.VarChar, 100);
            SqlParms[6].Direction = ParameterDirection.Input;
            SqlParms[6].Value = BookSYSParking.vehicleType;

            SqlParms[7] = new SqlParameter("@PaymentMode", SqlDbType.VarChar, 100);
            SqlParms[7].Direction = ParameterDirection.Input;
            SqlParms[7].Value = BookSYSParking.paymentMode;

            SqlParms[8] = new SqlParameter("@PaymentAmount", SqlDbType.VarChar, 100);
            SqlParms[8].Direction = ParameterDirection.Input;
            SqlParms[8].Value = BookSYSParking.paymentAmount;

            SqlParms[9] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
            SqlParms[9].Direction = ParameterDirection.Output;

            SqlParms[10] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
            SqlParms[10].Direction = ParameterDirection.Output;

            SqlParms[11] = new SqlParameter("@BookingId", SqlDbType.VarChar, 150);
            SqlParms[11].Direction = ParameterDirection.Output;

            SqlParms[12] = new SqlParameter("@VehicleWheel", SqlDbType.VarChar, 100);
            SqlParms[12].Direction = ParameterDirection.Input;
            SqlParms[12].Value = BookSYSParking.vehicleWheel;


            SqlParms[13] = new SqlParameter("@MonthlySubscription", SqlDbType.VarChar, 100);
            SqlParms[13].Direction = ParameterDirection.Input;
            SqlParms[13].Value = BookSYSParking.monthlySubscription;

            SqlParms[14] = new SqlParameter("@HourBaseParking", SqlDbType.VarChar, 100);
            SqlParms[14].Direction = ParameterDirection.Input;
            SqlParms[14].Value = BookSYSParking.hourBaseParking;

            SqlParms[15] = new SqlParameter("@BookingStatus", SqlDbType.VarChar, 100);
            SqlParms[15].Direction = ParameterDirection.Input;
            SqlParms[15].Value = BookSYSParking.BookingStatus;

            SqlParms[16] = new SqlParameter("@BookedInTimeMonthly", SqlDbType.VarChar, 100);
            SqlParms[16].Direction = ParameterDirection.Input;
            SqlParms[16].Value = BookSYSParking.BookedInTimeMonthly;

            SqlParms[17] = new SqlParameter("@BookedOutDatetimeMonthly", SqlDbType.VarChar, 100);
            SqlParms[17].Direction = ParameterDirection.Input;
            SqlParms[17].Value = BookSYSParking.BookedOutTimeMonthly;

            SqlParms[18] = new SqlParameter("@Bankname", SqlDbType.VarChar, 100);
            SqlParms[18].Direction = ParameterDirection.Input;
            SqlParms[18].Value = BookSYSParking.Bank_Name;

            SqlParms[19] = new SqlParameter("@CardType", SqlDbType.VarChar, 100);
            SqlParms[19].Direction = ParameterDirection.Input;
            SqlParms[19].Value = BookSYSParking.Card_Type;




            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Book_ParkingRYS", SqlParms);
            //DataRow drow = ds.Tables[0].Rows.;
            //DataRow dr in ds.Tables[1].Columns[0];
            //DataRow drr in ds.Tables[2].Rows;

            if (ds != null)
            {
                if (Convert.ToString(SqlParms[10].Value) == "true" && Convert.ToInt64(SqlParms[11].Value) != 0)
                {
                    bookSYSParkingResult.Status = true;
                    bookSYSParkingResult.Error = "Booked Successfully.";
                    bookSYSParkingResult.bookingID = Convert.ToInt64(SqlParms[11].Value);

                    foreach (DataRow drow in ds.Tables[0].Rows)
                    {
                        bookSYSParkingResult.Booked_Id = Convert.ToInt64(drow["Booked_Id"]);
                        bookSYSParkingResult.VehicalNumber = Convert.ToString(drow["VehicalNumber"]);
                        bookSYSParkingResult.MobileNo = Convert.ToString(drow["MobileNo"]);
                        bookSYSParkingResult.BookingDateTime = Convert.ToString(drow["BookingDateTime"]);
                        bookSYSParkingResult.BookedIntime = Convert.ToString(drow["BookedIntime"]);
                        bookSYSParkingResult.BookedOutTime = Convert.ToString(drow["BookedOutTime"]);
                        bookSYSParkingResult.PaymentMode = Convert.ToString(drow["PaymentMode"]);
                        bookSYSParkingResult.parking_address = Convert.ToString(drow["parking_address"]);
                        bookSYSParkingResult.OwnerComments = Convert.ToString(drow["OwnerComments"]);
                        bookSYSParkingResult.street = Convert.ToString(drow["street"]);
                        bookSYSParkingResult.city = Convert.ToString(drow["city"]);
                        bookSYSParkingResult.PropertyPinCode = Convert.ToInt64(drow["PropertyPinCode"]);
                        bookSYSParkingResult.Park_Space_Name = Convert.ToString(drow["Park_Space_Name"]);
                        bookSYSParkingResult.OTP = Convert.ToString(drow["OTP"]);
                        bookSYSParkingResult.TWAttendantId = Convert.ToInt64(drow["TWAttendantId"]);
                        bookSYSParkingResult.BookedInTimeMonthly = Convert.ToString(drow["BookedInTimeMonthly"]);
                        bookSYSParkingResult.BookedOutTimeMonthly = Convert.ToString(drow["BookedOutTimeMonthly"]);
                    }

                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        bookSYSParkingResult.First_Name = Convert.ToString(dr["First_Name"]);
                        bookSYSParkingResult.Last_Name = Convert.ToString(dr["Last_Name"]);
                        bookSYSParkingResult.Phone_No1 = Convert.ToString(dr["Phone_No1"]);
                    }

                    foreach (DataRow drr in ds.Tables[2].Rows)
                    {
                        bookSYSParkingResult.Attendant_Name = Convert.ToString(drr["Attendant_Name"]);
                        bookSYSParkingResult.Attendant_Phone_No = Convert.ToString(drr["Attendant_Phone_No"]);
                        //bookSYSParkingResult.Phone_No1 = Convert.ToString(drr["Phone_No1"]);
                    }

                    //foreach (DataRow drrr in ds.Tables[3].Rows)
                    //{
                    //    //bookSYSParkingResult.Attendant_Name = Convert.ToString(drr["Attendant_Name"]);
                    //    //bookSYSParkingResult.Attendant_Phone_No = Convert.ToString(drr["Attendant_Phone_No"]);
                    //    Int64 phoneNo = Convert.ToInt64(drrr["Phone_No1"]);
                    //    OTPBEL OTPBel = new OTPBEL();
                    //    var OTPList = OTPBel.GetAndSendOTPBooking(Convert.ToInt64(phoneNo), Convert.ToInt64(SqlParms[11].Value));

                    //}

                }
            }

            return bookSYSParkingResult;

            //else
            //   {
            //       Error = Convert.ToString(SqlParms[9].Value);
            //      // return;
            //       //registerSYSParkingAreaResult.Parking_id = 0; 
            //   }

        }

        public List<StandardCodesResponse> Get_AllCodes(out string errorMessage)
        {
            DataSet ds = null;
            errorMessage = string.Empty;
            SqlParameter[] SqlParms = new SqlParameter[1];



            //SqlParms[0] = new SqlParameter("@owner_id", SqlDbType.VarChar, 4000);
            //SqlParms[0].Direction = ParameterDirection.Input;
            //SqlParms[0].Value = ownerId;


            SqlParms[0] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
            SqlParms[0].Direction = ParameterDirection.Output;

            //SqlParms[2] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
            //SqlParms[2].Direction = ParameterDirection.Output;

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_AllCodes", SqlParms);
            //system       listRegisterSYSParkingAreaResult = new List<RegisterSYSParkingAreaResult>();
            List<SystemCodes> syscode = new List<SystemCodes>();
            List<StandardCodesResponse> liststandardCodesResponse = new List<StandardCodesResponse>();
            StandardCodesResponse standardCodesResponse = new StandardCodesResponse();
            SystemCodes systemCodes;// = new SystemCodes();
            List<SystemCodes> syscode1;// = new List<SystemCodes>();
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0 && Convert.ToString(SqlParms[0].Value) == "" && string.IsNullOrWhiteSpace(Convert.ToString(SqlParms[0].Value)))
                {
                    string cate = string.Empty;
                    //foreach (DataRow drow in ds.Tables[0].Rows)
                    //{
                    for (int i = 0; i < ds.Tables[0].Rows.Count; )
                    {
                        cate = Convert.ToString(ds.Tables[0].Rows[i]["MasterType"]);
                        syscode1 = new List<SystemCodes>();
                        //foreach (DataRow dr in ds.Tables[0].Rows )
                        //{
                        for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                        {
                            if (cate == Convert.ToString(ds.Tables[0].Rows[j]["MasterType"]))// && standardCodesResponse.Category != cate)
                            {
                                systemCodes = new SystemCodes(); ;
                                systemCodes.PropertyName = Convert.ToString(ds.Tables[0].Rows[j]["MasterType"]);
                                systemCodes.PropertyCode = Convert.ToString(ds.Tables[0].Rows[j]["Sys_Code"]);
                                systemCodes.PropertyDescription = Convert.ToString(ds.Tables[0].Rows[j]["Sys_description"]);
                                syscode1.Add(systemCodes);
                                i++;
                            }

                        }
                        standardCodesResponse = new StandardCodesResponse();
                        standardCodesResponse.Category = cate;
                        standardCodesResponse.CategoryProperty = syscode1;
                        liststandardCodesResponse.Add(standardCodesResponse);
                        // liststandardCodesResponse.Add(standardCodesResponse);
                    }
                    //syscode = (from DataRow CodeRows in ds.Tables[0].Rows
                    //                     select new SystemCodes
                    //                     {

                    //                         PropertyName = Convert.ToString(CodeRows["MasterType"]),

                    //                         PropertyCode = (CodeRows["Sys_Code"] == null ? "" : Convert.ToString(CodeRows["Sys_Code"])),

                    //                        PropertyDescription = (CodeRows["Sys_description"] == null ? "" : Convert.ToString(CodeRows["Sys_description"])),

                    //                     }
                    //                ).ToList<SystemCodes>();

                    return liststandardCodesResponse;// syscode;

                }
            }

            else
            {
                errorMessage = Convert.ToString(SqlParms[0].Value);
                // return;
                //registerSYSParkingAreaResult.Parking_id = 0; 
            }
            return liststandardCodesResponse;// syscode;
        }

        public GetEstimatedCostBEL EstimatedCost(string BookedIntime, string BookedOutTime, string Parking_ID, string Vehicle_Type, string MonthlySuscription)
        {
            GetEstimatedCostBEL EstimatedCostBEL = new GetEstimatedCostBEL();
            DataSet ds = null;

            SqlParameter[] SqlParms = new SqlParameter[6];
            SqlParms[0] = new SqlParameter("@parking_ID", SqlDbType.VarChar, 100);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = Parking_ID;

            SqlParms[1] = new SqlParameter("@BookedIntime", SqlDbType.VarChar);
            SqlParms[1].Direction = ParameterDirection.Input;
            SqlParms[1].Value = BookedIntime;

            SqlParms[2] = new SqlParameter("@BookedOutTime", SqlDbType.VarChar);
            SqlParms[2].Direction = ParameterDirection.Input;
            SqlParms[2].Value = BookedOutTime;


            SqlParms[3] = new SqlParameter("@MonthlySubscription", SqlDbType.VarChar);
            SqlParms[3].Direction = ParameterDirection.Input;
            SqlParms[3].Value = MonthlySuscription;

            SqlParms[4] = new SqlParameter("@vehiclewheels", SqlDbType.VarChar);
            SqlParms[4].Direction = ParameterDirection.Input;
            SqlParms[4].Value = Vehicle_Type;

            SqlParms[5] = new SqlParameter("@Error", SqlDbType.VarChar, 100);
            SqlParms[5].Direction = ParameterDirection.Output;

            //SqlParms[6] = new SqlParameter("@TotalAmount", SqlDbType.VarChar, 100);
            //SqlParms[6].Direction = ParameterDirection.Output;


            //SqlParms[4].Value = flag;
            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "GET_EstimatedBill", SqlParms);

            if (ds != null)
            {
                if (Convert.ToString(SqlParms[5].Value) == "0" && !string.IsNullOrWhiteSpace(Convert.ToString(SqlParms[5].Value)))
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //(drow["Parking_Space_Name"]) == DBNull.Value ? "" : Convert.ToString(drow["Parking_Space_Name"]);
                        EstimatedCostBEL.TotalAmount = (ds.Tables[0].Rows[0]["TotalAmount"]) == DBNull.Value ? "" : Convert.ToString(ds.Tables[0].Rows[0]["TotalAmount"]);
                        EstimatedCostBEL.TotalDays = (ds.Tables[0].Rows[0]["TotalDays"]) == DBNull.Value ? "" : Convert.ToString(ds.Tables[0].Rows[0]["TotalDays"]);
                        EstimatedCostBEL.Totalhours = (ds.Tables[0].Rows[0]["Totalhours"]) == DBNull.Value ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Totalhours"]);
                        EstimatedCostBEL.TotalMinutes = (ds.Tables[0].Rows[0]["TotalMinutes"]) == DBNull.Value ? "" : Convert.ToString(ds.Tables[0].Rows[0]["TotalMinutes"]);
                        EstimatedCostBEL.Totalmonths = (ds.Tables[0].Rows[0]["Totalmonths"]) == DBNull.Value ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Totalmonths"]);
                        EstimatedCostBEL.BaseAmount = (ds.Tables[0].Rows[0]["BaseAmount"]) == DBNull.Value ? "" : Convert.ToString(ds.Tables[0].Rows[0]["BaseAmount"]);
                        EstimatedCostBEL.TWServiceCharge = (ds.Tables[0].Rows[0]["TWServiceCharge"]) == DBNull.Value ? "" : Convert.ToString(ds.Tables[0].Rows[0]["TWServiceCharge"]);
                        EstimatedCostBEL.BookingTDSAmount = (ds.Tables[0].Rows[0]["BookingFees"]) == DBNull.Value ? "" : Convert.ToString(ds.Tables[0].Rows[0]["BookingFees"]);
                        EstimatedCostBEL.RYSTDSAmount = (ds.Tables[0].Rows[0]["BoookingRecievedFees"]) == DBNull.Value ? "" : Convert.ToString(ds.Tables[0].Rows[0]["BoookingRecievedFees"]);
                        EstimatedCostBEL.PayableToRYSAmount = (ds.Tables[0].Rows[0]["PayableToRYSAmount"]) == DBNull.Value ? "" : Convert.ToString(ds.Tables[0].Rows[0]["PayableToRYSAmount"]);

                        EstimatedCostBEL.Status = true;
                        EstimatedCostBEL.Error = string.Empty;
                    }
                }
                else EstimatedCostBEL.Error = Convert.ToString(SqlParms[5].Value);
            }
            else if (Convert.ToString(SqlParms[5].Value) != "0" && string.IsNullOrWhiteSpace(Convert.ToString(SqlParms[5].Value)))
                EstimatedCostBEL.Error = Convert.ToString(SqlParms[5].Value);
            return EstimatedCostBEL;
        }

        //public CheckInCheckOutBEL UpdateCheckInCheckOut(string bookingId, DateTime CheckInCheckOutTime, string whodidcheckoutID)
        //{
        //    CheckInCheckOutBEL checkInCheckOutBEL = new CheckInCheckOutBEL();
        //    DataSet ds = null;

        //    SqlParameter[] SqlParms = new SqlParameter[14];
        //    SqlParms[0] = new SqlParameter("@BookingId", SqlDbType.VarChar, 100);
        //    SqlParms[0].Direction = ParameterDirection.Input;
        //    SqlParms[0].Value = bookingId;

        //    SqlParms[1] = new SqlParameter("@CheckInCheckOutTime", SqlDbType.DateTime, 100);
        //    SqlParms[1].Direction = ParameterDirection.Input;
        //    SqlParms[1].Value = CheckInCheckOutTime;

        //    SqlParms[2] = new SqlParameter("@CheckInCheckOutTime", SqlDbType.DateTime, 100);
        //    SqlParms[2].Direction = ParameterDirection.Input;
        //    SqlParms[2].Value = CheckInCheckOutTime;

        //    SqlParms[3] = new SqlParameter("@Error", SqlDbType.VarChar, 100);
        //    SqlParms[3].Direction = ParameterDirection.Output;


        //    SqlParms[2] = new SqlParameter("@Status", SqlDbType.Bit);
        //    SqlParms[2].Direction = ParameterDirection.Output;
        //    //SqlParms[3].Value = flag;


        //    //SqlParms[4].Value = flag;

        //    SqlParms[4] = new SqlParameter("@TotalAmount", SqlDbType.VarChar, 100);
        //    SqlParms[4].Direction = ParameterDirection.Output;

        //    SqlParms[5] = new SqlParameter("@TotalDays", SqlDbType.VarChar, 100);
        //    SqlParms[5].Direction = ParameterDirection.Output;

        //    SqlParms[6] = new SqlParameter("@Totalhours", SqlDbType.VarChar, 100);
        //    SqlParms[6].Direction = ParameterDirection.Output;

        //    SqlParms[7] = new SqlParameter("@TotalMinutes", SqlDbType.VarChar, 100);
        //    SqlParms[7].Direction = ParameterDirection.Output;

        //    SqlParms[8] = new SqlParameter("@ExtraMinutes", SqlDbType.VarChar, 100);
        //    SqlParms[8].Direction = ParameterDirection.Output;

        //    SqlParms[9] = new SqlParameter("@ExtraHours", SqlDbType.VarChar, 100);
        //    SqlParms[9].Direction = ParameterDirection.Output;

        //    SqlParms[10] = new SqlParameter("@ExtraDays", SqlDbType.VarChar, 100);
        //    SqlParms[10].Direction = ParameterDirection.Output;

        //    SqlParms[11] = new SqlParameter("@ExtraMonth", SqlDbType.VarChar, 100);
        //    SqlParms[11].Direction = ParameterDirection.Output;


        //    SqlParms[12] = new SqlParameter("@ExtraAmountonCheckout", SqlDbType.VarChar, 100);
        //    SqlParms[12].Direction = ParameterDirection.Output;

        //    SqlParms[13] = new SqlParameter("@Final_amount", SqlDbType.VarChar, 100);
        //    SqlParms[13].Direction = ParameterDirection.Output;


        //    ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "UpdateBookingCheckOut", SqlParms);

        //    if (ds != null)
        //    {
        //        if (Convert.ToString(SqlParms[4].Value) == "True" && Convert.ToString(SqlParms[3].Value) == "")
        //        {
        //            checkInCheckOutBEL.Status = true;
        //            checkInCheckOutBEL.Error = "Checked Out Successfully.";

        //            foreach (DataRow drow in ds.Tables[0].Rows)
        //            {

        //                checkInCheckOutBEL.TotalAmount = Convert.ToString(drow["TotalAmount"]);
        //                checkInCheckOutBEL.Total_Days = Convert.ToString(drow["Total_Days"]);
        //                checkInCheckOutBEL.Total_Hour = Convert.ToString(drow["Total_Hour"]);
        //                checkInCheckOutBEL.Total_Min = Convert.ToString(drow["Total_Min"]);
        //                checkInCheckOutBEL.ExtraAmount = Convert.ToString(drow["ExtraAmount"]);
        //                checkInCheckOutBEL.Extra_Days = Convert.ToString(drow["Extra_Days"]);
        //                checkInCheckOutBEL.Extra_Hour = Convert.ToString(drow["Extra_Hour"]);
        //                checkInCheckOutBEL.Extra_Min = Convert.ToString(drow["Extra_Min"]);
        //                checkInCheckOutBEL.Extra_month = Convert.ToString(drow["Extra_month"]);
        //                //checkInCheckOutBEL.Final_amount = Convert.ToString(drow["Final_amount"]);
        //                checkInCheckOutBEL.Base_Amount = Convert.ToString(drow["Base_Amount"]);

        //            }

        //        }
        //    }

        //    return checkInCheckOutBEL;
        //}

        public SYSPaymentResponse RYSBookSpace(BookSYSParking BookSYSParking)
        {
            SYSPaymentResponse bookSYSParkingResult = new SYSPaymentResponse();
            DataSet ds = null;
            //Error = string.Empty;
            //BookSYSParkingResult bookSYSParkingResult = new BookSYSParkingResult();
            SqlParameter[] SqlParms = new SqlParameter[19];
            SqlParms[0] = new SqlParameter("@ParkingId", SqlDbType.VarChar, 400);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = BookSYSParking.parkingId;

            //SqlParms[1] = new SqlParameter("@OwnerId", SqlDbType.VarChar, 100);
            //SqlParms[1].Direction = ParameterDirection.Input;
            //SqlParms[1].Value = BookSYSParking.owner_userId;

            SqlParms[1] = new SqlParameter("@UserId", SqlDbType.VarChar, 100);
            SqlParms[1].Direction = ParameterDirection.Input;
            SqlParms[1].Value = BookSYSParking.login_userId;

            SqlParms[2] = new SqlParameter("@BookedInTime", SqlDbType.DateTime);
            SqlParms[2].Direction = ParameterDirection.Input;
            SqlParms[2].Value = BookSYSParking.bookedInTime;

            SqlParms[3] = new SqlParameter("@BookedOutDatetime", SqlDbType.DateTime);
            SqlParms[3].Direction = ParameterDirection.Input;
            SqlParms[3].Value = BookSYSParking.bookedOutDatetime;

            SqlParms[4] = new SqlParameter("@VehicalNo", SqlDbType.VarChar, 100);
            SqlParms[4].Direction = ParameterDirection.Input;
            SqlParms[4].Value = BookSYSParking.vehicalNo;

            SqlParms[5] = new SqlParameter("@VehicleType", SqlDbType.VarChar, 100);
            SqlParms[5].Direction = ParameterDirection.Input;
            SqlParms[5].Value = BookSYSParking.vehicleType;

            SqlParms[6] = new SqlParameter("@PaymentMode", SqlDbType.VarChar, 100);
            SqlParms[6].Direction = ParameterDirection.Input;
            SqlParms[6].Value = BookSYSParking.paymentMode;

            SqlParms[7] = new SqlParameter("@PaymentAmount", SqlDbType.VarChar, 100);
            SqlParms[7].Direction = ParameterDirection.Input;
            SqlParms[7].Value = BookSYSParking.paymentAmount;

            SqlParms[8] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
            SqlParms[8].Direction = ParameterDirection.Output;

            SqlParms[9] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
            SqlParms[9].Direction = ParameterDirection.Output;

            SqlParms[10] = new SqlParameter("@BookingId", SqlDbType.VarChar, 150);
            SqlParms[10].Direction = ParameterDirection.Output;

            SqlParms[11] = new SqlParameter("@MonthlySubscription", SqlDbType.VarChar, 100);
            SqlParms[11].Direction = ParameterDirection.Input;
            SqlParms[11].Value = BookSYSParking.monthlySubscription;

            SqlParms[12] = new SqlParameter("@TransactionID", SqlDbType.VarChar, 100);
            SqlParms[12].Direction = ParameterDirection.Input;
            SqlParms[12].Value = BookSYSParking.Transaction_ID;

            SqlParms[13] = new SqlParameter("@BaseAmount", SqlDbType.VarChar, 100);
            SqlParms[13].Direction = ParameterDirection.Input;
            SqlParms[13].Value = BookSYSParking.BaseAmount;

            SqlParms[14] = new SqlParameter("@PayableToRYSAmount", SqlDbType.VarChar, 100);
            SqlParms[14].Direction = ParameterDirection.Input;
            SqlParms[14].Value = BookSYSParking.PayableToRYSAmount;

            SqlParms[15] = new SqlParameter("@BookingTDSAmount", SqlDbType.VarChar, 100);
            SqlParms[15].Direction = ParameterDirection.Input;
            SqlParms[15].Value = BookSYSParking.BookingTDSAmount;

            SqlParms[16] = new SqlParameter("@TotalAmount", SqlDbType.VarChar, 100);
            SqlParms[16].Direction = ParameterDirection.Input;
            SqlParms[16].Value = BookSYSParking.TotalAmount;

            SqlParms[17] = new SqlParameter("@TWServiceCharge", SqlDbType.VarChar, 100);
            SqlParms[17].Direction = ParameterDirection.Input;
            SqlParms[17].Value = BookSYSParking.TWServiceCharge;

            SqlParms[18] = new SqlParameter("@RYSTDSAmount", SqlDbType.VarChar, 100);
            SqlParms[18].Direction = ParameterDirection.Input;
            SqlParms[18].Value = BookSYSParking.RYSTDSAmount;

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "RYS_BookSpace_AfterPayment", SqlParms);
            //DataRow drow = ds.Tables[0].Rows.;
            //DataRow dr in ds.Tables[1].Columns[0];
            //DataRow drr in ds.Tables[2].Rows;

            if (ds != null)
            {
                if (Convert.ToString(SqlParms[9].Value) == "true" && Convert.ToString(SqlParms[8].Value) == "0")
                {
                    bookSYSParkingResult.Status = true;
                    //  bookSYSParkingResult.Error = "Booked Successfully.";
                    //(drow["Parking_Space_Name"]) == DBNull.Value ? "" : Convert.ToString(drow["Parking_Space_Name"]);
                    bookSYSParkingResult.Booking_ID = Convert.ToString(SqlParms[11].Value);

                    foreach (DataRow drow in ds.Tables[0].Rows)
                    {

                        bookSYSParkingResult.Booking_ID = (drow["Booked_Id"]) == DBNull.Value ? "" : Convert.ToString(drow["Booked_Id"]); //Convert.ToString(drow["Booked_Id"]);
                        bookSYSParkingResult.Payment_Transaction_ID = (drow["TransactionID"]) == DBNull.Value ? "" : Convert.ToString(drow["TransactionID"]); //Convert.ToString(drow["TransactionID"]);
                        bookSYSParkingResult.PaidAmount = (drow["TotalAmount"]) == DBNull.Value ? "" : Convert.ToString(drow["TotalAmount"]); //Convert.ToString(drow["TotalAmount"]);

                    }


                    string bookedby = string.Empty;
                    string UserType = string.Empty;
                    string vehicleno = string.Empty;
                    string parkname = string.Empty;
                    string BookedInTime = string.Empty;
                    string BookedOutDatetime = string.Empty;
                    // DateTime checkout = DateTime.Compare;
                    Int64 AttendentNo = 0;
                    Int64 POphoneNo = 0;
                    Int64 VOphoneNo = 0;
                    Int64 SNphoneNo = 0;
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow drrr in ds.Tables[1].Rows)
                        {

                            bookSYSParkingResult.Attendant_Phone_No = (drrr["AttendentPhone"]) == DBNull.Value ? 0 : Convert.ToInt64(drrr["AttendentPhone"]); //Convert.ToInt64(drrr["AttendentPhone"]);
                            bookSYSParkingResult.Attendant_Name = (drrr["AttendentName"]) == DBNull.Value ? "" : Convert.ToString(drrr["AttendentName"]); //Convert.ToString(drrr["AttendentName"]);
                            bookSYSParkingResult.Full_Name = (drrr["Full_Name"]) == DBNull.Value ? "" : Convert.ToString(drrr["Full_Name"]); //Convert.ToString(drrr["Full_Name"]);
                            //bookSYSParkingResult.Number = (drrr["Phone_No1"]) == DBNull.Value ? "" : Convert.ToString(drrr["Phone_No1"]); //Convert.ToString(drrr["Phone_No1"]);
                            bookSYSParkingResult.Owner_Address = (drrr["ParkingAddress"]) == DBNull.Value ? "" : Convert.ToString(drrr["ParkingAddress"]); //Convert.ToString(drrr["ParkingAddress"]);
                            bookSYSParkingResult.Parking_support_no = (drrr["Phone_No1"]) == DBNull.Value ? "" : Convert.ToString(drrr["Phone_No1"]); //Convert.ToString(drrr["Phone_No1"]);
                            //bookSYSParkingResult.vehicleno = Convert.ToString(drrr["VehicleNumber"]);
                            parkname = (drrr["ParkingName"]) == DBNull.Value ? "" : Convert.ToString(drrr["ParkingName"]); //Convert.ToString(drrr["ParkingName"]);
                            vehicleno = (drrr["VehicleNumber"]) == DBNull.Value ? "" : Convert.ToString(drrr["VehicleNumber"]); //Convert.ToString(drrr["VehicleNumber"]);
                            bookedby = (drrr["bookedby"]) == DBNull.Value ? "" : Convert.ToString(drrr["bookedby"]); //Convert.ToString(drrr["bookedby"]);
                            UserType = (drrr["User_Type"]) == DBNull.Value ? "" : Convert.ToString(drrr["User_Type"]); //Convert.ToString(drrr["User_Type"]);
                            BookedInTime = (drrr["BookedIntime"]) == DBNull.Value ? "" : Convert.ToString(drrr["BookedIntime"]); //Convert.ToString(drrr["BookedIntime"]);
                            BookedOutDatetime = (drrr["BookedOutTime"]) == DBNull.Value ? "" : Convert.ToString(drrr["BookedOutTime"]); //Convert.ToString(drrr["BookedOutTime"]);
                            AttendentNo = (drrr["AttendentPhone"]) == DBNull.Value ? 0 : Convert.ToInt64(drrr["AttendentPhone"]); //Convert.ToInt64(drrr["AttendentPhone"]);
                            SNphoneNo = (drrr["Phone_No1"]) == DBNull.Value ? 0 : Convert.ToInt64(drrr["Phone_No1"]);
                            if (UserType == "VO")
                                VOphoneNo = (drrr["phone_no"]) == DBNull.Value ? 0 : Convert.ToInt64(drrr["phone_no"]);//Convert.ToInt64(drrr["Phone_No1"]);
                            else if (UserType == "PO")
                                POphoneNo = (drrr["phone_no"]) == DBNull.Value ? 0 : Convert.ToInt64(drrr["phone_no"]); //Convert.ToInt64(drrr["Phone_No1"]);
                        }


                        OTPBEL OTPBel = new OTPBEL();
                        var OTPList = OTPBel.RYSBookingMessage(Convert.ToInt64(VOphoneNo), Convert.ToInt64(POphoneNo), bookedby, Convert.ToString(vehicleno), Convert.ToString(BookedInTime), Convert.ToString(BookedOutDatetime), Convert.ToString(parkname), Convert.ToInt64(SNphoneNo), Convert.ToInt64(AttendentNo), Convert.ToInt64(SqlParms[0].Value));
                    }

                    //foreach (DataRow dr in ds.Tables[1].Rows)
                    //{
                    //    bookSYSParkingResult.Full_Name = Convert.ToString(dr["Full_Name"]);
                    //    //bookSYSParkingResult.Last_Name = Convert.ToString(dr["Last_Name"]);
                    //    bookSYSParkingResult.Owner_Address = Convert.ToString(dr["owner_address"]);
                    //    bookSYSParkingResult.Number = Convert.ToString(dr["Phone_No1"]);
                    //    String phoneNo = Convert.ToString(dr["Phone_No1"]);
                    //    OTPBEL OTPBel = new OTPBEL();
                    //    var OTPList = OTPBel.BookingDetailstoPO(Convert.ToInt64(phoneNo), Convert.ToInt64(SqlParms[10].Value));


                    //}

                    //foreach (DataRow drr in ds.Tables[2].Rows)
                    //{
                    //    bookSYSParkingResult.Attendant_Name = Convert.ToString(drr["Attendant_Name"]);
                    //    bookSYSParkingResult.Attendant_Phone_No = Convert.ToString(drr["Attendant_Phone_No"]);
                    //    //bookSYSParkingResult.Phone_No1 = Convert.ToString(drr["Phone_No1"]);
                    //    String Attendant_Phone_No = Convert.ToString(drr["Attendant_Phone_No"]);
                    //    OTPBEL OTPBel = new OTPBEL();
                    //    var OTPList = OTPBel.BookingDetailstoTWA(Convert.ToInt64(Attendant_Phone_No), Convert.ToInt64(SqlParms[10].Value));


                    //}

                    //foreach (DataRow drrr in ds.Tables[3].Rows)
                    //{
                    //    //bookSYSParkingResult.Attendant_Name = Convert.ToString(drr["Attendant_Name"]);
                    //    //bookSYSParkingResult.Attendant_Phone_No = Convert.ToString(drr["Attendant_Phone_No"]);
                    //    String phoneNo = Convert.ToString(drrr["Phone_No1"]);
                    //    OTPBEL OTPBel = new OTPBEL();
                    //    var OTPList = OTPBel.GetAndSendOTPBooking(Convert.ToInt64(phoneNo), Convert.ToInt64(SqlParms[10].Value));

                    //}

                }
                else bookSYSParkingResult.Error = Convert.ToString(SqlParms[8].Value);
            }
            else if (Convert.ToString(SqlParms[8].Value) != "0" && !string.IsNullOrWhiteSpace(Convert.ToString(SqlParms[8].Value)))
                bookSYSParkingResult.Error = Convert.ToString(SqlParms[9].Value);
            return bookSYSParkingResult;

            //else
            //   {
            //       Error = Convert.ToString(SqlParms[9].Value);
            //      // return;
            //       //registerSYSParkingAreaResult.Parking_id = 0; 
            //   }

        }






        //public List<RegsiterParkingAreaResult> GetRYSParkingDetails(GetPrivateParkingSYSResponseBEL getPrivateParkingSYSResponse)
        //{
        //    throw new NotImplementedException();
        //}

        public List<AvailableParkingAreaRYSResult> GetAvailableParkingRYS(ParkingAreaBEL parking)
        {
            List<AvailableParkingAreaRYSResult> AvailableParkings = new List<AvailableParkingAreaRYSResult>();
            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[4];

                SqlParms[0] = new SqlParameter("@Latitude", SqlDbType.Decimal, 10);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = parking.Latitude;

                SqlParms[1] = new SqlParameter("@Longitude", SqlDbType.Decimal, 10);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = parking.Longitude;

                SqlParms[2] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[2].Direction = ParameterDirection.Output;

                SqlParms[3] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
                SqlParms[3].Direction = ParameterDirection.Output;

                //SqlParms[7] = new SqlParameter("@UserId", SqlDbType.NVarChar, 20);
                //SqlParms[7].Direction = ParameterDirection.Output;



                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "SP_GetPrivateParkingSYS", SqlParms);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        AvailableParkings = (from DataRow parkingrow in ds.Tables[0].Rows
                                             select new AvailableParkingAreaRYSResult
                                             {
                                                 Parking_Id = (parkingrow["Distance"] == null ? 0 : Convert.ToInt64(parkingrow["Parking_id"])),

                                                 Distance = (parkingrow["Distance"] == System.DBNull.Value ? "" : Convert.ToString(parkingrow["Distance"])),
                                                 lattitude = (parkingrow["lattitude"] == null ? "" : Convert.ToString(parkingrow["lattitude"])),
                                                 longitude = (parkingrow["longitude"] == null ? "" : Convert.ToString(parkingrow["longitude"])),
                                                 CarBasicCharge = (parkingrow["CarBasicCharge"] == System.DBNull.Value ? "" : Convert.ToString(parkingrow["CarBasicCharge"])),
                                                 BikeBasicCharge = (parkingrow["BikeBasicCharge"] == System.DBNull.Value ? "" : Convert.ToString(parkingrow["BikeBasicCharge"])),
                                                 CarDailyCharge = (parkingrow["CarDailyCharge"] == System.DBNull.Value ? "" : Convert.ToString(parkingrow["CarDailyCharge"])),
                                                 bikedailycharge = (parkingrow["bikedailycharge"] == System.DBNull.Value ? "" : Convert.ToString(parkingrow["bikedailycharge"])),
                                                 carmonthlycharge = (parkingrow["carmonthlycharge"] == System.DBNull.Value ? "" : Convert.ToString(parkingrow["carmonthlycharge"])),
                                                 bikemonthlycharge = (parkingrow["bikemonthycharge"] == System.DBNull.Value ? "" : Convert.ToString(parkingrow["bikemonthycharge"])),

                                                //CarBasicCharge = (parkingrow["CarBasicCharge"] == System.DBNull.Value ? "" : Convert.ToString(parkingrow["CarBasicCharge"])),
                                                ErrorMessage = Convert.ToString(SqlParms[2].Value) == null ? "" : Convert.ToString(SqlParms[2].Value),
                                                
                                                 Success = true
                                             }
                                        ).ToList<AvailableParkingAreaRYSResult>();

                        return AvailableParkings;

                    }
                }
                else
                {
                    AvailableParkings.Add(new AvailableParkingAreaRYSResult() { Success = false, ErrorMessage = Convert.ToString(SqlParms[2].Value) == null ? "" : Convert.ToString(SqlParms[2].Value) });
                    return AvailableParkings;
                }

            }
            catch (Exception ex)
            {
                AvailableParkings.Add(new AvailableParkingAreaRYSResult() { Success = false, ErrorMessage = Convert.ToString(ex.Message.ToString()) });
                ErrorLog.Log(this.GetType().Name, MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), HttpContext.Current.Session["userPhoneNo/Email"] != null ? HttpContext.Current.Session["userPhoneNo/Email"].ToString() : "");
                return AvailableParkings;
            }

            return AvailableParkings;
        }

        //Added by Neeraj to get details on Profile click

        public List<OnProfileClick> Profile_Click(string User_id, out string errormessage)
        {

            //List<OnProfileClick> PC = new List<OnProfileClick>();
            DataSet ds = null;
            errormessage = string.Empty;
            SqlParameter[] SqlParms = new SqlParameter[2];



            SqlParms[0] = new SqlParameter("@User_id", SqlDbType.VarChar, 4000);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = User_id;


            SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
            SqlParms[1].Direction = ParameterDirection.Output;

            //SqlParms[2] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
            //SqlParms[2].Direction = ParameterDirection.Output;

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_ProfileDetail", SqlParms);
            List<OnProfileClick> PC = new List<OnProfileClick>();
            if (ds != null)
            {
                OnProfileClick opc;
                if ((SqlParms[1].Value == null || SqlParms[1].Value == "" || SqlParms[1].Value == "0"))
                {

                    foreach (DataRow drow in ds.Tables[0].Rows)
                    {
                        opc = new OnProfileClick();

                        //registerSYSParkingAreaResult.Parking_Space_Name = (drow["Parking_Space_Name"]) == DBNull.Value ? "" : Convert.ToString(drow["Parking_Space_Name"]);

                        opc.Full_Name = (drow["Full_Name"]) == DBNull.Value ? "" : Convert.ToString(drow["Full_Name"]);
                        opc.User_id = (drow["User_id"]) == DBNull.Value ? 0 : Convert.ToInt64(drow["User_id"]);
                        opc.Email_id = (drow["Email_id"]) == DBNull.Value ? "" : Convert.ToString(drow["Email_id"]);
                        opc.DOB = (drow["DOB"]) == DBNull.Value ? "" : Convert.ToString(drow["DOB"]);
                        opc.Phone_No1 = (drow["Phone_No1"]) == DBNull.Value ? "" : Convert.ToString(drow["Phone_No1"]);
                        opc.Phone_No2 = (drow["Phone_No2"]) == DBNull.Value ? "" : Convert.ToString(drow["Phone_No2"]);
                        opc.PAN = (drow["PAN"]) == DBNull.Value ? "" : Convert.ToString(drow["PAN"]);
                        opc.Driving_License = (drow["Driving_License"]) == DBNull.Value ? "" : Convert.ToString(drow["Driving_License"]);
                        Vehicles vcl = new Vehicles();
                        List<Vehicles> Listvcl = new List<Vehicles>();
                        vcl.Vehicle_No = ds.Tables[0].Rows[0]["Vehicle_No1"].ToString();
                        vcl.Vehicle_Type = ds.Tables[0].Rows[0]["Vehicle_Type1"].ToString();
                        vcl.Vehicle_Model = ds.Tables[0].Rows[0]["Vehicle_Model1"].ToString();
                        Listvcl.Add(vcl);
                        vcl = new Vehicles();
                        vcl.Vehicle_No = ds.Tables[0].Rows[0]["Vehicle_No2"].ToString();
                        vcl.Vehicle_Type = ds.Tables[0].Rows[0]["Vehicle_Type2"].ToString();
                        vcl.Vehicle_Model = ds.Tables[0].Rows[0]["Vehicle_Model2"].ToString();
                        Listvcl.Add(vcl);

                        vcl = new Vehicles();
                        vcl.Vehicle_No = ds.Tables[0].Rows[0]["Vehicle_No3"].ToString();
                        vcl.Vehicle_Type = ds.Tables[0].Rows[0]["Vehicle_Type3"].ToString();
                        vcl.Vehicle_Model = ds.Tables[0].Rows[0]["Vehicle_Model3"].ToString();
                        Listvcl.Add(vcl);

                        vcl = new Vehicles();
                        vcl.Vehicle_No = ds.Tables[0].Rows[0]["Vehicle_No4"].ToString();
                        vcl.Vehicle_Type = ds.Tables[0].Rows[0]["Vehicle_Type4"].ToString();
                        vcl.Vehicle_Model = ds.Tables[0].Rows[0]["Vehicle_Model4"].ToString();
                        Listvcl.Add(vcl);



                        vcl = new Vehicles();
                        vcl.Vehicle_No = ds.Tables[0].Rows[0]["Vehicle_No5"].ToString();
                        vcl.Vehicle_Type = ds.Tables[0].Rows[0]["Vehicle_Type5"].ToString();
                        vcl.Vehicle_Model = ds.Tables[0].Rows[0]["Vehicle_Model5"].ToString();
                        Listvcl.Add(vcl);

                        opc.listVehicle = Listvcl;



                        //opc.Vehicle_Id = Convert.ToInt32(ds.Tables[0].Rows[0]["Vehicle_Id"]);




                        PC.Add(opc);
                    }

                }

                else
                {
                    errormessage = Convert.ToString(SqlParms[1].Value);
                    // return;
                    //registerSYSParkingAreaResult.Parking_id = 0; 
                }
            }
            return PC;

        }



        //Added by Neeraj to get details on individual parking click click

        public List<OnParkingBubbleClick> Parking_Click(string Parking_id, out string errormessage)
        {

            //List<OnProfileClick> PC = new List<OnProfileClick>();
            DataSet ds = null;
            errormessage = string.Empty;
            SqlParameter[] SqlParms = new SqlParameter[2];



            SqlParms[0] = new SqlParameter("@Parking_id", SqlDbType.VarChar, 4000);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = Parking_id;


            SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
            SqlParms[1].Direction = ParameterDirection.Output;

            //SqlParms[2] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
            //SqlParms[2].Direction = ParameterDirection.Output;

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_IndividualParkingDetail", SqlParms);
            List<OnParkingBubbleClick> PBC = new List<OnParkingBubbleClick>();
            if (ds != null)
            {
                OnParkingBubbleClick OPBC;
                if ((SqlParms[1].Value == null || SqlParms[1].Value == "" || SqlParms[1].Value == "0"))
                {

                    foreach (DataRow drow in ds.Tables[0].Rows)
                    {
                        OPBC = new OnParkingBubbleClick();
                        //(drow["Booked_Id"]) == DBNull.Value ? 0 : Convert.ToInt64(drow["Booked_Id"]);

                        OPBC.Parking_Space_Name = (drow["Parking_Space_Name"]) == DBNull.Value ? "" : Convert.ToString(drow["Parking_Space_Name"]);// Convert.ToString(drow["Parking_Space_Name"]);
                        OPBC.Parking_id = (drow["Parking_id"]) == DBNull.Value ? 0 : Convert.ToInt64(drow["Parking_id"]); //Convert.ToInt64(drow["Parking_id"]);
                        OPBC.Parking_Owner_Comment = (drow["Parking_Owner_Comment"]) == DBNull.Value ? "" : Convert.ToString(drow["Parking_Owner_Comment"]);// Convert.ToString(drow["Parking_Owner_Comment"]);
                        OPBC.parking_address = (drow["parking_address"]) == DBNull.Value ? "" : Convert.ToString(drow["parking_address"]); //Convert.ToString(drow["parking_address"]);
                        OPBC.CarBasicCharge = (drow["CarBasicCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["CarBasicCharge"]); //Convert.ToString(drow["CarBasicCharge"]);
                        OPBC.CarDailyCharge = (drow["CarDailyCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["CarDailyCharge"]); //Convert.ToString(drow["CarDailyCharge"]);
                        OPBC.carmonthlycharge = (drow["carmonthlycharge"]) == DBNull.Value ? "" : Convert.ToString(drow["carmonthlycharge"]);// Convert.ToString(drow["carmonthlycharge"]);
                        OPBC.BikeBasicCharge = (drow["BikeBasicCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["BikeBasicCharge"]); //Convert.ToString(drow["BikeBasicCharge"]);
                        OPBC.bikedailycharge = (drow["bikedailycharge"]) == DBNull.Value ? "" : Convert.ToString(drow["bikedailycharge"]); //ds.Tables[0].Rows[0]["bikedailycharge"].ToString();
                        OPBC.bikemonthycharge = (drow["bikemonthycharge"]) == DBNull.Value ? "" : Convert.ToString(drow["bikemonthycharge"]); //ds.Tables[0].Rows[0]["bikemonthycharge"].ToString();
                        OPBC.No_Of_Space_Avaiable = (drow["No_Of_Space_Avaiable"]) == DBNull.Value ? "" : Convert.ToString(drow["No_Of_Space_Avaiable"]); //ds.Tables[0].Rows[0]["No_Of_Space_Avaiable"].ToString();
                        OPBC.No_Of_Space_Avaiable_bike = (drow["No_Of_Space_Avaiable_bike"]) == DBNull.Value ? "" : Convert.ToString(drow["No_Of_Space_Avaiable_bike"]); //ds.Tables[0].Rows[0]["No_Of_Space_Avaiable_bike"].ToString();
                        OPBC.TimeFrom = (drow["TimeFrom"]) == DBNull.Value ? "" : Convert.ToString(drow["TimeFrom"]); //ds.Tables[0].Rows[0]["TimeFrom"].ToString();
                        OPBC.TimeTo = (drow["TimeTo"]) == DBNull.Value ? "" : Convert.ToString(drow["TimeTo"]); //ds.Tables[0].Rows[0]["TimeTo"].ToString();
                        OPBC.DaysSelected = (drow["DaysSelected"]) == DBNull.Value ? "" : Convert.ToString(drow["DaysSelected"]); //ds.Tables[0].Rows[0]["DaysSelected"].ToString();
                        OPBC.FacilitySelected = (drow["FacilitySelected"]) == DBNull.Value ? "" : Convert.ToString(drow["FacilitySelected"]); //ds.Tables[0].Rows[0]["FacilitySelected"].ToString();
                        OPBC.DescriptionSelected = (drow["DescriptionSelected"]) == DBNull.Value ? "" : Convert.ToString(drow["DescriptionSelected"]); //ds.Tables[0].Rows[0]["DescriptionSelected"].ToString();
                        OPBC.Parking_Available_Date = (drow["Parking_Available_Date"]) == DBNull.Value ? "" : Convert.ToString(drow["Parking_Available_Date"]);

                        PBC.Add(OPBC);
                    }

                }

                else
                {
                    errormessage = Convert.ToString(SqlParms[1].Value);
                    // return;
                    //registerSYSParkingAreaResult.Parking_id = 0; 
                }
            }
            return PBC;

        }



        //Added by Neeraj to get details on Profile click

        public List<VehicleInfo> Vehicle_Info(string User_id, out string errormessage)
        {

            //List<OnProfileClick> PC = new List<OnProfileClick>();
            DataSet ds = null;
            errormessage = string.Empty;
            SqlParameter[] SqlParms = new SqlParameter[2];



            SqlParms[0] = new SqlParameter("@User_id", SqlDbType.VarChar, 4000);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = User_id;


            SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
            SqlParms[1].Direction = ParameterDirection.Output;

            //SqlParms[2] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
            //SqlParms[2].Direction = ParameterDirection.Output;

            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_VehicleInfo", SqlParms);
            List<VehicleInfo> PC = new List<VehicleInfo>();
            if (ds != null)
            {
                VehicleInfo opc;
                if ((SqlParms[1].Value == null || SqlParms[1].Value == "" || SqlParms[1].Value == "0"))
                {

                    foreach (DataRow drow in ds.Tables[0].Rows)
                    {
                        opc = new VehicleInfo();

                        opc.User_id = Convert.ToInt64(drow["User_id"]);

                        UsersVehicles vcl = new UsersVehicles();

                        List<UsersVehicles> Listvcl = new List<UsersVehicles>();
                        vcl.Vehicle_No = ds.Tables[0].Rows[0]["Vehicle_No1"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_No1"].ToString();  //ds.Tables[0].Rows[0]["Vehicle_No1"].ToString();
                        vcl.Vehicle_Type = ds.Tables[0].Rows[0]["Vehicle_Type1"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_Type1"].ToString(); //ds.Tables[0].Rows[0]["Vehicle_Type1"].ToString();
                        vcl.Vehicle_Model = ds.Tables[0].Rows[0]["Vehicle_Model1"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_Model1"].ToString(); //ds.Tables[0].Rows[0]["Vehicle_Model1"].ToString();
                        Listvcl.Add(vcl);
                        vcl = new UsersVehicles();
                        vcl.Vehicle_No = ds.Tables[0].Rows[0]["Vehicle_No2"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_No2"].ToString(); //ds.Tables[0].Rows[0]["Vehicle_No2"].ToString();
                        vcl.Vehicle_Type = ds.Tables[0].Rows[0]["Vehicle_Type2"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_Type2"].ToString(); //ds.Tables[0].Rows[0]["Vehicle_Type2"].ToString();
                        vcl.Vehicle_Model = ds.Tables[0].Rows[0]["Vehicle_Model2"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_Model2"].ToString(); //ds.Tables[0].Rows[0]["Vehicle_Model2"].ToString();
                        Listvcl.Add(vcl);

                        vcl = new UsersVehicles();
                        vcl.Vehicle_No = ds.Tables[0].Rows[0]["Vehicle_No3"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_No3"].ToString(); //ds.Tables[0].Rows[0]["Vehicle_No3"].ToString();
                        vcl.Vehicle_Type = ds.Tables[0].Rows[0]["Vehicle_Type3"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_Type3"].ToString(); //ds.Tables[0].Rows[0]["Vehicle_Type3"].ToString();
                        vcl.Vehicle_Model = ds.Tables[0].Rows[0]["Vehicle_Model3"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_Model3"].ToString(); //ds.Tables[0].Rows[0]["Vehicle_Model3"].ToString();
                        Listvcl.Add(vcl);

                        vcl = new UsersVehicles();
                        vcl.Vehicle_No = ds.Tables[0].Rows[0]["Vehicle_No4"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_No4"].ToString(); //ds.Tables[0].Rows[0]["Vehicle_No4"].ToString();
                        vcl.Vehicle_Type = ds.Tables[0].Rows[0]["Vehicle_Type4"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_Type4"].ToString(); //ds.Tables[0].Rows[0]["Vehicle_Type4"].ToString();
                        vcl.Vehicle_Model = ds.Tables[0].Rows[0]["Vehicle_Model4"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_Model4"].ToString(); //ds.Tables[0].Rows[0]["Vehicle_Model4"].ToString();
                        Listvcl.Add(vcl);



                        vcl = new UsersVehicles();
                        vcl.Vehicle_No = ds.Tables[0].Rows[0]["Vehicle_No5"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_No5"].ToString(); //ds.Tables[0].Rows[0]["Vehicle_No5"].ToString();
                        vcl.Vehicle_Type = ds.Tables[0].Rows[0]["Vehicle_Type5"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_Type5"].ToString(); //ds.Tables[0].Rows[0]["Vehicle_Type5"].ToString();
                        vcl.Vehicle_Model = ds.Tables[0].Rows[0]["Vehicle_Model5"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["Vehicle_Model5"].ToString(); //ds.Tables[0].Rows[0]["Vehicle_Model5"].ToString();
                        Listvcl.Add(vcl);

                        opc.listVehicle = Listvcl;



                        //opc.Vehicle_Id = Convert.ToInt32(ds.Tables[0].Rows[0]["Vehicle_Id"]);




                        PC.Add(opc);
                    }

                }

                else
                {
                    errormessage = Convert.ToString(SqlParms[1].Value);
                    // return;
                    //registerSYSParkingAreaResult.Parking_id = 0; 
                }
            }
            return PC;

        }


        public CheckInCheckOutBEL UpdateCheckInCheckOut(string bookingId, DateTime CheckInCheckOutTime,string whodidcheckoutID, out string errormessage)
        {
            CheckInCheckOutBEL checkInCheckOutBEL = new CheckInCheckOutBEL();
            DataSet ds = null;
            errormessage = string.Empty;

            SqlParameter[] SqlParms = new SqlParameter[15];
            SqlParms[0] = new SqlParameter("@BookingId", SqlDbType.VarChar, 100);
            SqlParms[0].Direction = ParameterDirection.Input;
            SqlParms[0].Value = bookingId;

            SqlParms[1] = new SqlParameter("@CheckInCheckOutTime", SqlDbType.DateTime, 100);
            SqlParms[1].Direction = ParameterDirection.Input;
            SqlParms[1].Value = CheckInCheckOutTime;

            SqlParms[2] = new SqlParameter("@whodidcheckoutID", SqlDbType.VarChar, 100);
            SqlParms[2].Direction = ParameterDirection.Input;
            SqlParms[2].Value = whodidcheckoutID;

            SqlParms[3] = new SqlParameter("@Error", SqlDbType.VarChar, 100);
            SqlParms[3].Direction = ParameterDirection.Output;


            SqlParms[4] = new SqlParameter("@Status", SqlDbType.Bit);
            SqlParms[4].Direction = ParameterDirection.Output;
            //SqlParms[3].Value = flag;


            //SqlParms[4].Value = flag;

            SqlParms[5] = new SqlParameter("@TotalAmount", SqlDbType.VarChar, 100);
            SqlParms[5].Direction = ParameterDirection.Output;

            SqlParms[6] = new SqlParameter("@TotalDays", SqlDbType.VarChar, 100);
            SqlParms[6].Direction = ParameterDirection.Output;

            SqlParms[7] = new SqlParameter("@Totalhours", SqlDbType.VarChar, 100);
            SqlParms[7].Direction = ParameterDirection.Output;

            SqlParms[8] = new SqlParameter("@TotalMinutes", SqlDbType.VarChar, 100);
            SqlParms[8].Direction = ParameterDirection.Output;

            SqlParms[9] = new SqlParameter("@ExtraMinutes", SqlDbType.VarChar, 100);
            SqlParms[9].Direction = ParameterDirection.Output;

            SqlParms[10] = new SqlParameter("@ExtraHours", SqlDbType.VarChar, 100);
            SqlParms[10].Direction = ParameterDirection.Output;

            SqlParms[11] = new SqlParameter("@ExtraDays", SqlDbType.VarChar, 100);
            SqlParms[11].Direction = ParameterDirection.Output;

            SqlParms[12] = new SqlParameter("@ExtraMonth", SqlDbType.VarChar, 100);
            SqlParms[12].Direction = ParameterDirection.Output;


            SqlParms[13] = new SqlParameter("@ExtraAmountonCheckout", SqlDbType.VarChar, 100);
            SqlParms[13].Direction = ParameterDirection.Output;

            //SqlParms[13] = new SqlParameter("@Final_amount", SqlDbType.VarChar, 100);
            //SqlParms[13].Direction = ParameterDirection.Output;

            SqlParms[14] = new SqlParameter("@BaseAmount", SqlDbType.VarChar, 100);
            SqlParms[14].Direction = ParameterDirection.Output;


            ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "UpdateBookingCheckOut", SqlParms);

            if (ds != null)
            {
                if (Convert.ToString(SqlParms[4].Value) == "True" && Convert.ToString(SqlParms[3].Value) == "")
                {
                    checkInCheckOutBEL.Status = true;
                    checkInCheckOutBEL.Error = "Checked Out Successfully.";

                    foreach (DataRow drow in ds.Tables[0].Rows)
                    {

                        checkInCheckOutBEL.TotalAmount = (drow["TotalAmount"]) == DBNull.Value ? "" : Convert.ToString(drow["TotalAmount"]); //Convert.ToString(drow["TotalAmount"]);
                        checkInCheckOutBEL.Total_Days = (drow["Total_Days"]) == DBNull.Value ? "" : Convert.ToString(drow["Total_Days"]); //Convert.ToString(drow["Total_Days"]);
                        checkInCheckOutBEL.Total_Hour = (drow["Total_Hour"]) == DBNull.Value ? "" : Convert.ToString(drow["Total_Hour"]); //Convert.ToString(drow["Total_Hour"]);
                        checkInCheckOutBEL.Total_Min = (drow["Total_Min"]) == DBNull.Value ? "" : Convert.ToString(drow["Total_Min"]); //Convert.ToString(drow["Total_Min"]);
                        checkInCheckOutBEL.ExtraAmount = (drow["ExtraAmount"]) == DBNull.Value ? "" : Convert.ToString(drow["ExtraAmount"]); //Convert.ToString(drow["ExtraAmount"]);
                        checkInCheckOutBEL.Extra_Days = (drow["Extra_Days"]) == DBNull.Value ? "" : Convert.ToString(drow["Extra_Days"]); //Convert.ToString(drow["Extra_Days"]);
                        checkInCheckOutBEL.Extra_Hour = (drow["Extra_Hour"]) == DBNull.Value ? "" : Convert.ToString(drow["Extra_Hour"]); //Convert.ToString(drow["Extra_Hour"]);
                        checkInCheckOutBEL.Extra_Min = (drow["Extra_Min"]) == DBNull.Value ? "" : Convert.ToString(drow["Extra_Min"]); //Convert.ToString(drow["Extra_Min"]);
                        checkInCheckOutBEL.Extra_month = (drow["Extra_month"]) == DBNull.Value ? "" : Convert.ToString(drow["Extra_month"]); //Convert.ToString(drow["Extra_month"]);
                        //checkInCheckOutBEL.Final_amount = Convert.ToString(drow["Final_amount"]);
                        checkInCheckOutBEL.Base_Amount = (drow["Base_Amount"]) == DBNull.Value ? "" : Convert.ToString(drow["Base_Amount"]); //Convert.ToString(drow["Base_Amount"]);

                    }

                    //cancelbookingBEL.Success = true;
                    string checkedoutBy = string.Empty;
                    string UserType = string.Empty;
                    string vehicleno = string.Empty;
                    string parkname = string.Empty;
                   // DateTime checkout = DateTime.Compare;
                    Int64 AttendentNo = 0;
                    Int64 POphoneNo = 0;
                    Int64 VOphoneNo = 0;
                    Int64 SNphoneNo = 0;
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow drrr in ds.Tables[1].Rows)
                        {

                            AttendentNo = (drrr["AttendentPhone"]) == DBNull.Value ? 0 : Convert.ToInt64(drrr["AttendentPhone"]);
                            parkname = (drrr["ParkingName"]) == DBNull.Value ? "" : Convert.ToString(drrr["ParkingName"]);
                            vehicleno = (drrr["VehicleNumber"]) == DBNull.Value ? "" : Convert.ToString(drrr["VehicleNumber"]);
                            checkedoutBy = (drrr["CheckedoutBy"]) == DBNull.Value ? "" : Convert.ToString(drrr["CheckedoutBy"]);
                            UserType = (drrr["User_Type"]) == DBNull.Value ? "" : Convert.ToString(drrr["User_Type"]);
                            SNphoneNo = (drrr["Phone_No1"]) == DBNull.Value ? 0 : Convert.ToInt64(drrr["Phone_No1"]);
                            if (UserType == "VO")
                                VOphoneNo = Convert.ToInt64(drrr["phone_no"]);
                            else if (UserType == "PO")
                                POphoneNo = Convert.ToInt64(drrr["phone_no"]);
                        }


                        OTPBEL OTPBel = new OTPBEL();
                        var OTPList = OTPBel.RYSSendCheckoutMessage(Convert.ToInt64(VOphoneNo), Convert.ToInt64(POphoneNo), checkedoutBy, Convert.ToString(vehicleno), Convert.ToString(parkname), Convert.ToInt64(AttendentNo), Convert.ToInt64(SNphoneNo), Convert.ToInt64(SqlParms[0].Value));
                    }



                }
                else
                {
                    errormessage = Convert.ToString(SqlParms[2].Value);
                    // return;
                    //registerSYSParkingAreaResult.Parking_id = 0; 
                }
            }

            return checkInCheckOutBEL;
        }


        // Added by neeraj to cancel the booking

        public CancelbookingBEL Cancel(string BookedID, string CancellationReason, string whocancelledID)
        {
            DataSet ds = null;
            CancelbookingBEL cancelbookingBEL = new CancelbookingBEL();
            SqlParameter[] SqlParms = new SqlParameter[5];
            try
            {
                SqlParms[0] = new SqlParameter("@BookedID", SqlDbType.VarChar, 40);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = BookedID;

                SqlParms[1] = new SqlParameter("@CancellationReason", SqlDbType.VarChar, 4000);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = CancellationReason;

                SqlParms[2] = new SqlParameter("@whocancelledID", SqlDbType.VarChar, 4000);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = whocancelledID;

                SqlParms[3] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[3].Direction = ParameterDirection.Output;

                SqlParms[4] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
                SqlParms[4].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Cancel_Booking", SqlParms);

                if (ds != null)
                {
                    if (Convert.ToString(SqlParms[3].Value) == "" && string.IsNullOrWhiteSpace(Convert.ToString(SqlParms[3].Value)))
                    {
                        cancelbookingBEL.Success = true;
                        string cancelledBy = string.Empty;
                        string UserType = string.Empty;
                        string vehicleno = string.Empty;
                        string Canrsn = string.Empty;
                        Int64 AttendentNo = 0;
                        Int64 POphoneNo = 0;
                        Int64 VOphoneNo = 0;
                        Int64 SNphoneNo = 0;
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow drrr in ds.Tables[0].Rows)
                            {

                                AttendentNo = Convert.ToInt64(drrr["AttendentPhone"]);
                                Canrsn = Convert.ToString(drrr["CancellationReason"]);
                                vehicleno = Convert.ToString(drrr["VehicleNumber"]);
                                cancelledBy = Convert.ToString(drrr["CancelledBy"]);
                                UserType = Convert.ToString(drrr["User_Type"]);
                                SNphoneNo = Convert.ToInt64(drrr["Phone_No1"]);
                                if (UserType == "VO")
                                    VOphoneNo = Convert.ToInt64(drrr["Phone_No1"]);
                                else if (UserType == "PO")
                                    POphoneNo = Convert.ToInt64(drrr["Phone_No1"]);
                            }


                            OTPBEL OTPBel = new OTPBEL();
                            var OTPList = OTPBel.RYSSendCancellationMessage(Convert.ToInt64(VOphoneNo), Convert.ToInt64(POphoneNo), cancelledBy, Convert.ToString(vehicleno), Convert.ToString(Canrsn), Convert.ToInt64(AttendentNo), Convert.ToInt64(SNphoneNo), Convert.ToInt64(SqlParms[0].Value));
                        }

                        
                     }
                    else
                        cancelbookingBEL.ErrorMessage = Convert.ToString(SqlParms[3].Value);
                }
                return cancelbookingBEL;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
            }
            return cancelbookingBEL;
        }


        // Added by neeraj to support the booking

        public SupportbookingBEL Support(string BookedID, string SupportTitle, string Support_Description, string SupportRequiredID)
        {
            DataSet ds = null;
            SupportbookingBEL supportbookingBEL = new SupportbookingBEL();
            SqlParameter[] SqlParms = new SqlParameter[6];
            try
            {
                SqlParms[0] = new SqlParameter("@BookedID", SqlDbType.VarChar, 40);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = BookedID;

                SqlParms[1] = new SqlParameter("@SupportTitle", SqlDbType.VarChar, 4000);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = SupportTitle;

                SqlParms[2] = new SqlParameter("@Support_Description", SqlDbType.VarChar, 4000);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = Support_Description;

                SqlParms[3] = new SqlParameter("@SupportRequiredID", SqlDbType.VarChar, 4000);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = SupportRequiredID;

                SqlParms[4] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[4].Direction = ParameterDirection.Output;

                SqlParms[5] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
                SqlParms[5].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Booking_Support", SqlParms);

                if (ds != null)
                {
                    if (Convert.ToString(SqlParms[4].Value) == "" && string.IsNullOrWhiteSpace(Convert.ToString(SqlParms[4].Value)))
                    {
                        supportbookingBEL.Success = true;

                        foreach (DataRow drrr in ds.Tables[0].Rows)
                        {
                            //bookSYSParkingResult.Attendant_Name = Convert.ToString(drr["Attendant_Name"]);
                            //bookSYSParkingResult.Attendant_Phone_No = Convert.ToString(drr["Attendant_Phone_No"]);
                            Int64 phoneNo = Convert.ToInt64(drrr["Phone_No1"]);
                            OTPBEL OTPBel = new OTPBEL();
                            var OTPList = OTPBel.SendSupportConfirmationtoID(Convert.ToInt64(phoneNo), Convert.ToInt64(SqlParms[0].Value));

                        }

                        //foreach (DataRow drrr in ds.Tables[1].Rows)
                        //{
                        //    //bookSYSParkingResult.Attendant_Name = Convert.ToString(drr["Attendant_Name"]);
                        //    //bookSYSParkingResult.Attendant_Phone_No = Convert.ToString(drr["Attendant_Phone_No"]);
                        //    Int64 phoneNo = Convert.ToInt64(drrr["Phone_No1"]);
                        //    OTPBEL OTPBel = new OTPBEL();
                        //    var OTPList = OTPBel.SendCancelConfirmationtoSO(Convert.ToInt64(phoneNo), Convert.ToInt64(SqlParms[0].Value));

                        //}

                    }
                    else
                        supportbookingBEL.ErrorMessage = Convert.ToString(SqlParms[3].Value);
                }
                return supportbookingBEL;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
            }
            return supportbookingBEL;
        }

        //upload file

         public RegsiterParkingAreaResult RegisterParkingAreaUploadFile(RegisterParkingAreaBEL ParkingBEL)
        {
            RegsiterParkingAreaResult ParkingDetail = new RegsiterParkingAreaResult();
            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[16];

                SqlParms[0] = new SqlParameter("@owner_id", SqlDbType.Int);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = ParkingBEL.owner_id;

                SqlParms[1] = new SqlParameter("@Space_type", SqlDbType.VarChar, 50);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = ParkingBEL.Space_type;

                SqlParms[2] = new SqlParameter("@Property_type", SqlDbType.VarChar, 50);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = ParkingBEL.Property_type;

                SqlParms[3] = new SqlParameter("@No_of_space", SqlDbType.Int);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = ParkingBEL.No_of_space;

                SqlParms[4] = new SqlParameter("@VechileType", SqlDbType.VarChar, 50);
                SqlParms[4].Direction = ParameterDirection.Input;
                SqlParms[4].Value = ParkingBEL.VechileType;

                SqlParms[5] = new SqlParameter("@PropertyVerifiedStatus", SqlDbType.Char, 1);
                SqlParms[5].Direction = ParameterDirection.Input;
                SqlParms[5].Value = ParkingBEL.PropertyVerifiedStatus;

                SqlParms[6] = new SqlParameter("@FacilityGroupId", SqlDbType.VarChar, 5);
                SqlParms[6].Direction = ParameterDirection.Input;
                SqlParms[6].Value = ParkingBEL.FacilityGroupId;

                SqlParms[7] = new SqlParameter("@PropertyAddress", SqlDbType.VarChar);
                SqlParms[7].Direction = ParameterDirection.Input;
                SqlParms[7].Value = ParkingBEL.PropertyAddress;

                SqlParms[8] = new SqlParameter("@PropertyPinCode", SqlDbType.Int);
                SqlParms[8].Direction = ParameterDirection.Input;
                SqlParms[8].Value = ParkingBEL.PropertyPinCode;

                SqlParms[9] = new SqlParameter("@PropertyZone", SqlDbType.VarChar, 100);
                SqlParms[9].Direction = ParameterDirection.Input;
                SqlParms[9].Value = ParkingBEL.PropertyZone;

                SqlParms[10] = new SqlParameter("@PropertyLandMark", SqlDbType.VarChar, 100);
                SqlParms[10].Direction = ParameterDirection.Input;
                SqlParms[10].Value = ParkingBEL.PropertyLandMark;

                SqlParms[11] = new SqlParameter("@OwnerComments", SqlDbType.VarChar);
                SqlParms[11].Direction = ParameterDirection.Input;
                SqlParms[11].Value = ParkingBEL.OwnerComments;

                SqlParms[12] = new SqlParameter("@lattitude", SqlDbType.Decimal, 19);
                SqlParms[12].Direction = ParameterDirection.Input;
                SqlParms[12].Value = ParkingBEL.lattitude;

                SqlParms[13] = new SqlParameter("@longitude", SqlDbType.Decimal, 19);
                SqlParms[13].Direction = ParameterDirection.Input;
                SqlParms[13].Value = ParkingBEL.longitude;


             

                SqlParms[14] = new SqlParameter("@Error", SqlDbType.VarChar, 300);
                SqlParms[14].Direction = ParameterDirection.Output;

                SqlParms[15] = new SqlParameter("@Parking_id", SqlDbType.NVarChar, 150);
                SqlParms[15].Direction = ParameterDirection.Output;

               
                //** changes done


                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "Insert_RegParkingArea", SqlParms);

                if (Convert.ToString(SqlParms[15].Value) != "0" && Convert.ToString(SqlParms[14].Value) == "")
                {
                    ParkingDetail.Parking_Id = Convert.ToInt32(SqlParms[15].Value);
                    ParkingDetail.Success = true;
                    ParkingDetail.ErrorMessage = "";

                    return ParkingDetail;

                }
                else
                {
                    ParkingDetail.Parking_Id = 0;
                    ParkingDetail.Success = false;
                    ParkingDetail.ErrorMessage = Convert.ToString(SqlParms[14].Value);
                    return ParkingDetail;
                }


            }
            catch (Exception ex)
            {
                ParkingDetail.Parking_Id = 0;
                ParkingDetail.Success = false;
                ParkingDetail.ErrorMessage = ex.Message.ToString();
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
                return ParkingDetail;
            }

            //return ParkingDetail;
        }


         // to get public parking on App

         #region  Get available parking Public on APP


         public List<AvailablePublicParkingAreaResult> GetAvailableParkingPublicForApp(PublicParkingAreaBEL ParkingBEL)
         {
             List<AvailablePublicParkingAreaResult> AvailableParkings = new List<AvailablePublicParkingAreaResult>();
             try
             {

                 DataSet ds = null;

                 SqlParameter[] SqlParms = new SqlParameter[7];

                 SqlParms[0] = new SqlParameter("@Latitude", SqlDbType.Decimal, 10);
                 SqlParms[0].Direction = ParameterDirection.Input;
                 SqlParms[0].Value = ParkingBEL.Main_Latitude;

                 SqlParms[1] = new SqlParameter("@Longitude", SqlDbType.Decimal, 10);
                 SqlParms[1].Direction = ParameterDirection.Input;
                 SqlParms[1].Value = ParkingBEL.Main_Longitude;

                 SqlParms[2] = new SqlParameter("@distance", SqlDbType.Int);
                 SqlParms[2].Direction = ParameterDirection.Input;
                 SqlParms[2].Value = ParkingBEL.Distance;

                 //SqlParms[3] = new SqlParameter("@FromDateTime", SqlDbType.VarChar, 19);
                 //SqlParms[3].Direction = ParameterDirection.Input;
                 //SqlParms[3].Value = ParkingBEL.FromDateTime;

                 //SqlParms[4] = new SqlParameter("@ToDateTime", SqlDbType.VarChar, 19);
                 //SqlParms[4].Direction = ParameterDirection.Input;
                 //SqlParms[4].Value = ParkingBEL.ToDateTime;

                 SqlParms[3] = new SqlParameter("@ParkingClass", SqlDbType.VarChar, 19);
                 SqlParms[3].Direction = ParameterDirection.Input;
                 SqlParms[3].Value = ParkingBEL.ParkingClass;

                 SqlParms[4] = new SqlParameter("@OrderBy", SqlDbType.VarChar, 40);
                 SqlParms[4].Direction = ParameterDirection.Input;
                 SqlParms[4].Value = ParkingBEL.OrderBy;
                 //Popular Parking check
                 SqlParms[5] = new SqlParameter("@PopularParking", SqlDbType.Bit);
                 SqlParms[5].Direction = ParameterDirection.Input;
                 SqlParms[5].Value = ParkingBEL.PopularParking;

                 SqlParms[6] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                 SqlParms[6].Direction = ParameterDirection.Output;

                 //SqlParms[7] = new SqlParameter("@UserId", SqlDbType.NVarChar, 20);
                 //SqlParms[7].Direction = ParameterDirection.Output;



                 ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "SP_GetAvailableParkingPublicforApp", SqlParms);

                 if (ds != null)
                 {
                     if (ds.Tables[0].Rows.Count > 0 && Convert.ToString(SqlParms[5].Value) != "0")
                     {
                         AvailableParkings = (from DataRow parkingrow in ds.Tables[0].Rows
                                              select new AvailablePublicParkingAreaResult
                                              {

                                                  Parking_id = Convert.ToString(parkingrow["Parking_id"]),

                                                  lattitude = (parkingrow["lattitude"] == null ? "" : Convert.ToString(parkingrow["lattitude"])),

                                                  longitude = (parkingrow["longitude"] == null ? "" : Convert.ToString(parkingrow["longitude"])),

                                                  CarBasicCharge = (parkingrow["CarBasicCharge"] == System.DBNull.Value ? "" : Convert.ToString(parkingrow["CarBasicCharge"])),

                                                  BikeBasicCharge = (parkingrow["CarBasicCharge"] == System.DBNull.Value ? "" : Convert.ToString(parkingrow["CarBasicCharge"])),


                                                  Distance = (parkingrow["Distance"] == null ? "" : Convert.ToString(parkingrow["Distance"])),

                                                  //uptohour = (parkingrow["daycycle1uptoHours"] ==  System.DBNull.Value ? 0 : Convert.ToInt32(parkingrow["daycycle1uptoHours"])),

                                                  BookingAvailableFlag = (parkingrow["BookingAvailableFlag"] == null ? "" : Convert.ToString(parkingrow["BookingAvailableFlag"])),

                                                  //vehiclewheels = (parkingrow["vehiclewheels"] ==  System.DBNull.Value ? 0 : Convert.ToInt32(parkingrow["vehiclewheels"])),
                                                  
                                                  Success = true,
                                              }
                                         ).ToList<AvailablePublicParkingAreaResult>();

                         return AvailableParkings;

                     }
                 }
                 else
                 {
                     AvailableParkings.Add(new AvailablePublicParkingAreaResult() { Success = false, ErrorMessage = Convert.ToString(SqlParms[5].Value) });
                     return AvailableParkings;
                 }

             }
             catch (Exception ex)
             {
                 AvailableParkings.Add(new AvailablePublicParkingAreaResult() { Success = false, ErrorMessage = Convert.ToString(ex.Message.ToString()) });
                 ErrorLog.Log(this.GetType().Name, MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), HttpContext.Current.Session["userPhoneNo/Email"] != null ? HttpContext.Current.Session["userPhoneNo/Email"].ToString() : "");
                 return AvailableParkings;
             }

             return AvailableParkings;
         }

         #endregion


        // public parking bubble click
         public List<OnPublicParkingBubbleClick> Public_Parking_Click(string Parking_id, out string errormessage)
         {

             //List<OnProfileClick> PC = new List<OnProfileClick>();
             DataSet ds = null;
             errormessage = string.Empty;
             SqlParameter[] SqlParms = new SqlParameter[2];



             SqlParms[0] = new SqlParameter("@Parking_id", SqlDbType.VarChar, 4000);
             SqlParms[0].Direction = ParameterDirection.Input;
             SqlParms[0].Value = Parking_id;


             SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
             SqlParms[1].Direction = ParameterDirection.Output;

             //SqlParms[2] = new SqlParameter("@Status", SqlDbType.VarChar, 150);
             //SqlParms[2].Direction = ParameterDirection.Output;

             ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_IndividualPublicParkingDetail", SqlParms);
             List<OnPublicParkingBubbleClick> PBC = new List<OnPublicParkingBubbleClick>();
             if (ds != null)
             {
                 OnPublicParkingBubbleClick OPBC;
                 if ((SqlParms[1].Value == null || SqlParms[1].Value == "" || SqlParms[1].Value == "0"))
                 {

                     foreach (DataRow drow in ds.Tables[0].Rows)
                     {
                         OPBC = new OnPublicParkingBubbleClick();
                         //(drow["Booked_Id"]) == DBNull.Value ? 0 : Convert.ToInt64(drow["Booked_Id"]);

                         OPBC.lattitude = (drow["lattitude"]) == DBNull.Value ? "" : Convert.ToString(drow["lattitude"]);// Convert.ToString(drow["Parking_Space_Name"]);
                         OPBC.parking_id = (drow["parking_id"]) == DBNull.Value ? 0 : Convert.ToInt64(drow["parking_id"]); //Convert.ToInt64(drow["Parking_id"]);
                         OPBC.longitude = (drow["longitude"]) == DBNull.Value ? "" : Convert.ToString(drow["longitude"]);// Convert.ToString(drow["Parking_Owner_Comment"]);
                         OPBC.propertyaddress = (drow["propertyaddress"]) == DBNull.Value ? "" : Convert.ToString(drow["propertyaddress"]); //Convert.ToString(drow["parking_address"]);
                         OPBC.No_of_space = (drow["No_of_space"]) == DBNull.Value ? "" : Convert.ToString(drow["No_of_space"]); //Convert.ToString(drow["CarBasicCharge"]);
                         OPBC.No_of_space_bike = (drow["No_of_space_bike"]) == DBNull.Value ? "" : Convert.ToString(drow["No_of_space_bike"]); //Convert.ToString(drow["CarDailyCharge"]);
                         OPBC.Space_type = (drow["Space_type"]) == DBNull.Value ? "" : Convert.ToString(drow["Space_type"]);// Convert.ToString(drow["carmonthlycharge"]);
                         OPBC.Property_type = (drow["Property_type"]) == DBNull.Value ? "" : Convert.ToString(drow["Property_type"]); //Convert.ToString(drow["BikeBasicCharge"]);
                         OPBC.Description = (drow["OwnerComments"]) == DBNull.Value ? "" : Convert.ToString(drow["OwnerComments"]); //ds.Tables[0].Rows[0]["bikedailycharge"].ToString();
                         OPBC.BookingAvailableFlag = (drow["BookingAvailableFlag"]) == DBNull.Value ? "" : Convert.ToString(drow["BookingAvailableFlag"]); //ds.Tables[0].Rows[0]["bikemonthycharge"].ToString();
                         OPBC.CarBasicCharge = (drow["CarBasicCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["CarBasicCharge"]); //ds.Tables[0].Rows[0]["No_Of_Space_Avaiable"].ToString();
                         OPBC.BikeBasicCharge = (drow["BikeBasicCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["BikeBasicCharge"]); //ds.Tables[0].Rows[0]["No_Of_Space_Avaiable_bike"].ToString();
                         OPBC.BikeNightCharge = (drow["BikeNightCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["BikeNightCharge"]); //ds.Tables[0].Rows[0]["TimeFrom"].ToString();
                         OPBC.CarNightCharge = (drow["CarNightCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["CarNightCharge"]); //ds.Tables[0].Rows[0]["TimeTo"].ToString();
                         OPBC.BikeMonthlyDayCharge = (drow["BikeMonthlyDayCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["BikeMonthlyDayCharge"]); //ds.Tables[0].Rows[0]["DaysSelected"].ToString();
                         OPBC.CarMonthlyDayCharge = (drow["CarMonthlyDayCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["CarMonthlyDayCharge"]); //ds.Tables[0].Rows[0]["FacilitySelected"].ToString();
                         OPBC.BikeMonthlyNightCharge = (drow["BikeMonthlyNightCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["BikeMonthlyNightCharge"]); //ds.Tables[0].Rows[0]["DescriptionSelected"].ToString();
                         OPBC.CarMonthlyNightCharge = (drow["CarMonthlyNightCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["CarMonthlyNightCharge"]);
                         OPBC.BikeMonthlyCharge = (drow["BikeMonthlyCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["BikeMonthlyCharge"]);
                         OPBC.CarMonthlyCharge = (drow["CarMonthlyCharge"]) == DBNull.Value ? "" : Convert.ToString(drow["CarMonthlyCharge"]);
                        // OPBC.CarMonthlyNightCharge = (drow["Parking_Available_Date"]) == DBNull.Value ? "" : Convert.ToString(drow["Parking_Available_Date"]);


                         PBC.Add(OPBC);
                     }

                 }

                 else
                 {
                     errormessage = Convert.ToString(SqlParms[1].Value);
                     // return;
                     //registerSYSParkingAreaResult.Parking_id = 0; 
                 }
             }
             return PBC;

         }

    }
}


