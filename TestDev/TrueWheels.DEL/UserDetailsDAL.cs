
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TrueWheels.BEL;

namespace TrueWheels.DAL
{
    public class UserDetailsDAL
    {
        string connectionString = ConfigurationManager.ConnectionStrings["conStr"].ConnectionString;
        Transaction transaction = new Transaction();

        public UserDetailsBEL FunAuthenticateUser(UserDetailsBEL Ubal)
        {
            UserDetailsBEL Userdetail = new UserDetailsBEL();
            //SqlConnection conn = new SqlConnection();
            DataSet ds = null;
            try
            {
                SqlParameter[] SqlParms = new SqlParameter[6];

                SqlParms[0] = new SqlParameter("@User_Id", SqlDbType.NVarChar, 50);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = Ubal.User_ID;

                SqlParms[1] = new SqlParameter("@Phone_No1", SqlDbType.NVarChar, 50);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = Ubal.Phone_No1;


                SqlParms[2] = new SqlParameter("@Email_Id", SqlDbType.NVarChar, 50);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = Ubal.Email_Id;


                SqlParms[3] = new SqlParameter("@Password", SqlDbType.NVarChar, 500);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = Ubal.Password;



                SqlParms[4] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[4].Direction = ParameterDirection.Output;

                SqlParms[5] = new SqlParameter("@UserId", SqlDbType.NVarChar, 20);
                SqlParms[5].Direction = ParameterDirection.Output;



                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "SP_LoginUser", SqlParms);
                //&& Convert.ToString(SqlParms[5].Value) != "0"
                if (Convert.ToString(SqlParms[4].Value) == string.Empty)
                {
                    Userdetail.User_ID = ds.Tables[0].Rows[0]["User_ID"].ToString();
                    // Userdetail.Alternate_Email_Id = ds.Tables[0].Rows[0]["Alternate_Email_Id"].ToString();
                    Userdetail.Email_Id = ds.Tables[0].Rows[0]["Email_Id"].ToString();
                    // Userdetail.First_Name = ds.Tables[0].Rows[0]["First_Name"].ToString();
                    Userdetail.Last_Login = ds.Tables[0].Rows[0]["Last_Login"].ToString();
                    // Userdetail.Last_Name = ds.Tables[0].Rows[0]["Last_Name"].ToString();
                    //Userdetail.Owner_Address = ds.Tables[0].Rows[0]["Owner_Address"].ToString();
                    Userdetail.Phone_No1 = ds.Tables[0].Rows[0]["Phone_No1"].ToString();
                    //  Userdetail.Phone_No2 = ds.Tables[0].Rows[0]["Phone_No2"].ToString();
                    // Userdetail.SignUp_Mode_ID = ds.Tables[0].Rows[0]["SignUp_Mode_ID"].ToString();
                    Userdetail.User_Name = ds.Tables[0].Rows[0]["User_Name"].ToString();
                    Userdetail.ErrorMessage = string.Empty;

                    // ResultDTO. = Convert.ToString(SqlParms[5].Value);
                }
                else
                {
                    Userdetail.User_Name = "";
                    Userdetail.User_ID = "0";
                    Userdetail.ErrorMessage = Convert.ToString(SqlParms[4].Value);
                }

            }
            catch (Exception ex)
            {
                Userdetail.User_Name = "";
                Userdetail.User_ID = "0";
                Userdetail.ErrorMessage = ex.Message.ToString();

            }
            return Userdetail;

        }

        #region User Registration
        public UserDetailsBEL AddUserDetails(UserDetailsBEL userDetailsBEL)
        {
            UserDetailsBEL Userdetail = new UserDetailsBEL();
            DataSet ds = null;
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[9];

                SqlParms[0] = new SqlParameter("@First_Name", SqlDbType.NVarChar, 50);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = userDetailsBEL.First_Name;

                SqlParms[1] = new SqlParameter("@Last_Name", SqlDbType.NVarChar, 50);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = userDetailsBEL.Last_Name;

                SqlParms[2] = new SqlParameter("@Password", SqlDbType.NVarChar, 500);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = userDetailsBEL.Password;

                SqlParms[3] = new SqlParameter("@Phone_No1", SqlDbType.NVarChar, 50);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = userDetailsBEL.Phone_No1;

                SqlParms[4] = new SqlParameter("@Email_Id", SqlDbType.NVarChar, 50);
                SqlParms[4].Direction = ParameterDirection.Input;
                SqlParms[4].Value = userDetailsBEL.Email_Id;

                SqlParms[5] = new SqlParameter("@SignUp_Mode_ID", SqlDbType.NVarChar, 2);
                SqlParms[5].Direction = ParameterDirection.Input;
                SqlParms[5].Value = userDetailsBEL.SignUp_Mode_ID;

                SqlParms[6] = new SqlParameter("@Other_FB_GG_ID", SqlDbType.VarChar, 500);
                SqlParms[6].Direction = ParameterDirection.Input;
                if (userDetailsBEL.SignUp_Mode_ID == "FB")
                {
                    if (userDetailsBEL.Other_ID != string.Empty)
                        SqlParms[6].Value = userDetailsBEL.Other_ID;
                    //SqlParms[6].Value = Convert.ToUInt64(userDetailsBEL.Other_ID);
                    else
                        SqlParms[6].Value = 0;
                }
                else if (userDetailsBEL.SignUp_Mode_ID == "GG")
                {
                    if (userDetailsBEL.Other_ID != string.Empty)
                        SqlParms[6].Value = userDetailsBEL.Other_ID;
                    else
                        SqlParms[6].Value = 0;
                }

                SqlParms[7] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[7].Direction = ParameterDirection.Output;

                SqlParms[8] = new SqlParameter("@UserId", SqlDbType.NVarChar, 20);
                SqlParms[8].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Insert_UserLogin", SqlParms);
                //&& Convert.ToString(SqlParms[5].Value) != "0"
                if (ds == null && ds.Tables.Count <= 0)
                {
                    Userdetail.User_Name = "";
                    Userdetail.User_ID = "0";
                    Userdetail.ErrorMessage = Convert.ToString(SqlParms[7].Value) + " Data Not Found";
                }
                else if (Convert.ToString(SqlParms[7].Value) == string.Empty)
                {
                    Userdetail.User_ID = ds.Tables[0].Rows[0]["User_ID"].ToString();
                    Userdetail.Email_Id = ds.Tables[0].Rows[0]["Email_Id"].ToString();
                    Userdetail.Last_Login = ds.Tables[0].Rows[0]["Last_Login"].ToString();
                    Userdetail.Phone_No1 = ds.Tables[0].Rows[0]["Phone_No1"].ToString();
                    Userdetail.User_Name = ds.Tables[0].Rows[0]["User_Name"].ToString();
                    Userdetail.ErrorMessage = string.Empty;

                    // ResultDTO. = Convert.ToString(SqlParms[5].Value);
                }
                else
                {
                    Userdetail.User_Name = "";
                    Userdetail.User_ID = "0";
                    Userdetail.ErrorMessage = Convert.ToString(SqlParms[7].Value);
                }

            }
            catch (Exception ex)
            {
                Userdetail.User_Name = "";
                Userdetail.User_ID = "0";
                Userdetail.ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }

            return Userdetail;
        }
        #endregion

        public Transaction UpdateCred(UserDetailsBEL Ubal)
        {
            Transaction ResultDTO = new Transaction();
            //SqlConnection conn = new SqlConnection();
            try
            {
                SqlParameter[] SqlParms = new SqlParameter[4];



                SqlParms[0] = new SqlParameter("@Phone_No1", SqlDbType.VarChar, 50);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = Ubal.Phone_No1;

                SqlParms[1] = new SqlParameter("@Password", SqlDbType.VarChar, 500);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = Ubal.Password;


                SqlParms[2] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[2].Direction = ParameterDirection.Output;

                SqlParms[3] = new SqlParameter("@UserId", SqlDbType.VarChar, 20);
                SqlParms[3].Direction = ParameterDirection.Output;



                SqlHelper.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, "Upate_Password", SqlParms);

                if (Convert.ToString(SqlParms[2].Value) == string.Empty && Convert.ToString(SqlParms[3].Value) != "0")
                {
                    ResultDTO.Success = true;
                    ResultDTO.Message = "Valid User";
                    ResultDTO.TransactionId = Convert.ToString(SqlParms[3].Value);
                }
                else
                {
                    ResultDTO.Success = false;
                    ResultDTO.Message = "Failed";
                    ResultDTO.ErrorMessage = Convert.ToString(SqlParms[2].Value);
                }

            }
            catch (Exception ex)
            {
                ResultDTO.Success = false;
                ResultDTO.Message = "DataBase Error Occured";
                ResultDTO.ErrorMessage = ex.Message.ToString();
            }
            return ResultDTO;

        }

        #region  RegistraParkingAreaBEL
        public List<DashBoardBEL> GetMenuMapping(string User_id)
        {
            List<DashBoardBEL> UserMenus = new List<DashBoardBEL>();
            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[2];

                SqlParms[0] = new SqlParameter("@User_id", SqlDbType.NVarChar);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = User_id;


                SqlParms[1] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;

                //SqlParms[7] = new SqlParameter("@UserId", SqlDbType.NVarChar, 20);
                //SqlParms[7].Direction = ParameterDirection.Output;



                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_MenuList", SqlParms);

                if (ds != null && ds.Tables[0].Rows.Count > 0 && Convert.ToString(SqlParms[1].Value) != "0")
                {
                    UserMenus = (from DataRow parkingrow in ds.Tables[0].Rows
                                 select new DashBoardBEL
                                 {
                                     User_id = Convert.ToString(parkingrow["User_id"]),
                                     Menu_id = Convert.ToString(parkingrow["Menu_id"]),
                                     Menu_description = Convert.ToString(parkingrow["Menu_description"]),
                                     Menu_Url = Convert.ToString(parkingrow["Menu_Url"]),

                                     Success = true,
                                     ErrorMessage = "",
                                 }
                                    ).ToList<DashBoardBEL>();

                    return UserMenus;

                }
                else
                {
                    UserMenus.Add(new DashBoardBEL() { Success = false, ErrorMessage = Convert.ToString(SqlParms[1].Value) });
                    return UserMenus;
                }

            }
            catch (Exception ex)
            {
                UserMenus.Add(new DashBoardBEL() { Success = false, ErrorMessage = Convert.ToString(ex.Message.ToString()) });
                return UserMenus;
            }

            //  return AvailableParkings;
        }
        #endregion

        //public UserDetailsBEL CheckPhoneNoExists(string Phone_No)
        //{
        //    UserDetailsBEL Userdetail = new UserDetailsBEL();
        //    //SqlConnection conn = new SqlConnection();
        //    DataSet ds = null;
        //    try
        //    {
        //        SqlParameter[] SqlParms = new SqlParameter[2];

        //        SqlParms[0] = new SqlParameter("@Phone_No", SqlDbType.VarChar, 10);
        //        SqlParms[0].Direction = ParameterDirection.Input;
        //        SqlParms[0].Value = Phone_No;

        //        SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
        //        SqlParms[1].Direction = ParameterDirection.Output;

        //        ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "TW_CheckPhoneNo", SqlParms);
        //        //&& Convert.ToString(SqlParms[5].Value) != "0"
        //        if (Convert.ToString(SqlParms[1].Value) == string.Empty)
        //        {
        //            Userdetail.User_ID = ds.Tables[0].Rows[0]["User_ID"].ToString();
        //            Userdetail.Alternate_Email_Id = ds.Tables[0].Rows[0]["Alternate_Email_Id"].ToString();
        //            Userdetail.Email_Id = ds.Tables[0].Rows[0]["Email_Id"].ToString();
        //            Userdetail.First_Name = ds.Tables[0].Rows[0]["First_Name"].ToString();
        //            Userdetail.Last_Login = ds.Tables[0].Rows[0]["Last_Login"].ToString();
        //            Userdetail.Last_Name = ds.Tables[0].Rows[0]["Last_Name"].ToString();
        //            //Userdetail.Owner_Address = ds.Tables[0].Rows[0]["Owner_Address"].ToString();
        //            Userdetail.Phone_No1 = ds.Tables[0].Rows[0]["Phone_No1"].ToString();
        //            Userdetail.Phone_No2 = ds.Tables[0].Rows[0]["Phone_No2"].ToString();
        //            // Userdetail.SignUp_Mode_ID = ds.Tables[0].Rows[0]["SignUp_Mode_ID"].ToString();
        //            //Userdetail.User_Name = ds.Tables[0].Rows[0]["User_Name"].ToString();
        //            Userdetail.ErrorMessage = string.Empty;
        //            Userdetail.User_Name = ds.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds.Tables[0].Rows[0]["Last_Name"].ToString(); ;
        //            // Userdetail.Last_Login = 

        //            // ResultDTO. = Convert.ToString(SqlParms[5].Value);
        //        }
        //        else
        //        {
        //            Userdetail.User_Name = "";
        //            Userdetail.User_ID = "0";
        //            Userdetail.ErrorMessage = Convert.ToString(SqlParms[4].Value);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Userdetail.User_Name = "";
        //        Userdetail.User_ID = "0";
        //        Userdetail.ErrorMessage = ex.Message.ToString();

        //    }
        //    return Userdetail;

        //}


        #region  UserDashboard
        public PersonalinfoBEL GetProfileInfo(string User_id)
        {
            PersonalinfoBEL personelInfo = new PersonalinfoBEL();

            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[2];

                SqlParms[0] = new SqlParameter("@User_id", SqlDbType.NVarChar);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = User_id;


                SqlParms[1] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;

                //SqlParms[7] = new SqlParameter("@UserId", SqlDbType.NVarChar, 20);
                //SqlParms[7].Direction = ParameterDirection.Output;

                //jfhjff

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_ProfileInfo", SqlParms);

                if (Convert.ToString(SqlParms[1].Value) == string.Empty)
                {
                    personelInfo.userId = Convert.ToInt32(ds.Tables[0].Rows[0]["User_id"]);
                    personelInfo.firstName = ds.Tables[0].Rows[0]["First_Name"].ToString();
                    personelInfo.lastName = ds.Tables[0].Rows[0]["Last_Name"].ToString();
                    if (ds.Tables[0].Rows[0]["DOB"] != DBNull.Value)
                    {
                        personelInfo.dob = Convert.ToDateTime(ds.Tables[0].Rows[0]["DOB"]);
                    }

                    personelInfo.emailID = ds.Tables[0].Rows[0]["Email_Id"].ToString();
                    personelInfo.alternateEmailId = ds.Tables[0].Rows[0]["Alternate_Email_Id"].ToString();
                    personelInfo.mobileNo = ds.Tables[0].Rows[0]["Phone_No1"].ToString();
                    personelInfo.alternateNo = ds.Tables[0].Rows[0]["Phone_No2"].ToString();
                    personelInfo.drivingLicense = ds.Tables[0].Rows[0]["Driving_License"].ToString();
                    personelInfo.panNo = ds.Tables[0].Rows[0]["PAN"].ToString();
                    personelInfo.ownerAddress = ds.Tables[0].Rows[0]["Owner_Address"].ToString();
                    personelInfo.ownerVerificationStatus = Convert.ToChar(ds.Tables[0].Rows[0]["OwnerVerificationStatus"]);
                    personelInfo.IsMobileVerified = ds.Tables[0].Rows[0]["IsMobileVarified"].ToString();
                    personelInfo.SignUp_Mode_ID = ds.Tables[0].Rows[0]["SignUp_Mode_ID"].ToString();
                    personelInfo.lastLogin = ds.Tables[0].Rows[0]["Last_Login"].ToString();
                    // personelInfo.Password = Secure.Decrypt(ds.Tables[0].Rows[0]["Password"].ToString());
                    personelInfo.profilePicPath = ds.Tables[0].Rows[0]["proilePicName"].ToString();
                    return personelInfo;

                }
                else
                {
                    personelInfo.success = false;
                    personelInfo.errorMessage = Convert.ToString(SqlParms[1].Value);
                    return personelInfo;
                }

            }
            catch (Exception ex)
            {
                personelInfo.success = false;
                personelInfo.errorMessage = Convert.ToString(ex.Message.ToString());
                return personelInfo;
            }

            //  return AvailableParkings;
        }


        public PersonalinfoBEL UpdateProfileInfo(PersonalinfoBEL personelInfo)
        {
            DataSet ds = null;
            PersonalinfoBEL personelInfoUpdated = new PersonalinfoBEL();
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[11];

                SqlParms[0] = new SqlParameter("@User_id", SqlDbType.Int);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = personelInfo.userId;

                SqlParms[1] = new SqlParameter("@First_Name", SqlDbType.NVarChar, 50);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = personelInfo.firstName;

                SqlParms[2] = new SqlParameter("@Last_Name", SqlDbType.NVarChar, 50);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = personelInfo.lastName;

                SqlParms[3] = new SqlParameter("@Email_id", SqlDbType.NVarChar, 500);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = personelInfo.emailID;

                SqlParms[4] = new SqlParameter("@Alternate_Email_id", SqlDbType.NVarChar, 50);
                SqlParms[4].Direction = ParameterDirection.Input;
                SqlParms[4].Value = personelInfo.alternateEmailId;

                SqlParms[5] = new SqlParameter("@DOB", SqlDbType.DateTime);
                SqlParms[5].Direction = ParameterDirection.Input;
                SqlParms[5].Value = personelInfo.dob;

                SqlParms[6] = new SqlParameter("@Phone_No2", SqlDbType.NVarChar, 50);
                SqlParms[6].Direction = ParameterDirection.Input;
                SqlParms[6].Value = personelInfo.alternateNo;

                SqlParms[7] = new SqlParameter("@Driving_License", SqlDbType.NVarChar, 50);
                SqlParms[7].Direction = ParameterDirection.Input;
                SqlParms[7].Value = personelInfo.drivingLicense;

                SqlParms[8] = new SqlParameter("@PAN", SqlDbType.NVarChar, 50);
                SqlParms[8].Direction = ParameterDirection.Input;
                SqlParms[8].Value = personelInfo.panNo;
                if (personelInfo.newPassword != null)
                {
                    SqlParms[9] = new SqlParameter("@Password", SqlDbType.NVarChar, 50);
                    SqlParms[9].Direction = ParameterDirection.Input;
                    SqlParms[9].Value = Secure.Encrypt(personelInfo.newPassword);
                }

                SqlParms[10] = new SqlParameter("@Error", SqlDbType.NVarChar, 50);
                SqlParms[10].Direction = ParameterDirection.Output;



                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Update_ProfileInfo", SqlParms);

                if (Convert.ToString(SqlParms[10].Value) == string.Empty)
                {
                    personelInfoUpdated.userId = Convert.ToInt32(ds.Tables[0].Rows[0]["User_id"]);
                    personelInfoUpdated.firstName = ds.Tables[0].Rows[0]["First_Name"].ToString();
                    personelInfoUpdated.lastName = ds.Tables[0].Rows[0]["Last_Name"].ToString();
                    if (ds.Tables[0].Rows[0]["DOB"] != DBNull.Value)
                    {
                        personelInfoUpdated.dob = Convert.ToDateTime(ds.Tables[0].Rows[0]["DOB"]);
                    }

                    personelInfoUpdated.emailID = ds.Tables[0].Rows[0]["Email_Id"].ToString();
                    personelInfoUpdated.alternateEmailId = ds.Tables[0].Rows[0]["Alternate_Email_Id"].ToString();
                    personelInfoUpdated.mobileNo = ds.Tables[0].Rows[0]["Phone_No1"].ToString();
                    personelInfoUpdated.alternateNo = ds.Tables[0].Rows[0]["Phone_No2"].ToString();
                    personelInfoUpdated.drivingLicense = ds.Tables[0].Rows[0]["Driving_License"].ToString();
                    personelInfoUpdated.panNo = ds.Tables[0].Rows[0]["PAN"].ToString();
                    personelInfoUpdated.ownerAddress = ds.Tables[0].Rows[0]["Owner_Address"].ToString();
                    personelInfoUpdated.ownerVerificationStatus = Convert.ToChar(ds.Tables[0].Rows[0]["OwnerVerificationStatus"]);

                    personelInfoUpdated.lastLogin = ds.Tables[0].Rows[0]["Last_Login"].ToString();
                    personelInfoUpdated.password = Secure.Encrypt(ds.Tables[0].Rows[0]["Password"].ToString());
                    personelInfoUpdated.profilePicPath = ds.Tables[0].Rows[0]["proilePicName"].ToString();
                    return personelInfoUpdated;

                }
                else
                {
                    personelInfo.success = false;
                    personelInfo.errorMessage = Convert.ToString(SqlParms[10].Value);
                    return personelInfo;
                }

            }
            catch (Exception ex)
            {
                personelInfo.success = false;
                personelInfo.errorMessage = Convert.ToString(ex.Message.ToString());
                return personelInfo;
            }

            //  return AvailableParkings;
        }

       //add by neeraj for App Update Profile

        public PersonalinfoAppBEL UpdateProfileInfoApp(PersonalinfoAppBEL personelInfoapp)
        {
            DataSet ds = null;
            PersonalinfoAppBEL personelInfoUpdated = new PersonalinfoAppBEL();
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[24];

                SqlParms[0] = new SqlParameter("@User_id", SqlDbType.Int);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = personelInfoapp.userId;

                SqlParms[1] = new SqlParameter("@Full_Name", SqlDbType.NVarChar, 50);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = personelInfoapp.Full_Name;

                //SqlParms[2] = new SqlParameter("@Last_Name", SqlDbType.NVarChar, 50);
                //SqlParms[2].Direction = ParameterDirection.Input;
                //SqlParms[2].Value = personelInfoapp.lastName;

                SqlParms[2] = new SqlParameter("@Email_id", SqlDbType.NVarChar, 500);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = personelInfoapp.emailID;

                //SqlParms[4] = new SqlParameter("@Alternate_Email_id", SqlDbType.NVarChar, 50);
                //SqlParms[4].Direction = ParameterDirection.Input;
                //SqlParms[4].Value = personelInfoapp.alternateEmailId;

                SqlParms[3] = new SqlParameter("@DOB", SqlDbType.DateTime);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = personelInfoapp.dob;

                SqlParms[4] = new SqlParameter("@Phone_No2", SqlDbType.NVarChar, 50);
                SqlParms[4].Direction = ParameterDirection.Input;
                SqlParms[4].Value = personelInfoapp.alternateNo;

                SqlParms[5] = new SqlParameter("@Phone_No1", SqlDbType.NVarChar, 50);
                SqlParms[5].Direction = ParameterDirection.Input;
                SqlParms[5].Value = personelInfoapp.Phone_No1;

                SqlParms[6] = new SqlParameter("@Driving_License", SqlDbType.NVarChar, 50);
                SqlParms[6].Direction = ParameterDirection.Input;
                SqlParms[6].Value = personelInfoapp.drivingLicense;

                SqlParms[7] = new SqlParameter("@PAN", SqlDbType.NVarChar, 50);
                SqlParms[7].Direction = ParameterDirection.Input;
                SqlParms[7].Value = personelInfoapp.panNo;

                SqlParms[8] = new SqlParameter("@Vehicle_No1", SqlDbType.NVarChar, 50);
                SqlParms[8].Direction = ParameterDirection.Input;
                SqlParms[8].Value = personelInfoapp.Vehicle_No1;

                SqlParms[9] = new SqlParameter("@Vehicle_No2", SqlDbType.NVarChar, 50);
                SqlParms[9].Direction = ParameterDirection.Input;
                SqlParms[9].Value = personelInfoapp.Vehicle_No2;

                SqlParms[10] = new SqlParameter("@Vehicle_No3", SqlDbType.NVarChar, 50);
                SqlParms[10].Direction = ParameterDirection.Input;
                SqlParms[10].Value = personelInfoapp.Vehicle_No3;

                SqlParms[11] = new SqlParameter("@Vehicle_No4", SqlDbType.NVarChar, 50);
                SqlParms[11].Direction = ParameterDirection.Input;
                SqlParms[11].Value = personelInfoapp.Vehicle_No4;

                SqlParms[12] = new SqlParameter("@Vehicle_No5", SqlDbType.NVarChar, 50);
                SqlParms[12].Direction = ParameterDirection.Input;
                SqlParms[12].Value = personelInfoapp.Vehicle_No5;

                SqlParms[13] = new SqlParameter("@Vehicle_Model1", SqlDbType.NVarChar, 50);
                SqlParms[13].Direction = ParameterDirection.Input;
                SqlParms[13].Value = personelInfoapp.Vehicle_Model1;

                SqlParms[14] = new SqlParameter("@Vehicle_Model2", SqlDbType.NVarChar, 50);
                SqlParms[14].Direction = ParameterDirection.Input;
                SqlParms[14].Value = personelInfoapp.Vehicle_Model2;

                SqlParms[15] = new SqlParameter("@Vehicle_Model3", SqlDbType.NVarChar, 50);
                SqlParms[15].Direction = ParameterDirection.Input;
                SqlParms[15].Value = personelInfoapp.Vehicle_Model3;

                SqlParms[16] = new SqlParameter("@Vehicle_Model4", SqlDbType.NVarChar, 50);
                SqlParms[16].Direction = ParameterDirection.Input;
                SqlParms[16].Value = personelInfoapp.Vehicle_Model4;

                SqlParms[17] = new SqlParameter("@Vehicle_Model5", SqlDbType.NVarChar, 50);
                SqlParms[17].Direction = ParameterDirection.Input;
                SqlParms[17].Value = personelInfoapp.Vehicle_Model5;

                SqlParms[18] = new SqlParameter("@Vehicle_Type1", SqlDbType.NVarChar, 50);
                SqlParms[18].Direction = ParameterDirection.Input;
                SqlParms[18].Value = personelInfoapp.Vehicle_Type1;

                SqlParms[19] = new SqlParameter("@Vehicle_Type2", SqlDbType.NVarChar, 50);
                SqlParms[19].Direction = ParameterDirection.Input;
                SqlParms[19].Value = personelInfoapp.Vehicle_Type2;

                SqlParms[20] = new SqlParameter("@Vehicle_Type3", SqlDbType.NVarChar, 50);
                SqlParms[20].Direction = ParameterDirection.Input;
                SqlParms[20].Value = personelInfoapp.Vehicle_Type3;

                SqlParms[21] = new SqlParameter("@Vehicle_Type4", SqlDbType.NVarChar, 50);
                SqlParms[21].Direction = ParameterDirection.Input;
                SqlParms[21].Value = personelInfoapp.Vehicle_Type4;

                SqlParms[22] = new SqlParameter("@Vehicle_Type5", SqlDbType.NVarChar, 50);
                SqlParms[22].Direction = ParameterDirection.Input;
                SqlParms[22].Value = personelInfoapp.Vehicle_Type5;


                //if (personelInfoapp.newPassword != null)
                //{
                //    SqlParms[9] = new SqlParameter("@Password", SqlDbType.NVarChar, 50);
                //    SqlParms[9].Direction = ParameterDirection.Input;
                //    SqlParms[9].Value = Secure.Encrypt(personelInfoapp.newPassword);
                //}

                SqlParms[23] = new SqlParameter("@Error", SqlDbType.NVarChar, 50);
                SqlParms[23].Direction = ParameterDirection.Output;



                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Update_ProfileInfoApp", SqlParms);

                if (Convert.ToString(SqlParms[23].Value) == string.Empty)
                {
                    personelInfoUpdated.userId = Convert.ToInt32(ds.Tables[0].Rows[0]["User_id"]);
                    personelInfoUpdated.Full_Name = ds.Tables[0].Rows[0]["Full_Name"].ToString();
                    //personelInfoUpdated.lastName = ds.Tables[0].Rows[0]["Last_Name"].ToString();
                    if (ds.Tables[0].Rows[0]["DOB"] != DBNull.Value)
                    {
                        personelInfoUpdated.dob = Convert.ToDateTime(ds.Tables[0].Rows[0]["DOB"]);
                    }

                    personelInfoUpdated.emailID = ds.Tables[0].Rows[0]["Email_Id"].ToString();
                    //personelInfoUpdated.alternateEmailId = ds.Tables[0].Rows[0]["Alternate_Email_Id"].ToString();
                    personelInfoUpdated.mobileNo = ds.Tables[0].Rows[0]["Phone_No1"].ToString();
                    personelInfoUpdated.alternateNo = ds.Tables[0].Rows[0]["Phone_No2"].ToString();
                    personelInfoUpdated.drivingLicense = ds.Tables[0].Rows[0]["Driving_License"].ToString();
                    personelInfoUpdated.panNo = ds.Tables[0].Rows[0]["PAN"].ToString();
                    personelInfoUpdated.ownerAddress = ds.Tables[0].Rows[0]["Owner_Address"].ToString();
                    personelInfoUpdated.ownerVerificationStatus = Convert.ToChar(ds.Tables[0].Rows[0]["OwnerVerificationStatus"]);

                    personelInfoUpdated.lastLogin = ds.Tables[0].Rows[0]["Last_Login"].ToString();
                    personelInfoUpdated.password = Secure.Encrypt(ds.Tables[0].Rows[0]["Password"].ToString());
                    personelInfoUpdated.Vehicle_No1 = ds.Tables[0].Rows[0]["Vehicle_No1"].ToString();
                    personelInfoUpdated.Vehicle_Type1 = ds.Tables[0].Rows[0]["Vehicle_Type1"].ToString();
                    personelInfoUpdated.Vehicle_Model1 = ds.Tables[0].Rows[0]["Vehicle_Model1"].ToString();
                    personelInfoUpdated.Vehicle_No2 = ds.Tables[0].Rows[0]["Vehicle_No2"].ToString();
                    personelInfoUpdated.Vehicle_Type2 = ds.Tables[0].Rows[0]["Vehicle_Type2"].ToString();
                    personelInfoUpdated.Vehicle_Model2 = ds.Tables[0].Rows[0]["Vehicle_Model2"].ToString();
                    personelInfoUpdated.Vehicle_No3 = ds.Tables[0].Rows[0]["Vehicle_No3"].ToString();
                    personelInfoUpdated.Vehicle_Type3 = ds.Tables[0].Rows[0]["Vehicle_Type3"].ToString();
                    personelInfoUpdated.Vehicle_Model3 = ds.Tables[0].Rows[0]["Vehicle_Model3"].ToString();
                    personelInfoUpdated.Vehicle_No4 = ds.Tables[0].Rows[0]["Vehicle_No4"].ToString();
                    personelInfoUpdated.Vehicle_Type4 = ds.Tables[0].Rows[0]["Vehicle_Type4"].ToString();
                    personelInfoUpdated.Vehicle_Model4 = ds.Tables[0].Rows[0]["Vehicle_Model4"].ToString();
                    personelInfoUpdated.Vehicle_No5 = ds.Tables[0].Rows[0]["Vehicle_No5"].ToString();
                    personelInfoUpdated.Vehicle_Type5 = ds.Tables[0].Rows[0]["Vehicle_Type5"].ToString();
                    personelInfoUpdated.Vehicle_Model5 = ds.Tables[0].Rows[0]["Vehicle_Model5"].ToString();
                    personelInfoUpdated.Vehicle_Id = Convert.ToInt32(ds.Tables[0].Rows[0]["Vehicle_Id"]);
                    //personelInfoUpdated.Vehicle_Type1 = ds.Tables[0].Rows[0]["proilePicName"].ToString();

                    return personelInfoUpdated;

                }
                else
                {
                    personelInfoapp.success = false;
                    personelInfoapp.errorMessage = Convert.ToString(SqlParms[10].Value);
                    return personelInfoapp;
                }

            }
            catch (Exception ex)
            {
                personelInfoapp.success = false;
                personelInfoapp.errorMessage = Convert.ToString(ex.Message.ToString());
                return personelInfoapp;
            }

            //  return AvailableParkings;
        }

        public List<UserInboxBEL> GetUserInbox(string User_id)
        {
            List<UserInboxBEL> UserInboxList = new List<UserInboxBEL>();

            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[2];

                SqlParms[0] = new SqlParameter("@User_id", SqlDbType.NVarChar);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = User_id;


                SqlParms[1] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;

                //SqlParms[7] = new SqlParameter("@UserId", SqlDbType.NVarChar, 20);
                //SqlParms[7].Direction = ParameterDirection.Output;

                //jfhjff

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_Notifications", SqlParms);


                if (ds != null && ds.Tables[0].Rows.Count > 0 && Convert.ToString(SqlParms[1].Value) != "0")
                {

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        UserInboxBEL UserInbox = new UserInboxBEL();

                        UserInbox.User_Id = Convert.ToInt32(ds.Tables[0].Rows[i]["User_id"]);
                        UserInbox.Notification_ID = Convert.ToInt32(ds.Tables[0].Rows[i]["Notification_ID"]);
                        UserInbox.Subject = ds.Tables[0].Rows[i]["Subject"].ToString();
                        UserInbox.Message = ds.Tables[0].Rows[i]["Message"].ToString();
                        if (ds.Tables[0].Rows[i]["Dt_Created"] != DBNull.Value)
                        {
                            UserInbox.Dt_Created = Convert.ToDateTime(ds.Tables[0].Rows[i]["Dt_Created"]);
                        }

                        UserInbox.READYN = Convert.ToChar(ds.Tables[0].Rows[i]["READYN"]);
                        UserInbox.UserSpecific = Convert.ToChar(ds.Tables[0].Rows[i]["UserSpecific"]);

                        UserInboxList.Add(UserInbox);
                    }

                    return UserInboxList;
                }

                else
                {

                    UserInboxBEL UserInbox = new UserInboxBEL();
                    UserInbox.Success = false;
                    UserInbox.ErrorMessage = Convert.ToString(SqlParms[1].Value);
                    UserInboxList.Add(UserInbox);
                    return UserInboxList;
                }


            }
            catch (Exception ex)
            {
                UserInboxBEL UserInbox = new UserInboxBEL();
                UserInbox.Success = false;
                UserInbox.ErrorMessage = Convert.ToString(ex.Message.ToString());
                UserInboxList.Add(UserInbox);
                return UserInboxList;
            }

            //  return AvailableParkings;
        }

        public UserInboxBEL DeleteUserNotification(string User_id, string notification_id)
        {
            UserInboxBEL UserInbox = new UserInboxBEL();

            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[3];

                SqlParms[0] = new SqlParameter("@User_id", SqlDbType.NVarChar);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = User_id;

                SqlParms[1] = new SqlParameter("@Notification_id", SqlDbType.NVarChar);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = notification_id;


                SqlParms[2] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[2].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Delete_Notification", SqlParms);

                if (Convert.ToString(SqlParms[2].Value) == string.Empty)
                {
                    UserInbox.Success = true;
                }
                else
                {
                    UserInbox.Success = false;
                }

                return UserInbox;

            }
            catch (Exception ex)
            {

                UserInbox.Success = false;
                UserInbox.ErrorMessage = Convert.ToString(ex.Message.ToString());

                return UserInbox;
            }
        }

        #endregion


        public UserDetailsBEL CheckPhoneNoExists(string Phone_No)
        {
            UserDetailsBEL Userdetail = new UserDetailsBEL();
            //SqlConnection conn = new SqlConnection();
            DataSet ds = null;
            try
            {
                SqlParameter[] SqlParms = new SqlParameter[2];

                SqlParms[0] = new SqlParameter("@Phone_No", SqlDbType.VarChar, 10);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = Phone_No;

                SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "TW_CheckPhoneNo", SqlParms);
                //&& Convert.ToString(SqlParms[5].Value) != "0"
                if (Convert.ToString(SqlParms[1].Value) == string.Empty)
                {
                    Userdetail.User_ID = ds.Tables[0].Rows[0]["User_ID"].ToString();
                    Userdetail.Alternate_Email_Id = ds.Tables[0].Rows[0]["Alternate_Email_Id"].ToString();
                    Userdetail.Email_Id = ds.Tables[0].Rows[0]["Email_Id"].ToString();
                    Userdetail.First_Name = ds.Tables[0].Rows[0]["First_Name"].ToString();
                    Userdetail.Last_Login = ds.Tables[0].Rows[0]["Last_Login"].ToString();
                    Userdetail.Last_Name = ds.Tables[0].Rows[0]["Last_Name"].ToString();
                    //Userdetail.Owner_Address = ds.Tables[0].Rows[0]["Owner_Address"].ToString();
                    Userdetail.Phone_No1 = ds.Tables[0].Rows[0]["Phone_No1"].ToString();
                    Userdetail.Phone_No2 = ds.Tables[0].Rows[0]["Phone_No2"].ToString();
                    // Userdetail.SignUp_Mode_ID = ds.Tables[0].Rows[0]["SignUp_Mode_ID"].ToString();
                    //Userdetail.User_Name = ds.Tables[0].Rows[0]["User_Name"].ToString();
                    Userdetail.ErrorMessage = string.Empty;
                    Userdetail.User_Name = ds.Tables[0].Rows[0]["First_Name"].ToString() + " " + ds.Tables[0].Rows[0]["Last_Name"].ToString(); ;
                    // Userdetail.Last_Login = 

                    // ResultDTO. = Convert.ToString(SqlParms[5].Value);
                }
                else
                {
                    Userdetail.User_Name = "";
                    Userdetail.User_ID = "0";
                    Userdetail.ErrorMessage = Convert.ToString(SqlParms[1].Value);
                }

            }
            catch (Exception ex)
            {
                Userdetail.User_Name = "";
                Userdetail.User_ID = "0";
                Userdetail.ErrorMessage = ex.Message.ToString();

            }
            return Userdetail;

        }

        public void SetProfilePic(string User_id, string ProfilePic)
        {
            UserInboxBEL UserInbox = new UserInboxBEL();

            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[3];

                SqlParms[0] = new SqlParameter("@user_id", SqlDbType.NVarChar);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = User_id;

                SqlParms[1] = new SqlParameter("@proilePicName", SqlDbType.NVarChar);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = ProfilePic;


                SqlParms[2] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[2].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Set_ProfilePic", SqlParms);

                if (Convert.ToString(SqlParms[2].Value) == string.Empty)
                {
                    //UserInbox.Success = true;
                }
                else
                {
                    // UserInbox.Success = false;
                }

                //  return UserInbox;

            }
            catch (Exception ex)
            {

                UserInbox.Success = false;
                UserInbox.ErrorMessage = Convert.ToString(ex.Message.ToString());

                // return UserInbox;
            }
        }

        #region "Contact Us by Amit"
        public object SaveUserContactUs(UserDetailsBEL objUserDetailsBEL)
        {
            bool status = false;
            DataSet ds = null;
            try
            {
                SqlParameter[] SqlParms = new SqlParameter[6];

                SqlParms[0] = new SqlParameter("@Name", SqlDbType.VarChar, 100);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = objUserDetailsBEL.Name;

                SqlParms[1] = new SqlParameter("@Email", SqlDbType.VarChar, 100);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = objUserDetailsBEL.Email;

                SqlParms[2] = new SqlParameter("@Subject", SqlDbType.VarChar, 1000);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = objUserDetailsBEL.Subject;

                SqlParms[3] = new SqlParameter("@Message", SqlDbType.VarChar, 6000);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = objUserDetailsBEL.Message;

                SqlParms[4] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[4].Direction = ParameterDirection.Output;

                SqlParms[5] = new SqlParameter("@Id", SqlDbType.NVarChar, 20);
                SqlParms[5].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "SaveContactUsDetails", SqlParms);

                if (!ds.HasErrors && ds != null)
                {
                    var referenceId = ds.Tables[0].Rows[0]["Id"].ToString();
                    return referenceId;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
            }
            return status;
        }
        #endregion "Contact Us"

        #region "FAQ by Amit"
        public List<FAQBEL> GetFAQQuestionAnswerList()
        {
            bool status = false;
            DataSet ds = null;
            FAQBEL faq = new FAQBEL();
            List<FAQBEL> lstFAQ = new List<FAQBEL>();
            try
            {
                SqlParameter[] SqlParms = new SqlParameter[1];
                SqlParms[0] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[0].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "FAQList", SqlParms);


                if (ds.Tables[0].Rows.Count > 0)
                {
                    lstFAQ = (from DataRow faqrow in ds.Tables[0].Rows
                              select new FAQBEL
                              {
                                  Id = Convert.ToString(faqrow["Id"]),
                                  QuestionDesc = Convert.ToString(faqrow["QuestionDesc"]),
                                  id = Convert.ToInt16(faqrow["id"]),
                                  QuestionId = Convert.ToInt16(faqrow["Question_Id"]),
                                  AnswerDesc = Convert.ToString(faqrow["AnswerDesc"]),
                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
            }
            return lstFAQ;
        }
        #endregion "FAQ"

        public ProfileOTPPhoneNoVerified IsUserVerifiedProfile(string userId)
        {
            ProfileOTPPhoneNoVerified objProfileOTPVerified = new ProfileOTPPhoneNoVerified();
            if (!string.IsNullOrEmpty(userId))
            {
                try
                {

                    DataSet ds = null;

                    SqlParameter[] SqlParms = new SqlParameter[3];

                    SqlParms[0] = new SqlParameter("@User_id", SqlDbType.NVarChar);
                    SqlParms[0].Direction = ParameterDirection.Input;
                    SqlParms[0].Value = userId;

                    SqlParms[1] = new SqlParameter("@IsMobileVarified", SqlDbType.NVarChar);
                    SqlParms[1].Direction = ParameterDirection.Input;
                    SqlParms[1].Value = "Y";


                    SqlParms[2] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                    SqlParms[2].Direction = ParameterDirection.Output;
                    ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Update_UserProfileVerified", SqlParms);

                    if (Convert.ToString(SqlParms[2].Value) == string.Empty)
                        objProfileOTPVerified.VerifiedSuccessfully = true;
                    else
                    {
                        objProfileOTPVerified.VerifiedSuccessfully = false;
                    }
                    return objProfileOTPVerified;
                }
                catch (Exception ex)
                {

                    ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);

                }
            }
            return objProfileOTPVerified;
        }

        public ProfileOTPPhoneNoVerified UserPhoneNoVerification(string userId, string mobileno)
        {
            ProfileOTPPhoneNoVerified objProfilePhoneVerified = new ProfileOTPPhoneNoVerified();
            if (!string.IsNullOrEmpty(userId))
            {
                try
                {

                    DataSet ds = null;

                    SqlParameter[] SqlParms = new SqlParameter[3];

                    SqlParms[0] = new SqlParameter("@User_id", SqlDbType.NVarChar);
                    SqlParms[0].Direction = ParameterDirection.Input;
                    SqlParms[0].Value = userId;

                    SqlParms[1] = new SqlParameter("@PhoneNo", SqlDbType.NVarChar);
                    SqlParms[1].Direction = ParameterDirection.Input;
                    SqlParms[1].Value = mobileno;


                    SqlParms[2] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                    SqlParms[2].Direction = ParameterDirection.Output;
                    ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Update_GG_FB_PhoneNo", SqlParms);

                    if (Convert.ToString(SqlParms[2].Value) == string.Empty)
                        objProfilePhoneVerified.VerifiedSuccessfully = true;
                    else if (Convert.ToString(SqlParms[2].Value) == "Phone No. already exist.")
                    {
                        objProfilePhoneVerified.ErrorMessage = "Phone No. already exist.";
                    }
                    else
                    {
                        objProfilePhoneVerified.VerifiedSuccessfully = false;
                    }
                    return objProfilePhoneVerified;
                }
                catch (Exception ex)
                {
                    ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
                }
            }
            return objProfilePhoneVerified;
        }


        #region PB Registration
        public PBDetailsBEL AddPB(PBDetailsBEL userDetailsBEL)
        {
            PBDetailsBEL Userdetail = new PBDetailsBEL();
            DataSet ds = null;
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[11];

                SqlParms[0] = new SqlParameter("@User_Name", SqlDbType.VarChar, 50);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = userDetailsBEL.user_Name;

                SqlParms[1] = new SqlParameter("@Phone_No1", SqlDbType.VarChar, 50);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = userDetailsBEL.phone_No1;

                SqlParms[2] = new SqlParameter("@Password", SqlDbType.VarChar);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = userDetailsBEL.Password;

                SqlParms[3] = new SqlParameter("@Email_Id", SqlDbType.VarChar);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = userDetailsBEL.Email_Id;

                SqlParms[4] = new SqlParameter("@Address", SqlDbType.VarChar);
                SqlParms[4].Direction = ParameterDirection.Input;
                SqlParms[4].Value = userDetailsBEL.Address;

                SqlParms[5] = new SqlParameter("@DateOfBirth", SqlDbType.VarChar);
                SqlParms[5].Direction = ParameterDirection.Input;
                SqlParms[5].Value = userDetailsBEL.dob;

                SqlParms[6] = new SqlParameter("@IsAdminRightsAssigned", SqlDbType.Char, 1);
                SqlParms[6].Direction = ParameterDirection.Input;
                SqlParms[6].Value = userDetailsBEL.isAdminRightsAssigned;

                SqlParms[7] = new SqlParameter("@pa_Custid", SqlDbType.Char);
                SqlParms[7].Direction = ParameterDirection.Input;
                SqlParms[7].Value = userDetailsBEL.pA_CustId;

                SqlParms[8] = new SqlParameter("@workingLocation", SqlDbType.Int);
                SqlParms[8].Direction = ParameterDirection.Input;
                SqlParms[8].Value = userDetailsBEL.working_location;

                SqlParms[9] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[9].Direction = ParameterDirection.Output;

                SqlParms[10] = new SqlParameter("@UserId", SqlDbType.NVarChar, 20);
                SqlParms[10].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "PB_Signup", SqlParms);
                //&& Convert.ToString(SqlParms[5].Value) != "0"
                if (ds == null && ds.Tables.Count <= 0)
                {
                    Userdetail.user_Name = "";
                    Userdetail.user_ID = "0";
                    Userdetail.ErrorMessage = Convert.ToString(SqlParms[9].Value) + " Data Not Found";
                }
                else if (Convert.ToString(SqlParms[9].Value) == string.Empty || Convert.ToString(SqlParms[9].Value) == "")
                {
                    Userdetail.user_ID = Convert.ToString(SqlParms[10].Value); // ds.Tables[0].Rows[0]["User_ID"].ToString();
                    // Userdetail.Email_Id = ds.Tables[0].Rows[0]["Email_Id"].ToString();
                    //Userdetail.Phone_No1 = ds.Tables[0].Rows[0]["Phone_No1"].ToString();
                    // Userdetail.User_Name = ds.Tables[0].Rows[0]["User_Name"].ToString();
                    Userdetail.ErrorMessage = string.Empty;

                    // ResultDTO. = Convert.ToString(SqlParms[5].Value);
                }
                else
                {
                    Userdetail.user_Name = "";
                    Userdetail.user_ID = "0";
                    Userdetail.ErrorMessage = Convert.ToString(SqlParms[9].Value);
                }

            }
            catch (Exception ex)
            {
                Userdetail.user_Name = "";
                Userdetail.user_ID = "0";
                Userdetail.ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }

            return Userdetail;
        }
        #endregion

        #region Authenticate Token
        public string AuthenticateToken(string Token)
        {

            DataSet ds = null;
            string User_id = string.Empty;
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[3];

                SqlParms[0] = new SqlParameter("@Auth_Token", SqlDbType.VarChar, 40);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = Token;

                SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;

                SqlParms[2] = new SqlParameter("@UserId", SqlDbType.VarChar, 20);
                SqlParms[2].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Authenticate_Token", SqlParms);
                if (Convert.ToString(SqlParms[1].Value) == string.Empty || Convert.ToString(SqlParms[1].Value) == "")
                {
                    User_id = Convert.ToString(SqlParms[2].Value);

                    // ResultDTO. = Convert.ToString(SqlParms[5].Value);
                }
                else
                {
                    //Userdetail.User_Name = "";
                    User_id = "0";
                    //Userdetail.ErrorMessage = Convert.ToString(SqlParms[8].Value);
                }

            }
            catch (Exception ex)
            {
                //  Userdetail.User_Name = "";
                User_id = "0";
                // Userdetail.ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }

            return User_id;
        }
        #endregion

        #region Authenticate Token
        public string AuthenticateTokenPB(string Token)
        {

            DataSet ds = null;
            string User_id = string.Empty;
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[3];

                SqlParms[0] = new SqlParameter("@Auth_Token", SqlDbType.VarChar, 40);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = Token;

                SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;

                SqlParms[2] = new SqlParameter("@UserId", SqlDbType.VarChar, 20);
                SqlParms[2].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Authenticate_PBToken", SqlParms);
                if (Convert.ToString(SqlParms[1].Value) == string.Empty || Convert.ToString(SqlParms[1].Value) == "")
                {
                    User_id = Convert.ToString(SqlParms[2].Value);

                    // ResultDTO. = Convert.ToString(SqlParms[5].Value);
                }
                else
                {
                    //Userdetail.User_Name = "";
                    User_id = "0";
                    //Userdetail.ErrorMessage = Convert.ToString(SqlParms[8].Value);
                }

            }
            catch (Exception ex)
            {
                //  Userdetail.User_Name = "";
                User_id = "0";
                // Userdetail.ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }

            return User_id;
        }
        #endregion

        public List<LocationsIDName> AuthenticatePAPB(UtilityLoginBEL Ubal, out string error)
        {
            UserDetailsBEL Userdetail = new UserDetailsBEL();
            List<LocationsIDName> locationsIDName = new List<LocationsIDName>();

            DataSet ds = null;
            try
            {
                SqlParameter[] SqlParms = new SqlParameter[6];



                SqlParms[0] = new SqlParameter("@Phone_No1", SqlDbType.VarChar);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = Ubal.MobileNumber;
                SqlParms[1] = new SqlParameter("@Password", SqlDbType.VarChar, 500);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = Ubal.Password;
                SqlParms[2] = new SqlParameter("@User_Type", SqlDbType.VarChar, 2);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = Ubal.UserType;
                SqlParms[3] = new SqlParameter("@AuthToken", SqlDbType.VarChar, 40);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = Ubal.AuthToken;

                SqlParms[4] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[4].Direction = ParameterDirection.Output;

                SqlParms[5] = new SqlParameter("@UserId", SqlDbType.VarChar, 20);
                SqlParms[5].Direction = ParameterDirection.Output;



                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "SP_LoginPAPB", SqlParms);
                //&& Convert.ToString(SqlParms[5].Value) != "0"

                if (Convert.ToString(SqlParms[4].Value) == string.Empty && ds.Tables[0].Rows.Count > 0)
                {
                    error = "";
                    locationsIDName = (from DataRow parkingrow in ds.Tables[0].Rows
                                       select new LocationsIDName
                                      {
                                          parking_address = parkingrow["Parking_Name"].ToString(),
                                          Parking_id = parkingrow["ParkingId"].ToString()

                                      }).ToList();
                    return locationsIDName;

                }
                else
                {
                    //Userdetail.User_Name = "";
                    //Userdetail.User_ID = "0";
                    error = Convert.ToString(SqlParms[4].Value);
                }

            }
            catch (Exception ex)
            {
                //Userdetail.User_Name = "";
                //Userdetail.User_ID = "0";

                error = ex.Message.ToString();
                throw ex;

            }
            return locationsIDName;

        }

        #region PB Image n doc url
        public PBDetailsBEL UpdateImageDocURL(PBDetailsBEL userDetailsBEL)
        {
            PBDetailsBEL Userdetail = new PBDetailsBEL();
            DataSet ds = null;
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[4];

                SqlParms[0] = new SqlParameter("@User_id", SqlDbType.VarChar, 50);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = userDetailsBEL.user_ID;

                SqlParms[1] = new SqlParameter("@profilePicURL", SqlDbType.VarChar);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = userDetailsBEL.picUrl;

                SqlParms[2] = new SqlParameter("@IDProofURL", SqlDbType.VarChar);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = userDetailsBEL.docURL;


                SqlParms[3] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[3].Direction = ParameterDirection.Output;



                SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Update_DocUrls", SqlParms);

                if (Convert.ToString(SqlParms[3].Value) == string.Empty || Convert.ToString(SqlParms[3].Value) == "")
                {
                    Userdetail.ErrorMessage = string.Empty;

                }
                else
                {

                    Userdetail.ErrorMessage = Convert.ToString(SqlParms[3].Value);
                }

            }
            catch (Exception ex)
            {
                Userdetail.user_Name = "";
                Userdetail.user_ID = "0";
                Userdetail.ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }

            return Userdetail;
        }
        #endregion
        #region PB Updateprofile
        public PBDetailsBEL UpdatePBProfile(PBDetailsBEL userDetailsBEL)
        {
            PBDetailsBEL Userdetail = new PBDetailsBEL();
            DataSet ds = null;
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[8];

                SqlParms[0] = new SqlParameter("@User_Name", SqlDbType.VarChar, 50);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = userDetailsBEL.user_Name;

                SqlParms[1] = new SqlParameter("@Phone_No1", SqlDbType.VarChar, 50);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = userDetailsBEL.phone_No1;

                SqlParms[2] = new SqlParameter("@WorkingLocation", SqlDbType.Int);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = userDetailsBEL.working_location;

                SqlParms[3] = new SqlParameter("@Email_Id", SqlDbType.VarChar);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = userDetailsBEL.Email_Id;

                SqlParms[4] = new SqlParameter("@Address", SqlDbType.VarChar);
                SqlParms[4].Direction = ParameterDirection.Input;
                SqlParms[4].Value = userDetailsBEL.Address;

                SqlParms[5] = new SqlParameter("@DateOfBirth", SqlDbType.VarChar);
                SqlParms[5].Direction = ParameterDirection.Input;
                SqlParms[5].Value = userDetailsBEL.dob;

                SqlParms[6] = new SqlParameter("@User_id", SqlDbType.Int);
                SqlParms[6].Direction = ParameterDirection.Input;
                SqlParms[6].Value = userDetailsBEL.user_ID;

                SqlParms[7] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[7].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Update_PBDetails", SqlParms);
                if (Convert.ToString(SqlParms[7].Value) == string.Empty || Convert.ToString(SqlParms[7].Value) == "")
                {
                    //Userdetail.User_ID = Convert.ToString(SqlParms[10].Value); // ds.Tables[0].Rows[0]["User_ID"].ToString();
                    // Userdetail.Email_Id = ds.Tables[0].Rows[0]["Email_Id"].ToString();
                    //Userdetail.Phone_No1 = ds.Tables[0].Rows[0]["Phone_No1"].ToString();
                    // Userdetail.User_Name = ds.Tables[0].Rows[0]["User_Name"].ToString();
                    Userdetail.ErrorMessage = string.Empty;

                    // ResultDTO. = Convert.ToString(SqlParms[5].Value);
                }
                else
                {
                    Userdetail.user_Name = "";
                    Userdetail.user_ID = "0";
                    Userdetail.ErrorMessage = Convert.ToString(SqlParms[7].Value);
                }

            }
            catch (Exception ex)
            {
                Userdetail.user_Name = "";
                Userdetail.user_ID = "0";
                Userdetail.ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }

            return Userdetail;
        }
        #endregion

        #region PB Deactivate PB
        public PBDetailsBEL DeactivatePB(string PB_ID, string PA_UserID)
        {
            PBDetailsBEL Userdetail = new PBDetailsBEL();
            DataSet ds = null;
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[3];

                SqlParms[0] = new SqlParameter("@PAUserid", SqlDbType.Int);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = PA_UserID;

                SqlParms[1] = new SqlParameter("@User_id", SqlDbType.Int);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = PB_ID;


                SqlParms[2] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[2].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Deactivate_PB", SqlParms);
                if (Convert.ToString(SqlParms[2].Value) == string.Empty || Convert.ToString(SqlParms[2].Value) == "")
                {
                    Userdetail.ErrorMessage = string.Empty;
                }
                else
                {
                    Userdetail.user_Name = "";
                    Userdetail.user_ID = "0";
                    Userdetail.ErrorMessage = Convert.ToString(SqlParms[2].Value);
                }

            }
            catch (Exception ex)
            {
                Userdetail.user_Name = "";
                Userdetail.user_ID = "0";
                Userdetail.ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }

            return Userdetail;
        }
        #endregion


        #region Get All PBs
        public List<PBDetailsBEL> GetAllPBs(string PA_UserID, out string error)
        {
            List<PBDetailsBEL> PBList = new List<PBDetailsBEL>();
            DataSet ds = null;
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[2];

                SqlParms[0] = new SqlParameter("@PAUserid", SqlDbType.Int);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = PA_UserID;

                SqlParms[1] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_AllPBs", SqlParms);
                if (Convert.ToString(SqlParms[1].Value) == string.Empty || Convert.ToString(SqlParms[1].Value) == "")
                {

                    error = "";

                    PBList = (from DataRow PBRow in ds.Tables[0].Rows
                              select new PBDetailsBEL
                              {
                                  user_ID = PBRow["user_id"].ToString(),
                                  user_Name = PBRow["User_Name"].ToString(),
                                  phone_No1 = PBRow["phone_No1"].ToString()
                                 ,
                                  Email_Id = PBRow["Email_Id"].ToString()
                                 ,
                                  Address = PBRow["Address"].ToString()
                                 ,
                                  status = PBRow["Status"].ToString()
                                 ,
                                  isMobileVerified = PBRow["isMobileVarified"].ToString()
                                 ,
                                  working_location = Convert.ToInt64(PBRow["workLOcationID"].ToString())
                                 ,
                                  workLocationName = PBRow["workLocationName"].ToString()
                                 ,
                                  isAdminRightsAssigned = PBRow["IsAdminRightsAssigned"].ToString()


                              }).ToList();
                    return PBList;
                }
                else
                {
                    //Userdetail.User_Name = "";
                    //Userdetail.User_ID = "0";
                    //Userdetail.ErrorMessage = Convert.ToString(SqlParms[1].Value);
                    error = Convert.ToString(SqlParms[4].Value);
                }

            }
            catch (Exception ex)
            {
                //Userdetail.User_Name = "";
                //Userdetail.User_ID = "0";
                //Userdetail.ErrorMessage = "DataBase Error Occured : " + ex.Message;
                error = ex.Message.ToString();
                throw ex;

            }

            return PBList;
        }
        #endregion


        #region  Get_VehicleDetail
        public UserBookingBEL Get_VehicleDetail(string VehicleNumber)
        {
            UserBookingBEL Userdetail = new UserBookingBEL();
            DataSet ds = null;
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[2];

                SqlParms[0] = new SqlParameter("@VehicleNumber", SqlDbType.VarChar);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = VehicleNumber;

                SqlParms[1] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_VehicleDetail", SqlParms);
                if (Convert.ToString(SqlParms[1].Value) == string.Empty || Convert.ToString(SqlParms[1].Value) == "")
                {
                    Userdetail.vehicleNumber = Convert.ToString(ds.Tables[0].Rows[0]["vehicalNumber"]);
                    Userdetail.mobileNumber = Convert.ToString(ds.Tables[0].Rows[0]["MobileNo"]);
                    Userdetail.expiryDate = Convert.ToString(ds.Tables[0].Rows[0]["ExpiryDate"]);
                    Userdetail.planOpt = Convert.ToString(ds.Tables[0].Rows[0]["PlanOpt"]);
                    Userdetail.CheckedIn = Convert.ToString(ds.Tables[0].Rows[0]["CheckedIn"]);
                    Userdetail.CheckedOut = Convert.ToString(ds.Tables[0].Rows[0]["CheckedOut"]);
                    Userdetail.mobileNumber = Convert.ToString(ds.Tables[0].Rows[0]["MobileNo"]);
                    Userdetail.ErrorMessage = string.Empty;
                }
                else
                {
                    //Userdetail.user_Name = "";
                    //Userdetail.user_ID = "0";
                    Userdetail.ErrorMessage = Convert.ToString(SqlParms[1].Value);
                }

            }
            catch (Exception ex)
            {
                //Userdetail.user_Name = "";
                //Userdetail.user_ID = "0";
                Userdetail.ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }

            return Userdetail;
        }
        #endregion

        #region  Get_VehiclePhonenumber
        public string Get_VehiclePhone(string VehicleNumber, out string ErrorMessage)
        {
            UserBookingBEL Userdetail = new UserBookingBEL();
            DataSet ds = null;
            string Number = string.Empty;
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[3];

                SqlParms[0] = new SqlParameter("@VehicleNumber", SqlDbType.VarChar);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = VehicleNumber;

                SqlParms[1] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;

                SqlParms[2] = new SqlParameter("@PhoneNo", SqlDbType.NVarChar, 12);
                SqlParms[2].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_VehicleMobileNumber", SqlParms);
                if (Convert.ToString(SqlParms[1].Value) == string.Empty || Convert.ToString(SqlParms[1].Value) == "")
                {
                    Number = Convert.ToString(SqlParms[2].Value);

                    ErrorMessage = string.Empty;
                }
                else
                {
                    //Userdetail.user_Name = "";
                    //Userdetail.user_ID = "0";
                    ErrorMessage = Convert.ToString(SqlParms[1].Value);
                }

            }
            catch (Exception ex)
            {
                //Userdetail.user_Name = "";
                //Userdetail.user_ID = "0";
                ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }

            return Number;
        }
        #endregion

        #region  Get_MiniReport
        public void Get_MiniReport(string PAUserid, out string ErrorMessage, out string TotVehicle, out string TotPbs)
        {
            UserBookingBEL Userdetail = new UserBookingBEL();
            DataSet ds = null;

            TotVehicle = string.Empty;
            TotPbs = string.Empty;
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[2];


                SqlParms[0] = new SqlParameter("@PAUserid", SqlDbType.Int);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = PAUserid;

                SqlParms[1] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;


                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_MiniReport", SqlParms);
                if (Convert.ToString(SqlParms[1].Value) == string.Empty || Convert.ToString(SqlParms[1].Value) == "")
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        TotVehicle = Convert.ToString(ds.Tables[0].Rows[0]["totalInVehicle"]);
                        TotPbs = Convert.ToString(ds.Tables[0].Rows[0]["totalPBAvailable"]);
                    }

                    ErrorMessage = string.Empty;
                }
                else
                {
                    //Userdetail.user_Name = "";
                    //Userdetail.user_ID = "0";
                    ErrorMessage = Convert.ToString(SqlParms[1].Value);
                }

            }
            catch (Exception ex)
            {
                //Userdetail.user_Name = "";
                //Userdetail.user_ID = "0";
                ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }


        }
        #endregion

        // Added by neeraj for SIGNUP from APP
        public UserDetailsfrmAppBEL AddUserDetailsFrmAPP(UserDetailsfrmAppBEL userDetailsBEL)
        {
            UserDetailsfrmAppBEL Userdetail = new UserDetailsfrmAppBEL();
            DataSet ds = null;
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[8];

                SqlParms[0] = new SqlParameter("@Full_Name", SqlDbType.NVarChar, 50);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = userDetailsBEL.Full_Name;

                //SqlParms[1] = new SqlParameter("@Last_Name", SqlDbType.NVarChar, 50);
                //SqlParms[1].Direction = ParameterDirection.Input;
                //SqlParms[1].Value = userDetailsBEL.Last_Name;

                //SqlParms[2] = new SqlParameter("@Password", SqlDbType.NVarChar, 500);
                //SqlParms[2].Direction = ParameterDirection.Input;
                //SqlParms[2].Value = userDetailsBEL.Password;

                SqlParms[1] = new SqlParameter("@Phone_No1", SqlDbType.NVarChar, 50);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = userDetailsBEL.Phone_No1;

                SqlParms[2] = new SqlParameter("@Alternate_No", SqlDbType.NVarChar, 50);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = userDetailsBEL.Alternate_No;

                SqlParms[3] = new SqlParameter("@Email_Id", SqlDbType.NVarChar, 50);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = userDetailsBEL.Email_Id;

                SqlParms[4] = new SqlParameter("@SignUp_Mode_ID", SqlDbType.NVarChar, 2);
                SqlParms[4].Direction = ParameterDirection.Input;
                SqlParms[4].Value = userDetailsBEL.SignUp_Mode_ID;

                SqlParms[5] = new SqlParameter("@Other_FB_GG_ID", SqlDbType.VarChar, 500);
                SqlParms[5].Direction = ParameterDirection.Input;
                if (userDetailsBEL.SignUp_Mode_ID == "FB")
                {
                    if (userDetailsBEL.Other_ID != string.Empty)
                        SqlParms[5].Value = userDetailsBEL.Other_ID;
                    //SqlParms[6].Value = Convert.ToUInt64(userDetailsBEL.Other_ID);
                    else
                        SqlParms[5].Value = 0;
                }
                else if (userDetailsBEL.SignUp_Mode_ID == "GG")
                {
                    if (userDetailsBEL.Other_ID != string.Empty)
                        SqlParms[5].Value = userDetailsBEL.Other_ID;
                    else
                        SqlParms[5].Value = 0;
                }

                SqlParms[6] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[6].Direction = ParameterDirection.Output;

                SqlParms[7] = new SqlParameter("@UserId", SqlDbType.NVarChar, 20);
                SqlParms[7].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Insert_UserLogininApp", SqlParms);
                //&& Convert.ToString(SqlParms[5].Value) != "0"
                if (ds == null && ds.Tables.Count <= 0)
                {
                    Userdetail.Full_Name = "";
                    Userdetail.User_ID = "0";
                    Userdetail.ErrorMessage = Convert.ToString(SqlParms[7].Value) + " Data Not Found";
                }
                else if (Convert.ToString(SqlParms[6].Value) == string.Empty)
                {
                    Userdetail.User_ID = (ds.Tables[0].Rows[0]["User_ID"]) == DBNull.Value ? "" : Convert.ToString(ds.Tables[0].Rows[0]["User_ID"]);//ds.Tables[0].Rows[0]["User_ID"].ToString();
                    //Userdetail.Email_Id = ds.Tables[0].Rows[0]["Email_Id"].ToString();
                    //Userdetail.Last_Login = ds.Tables[0].Rows[0]["Last_Login"].ToString();
                    Userdetail.Full_Name = (ds.Tables[0].Rows[0]["Full_Name"]) == DBNull.Value ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Full_Name"]);//ds.Tables[0].Rows[0]["Full_Name"].ToString();
                    Userdetail.Phone_No1 = (ds.Tables[0].Rows[0]["Phone_No1"]) == DBNull.Value ? "" : Convert.ToString(ds.Tables[0].Rows[0]["Phone_No1"]);//ds.Tables[0].Rows[0]["Phone_No1"].ToString();
                    //Userdetail.Full_Name = ds.Tables[0].Rows[0]["Full_Name"].ToString();
                    Userdetail.ErrorMessage = string.Empty;

                    // ResultDTO. = Convert.ToString(SqlParms[5].Value);
                }
                else
                {
                    Userdetail.Full_Name = "";
                    Userdetail.User_ID = "0";
                    Userdetail.ErrorMessage = Convert.ToString(SqlParms[6].Value);
                }

            }
            catch (Exception ex)
            {
                Userdetail.Full_Name = "";
                Userdetail.User_ID = "0";
                Userdetail.ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }

            return Userdetail;
        }


        #region  Get_BriefReport
        public BookingBriefReportBEL Get_BriefReport(string period, string pbWorkingLocation, out string ErrorMessage)
        {
            UserBookingBEL Userdetail = new UserBookingBEL();
            DataSet ds = null;
            string fromdate = DateTime.Now.ToString();
            string Todate = DateTime.Now.ToString();
            BookingBriefReportBEL BBReport = new BookingBriefReportBEL();


            try
            {
                if (period.ToUpper() == "D")
                {
                    fromdate = DateTime.Now.ToString();
                    Todate = DateTime.Now.ToString();
                }
                else if (period.ToUpper() == "M")
                {
                    Todate = DateTime.Now.ToString();
                    fromdate = DateTime.Now.AddDays(-30).ToString();
                }
                SqlParameter[] SqlParms = new SqlParameter[5];

                SqlParms[0] = new SqlParameter("@ParkingID", SqlDbType.Int);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = pbWorkingLocation;


                SqlParms[1] = new SqlParameter("@FromDate", SqlDbType.VarChar);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = fromdate;

                SqlParms[2] = new SqlParameter("@ToDate", SqlDbType.VarChar);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = Todate;

                SqlParms[3] = new SqlParameter("@Period", SqlDbType.Char);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = period.ToUpper();

                SqlParms[4] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[4].Direction = ParameterDirection.Output;

               // SqlDataReader rd;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_BriefReport", SqlParms);
                Dictionary<string, string> dics = new Dictionary<string, string>();
                if (Convert.ToString(SqlParms[4].Value) == string.Empty || Convert.ToString(SqlParms[4].Value) == "")
                {
                    if(ds.Tables[0].Rows.Count>0)
                    {
                        BBReport.RegularIN = ds.Tables[0].Rows[0]["RegularIn"].ToString();
                        BBReport.RegularOut = ds.Tables[0].Rows[0]["RegularOut"].ToString();
                        BBReport.RegularTot = Convert.ToString(Convert.ToInt64(BBReport.RegularIN) - Convert.ToInt64(BBReport.RegularOut));
                        BBReport.MBTVehicleIn = ds.Tables[0].Rows[0]["MBTVehicleIn"].ToString();
                        BBReport.MBTVehicleOut = ds.Tables[0].Rows[0]["MBTVehicleOut"].ToString();
                        BBReport.MBTVehicleTot = Convert.ToString(Convert.ToInt64(BBReport.MBTVehicleIn) - Convert.ToInt64(BBReport.MBTVehicleOut));
                        BBReport.MDTVehicleIn = ds.Tables[0].Rows[0]["MDTVehicleIn"].ToString();
                        BBReport.MDTVehicleOut = ds.Tables[0].Rows[0]["MDTVehicleOut"].ToString();
                        BBReport.MDTVehicleTot = Convert.ToString(Convert.ToInt64(BBReport.MDTVehicleIn) - Convert.ToInt64(BBReport.MDTVehicleOut));
                        BBReport.MNTVehicleIn = ds.Tables[0].Rows[0]["MNTVehicleIn"].ToString();
                        BBReport.MNTVehicleOut = ds.Tables[0].Rows[0]["MNTVehicleOut"].ToString();
                        BBReport.MNTVehicleTot = Convert.ToString(Convert.ToInt64(BBReport.MNTVehicleIn) - Convert.ToInt64(BBReport.MNTVehicleOut));
                    }
                    
                    
                    ErrorMessage = string.Empty;
                }
                else
                {
                    //Userdetail.user_Name = "";
                    //Userdetail.user_ID = "0";
                    ErrorMessage = Convert.ToString(SqlParms[4].Value);
                }

            }
            catch (Exception ex)
            {
                //Userdetail.user_Name = "";
                //Userdetail.user_ID = "0";
                ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }
            return BBReport;


        }
        #endregion


        #region  Get_Revenue
        public BookingBriefReportBEL Get_Revenue(string period, string pbWorkingLocation, out string ErrorMessage)
        {
            UserBookingBEL Userdetail = new UserBookingBEL();
            DataSet ds = null;
            string fromdate = DateTime.Now.ToString();
            string Todate = DateTime.Now.ToString();
            BookingBriefReportBEL BBReport = new BookingBriefReportBEL();
            string pbWorkingIds = string.Empty;
            //if(pbWorkingLocation.Count>1)
            //{
            //    foreach( string item in pbWorkingLocation)
            //    { pbWorkingIds = pbWorkingIds + item + ","; }
            //}

            try
            {
                if (period.ToUpper() == "D")
                {
                    fromdate = DateTime.Now.ToString();
                    Todate = DateTime.Now.ToString();
                }
                else if (period.ToUpper() == "M")
                {
                    Todate = DateTime.Now.ToString();
                    fromdate = DateTime.Now.AddDays(-30).ToString();
                }
                SqlParameter[] SqlParms = new SqlParameter[5];

                SqlParms[0] = new SqlParameter("@ParkingID", SqlDbType.VarChar);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = pbWorkingLocation;


                SqlParms[1] = new SqlParameter("@FromDate", SqlDbType.VarChar);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = fromdate;

                SqlParms[2] = new SqlParameter("@ToDate", SqlDbType.VarChar);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = Todate;

                SqlParms[3] = new SqlParameter("@Period", SqlDbType.Char);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = period.ToUpper();

                SqlParms[4] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[4].Direction = ParameterDirection.Output;

                SqlDataReader rd;

                rd = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Get_RevenueReport", SqlParms);
                Dictionary<string, string> dics = new Dictionary<string, string>();
                if (Convert.ToString(SqlParms[4].Value) == string.Empty || Convert.ToString(SqlParms[4].Value) == "")
                {
                    while (rd.Read())
                    {
                        dics[(string)rd["CollectionType"]] = rd["collectedamount"].ToString();
                        dics[(string)rd["HelmetChargeType"]] = rd["helmetCharge"].ToString();
                        //BBReport.DailyHelmetCollection = Convert.ToString(rd["DhelmetCharge"]);
                        //BBReport.WeeklyHelmetCollection = Convert.ToString(rd["WhelmetCharge"]);
                        //BBReport.MonthlyHelmetCollection = Convert.ToString(rd["MhelmetCharge"]);
                        //BBReport.TotalHelmetCollection = Convert.ToString(rd["ThelmetCharge"]);
                    }
                    if (dics.Keys.Contains("DailyCollection"))
                    {
                        BBReport.DailyCollection = dics["DailyCollection"].ToString();
                        if (dics.Keys.Contains("DhelmetCharge"))
                            BBReport.DailyHelmetCollection = dics["DhelmetCharge"].ToString();
                    }
                    else
                        BBReport.DailyCollection = "0";

                    if (dics.Keys.Contains("WeeklyCollection"))
                    {
                        BBReport.WeeklyCollection = dics["WeeklyCollection"].ToString();
                        if (dics.Keys.Contains("WhelmetCharge"))
                            BBReport.WeeklyHelmetCollection = dics["WhelmetCharge"].ToString();
                        
                    }
                    else
                        BBReport.WeeklyCollection = "0";

                    if (dics.Keys.Contains("MonthlyCollection"))
                    {
                        BBReport.MonthlyCollection = dics["MonthlyCollection"].ToString();
                        if (dics.Keys.Contains("MhelmetCharge"))
                            BBReport.MonthlyHelmetCollection = dics["MhelmetCharge"].ToString();
                        
                    }
                    else
                        BBReport.MonthlyCollection = "0";

                    if (dics.Keys.Contains("TotalCollection"))
                    {
                        BBReport.TotalCollection = dics["TotalCollection"].ToString();
                        if (dics.Keys.Contains("ThelmetCharge"))
                            BBReport.TotalHelmetCollection = dics["ThelmetCharge"].ToString();
                        
                    }
                    else
                        BBReport.TotalCollection = "0";


                    ErrorMessage = string.Empty;
                }
                else
                {
                    //Userdetail.user_Name = "";
                    //Userdetail.user_ID = "0";
                    ErrorMessage = Convert.ToString(SqlParms[4].Value);
                }

            }
            catch (Exception ex)
            {
                //Userdetail.user_Name = "";
                //Userdetail.user_ID = "0";
                ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }
            return BBReport;


        }
        #endregion

        #region PA Registration
        public PBDetailsBEL AddPA(PBDetailsBEL userDetailsBEL)
        {
            PBDetailsBEL Userdetail = new PBDetailsBEL();
            DataSet ds = null;
            try
            {
                DataTable dataTable = new DataTable("ParkingLocationList");
                dataTable.Columns.Add(new DataColumn("parking_id", typeof(int)));
                dataTable.Columns.Add(new DataColumn("Parking_Name", typeof(string)));
                foreach (var item in userDetailsBEL.parkingLocationList)
                {
                    dataTable.Rows.Add(item.parking_id, item.Parking_Name);
                }


                SqlParameter[] SqlParms = new SqlParameter[10];

                SqlParms[0] = new SqlParameter("@User_Name", SqlDbType.VarChar, 50);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = userDetailsBEL.user_Name;

                SqlParms[1] = new SqlParameter("@Phone_No1", SqlDbType.VarChar, 50);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = userDetailsBEL.phone_No1;

                SqlParms[2] = new SqlParameter("@Email_Id", SqlDbType.VarChar);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = userDetailsBEL.Email_Id;

                SqlParms[3] = new SqlParameter("@Password", SqlDbType.VarChar);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = Secure.Encrypt(userDetailsBEL.Password);

                SqlParms[4] = new SqlParameter("@Address", SqlDbType.VarChar);
                SqlParms[4].Direction = ParameterDirection.Input;
                SqlParms[4].Value = userDetailsBEL.Address;

                SqlParms[5] = new SqlParameter("@DateOfBirth", SqlDbType.VarChar);
                SqlParms[5].Direction = ParameterDirection.Input;
                SqlParms[5].Value = userDetailsBEL.dob;



                SqlParms[6] = new SqlParameter("@LocationList", SqlDbType.Structured);
                SqlParms[6].Direction = ParameterDirection.Input;
                SqlParms[6].Value = dataTable;

                SqlParms[7] = new SqlParameter("@User_Type", SqlDbType.VarChar);
                SqlParms[7].Direction = ParameterDirection.Input;
                SqlParms[7].Value = userDetailsBEL.userType;

                SqlParms[8] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[8].Direction = ParameterDirection.Output;

                SqlParms[9] = new SqlParameter("@UserId", SqlDbType.NVarChar, 20);
                SqlParms[9].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "PA_Signup", SqlParms);
                //&& Convert.ToString(SqlParms[5].Value) != "0"
                if (ds == null && ds.Tables.Count <= 0)
                {
                    Userdetail.user_Name = "";
                    Userdetail.user_ID = "0";
                    Userdetail.ErrorMessage = Convert.ToString(SqlParms[8].Value) + " Data Not Found";
                }
                else if (Convert.ToString(SqlParms[8].Value) == string.Empty || Convert.ToString(SqlParms[8].Value) == "")
                {
                    Userdetail.user_ID = Convert.ToString(SqlParms[9].Value); // ds.Tables[0].Rows[0]["User_ID"].ToString();
                    // Userdetail.Email_Id = ds.Tables[0].Rows[0]["Email_Id"].ToString();
                    //Userdetail.Phone_No1 = ds.Tables[0].Rows[0]["Phone_No1"].ToString();
                    // Userdetail.User_Name = ds.Tables[0].Rows[0]["User_Name"].ToString();
                    Userdetail.ErrorMessage = string.Empty;

                    // ResultDTO. = Convert.ToString(SqlParms[5].Value);
                }
                else
                {
                    Userdetail.user_Name = "";
                    Userdetail.user_ID = "0";
                    Userdetail.ErrorMessage = Convert.ToString(SqlParms[8].Value);
                }

            }
            catch (Exception ex)
            {
                Userdetail.user_Name = "";
                Userdetail.user_ID = "0";
                Userdetail.ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }

            return Userdetail;
        }
        #endregion

        public ParkingLocationList GetParkingIdAndNameById(int Id)
        {
            ParkingLocationList parkingLocationList = new ParkingLocationList();
            try
            {
                DataSet ds = null;
                SqlParameter[] SqlParms = new SqlParameter[2];

                SqlParms[0] = new SqlParameter("@ParkingId", SqlDbType.Int, 50);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = Id;

                SqlParms[1] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;


                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "GetParkingIdAndNameById", SqlParms);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    parkingLocationList.parking_id = Convert.ToInt32(ds.Tables[0].Rows[0]["Parking_id"]);
                    parkingLocationList.Parking_Name = Convert.ToString(ds.Tables[0].Rows[0]["parking_address"]);
                }
                else
                {
                    parkingLocationList.ErrorMessage = "Error Occured";

                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
            }
            return parkingLocationList;
        }

        public List<UserInboxBEL> GetUserNotification(string User_id)
        {
            List<UserInboxBEL> UserNotification = new List<UserInboxBEL>();
            UserInboxBEL userInboxBEL = new UserInboxBEL();
            try
            {

                DataSet ds = null;

                SqlParameter[] SqlParms = new SqlParameter[2];

                SqlParms[0] = new SqlParameter("@UserId", SqlDbType.NVarChar);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = User_id;



                SqlParms[1] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "GetNotificationByuserId", SqlParms);

                if (Convert.ToString(SqlParms[1].Value) == string.Empty && ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    UserNotification = (from DataRow NRow in ds.Tables[0].Rows
                                        select new UserInboxBEL
                                        {
                                            Notification_ID = Convert.ToInt32(NRow["ID"]),
                                            User_Id = Convert.ToInt32(NRow["User_Id"]),
                                            Subject = Convert.ToString(NRow["Subject"]),
                                            Message = Convert.ToString(NRow["Message"]),
                                            Dt_Created = Convert.ToDateTime(NRow["Dt_Created"]),
                                            READYN = Convert.ToChar(NRow["READYN"]),
                                            ActiveYN = Convert.ToChar(NRow["ActiveYN"]),
                                            UserSpecific = Convert.ToChar(NRow["UserSpecific"]),
                                            Parking_Id = Convert.ToInt32(NRow["ParkingID"]),
                                            Success = true
                                        }).ToList();

                }
                else
                {

                    userInboxBEL.Success = false;
                    userInboxBEL.ErrorMessage = "No records  found.";
                    UserNotification.Add(userInboxBEL);
                }

                return UserNotification;

            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
                userInboxBEL.Success = false;
                userInboxBEL.ErrorMessage = Convert.ToString(ex.Message.ToString());
                UserNotification.Add(userInboxBEL);
                return UserNotification;
            }
        }


        #region  GetExpiringBookings
        public List<UserBookingBEL> GetExpiringBookings(string ParkingID, out string Errormessage)
        {
            List<UserBookingBEL> ListUserBookingBEL = new List<UserBookingBEL>();
            // UserBookingBEL Userdetail = new UserBookingBEL();
            Errormessage = string.Empty;
            DataSet ds = null;
            try
            {

                SqlParameter[] SqlParms = new SqlParameter[2];

                SqlParms[0] = new SqlParameter("@ParkingID", SqlDbType.VarChar);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = ParkingID;

                SqlParms[1] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[1].Direction = ParameterDirection.Output;

                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "Get_ExpiringBookings", SqlParms);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (Convert.ToString(SqlParms[1].Value) == string.Empty || Convert.ToString(SqlParms[1].Value) == "")
                        {

                            ListUserBookingBEL = (from DataRow parkingrow in ds.Tables[0].Rows
                                                  select new UserBookingBEL
                                                  {
                                                      vehicleNumber = Convert.ToString(parkingrow["vehicalNumber"]),
                                                      BookedInOn = Convert.ToString(parkingrow["bookedIntime"]),
                                                      expiryDate = Convert.ToString(parkingrow["ExpiryDate"]),
                                                      planOpt = Convert.ToString(parkingrow["PlanOpt"]),
                                                      CheckedIn = Convert.ToString(parkingrow["CheckedInDateTime"]),
                                                      CheckedOut = Convert.ToString(parkingrow["CheckedOutDateTime"]),
                                                      BookingID = Convert.ToString(parkingrow["Booked_ID"]),
                                                      ErrorMessage = string.Empty,
                                                  }
                                             ).ToList<UserBookingBEL>();

                        }
                        else
                        {
                            //Userdetail.user_Name = "";
                            //Userdetail.user_ID = "0";
                            Errormessage = Convert.ToString(SqlParms[1].Value);
                        }
                    }
                    else
                    {
                        Errormessage = "No data found";
                    }
                }


            }
            catch (Exception ex)
            {
                //Userdetail.user_Name = "";
                //Userdetail.user_ID = "0";
                Errormessage = "DataBase Error Occured : " + ex.Message;

            }

            return ListUserBookingBEL;
        }
        #endregion

        #region TerminateExpiringBookings
        public string TerminateExpiringBooking(string BookingId, string NextAction)
        {
            //PBDetailsBEL Userdetail = new PBDetailsBEL();
            DataSet ds = null;
            string ErrMessage = string.Empty;
            try
            {
                //DataTable dataTable = new DataTable("ParkingLocationList");
                //dataTable.Columns.Add(new DataColumn("parking_id", typeof(int)));
                //dataTable.Columns.Add(new DataColumn("Parking_Name", typeof(string)));
                //foreach (var item in ListBookingIDs)
                //{
                //    dataTable.Rows.Add(item.Booking_ID);
                //}


                SqlParameter[] SqlParms = new SqlParameter[3];


                SqlParms[0] = new SqlParameter("@BookingID", SqlDbType.VarChar);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = BookingId;

                SqlParms[1] = new SqlParameter("@Action", SqlDbType.VarChar);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = NextAction;

                SqlParms[2] = new SqlParameter("@Error", SqlDbType.NVarChar, 150);
                SqlParms[2].Direction = ParameterDirection.Output;



                ds = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure, "UTL_TerminateExpiredPlanByBookingId", SqlParms);
                //&& Convert.ToString(SqlParms[5].Value) != "0"
                //if (ds == null && ds.Tables.Count <= 0)
                //{
                //    Userdetail.ErrorMessage =  Convert.ToString(SqlParms[2].Value) + " Data Not Found";
                //}
                //else 
                if (Convert.ToString(SqlParms[2].Value) == string.Empty || Convert.ToString(SqlParms[2].Value) == "")
                {
                    ErrMessage = string.Empty;
                    OTPBEL objSendSMS = new OTPBEL();
                    if (NextAction == "TNB")
                    {

                        objSendSMS.sendMessage(Convert.ToInt64(ds.Tables[0].Rows[0]["MobileNo"]),
                           "Greetings! Your Monthly Subscription for vehicle number " + Convert.ToString(ds.Tables[0].Rows[0]["VehicalNumber"]) + " has been expired today. After that you will be charged as per regular parking plan"
                       );
                    }
                    else
                    {
                        objSendSMS.sendMessage(Convert.ToInt64(ds.Tables[0].Rows[0]["MobileNo"]),
                               "Greetings! Your Monthly Subscription for vehicle number " + Convert.ToString(ds.Tables[0].Rows[0]["VehicalNumber"]) + " has been expired today. After that you will be charged as per regular parking plan"
                          );
                    }
                }
                else
                {

                    ErrMessage = Convert.ToString(SqlParms[2].Value);
                }

            }
            catch (Exception ex)
            {

                ErrMessage = "DataBase Error Occured : " + ex.Message;

            }

            return ErrMessage;
        }
        #endregion

         #region  Get_BriefReport_old
        public BookingBriefReportBEL Get_BriefReport_old(string period, string pbWorkingLocation, out string ErrorMessage)
        {
            UserBookingBEL Userdetail = new UserBookingBEL();
            DataSet ds = null;
            string fromdate = DateTime.Now.ToString();
            string Todate = DateTime.Now.ToString();
            BookingBriefReportBEL BBReport = new BookingBriefReportBEL();


            try
            {
                if (period.ToUpper() == "D")
                {
                    fromdate = DateTime.Now.ToString();
                    Todate = DateTime.Now.ToString();
                }
                else if (period.ToUpper() == "M")
                {
                    Todate = DateTime.Now.ToString();
                    fromdate = DateTime.Now.AddDays(-30).ToString();
                }
                SqlParameter[] SqlParms = new SqlParameter[5];

                SqlParms[0] = new SqlParameter("@ParkingID", SqlDbType.Int);
                SqlParms[0].Direction = ParameterDirection.Input;
                SqlParms[0].Value = pbWorkingLocation;


                SqlParms[1] = new SqlParameter("@FromDate", SqlDbType.VarChar);
                SqlParms[1].Direction = ParameterDirection.Input;
                SqlParms[1].Value = fromdate;

                SqlParms[2] = new SqlParameter("@ToDate", SqlDbType.VarChar);
                SqlParms[2].Direction = ParameterDirection.Input;
                SqlParms[2].Value = Todate;

                SqlParms[3] = new SqlParameter("@Period", SqlDbType.Char);
                SqlParms[3].Direction = ParameterDirection.Input;
                SqlParms[3].Value = period.ToUpper();

                SqlParms[4] = new SqlParameter("@Error", SqlDbType.VarChar, 150);
                SqlParms[4].Direction = ParameterDirection.Output;

                SqlDataReader rd;

                rd = SqlHelper.ExecuteReader(connectionString, CommandType.StoredProcedure, "Get_BriefReport", SqlParms);
                Dictionary<string, string> dics = new Dictionary<string, string>();
                if (Convert.ToString(SqlParms[4].Value) == string.Empty || Convert.ToString(SqlParms[4].Value) == "")
                {
                    while (rd.Read())
                    {
                        dics[(string)rd["monthlysubscription"]] = rd["counts"].ToString();
                    }
                    if (dics.Keys.Contains("RegularIn"))
                        BBReport.RegularIN = dics["RegularIn"].ToString();
                    else
                        BBReport.RegularIN = "0";

                    if (dics.Keys.Contains("RegularOut"))
                        BBReport.RegularOut = dics["RegularOut"].ToString();
                    else
                        BBReport.RegularOut = "0";

                    if (dics.Keys.Contains("RegularTot"))
                        BBReport.RegularTot = dics["RegularTot"].ToString();
                    else
                        BBReport.RegularTot = "0";

                    if (dics.Keys.Contains("MBTVehicleIn"))
                        BBReport.MBTVehicleIn = dics["MBTVehicleIn"].ToString();
                    else
                        BBReport.MBTVehicleIn = "0";

                    if (dics.Keys.Contains("MBTVehicleOut"))
                        BBReport.MBTVehicleOut = dics["MBTVehicleOut"].ToString();
                    else
                        BBReport.MBTVehicleOut = "0";

                    if (dics.Keys.Contains("MBTVehicleTot"))
                        BBReport.MBTVehicleTot = dics["MBTVehicleTot"].ToString();
                    else
                        BBReport.MBTVehicleTot = "0";

                    if (dics.Keys.Contains("MDTVehicleIn"))
                        BBReport.MDTVehicleIn = dics["MDTVehicleIn"].ToString();
                    else
                        BBReport.MDTVehicleIn = "0";

                    if (dics.Keys.Contains("MDTVehicleOut"))
                        BBReport.MDTVehicleOut = dics["MDTVehicleOut"].ToString();
                    else
                        BBReport.MDTVehicleOut = "0";

                    if (dics.Keys.Contains("MDTVehicleTot"))
                        BBReport.MDTVehicleTot = dics["MDTVehicleTot"].ToString();
                    else
                        BBReport.MDTVehicleTot = "0";

                    if (dics.Keys.Contains("MNTVehicleIn"))
                        BBReport.MNTVehicleIn = dics["MNTVehicleIn"].ToString();
                    else
                        BBReport.MNTVehicleIn = "0";

                    if (dics.Keys.Contains("MNTVehicleOut"))
                        BBReport.MNTVehicleOut = dics["MNTVehicleOut"].ToString();
                    else
                        BBReport.MNTVehicleOut = "0";

                    if (dics.Keys.Contains("MNTVehicleTot"))
                        BBReport.MNTVehicleTot = dics["MNTVehicleTot"].ToString();
                    else
                        BBReport.MNTVehicleTot = "0";


                    ErrorMessage = string.Empty;
                }
                else
                {
                    //Userdetail.user_Name = "";
                    //Userdetail.user_ID = "0";
                    ErrorMessage = Convert.ToString(SqlParms[4].Value);
                }

            }
            catch (Exception ex)
            {
                //Userdetail.user_Name = "";
                //Userdetail.user_ID = "0";
                ErrorMessage = "DataBase Error Occured : " + ex.Message;

            }
            return BBReport;


        }
        #endregion

    }


}
