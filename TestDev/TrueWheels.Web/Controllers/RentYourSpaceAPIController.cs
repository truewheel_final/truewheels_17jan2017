using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using TrueWheels.BEL;
using TrueWheels.DAL;
using TrueWheels.Web.Models;

namespace TrueWheels.Web.Controllers
{
    public class RentYourSpaceAPIController : ApiController
    {
        // GET: RentYourSpaceAPI
        [Route("api/Insert_RestSpace_RegParkingArea/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage Insert_RestSpace_RegParkingArea(RegisterParkingAreaBEL registerParkingAreaBEL)
        {
            var res = new HttpResponseMessage();
            try
            {

                JObject jContent = new JObject();
                ParkingAreaDAL Parkingdal = new ParkingAreaDAL();
                RegisterParkingAreaBEL ParkingBEL = new RegisterParkingAreaBEL();
                RegsiterParkingAreaResult ParkingResult = new RegsiterParkingAreaResult();
                if (registerParkingAreaBEL != null)
                {
                    ParkingResult.ErrorMessage = "";
                    if (registerParkingAreaBEL.owner_id == 0)
                        ParkingResult.ErrorMessage = "owner id can not be blank. ";
                    else
                        ParkingBEL.owner_id = registerParkingAreaBEL.owner_id;

                    //if (string.IsNullOrEmpty(registerParkingAreaBEL.Space_type))
                    //    ParkingResult.ErrorMessage += " Space_type can not be blank. ";
                    //else
                        ParkingBEL.Space_type = registerParkingAreaBEL.Space_type;

                    //if (string.IsNullOrEmpty(registerParkingAreaBEL.Property_type))
                    //    ParkingResult.ErrorMessage += " Property_type can not be blank. ";
                    //else
                        ParkingBEL.Property_type = registerParkingAreaBEL.Property_type;

                    ParkingBEL.No_of_space = registerParkingAreaBEL.No_of_space;

                    //ParkingBEL.VechileType = registerParkingAreaBEL.VechileType;

                    if (string.IsNullOrEmpty(registerParkingAreaBEL.PropertyAddress))
                        ParkingResult.ErrorMessage += " Property Address can not be blank. ";
                    else
                        ParkingBEL.PropertyAddress = registerParkingAreaBEL.PropertyAddress;

                    if (registerParkingAreaBEL.PropertyPinCode == 0)
                        ParkingResult.ErrorMessage += " PropertyPinCode can not be blank. ";
                    else
                        ParkingBEL.PropertyPinCode = registerParkingAreaBEL.PropertyPinCode;

                    //ParkingBEL.PropertyZone = registerParkingAreaBEL.PropertyZone;


                    ParkingBEL.PropertyLandMark = registerParkingAreaBEL.PropertyLandMark;

                    ParkingBEL.OwnerComments = registerParkingAreaBEL.OwnerComments;

                    if (string.IsNullOrEmpty(registerParkingAreaBEL.lattitude))
                        ParkingResult.ErrorMessage += " lattitude can not be blank. ";
                    else
                        ParkingBEL.lattitude = Convert.ToString(registerParkingAreaBEL.lattitude);

                    if (string.IsNullOrEmpty(registerParkingAreaBEL.longitude))
                        ParkingResult.ErrorMessage += " longitude can not be blank. ";
                    else
                        ParkingBEL.longitude = Convert.ToString(registerParkingAreaBEL.longitude);


                    ParkingBEL.parkingSpaceName = Convert.ToString(registerParkingAreaBEL.parkingSpaceName);

                    ParkingBEL.Status = Convert.ToString(registerParkingAreaBEL.Status);

                    
                    //if (string.IsNullOrEmpty(registerParkingAreaBEL.parkingAvailable))
                    //    ParkingResult.ErrorMessage += " parkingAvailable can not be blank. ";
                    //else
                    //    ParkingBEL.parkingAvailable = Convert.ToString(registerParkingAreaBEL.parkingAvailable);


                    //if (string.IsNullOrEmpty(registerParkingAreaBEL.TimeFrom))
                    //    ParkingResult.ErrorMessage += " datetimeFrom can not be blank. ";
                    //else
                        ParkingBEL.TimeFrom = registerParkingAreaBEL.TimeFrom;

                    //if (string.IsNullOrEmpty(registerParkingAreaBEL.TimeTo))
                    //    ParkingResult.ErrorMessage += " datetimeTo can not be blank. ";
                    //else
                        ParkingBEL.TimeTo = registerParkingAreaBEL.TimeTo;

                        ParkingBEL.BikeBasicCharge = registerParkingAreaBEL.BikeBasicCharge;

                        ParkingBEL.CarBasicCharge = registerParkingAreaBEL.CarBasicCharge;

                    ParkingBEL.open24hours = registerParkingAreaBEL.open24hours;

                    //ParkingBEL.description = registerParkingAreaBEL.description;

                    if (string.IsNullOrEmpty(registerParkingAreaBEL.daysSelected))
                        ParkingResult.ErrorMessage += " Select atleast a day. ";
                    else
                    {
                        //ParkingBEL.lstSelectedDays = new List<SelectedDays>();
                        //foreach (var item in registerParkingAreaBEL.lstSelectedDays)
                        ParkingBEL.daysSelected = registerParkingAreaBEL.daysSelected;
                    }

                    //ParkingBEL.lstDetailDescription = new List<DetailDescription>();
                    //foreach (var item in registerParkingAreaBEL.lstDetailDescription)
                    ParkingBEL.descriptionSelected = registerParkingAreaBEL.descriptionSelected;


                    //if (registerParkingAreaBEL.ListFacilityType != null)
                    //{
                    //    ParkingBEL.ListFacilityType = new List<FacilityType>();
                    //    foreach (var item in registerParkingAreaBEL.ListFacilityType)
                    ParkingBEL.facilitySelected = registerParkingAreaBEL.facilitySelected;
                    


                    // ** neeraj changes for daily , weeklyand monthly rates of bike and car on SYS 
                    ParkingBEL.CarDailyCharge = registerParkingAreaBEL.CarDailyCharge;

                    ParkingBEL.bikedailycharge = registerParkingAreaBEL.bikedailycharge;

                    //ParkingBEL.carweeklycharge = registerParkingAreaBEL.carweeklycharge;

                    //ParkingBEL.bikeweeklycharge = registerParkingAreaBEL.bikeweeklycharge;

                    ParkingBEL.carmonthlycharge = registerParkingAreaBEL.carmonthlycharge;

                    ParkingBEL.bikemonthycharge = registerParkingAreaBEL.bikemonthycharge;

                    //Added by neeraj as per whole Doc of APP

                    ParkingBEL.street = registerParkingAreaBEL.street;
                    ParkingBEL.city = registerParkingAreaBEL.city;
                    ParkingBEL.No_Of_Space_Avaiable_bike = registerParkingAreaBEL.No_Of_Space_Avaiable_bike;
                    ParkingBEL.Parking_Available_Date = registerParkingAreaBEL.Parking_Available_Date;
                    ParkingBEL.Hourly_Pricing = registerParkingAreaBEL.Hourly_Pricing;
                    ParkingBEL.state = registerParkingAreaBEL.state;
                 
                        ParkingBEL.Parking_support_no = registerParkingAreaBEL.Parking_support_no;
                   
                   // **// changes done

                    if (ParkingResult.ErrorMessage == "")
                    {
                        ParkingResult = Parkingdal.RegisterParkingArea(ParkingBEL);

                        if (ParkingResult.ErrorMessage == "" && ParkingResult.Parking_Id != 0)
                        {
                            jContent = new JObject(
                                     new JProperty("resultCode", 0),
                                     new JProperty("error", "Registered successfully"),
                                                     new JProperty("result",
                                                         new JObject(
                                                             new JProperty("Success", true),
                                                             new JProperty("Message", "Registered successfully"),
                                                             new JProperty("ParkingID", ParkingResult.Parking_Id))));

                        }
                        else
                        {
                            jContent = new JObject(
                                    new JProperty("resultCode", 1),
                                    new JProperty("error", "Error Occured"),
                                                    new JProperty("result",
                                                        new JObject(
                                                            new JProperty("Success", true),
                                                            new JProperty("Message", "Error Occured"),
                                                            new JProperty("ErrorMessage", ParkingResult.ErrorMessage))));
                        }
                    }
                    else
                    {
                        jContent = new JObject(
                                   new JProperty("resultCode", 2),
                                   new JProperty("error", "Input Null"),
                                                   new JProperty("result",
                                                       new JObject(
                                                           new JProperty("Success", false),
                                                           new JProperty("Message", "Input Null"),
                                                           new JProperty("ErrorMessage", ParkingResult.ErrorMessage))));
                    }
                }
                res.Content = new StringContent(jContent.ToString());
                return res;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
            }
            return res;
        }

        [Route("api/Deactivate_RestSpace/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage Deactivate_RestSpace(string parkingId, string ownerId)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            ParkingAreaDAL Parkingdal = new ParkingAreaDAL();
            //RegsiterParkingAreaResult result = new RegsiterParkingAreaResult();

            RegsiterParkingAreaResult ParkingResult = new RegsiterParkingAreaResult();
            //if (string.IsNullOrEmpty(parkingId) || string.IsNullOrEmpty(ownerId))
            //{
            //   // result.ErrorMessage = "owner id or parking id is null"
            //      jContent = new JObject(
            //                    new JProperty("resultCode", 0),
            //                    new JProperty("error", "owner id or parking id is null"),
            //                                    new JProperty("result",
            //                                        new JObject(
            //                                            new JProperty("Success", false),
            //                                            new JProperty("Message", "Error has occured."),
            //                                            new JProperty("ErrorMessage", result.ErrorMessage))));
            //      return jContent;
            //}
            var result = Parkingdal.Deactivate_RentSpace(parkingId, ownerId);
            if (result.Success == true)
            //if (result.ErrorMessage == "Deactivated successfully")
            {
                jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "Deactivated successfully"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "Deactivated successfully"))));
            }
            else
            {
                jContent = new JObject(
                                new JProperty("resultCode", 1),
                                new JProperty("error", "Error has occured."),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", "Error has occured."),
                                                        new JProperty("ErrorMessage", result.ErrorMessage))));
            }
            res.Content = new StringContent(jContent.ToString());
            return res;
        }

        [Route("api/Activate_RestSpace/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage Activate_RestSpace(string parkingId, string ownerId)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            ParkingAreaDAL Parkingdal = new ParkingAreaDAL();

            RegsiterParkingAreaResult ParkingResult = new RegsiterParkingAreaResult();
            //if (string.IsNullOrEmpty(parkingId) || string.IsNullOrEmpty(ownerId))
            //{
            //    jContent = new JObject(
            //                      new JProperty("resultCode", 3),
            //                      new JProperty("error", "Invalid owner id or parking id"),
            //                                      new JProperty("result",
            //                                          new JObject(
            //                                              new JProperty("Success", false),
            //                                              new JProperty("Message", "Invalid owner id or parking id"))));
            //}
            // res.Content = new StringContent(jContent.ToString());
            //return res;


            var result = Parkingdal.Activate_RentSpace(parkingId, ownerId);


            if (result.Success == true)
            {
                jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "Activated successfully"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "Activated successfully"))));
            }
            else
            {
                jContent = new JObject(
                                new JProperty("resultCode", 1),
                                new JProperty("error", "Error has occured."),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", "Error has occured."),
                                                        new JProperty("ErrorMessage", result.ErrorMessage))));
            }
            res.Content = new StringContent(jContent.ToString());
            return res;
        }


    [Route("api/Update_SYS_RegParkingArea/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage Update_SYS_RegParkingArea(RegisterParkingAreaBEL registerParkingAreaBEL)
        {
            var res = new HttpResponseMessage();
            try
            {
                JObject jContent = new JObject();
                ParkingAreaDAL Parkingdal = new ParkingAreaDAL();
                RegisterParkingAreaBEL ParkingBEL = new RegisterParkingAreaBEL();
                RegsiterParkingAreaResult ParkingResult = new RegsiterParkingAreaResult();
                if (registerParkingAreaBEL != null)
                {
                    ParkingResult.ErrorMessage = "";

                    if (registerParkingAreaBEL.owner_id == 0)
                        ParkingResult.ErrorMessage = "owner id can not be blank. ";
                    else
                        ParkingBEL.owner_id = registerParkingAreaBEL.owner_id;

                    //if (string.IsNullOrEmpty(registerParkingAreaBEL.Space_type))
                    //    ParkingResult.ErrorMessage += " Space_type can not be blank. ";
                    //else
                        ParkingBEL.Space_type = registerParkingAreaBEL.Space_type;

                    //if (string.IsNullOrEmpty(registerParkingAreaBEL.Property_type))
                    //    ParkingResult.ErrorMessage += " Property_type can not be blank. ";
                    //else
                        ParkingBEL.Property_type = registerParkingAreaBEL.Property_type;

                    ParkingBEL.No_of_space = registerParkingAreaBEL.No_of_space;

                    //ParkingBEL.VechileType = registerParkingAreaBEL.VechileType;

                    if (string.IsNullOrEmpty(registerParkingAreaBEL.PropertyAddress))
                        ParkingResult.ErrorMessage += " Property Address can not be blank. ";
                    else
                        ParkingBEL.PropertyAddress = registerParkingAreaBEL.PropertyAddress;

                    if (registerParkingAreaBEL.PropertyPinCode == 0)
                        ParkingResult.ErrorMessage += " PropertyPinCode can not be blank. ";
                    else
                        ParkingBEL.PropertyPinCode = registerParkingAreaBEL.PropertyPinCode;

                    //ParkingBEL.PropertyZone = registerParkingAreaBEL.PropertyZone;


                    ParkingBEL.PropertyLandMark = registerParkingAreaBEL.PropertyLandMark;

                    ParkingBEL.OwnerComments = registerParkingAreaBEL.OwnerComments;

                    if (string.IsNullOrEmpty(registerParkingAreaBEL.lattitude))
                        ParkingResult.ErrorMessage += " lattitude can not be blank. ";
                    else
                        ParkingBEL.lattitude = Convert.ToString(registerParkingAreaBEL.lattitude);

                    if (string.IsNullOrEmpty(registerParkingAreaBEL.longitude))
                        ParkingResult.ErrorMessage += " longitude can not be blank. ";
                    else
                        ParkingBEL.longitude = Convert.ToString(registerParkingAreaBEL.longitude);


                    ParkingBEL.parkingSpaceName = Convert.ToString(registerParkingAreaBEL.parkingSpaceName);

                    ParkingBEL.Status = Convert.ToString(registerParkingAreaBEL.Status);

                    //if (string.IsNullOrEmpty(registerParkingAreaBEL.parkingAvailable))
                    //    ParkingResult.ErrorMessage += " parkingAvailable can not be blank. ";
                    //else
                    //    ParkingBEL.parkingAvailable = Convert.ToString(registerParkingAreaBEL.parkingAvailable);


                    //if (string.IsNullOrEmpty(registerParkingAreaBEL.TimeFrom))
                    //    ParkingResult.ErrorMessage += " datetimeFrom can not be blank. ";
                    //else
                        ParkingBEL.TimeFrom = registerParkingAreaBEL.TimeFrom;

                    //if (string.IsNullOrEmpty(registerParkingAreaBEL.TimeTo))
                    //    ParkingResult.ErrorMessage += " datetimeTo can not be blank. ";
                    //else
                        ParkingBEL.TimeTo = registerParkingAreaBEL.TimeTo;

                    ParkingBEL.BikeBasicCharge = registerParkingAreaBEL.BikeBasicCharge;

                    ParkingBEL.CarBasicCharge = registerParkingAreaBEL.CarBasicCharge;

                    ParkingBEL.open24hours = registerParkingAreaBEL.open24hours;

                    //ParkingBEL.description = registerParkingAreaBEL.description;

                    ParkingBEL.parking_Id = registerParkingAreaBEL.parking_Id;


                    if (string.IsNullOrEmpty(registerParkingAreaBEL.daysSelected))
                        ParkingResult.ErrorMessage += " Select atleast a day. ";
                    else
                    {
                        //ParkingBEL.lstSelectedDays = new List<SelectedDays>();
                        //foreach (var item in registerParkingAreaBEL.lstSelectedDays)
                        ParkingBEL.daysSelected = registerParkingAreaBEL.daysSelected;
                    }

                    //ParkingBEL.lstDetailDescription = new List<DetailDescription>();
                    //foreach (var item in registerParkingAreaBEL.lstDetailDescription)
                    ParkingBEL.descriptionSelected = registerParkingAreaBEL.descriptionSelected;


                    //if (registerParkingAreaBEL.ListFacilityType != null)
                    //{
                    //    ParkingBEL.ListFacilityType = new List<FacilityType>();
                    //    foreach (var item in registerParkingAreaBEL.ListFacilityType)
                    ParkingBEL.facilitySelected = registerParkingAreaBEL.facilitySelected;
                    


                    //** changes by neeraj to update daily monthly and weekly price for car and bike
                    ParkingBEL.CarDailyCharge = registerParkingAreaBEL.CarDailyCharge;

                    ParkingBEL.bikedailycharge = registerParkingAreaBEL.bikedailycharge;

                    //ParkingBEL.carweeklycharge = registerParkingAreaBEL.carweeklycharge;

                    //ParkingBEL.bikeweeklycharge = registerParkingAreaBEL.bikeweeklycharge;

                    ParkingBEL.carmonthlycharge = registerParkingAreaBEL.carmonthlycharge;

                    ParkingBEL.bikemonthycharge = registerParkingAreaBEL.bikemonthycharge;

                    ParkingBEL.street = registerParkingAreaBEL.street;
                    ParkingBEL.city = registerParkingAreaBEL.city;
                    ParkingBEL.No_Of_Space_Avaiable_bike = registerParkingAreaBEL.No_Of_Space_Avaiable_bike;
                    ParkingBEL.Parking_Available_Date = registerParkingAreaBEL.Parking_Available_Date;
                    ParkingBEL.Hourly_Pricing = registerParkingAreaBEL.Hourly_Pricing;
                    ParkingBEL.state = registerParkingAreaBEL.state;
                    ParkingBEL.Parking_support_no = registerParkingAreaBEL.Parking_support_no;


                    //** changes done


                    if (ParkingResult.ErrorMessage == "")
                    {
                        ParkingResult = Parkingdal.UpdateSYSRegisterParkingArea(ParkingBEL);

                        if (ParkingResult.ErrorMessage == "" && ParkingResult.Parking_Id != 0)
                        {
                            jContent = new JObject(
                                     new JProperty("resultCode", 0),
                                     new JProperty("error", "Updated successfully"),
                                                     new JProperty("result",
                                                         new JObject(
                                                             new JProperty("Success", true),
                                                             new JProperty("Message", "Updated successfully"),
                                                             new JProperty("ParkingId", ParkingResult.Parking_Id))));

                        }
                        else
                        {
                            jContent = new JObject(
                                    new JProperty("resultCode", 1),
                                    new JProperty("error", "Error Occured"),
                                                    new JProperty("result",
                                                        new JObject(
                                                            new JProperty("Success", true),
                                                            new JProperty("Message", "Error Occured"),
                                                            new JProperty("ErrorMessage", ParkingResult.ErrorMessage))));
                        }
                    }
                    else
                    {
                        
                        jContent = new JObject(
                                  new JProperty("resultCode", 2),
                                  new JProperty("error", "Input Null"),
                                                  new JProperty("result",
                                                      new JObject(
                                                          new JProperty("Success", false),
                                                          new JProperty("Message", "Input Null"),
                                                          new JProperty("ErrorMessage", ParkingResult.ErrorMessage))));
                    }
                }
                res.Content = new StringContent(jContent.ToString());
                return res;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
            }
            return res;
        }




       





// new Select API by Neeraj Pandey

        [Route("api/GetAllRYSByOwnerID/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetAllRYSByOwnerID( string ownerId)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
           // Random ran = new Random();
           
                if(ownerId != null && !string.IsNullOrWhiteSpace(ownerId) && !string.IsNullOrEmpty(ownerId))
                {
                    try
                    {
                        ParkingAreaDAL Parkingdal = new ParkingAreaDAL();

                        List<RegisterSYSParkingAreaResult> ListParkingResult = new List<RegisterSYSParkingAreaResult>();
                        string Errormessage = string.Empty;
                        ListParkingResult = Parkingdal.Select_RentSpace( ownerId, out Errormessage);
                        string json = string.Empty;
                        if (ListParkingResult.Count()>0 && Errormessage == "" && string.IsNullOrEmpty(Errormessage) && string.IsNullOrWhiteSpace(Errormessage) )
                        {
                            json = JsonConvert.SerializeObject(ListParkingResult.ToArray());
                            var jsonArray = JArray.Parse(json);
                            jContent = new JObject(
                                    new JProperty("resultCode", 0),
                                    new JProperty("Success", true),
                                     
                                               new JProperty("result",
                                                  new JObject(
                                                       //new JProperty("authToken", objUserBEL.AuthToken),
                                                       new JProperty("Parkings", jsonArray))));
                           

                        }
                        else
                        {
                            json = JsonConvert.SerializeObject(ListParkingResult.ToArray());
                            var jsonArray = JArray.Parse(json);
                            jContent = new JObject(
                                //new JProperty("resultCode", 1),
                                //new JProperty("error", "Error has occured."),
                                                            new JProperty("result",
                                                                new JObject(
                                                                     new JProperty("Parkings", jsonArray))));
                                                                    //new JProperty("Success", false),
                                                                    //new JProperty("Message", "Error has occured."),
                                                                    //new JProperty("ErrorMessage", Errormessage))));
                        }
         
                    }
                
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                            new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }
                else
                {
                    
                    jContent = new JObject(
                        new JProperty("resultCode", 3),
                        new JProperty("error", "Invalid Owner Id"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", "Owner Id cannot be empty"),
                                                   new JProperty("Transation", null))));
                }
            
           
            res.Content = new StringContent(jContent.ToString());
            return res;

        }

        [Route("api/BookParking/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage BookParking(BookSYSParking bookSYSParking)
        {
            var res = new HttpResponseMessage();
            try
            {
                JObject jContent = new JObject();
                ParkingAreaDAL Parkingdal = new ParkingAreaDAL();
                BookSYSParking bookSYSParkingBEL = new BookSYSParking();
                if (bookSYSParking != null)
                {
                    if (bookSYSParking.login_userId != 0)
                        bookSYSParkingBEL.login_userId = bookSYSParking.login_userId;
                    else
                        bookSYSParkingBEL.Error += "Login User Id should be greater than zero.";

                    if (bookSYSParking.owner_userId != 0)
                        bookSYSParkingBEL.owner_userId = bookSYSParking.owner_userId;
                    else
                        bookSYSParkingBEL.Error += "Owner User Id should be greater than zero.";

                    if (!string.IsNullOrEmpty(bookSYSParking.parkingId))
                        bookSYSParkingBEL.parkingId = bookSYSParking.parkingId;
                    else
                        bookSYSParkingBEL.Error += "Parking Id should not be null and greater than zero.";

                    //if (bookSYSParking.bookedInTime))
                    bookSYSParkingBEL.bookedInTime = bookSYSParking.bookedInTime;
                    //else
                    //    bookSYSParkingBEL.Error += "checkedInDateTime cannot not be null.";

                    // if (!string.IsNullOrEmpty(bookSYSParking.bookedOutDatetime))
                    bookSYSParkingBEL.bookedOutDatetime = bookSYSParking.bookedOutDatetime;
                    //else
                    //    bookSYSParkingBEL.Error += "bookedOutDatetime cannot not be null.";

                    if (!string.IsNullOrEmpty(bookSYSParking.vehicalNo))
                        bookSYSParkingBEL.vehicalNo = bookSYSParking.vehicalNo;
                    else
                        bookSYSParkingBEL.Error += "vehicalNo cannot not be null.";

                    if (!string.IsNullOrEmpty(bookSYSParking.vehicleType))
                        bookSYSParkingBEL.vehicleType = bookSYSParking.vehicleType;
                    else
                        bookSYSParkingBEL.Error += "vehicleType cannot not be null.";

                    if (!string.IsNullOrEmpty(bookSYSParking.paymentMode))
                        bookSYSParkingBEL.paymentMode = bookSYSParking.paymentMode;
                    else
                        bookSYSParkingBEL.Error += "paymentMode cannot not be null.";

                    if (!string.IsNullOrEmpty(bookSYSParking.paymentAmount))
                        bookSYSParkingBEL.paymentAmount = bookSYSParking.paymentAmount;
                    else
                        bookSYSParkingBEL.Error += "paymentAmount cannot not be null.";

                    bookSYSParkingBEL.vehicleWheel = bookSYSParking.vehicleWheel;

                    bookSYSParkingBEL.monthlySubscription = bookSYSParking.monthlySubscription;

                    bookSYSParkingBEL.hourBaseParking = bookSYSParking.hourBaseParking;

                    // bookSYSParkingBEL.bookingStatus = bookSYSParking.bookingStatus;

                    if (bookSYSParkingBEL.Error == "" || bookSYSParkingBEL.Error == null)
                    {
                        var result = Parkingdal.BookParking(bookSYSParkingBEL);
                        if (result.Error == "" && result.bookingID != 0)
                        {
                            jContent = new JObject(
                                     new JProperty("resultCode", 0),
                                     new JProperty("error", "Booked successfully"),
                                                     new JProperty("result",
                                                         new JObject(
                                                             new JProperty("Success", true),
                                                             new JProperty("Message", "Booked successfully"),
                                                             new JProperty("BookingID", result.bookingID))));

                        }
                        else
                        {
                            jContent = new JObject(
                                    new JProperty("resultCode", 1),
                                    new JProperty("error", "Error Occured"),
                                                    new JProperty("result",
                                                        new JObject(
                                                            new JProperty("Success", true),
                                                            new JProperty("Message", "Error Occured"),
                                                            new JProperty("ErrorMessage", result.Error))));
                        }
                    }
                    else
                    {
                        jContent = new JObject(
                                   new JProperty("resultCode", 2),
                                   new JProperty("error", "Input Null"),
                                                   new JProperty("result",
                                                       new JObject(
                                                           new JProperty("Success", false),
                                                           new JProperty("Message", "Input Null"),
                                                           new JProperty("ErrorMessage", bookSYSParkingBEL.Error))));
                    }

                }
                res.Content = new StringContent(jContent.ToString());
                return res;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
            }
            return res;
        }

  
   //added by neeraj to get All RYS Parking on MAP

        [Route("api/GetPrivateParkingSYS/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetPrivateParkingSYS(ParkingAreaBEL parking)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            if (parking != null)
            {
                try
                {

                    ParkingAreaDAL Parkingdal = new ParkingAreaDAL();
                    RegisterParkingAreaBEL ParkingBEL = new RegisterParkingAreaBEL();
                    string Errormessage = string.Empty;
                   // List<RegsiterParkingAreaResult> ParkingResult = new List<RegsiterParkingAreaResult>();
                    ParkingAreaDAL parkingDal = new ParkingAreaDAL();
                   // ParkingAreaBEL parking = new ParkingAreaBEL();
                    List<AvailableParkingAreaRYSResult> availableParkingRYSList = parkingDal.GetAvailableParkingRYS(parking);
                    string json = string.Empty;
                    if (availableParkingRYSList.Count() > 0 && Errormessage == "" && string.IsNullOrEmpty(Errormessage) && string.IsNullOrWhiteSpace(Errormessage))
                    {
                        json = JsonConvert.SerializeObject(availableParkingRYSList.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("Success", true),

                                           new JProperty("result",
                                              new JObject(
                            //new JProperty("authToken", objUserBEL.AuthToken),
                                                   new JProperty("Parkings", jsonArray))));


                    }
                    else
                    {
                        jContent = new JObject(
                                        new JProperty("resultCode", 1),
                                        new JProperty("error", "No Parking Available"),
                                                        new JProperty("result",
                                                            new JObject(
                                                                new JProperty("Success", false),
                                                                new JProperty("Message", "No Parking Available"),
                                                                new JProperty("ErrorMessage", Errormessage))));
                    }

                }

                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 2),
                        new JProperty("error", "Exception Occured"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                   new JProperty("Transation", null))));

                }
            }

            else
            {

                jContent = new JObject(
                    new JProperty("resultCode", 3),
                    new JProperty("error", "Invalid Owner Id"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", "Owner Id cannot be empty"),
                                               new JProperty("Transation", null))));
            }


            res.Content = new StringContent(jContent.ToString());
            return res;
                    }
        
        
        
        
        // Get all System Codes by Neeraj Pandey

        [Route("api/GetAllSystemCodes/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetAllSystemCodes()
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            // Random ran = new Random();

            //if (ownerId != null && !string.IsNullOrWhiteSpace(ownerId) && !string.IsNullOrEmpty(ownerId))
            //{
                try
                {
                    ParkingAreaDAL Parkingdal = new ParkingAreaDAL();

                    StandardCodesResponse standardCodesResponse = new StandardCodesResponse();
                    List<SystemCodes> ListSystemCodes = new List<SystemCodes>();
                    List<StandardCodesResponse> ListStandardCodesResponse = new List<StandardCodesResponse>();
                    string Errormessage = string.Empty;
                    ListStandardCodesResponse = Parkingdal.Get_AllCodes(out Errormessage);
                    string json = string.Empty;
                    if (ListStandardCodesResponse.Count > 0 && Errormessage == "" && string.IsNullOrEmpty(Errormessage) && string.IsNullOrWhiteSpace(Errormessage))
                    {
                        json = JsonConvert.SerializeObject(ListStandardCodesResponse.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("Success", true),

                                           new JProperty("result",
                                              new JObject(
                            //new JProperty("authToken", objUserBEL.AuthToken),
                                                   new JProperty("SystemCodes", jsonArray))));


                    }
                    else
                    {
                        jContent = new JObject(
                                        new JProperty("resultCode", 1),
                                        new JProperty("error", "Error has occured."),
                                                        new JProperty("result",
                                                            new JObject(
                                                                new JProperty("Success", false),
                                                                new JProperty("Message", "Error has occured."),
                                                                new JProperty("ErrorMessage", Errormessage))));
                    }

                }

                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 2),
                        new JProperty("error", "Exception Occured"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                   new JProperty("Transation", null))));

                }
           // }
            //else
            //{

            //    jContent = new JObject(
            //        new JProperty("resultCode", 3),
            //        new JProperty("error", "Invalid Owner Id"),
            //                           new JProperty("result",
            //                               new JObject(
            //                                   new JProperty("Success", false),
            //                                   new JProperty("Message", "Invalid Input"),
            //                                   new JProperty("ErrorMessage", "Owner Id cannot be empty"),
            //                                   new JProperty("Transation", null))));
            //}


            res.Content = new StringContent(jContent.ToString());
            return res;
    

        }


          // Added by Neeraj To get Booking Made
        [Route("api/GetBookingMadeRYSByOwnerID/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetSYSBookingMade(string ownerId)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            // Random ran = new Random();

            if (ownerId != null && !string.IsNullOrWhiteSpace(ownerId) && !string.IsNullOrEmpty(ownerId))
            {
                try
                {
                    ParkingAreaDAL Parkingdal = new ParkingAreaDAL();

                    List<BookingMadeSYS> bookingMadeSYS = new List<BookingMadeSYS>();
                    string Errormessage = string.Empty;
                    bookingMadeSYS = Parkingdal.Booking_Made(ownerId, out Errormessage);
                    string json = string.Empty;
                    if (bookingMadeSYS.Count() > 0 && Errormessage == "" && string.IsNullOrEmpty(Errormessage) && string.IsNullOrWhiteSpace(Errormessage))
                    {
                        json = JsonConvert.SerializeObject(bookingMadeSYS.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("Success", true),

                                           new JProperty("result",
                                              new JObject(
                            //new JProperty("authToken", objUserBEL.AuthToken),
                                                   new JProperty("Booking Made", jsonArray))));


                    }
                    else
                    {
                        json = JsonConvert.SerializeObject(bookingMadeSYS.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                                       // new JProperty("resultCode", 1),
                                       // new JProperty("error", "No Booking Found"),
                                                        new JProperty("result",
                                                            new JObject(
                                                                new JProperty("Booking Made", jsonArray))));
                                                                //new JProperty("Success", false),
                                                                //new JProperty("Message", "No Booking Found"),
                                                                //new JProperty("ErrorMessage", Errormessage))));
                    }

                }

                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 2),
                        new JProperty("error", "Exception Occured"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                   new JProperty("Transation", null))));

                }
            }
            else
            {

                jContent = new JObject(
                    new JProperty("resultCode", 3),
                    new JProperty("error", "Invalid Owner Id"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", "Owner Id cannot be empty"),
                                               new JProperty("Transation", null))));
            }


            res.Content = new StringContent(jContent.ToString());
            return res;

        }

         // Added by Neeraj To get Booking Recieved
        [Route("api/GetBookingRecievedRYSByOwnerID/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetSYSBookingRecieved(string ownerId)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            // Random ran = new Random();

            if (ownerId != null && !string.IsNullOrWhiteSpace(ownerId) && !string.IsNullOrEmpty(ownerId))
            {
                try
                {
                    ParkingAreaDAL Parkingdal = new ParkingAreaDAL();

                    List<BookingRecievedSYS> abc = new List<BookingRecievedSYS>();
                    string Errormessage = string.Empty;
                    abc = Parkingdal.Booking_Recieved(ownerId, out Errormessage);
                    string json = string.Empty;
                    if (abc.Count() > 0 && Errormessage == "" && string.IsNullOrEmpty(Errormessage) && string.IsNullOrWhiteSpace(Errormessage))
                    {
                        json = JsonConvert.SerializeObject(abc.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("Success", true),

                                           new JProperty("result",
                                              new JObject(
                            //new JProperty("authToken", objUserBEL.AuthToken),
                                                   new JProperty("Booking Recieved", jsonArray))));


                    }
                    else
                    {
                        json = JsonConvert.SerializeObject(abc.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                                        //new JProperty("resultCode", 1),
                                        //new JProperty("error", "Booking Not Recieved"),
                                                        new JProperty("result",
                                                            new JObject(
                                                                 new JProperty("Booking Recieved", jsonArray))));
                                                                //new JProperty("Success", false),
                                                                //new JProperty("Message", "Booking Not Recieved"),
                                                                //new JProperty("ErrorMessage", Errormessage))));
                    }

                }

                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 2),
                        new JProperty("error", "Exception Occured"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                   new JProperty("Transation", null))));

                }
            }
            else
            {

                jContent = new JObject(
                    new JProperty("resultCode", 3),
                    new JProperty("error", "Invalid Owner Id"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", "Owner Id cannot be empty"),
                                               new JProperty("Transation", null))));
            }


            res.Content = new StringContent(jContent.ToString());
            return res;

        }
 [Route("api/GetEstimatedCost/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetEstimatedCost(CostEstimationModel costEstimationModel)
        {
            string json = string.Empty;
            var res = new HttpResponseMessage();
            JObject jContent = new JObject(); 
            if (costEstimationModel != null)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        ParkingAreaDAL parkingAreaDAL = new ParkingAreaDAL();

                        GetEstimatedCostBEL estimate = new GetEstimatedCostBEL();
                        estimate = parkingAreaDAL.EstimatedCost(costEstimationModel.BookedIntime, costEstimationModel.BookedOutTime,
                              costEstimationModel.Parking_ID, costEstimationModel.Vehicle_Type, costEstimationModel.MonthlySuscription);
                        if (!string.IsNullOrEmpty(estimate.Error))
                        {
                            //res.StatusCode = HttpStatusCode.Unauthorized;
                            jContent = new JObject(
                                                new JProperty("resultCode", 1),
                                                new JProperty("error", "Failed"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", "Error Occured"),
                                                        new JProperty("ErrorMessage", estimate.Error),
                                                        new JProperty("EstimateCost", "0")
                                                       )));
                        }
                        else
                        {
                            //res.Content = new StringContent(json);
                            jContent = new JObject(
                                                new JProperty("resultCode", 0),
                                                new JProperty("error", "Success"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "Success"),
                                                         new JProperty("TotalAmount", estimate.TotalAmount),
                                                         new JProperty("Totalhours", estimate.Totalhours),
                                                         new JProperty("TotalMinutes", estimate.TotalMinutes),
                                                         new JProperty("BookingTDSAmount", estimate.BookingTDSAmount),
                                                         new JProperty("RYSTDSAmount", estimate.RYSTDSAmount),
                                                         new JProperty("PayableToRYSAmount", estimate.PayableToRYSAmount),
                                                         new JProperty("Totalmonths", estimate.Totalmonths),
                                                         new JProperty("TWServiceCharge", estimate.TWServiceCharge),
                                                         new JProperty("BaseAmount", estimate.BaseAmount),
                                                         new JProperty("TotalDays", estimate.TotalDays)
                                                       )));
                        }


                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                                    new JProperty("error", "Failed"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }

                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();

                    var exceptionList = (from item in ModelState
                                         where item.Value.Errors.Any()
                                         select Convert.ToString(item.Value.Errors[0].Exception)).ToList();
                    errorList.AddRange(exceptionList);
                    //res.StatusCode = HttpStatusCode.BadRequest;

                    List<string> g = new List<string>();
                    jContent = new JObject(
                                new JProperty("resultCode", 3),
                                new JProperty("error", "Invalid Input"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),

                                                   new JProperty("ErrorMessage", errorList), //g = (errorList.Count < 1) ? exceptionList : errorList
                                                   new JProperty("Transation", null))));
                }
                
            }
            res.Content = new StringContent (jContent.ToString());
            return res;
        }

    
        // new on api on Individual Parking detail search 


        [Route("api/GetIndividualParkingDetails/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage OnParkingBubbleClick(string Parking_id)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            // Random ran = new Random();

            if (Parking_id != null && !string.IsNullOrWhiteSpace(Parking_id) && !string.IsNullOrEmpty(Parking_id))
            {
                try
                {
                    ParkingAreaDAL Parkingdal = new ParkingAreaDAL();

                    List<OnParkingBubbleClick> onParkingBubbleClick = new List<OnParkingBubbleClick>();
                    string Errormessage = string.Empty;
                    onParkingBubbleClick = Parkingdal.Parking_Click(Parking_id, out Errormessage);
                    string json = string.Empty;
                    if (onParkingBubbleClick.Count() > 0 && Errormessage == "" && string.IsNullOrEmpty(Errormessage) && string.IsNullOrWhiteSpace(Errormessage))
                    {
                        json = JsonConvert.SerializeObject(onParkingBubbleClick.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("Success", true),

                                           new JProperty("result",
                                              new JObject(
                            //new JProperty("authToken", objUserBEL.AuthToken),
                                                   new JProperty("ParkingDetail", jsonArray))));


                    }
                    else
                    {

                        json = JsonConvert.SerializeObject(onParkingBubbleClick.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                                        //new JProperty("resultCode", 1),
                                        //new JProperty("error", "Error has occured."),
                                                        new JProperty("result",
                                                            new JObject(
                                                                 new JProperty("ParkingDetail", jsonArray))));
                                                                //new JProperty("Success", false),
                                                                //new JProperty("Message", "Error has occured."),
                                                                //new JProperty("ErrorMessage", Errormessage))));
                    }

                }

                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 2),
                        new JProperty("error", "Exception Occured"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                   new JProperty("Transation", null))));

                }
            }
            else
            {

                jContent = new JObject(
                    new JProperty("resultCode", 3),
                    new JProperty("error", "Invalid Owner Id"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", "Owner Id cannot be empty"),
                                               new JProperty("Transation", null))));
            }


            res.Content = new StringContent(jContent.ToString());
            return res;

        }
          [Route("api/OnProfileClick/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage OnProfileClick(string User_id)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            // Random ran = new Random();

            if (User_id != null && !string.IsNullOrWhiteSpace(User_id) && !string.IsNullOrEmpty(User_id))
            {
                try
                {
                    ParkingAreaDAL Parkingdal = new ParkingAreaDAL();

                    List<OnProfileClick> onProfileClick = new List<OnProfileClick>();
                    string Errormessage = string.Empty;
                    onProfileClick = Parkingdal.Profile_Click(User_id, out Errormessage);
                    string json = string.Empty;
                    if (onProfileClick.Count() > 0 && Errormessage == "" && string.IsNullOrEmpty(Errormessage) && string.IsNullOrWhiteSpace(Errormessage))
                    {
                        json = JsonConvert.SerializeObject(onProfileClick.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("Success", true),

                                           new JProperty("result",
                                              new JObject(
                            //new JProperty("authToken", objUserBEL.AuthToken),
                                                   new JProperty("UserDetails", jsonArray))));


                    }
                    else
                    {
                        json = JsonConvert.SerializeObject(onProfileClick.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                                        //new JProperty("resultCode", 1),
                                        //new JProperty("error", "Error has occured."),
                                                        new JProperty("result",
                                                            new JObject(
                                                                 new JProperty("UserDetails", jsonArray))));
                    //                                            new JProperty("Success", false),
                    //                                            new JProperty("Message", "Error has occured."),
                    //                                            new JProperty("ErrorMessage", Errormessage))));
                    }

                }

                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 2),
                        new JProperty("error", "Exception Occured"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                   new JProperty("Transation", null))));

                }
            }
            else
            {

                jContent = new JObject(
                    new JProperty("resultCode", 3),
                    new JProperty("error", "Invalid Owner Id"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", "Owner Id cannot be empty"),
                                               new JProperty("Transation", null))));
            }


            res.Content = new StringContent(jContent.ToString());
            return res;

        }
      public string callPaymentgateway(SetPaymentModel spm, out string Error)
    {
        string Transaction_id = "0";
        if (spm.paymentMode == "COD")
        {
            Transaction_id = "-1";
            Error = "";
        }
        else
        {
            Transaction_id = ""; // call payment gatway api
            Error = ""; 
        }
        return Transaction_id;
                 
    }

        [Route("api/SetPayment/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage SetPayment(SetPaymentModel setPaymentModel)
        {
            string json = string.Empty;
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            if (setPaymentModel != null)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        string PaymentError = string.Empty;
                      setPaymentModel.Transaction_Id =  callPaymentgateway(setPaymentModel, out  PaymentError);
                      if (!String.IsNullOrEmpty(PaymentError) && setPaymentModel.Transaction_Id == "0")
                      {
                          jContent = new JObject(
                                                 new JProperty("resultCode", 1),
                                                 new JProperty("error", "Payment Failed"),
                                                 new JProperty("result",
                                                     new JObject(
                                                         new JProperty("Success", false),
                                                         new JProperty("Message", "Error Occured"),
                                                         new JProperty("ErrorMessage", "Payment Failed")
                                                                                                                )));
                      }
                      else
                      {
                          ParkingAreaDAL parkingAreaDAL = new ParkingAreaDAL();
                          //RYSBookSpaceRequest rbs = new RYSBookSpaceRequest();
                          BookSYSParking bookSYSParking = new BookSYSParking();
                          bookSYSParking.login_userId = setPaymentModel.login_userId;
                          bookSYSParking.owner_userId = setPaymentModel.login_userId;
                          bookSYSParking.parkingId = setPaymentModel.parkingId;
                          //bookSYSParking.Bank_Name = setPaymentModel.Bank_Name;
                          //bookSYSParking.Card_Type = setPaymentModel.Card_Type;
                          bookSYSParking.vehicalNo = setPaymentModel.vehicalNo;
                          //bookSYSParking.mobileNo = setPaymentModel.mobileNo;
                          bookSYSParking.bookedInTime = setPaymentModel.bookedInTime;
                          bookSYSParking.bookedOutDatetime = setPaymentModel.bookedOutDatetime;
                          bookSYSParking.checkedInDateTime = setPaymentModel.checkedInDateTime;
                          bookSYSParking.OTP = setPaymentModel.OTP;
                          bookSYSParking.vehicleType = setPaymentModel.vehicleType;
                          bookSYSParking.paymentMode = setPaymentModel.paymentMode;
                          //bookSYSParking.bookingID = setPaymentModel.bookingID;
                          //bookSYSParking.vehicleWheel = setPaymentModel.vehicleWheel;
                          bookSYSParking.paymentAmount = setPaymentModel.paymentAmount;
                          bookSYSParking.services = setPaymentModel.services;
                          bookSYSParking.vehicleWheel = setPaymentModel.vehicleWheel;
                          bookSYSParking.monthlySubscription = setPaymentModel.monthlySubscription;
                          bookSYSParking.BookingStatus = setPaymentModel.BookingStatus;
                          bookSYSParking.BaseAmount = setPaymentModel.BaseAmount;
                          bookSYSParking.PayableToRYSAmount = setPaymentModel.PayableToRYSAmount;
                          bookSYSParking.BookingTDSAmount = setPaymentModel.BookingTDSAmount;
                          bookSYSParking.TotalAmount = setPaymentModel.TotalAmount;
                          bookSYSParking.TWServiceCharge = setPaymentModel.TWServiceCharge;
                          bookSYSParking.RYSTDSAmount = setPaymentModel.RYSTDSAmount;
                          bookSYSParking.Transaction_ID = setPaymentModel.Transaction_Id;








                          SYSPaymentResponse pay = new SYSPaymentResponse();
                          pay = parkingAreaDAL.RYSBookSpace(bookSYSParking);

                          
                          //estimate = parkingAreaDAL.EstimatedCost(SetPaymentModel.BookedIntime, SetPaymentModel.BookedOutTime,
                          //      SetPaymentModel.Parking_ID, SetPaymentModel.Vehicle_Type, SetPaymentModel.MonthlySuscription);
                          if (!string.IsNullOrEmpty(pay.Error))
                          {
                              //res.StatusCode = HttpStatusCode.Unauthorized;
                              jContent = new JObject(
                                                  new JProperty("resultCode", 1),
                                                  new JProperty("error", "Failed"),
                                                  new JProperty("result",
                                                      new JObject(
                                                          new JProperty("Success", false),
                                                          new JProperty("Message", "Error Occured"),
                                                          new JProperty("ErrorMessage", pay.Error),
                                                          new JProperty("EstimateCost", "0")
                                                         )));
                          }
                          else
                          {
                              //res.Content = new StringContent(json);
                              jContent = new JObject(
                                                  new JProperty("resultCode", 0),
                                                  new JProperty("error", "Success"),
                                                  new JProperty("result",
                                                      new JObject(
                                                          new JProperty("Success", true),
                                                          new JProperty("Message", "Success"),
                                                           new JProperty("Attendant_Phone_No", pay.Attendant_Phone_No),
                                                           new JProperty("Attendant_Name", pay.Attendant_Name),
                                                           new JProperty("Owner Name ", pay.Full_Name),
                                                           new JProperty("Owner Number", pay.Parking_support_no),
                                                           new JProperty("Owner_Address", pay.Owner_Address),
                                                           new JProperty("Paid Amount", pay.PaidAmount),
                                                           new JProperty("Booking ID", pay.Booking_ID),
                                                           new JProperty("Payment Transaction ID", pay.Payment_Transaction_ID)
                                                         )));
                          }
                      }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                                    new JProperty("error", "Failed"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }

                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();

                    var exceptionList = (from item in ModelState
                                         where item.Value.Errors.Any()
                                         select Convert.ToString(item.Value.Errors[0].Exception)).ToList();
                    errorList.AddRange(exceptionList);
                    //res.StatusCode = HttpStatusCode.BadRequest;

                    List<string> g = new List<string>();
                    jContent = new JObject(
                                new JProperty("resultCode", 3),
                                new JProperty("error", "Invalid Input"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),

                                                   new JProperty("ErrorMessage", errorList), //g = (errorList.Count < 1) ? exceptionList : errorList
                                                   new JProperty("Transation", null))));
                }

            }
            res.Content = new StringContent(jContent.ToString());
            return res;
        }
        
        
        [Route("api/VehicleInfo/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage VehicleInfo(string User_id)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            // Random ran = new Random();

            if (User_id != null && !string.IsNullOrWhiteSpace(User_id) && !string.IsNullOrEmpty(User_id))
            {
                try
                {
                    ParkingAreaDAL Parkingdal = new ParkingAreaDAL();

                    List<VehicleInfo> vehicleInfo = new List<VehicleInfo>();
                    string Errormessage = string.Empty;
                    vehicleInfo = Parkingdal.Vehicle_Info(User_id, out Errormessage);
                    string json = string.Empty;
                    if (vehicleInfo.Count() > 0 && Errormessage == "" && string.IsNullOrEmpty(Errormessage) && string.IsNullOrWhiteSpace(Errormessage))
                    {
                        json = JsonConvert.SerializeObject(vehicleInfo.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("Success", true),

                                           new JProperty("result",
                                              new JObject(
                            //new JProperty("authToken", objUserBEL.AuthToken),
                                                   new JProperty("UserDetails", jsonArray))));


                    }
                    else
                    {
                        json = JsonConvert.SerializeObject(vehicleInfo.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                                        //new JProperty("resultCode", 1),
                                        //new JProperty("error", "Error has occured."),
                                                        new JProperty("result",
                                                            new JObject(
                                                                new JProperty("UserDetails", jsonArray))));
                                                                //new JProperty("Success", false),
                                                                //new JProperty("Message", "Error has occured."),
                                                                //new JProperty("ErrorMessage", Errormessage))));
                    }

                }

                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 2),
                        new JProperty("error", "Exception Occured"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                   new JProperty("Transation", null))));

                }
            }
            else
            {

                jContent = new JObject(
                    new JProperty("resultCode", 3),
                    new JProperty("error", "Invalid Owner Id"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", "Owner Id cannot be empty"),
                                               new JProperty("Transation", null))));
            }


            res.Content = new StringContent(jContent.ToString());
            return res;

        }

        [Route("api/UpdateCheckOut/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage UpdateCheckOut(string bookingId, DateTime CheckInOut, string whodidcheckoutID)
        {
            var res = new HttpResponseMessage();
            //ParkingAreaDAL Parkingdal = new ParkingAreaDAL();
            JObject jContent = new JObject();
            ParkingAreaDAL parkingAreaDAL = new ParkingAreaDAL();

            //CheckInCheckOutAPIModel CIO= new CheckInCheckOutAPIModel()
            string Errormessage = string.Empty;
            var result = parkingAreaDAL.UpdateCheckInCheckOut(bookingId, CheckInOut, whodidcheckoutID, out Errormessage);
            //CheckoutVehicle cov = new CheckoutVehicle();
            //cov = parkingAreaDAL.UpdateCheckInCheckOut(bookingId, CheckInOut);
            string json = string.Empty;
            if (result.Status == true)
            {
                //checkInCheckOutBEL.Status = true;
                //checkInCheckOutBEL.Error = "Updated Succesfully";
                jContent = new JObject(
                                    new JProperty("resultCode", 0),
                    ////new JProperty("error", "Checked Out successfully"),
                                                    new JProperty("result",
                                                        new JObject(
                                                            new JProperty("Success", true),
                                                            new JProperty("Message", "Checked Out successfully"),
                                                            new JProperty("Status", result.Status),
                                                            new JProperty("TotalAmount", result.TotalAmount),
                                                            new JProperty("Total_Days", result.Total_Days),
                                                            new JProperty("Total_Hour", result.Total_Hour),
                                                            new JProperty("Total_Min", result.Total_Min),
                                                            new JProperty("ExtraAmount", result.ExtraAmount),
                                                            new JProperty("Extra_Hour", result.Extra_Hour),
                                                            new JProperty("Extra_Min", result.Extra_Min),
                                                            new JProperty("Extra_month", result.Extra_month),
                                                            new JProperty("ExtraDays", result.Extra_Days),
                                                            new JProperty("Base_Amount", result.Base_Amount)

                                                            )));

                //json = JsonConvert.SerializeObject(cov.ToArray());
                //var jsonArray = JArray.Parse(json);
                //jContent = new JObject(
                //        new JProperty("resultCode", 0),
                //        new JProperty("Success", "Checked Out successfully"),

                //                   new JProperty("result",
                //                      new JObject(
                //    //new JProperty("authToken", objUserBEL.AuthToken),
                //                           new JProperty("Parkings", jsonArray))));

            }
            else
            {
                jContent = new JObject(
                        new JProperty("resultCode", 1),
                        new JProperty("error", "Check Out Fail"),
                                        new JProperty("result",
                                            new JObject(
                                                new JProperty("Success", false),
                                                new JProperty("Message", "Check Out Fail"),
                                                new JProperty("ErrorMessage", result.Error))));
            }

            res.Content = new StringContent(jContent.ToString());
            return res;
        }


        //Added by neeraj to Cancel the booking

        [Route("api/Booking_Cancel/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage Booking_Cancel(string BookedID, string CancellationReason, string whocancelledID)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            ParkingAreaDAL Parkingdal = new ParkingAreaDAL();

            CancelbookingBEL ParkingResult = new CancelbookingBEL();

            var result = Parkingdal.Cancel(BookedID, CancellationReason, whocancelledID);


            if (result.Success == true)
            {
                jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "Booking Cancelled"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "Booking Cancelled"))));
            }
            else
            {
                jContent = new JObject(
                                new JProperty("resultCode", 1),
                                new JProperty("error", "Error has occured."),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", "Error has occured."),
                                                        new JProperty("ErrorMessage", result.ErrorMessage))));
            }
            res.Content = new StringContent(jContent.ToString());
            return res;
        }


        [Route("api/Booking_Support/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage Booking_Support(string BookedID, string SupportTitle, string Support_Description, string SupportRequiredID)
        { 
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            ParkingAreaDAL Parkingdal = new ParkingAreaDAL();

            SupportbookingBEL ParkingResult = new SupportbookingBEL();

            var result = Parkingdal.Support(BookedID, SupportTitle, Support_Description, SupportRequiredID);


            if (result.Success == true)
            {
                jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "Support Request Recieved"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "Support Request Recieved"))));
            }
            else
            {
                jContent = new JObject(
                                new JProperty("resultCode", 1),
                                new JProperty("error", "Error has occured."),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", "Error has occured."),
                                                        new JProperty("ErrorMessage", result.ErrorMessage))));
            }
            res.Content = new StringContent(jContent.ToString());
            return res;
        }

        // On public parking Bubble click

        [Route("api/GetIndividualPublicParkingDetails/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage OnPublicParkingBubbleClick(string Parking_id)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            // Random ran = new Random();

            if (Parking_id != null && !string.IsNullOrWhiteSpace(Parking_id) && !string.IsNullOrEmpty(Parking_id))
            {
                try
                {
                    ParkingAreaDAL Parkingdal = new ParkingAreaDAL();

                    List<OnPublicParkingBubbleClick> onPublicParkingBubbleClick = new List<OnPublicParkingBubbleClick>();
                    string Errormessage = string.Empty;
                    onPublicParkingBubbleClick = Parkingdal.Public_Parking_Click(Parking_id, out Errormessage);
                    string json = string.Empty;
                    if (onPublicParkingBubbleClick.Count() > 0 && Errormessage == "" && string.IsNullOrEmpty(Errormessage) && string.IsNullOrWhiteSpace(Errormessage))
                    {
                        json = JsonConvert.SerializeObject(onPublicParkingBubbleClick.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("Success", true),

                                           new JProperty("result",
                                              new JObject(
                            //new JProperty("authToken", objUserBEL.AuthToken),
                                                   new JProperty("ParkingDetail", jsonArray))));


                    }
                    else
                    {

                        json = JsonConvert.SerializeObject(onPublicParkingBubbleClick.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                            //new JProperty("resultCode", 1),
                            //new JProperty("error", "Error has occured."),
                                                        new JProperty("result",
                                                            new JObject(
                                                                 new JProperty("ParkingDetail", jsonArray))));
                        //new JProperty("Success", false),
                        //new JProperty("Message", "Error has occured."),
                        //new JProperty("ErrorMessage", Errormessage))));
                    }

                }

                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 2),
                        new JProperty("error", "Exception Occured"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                   new JProperty("Transation", null))));

                }
            }
            else
            {

                jContent = new JObject(
                    new JProperty("resultCode", 3),
                    new JProperty("error", "Invalid Owner Id"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", "Owner Id cannot be empty"),
                                               new JProperty("Transation", null))));
            }


            res.Content = new StringContent(jContent.ToString());
            return res;

        }
    }
}
