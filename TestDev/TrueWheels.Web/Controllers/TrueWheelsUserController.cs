﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TrueWheels.BEL;
using TrueWheels.DAL;
using TrueWheels.Web.MailClass;
using TrueWheels.Web.Models;

namespace TrueWheels.Web.Controllers
{
    public class TrueWheelsUserController : Controller
    {
        //
        // GET: /TrueWheelsUser/
        public ActionResult Index()
        {
           // FormsAuthentication.
           // DLTrueWheelsUser dal = new DLTrueWheelsUser();
           // List<BETrueWheelsUser> employees = dal.Users.ToList();
            if (User.Identity.IsAuthenticated)
                return View();
            else
                return View("Index", "Home");

        }

        //User registration
        public ActionResult Create()
        {
            UserRegistrationViewModel user = new UserRegistrationViewModel();
            return View(user);
        }

        public ActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignUp(SignupViewModel vm)
        {
            UserDetailsBEL objUserBEL = new UserDetailsBEL();

            objUserBEL.Email_Id = vm.EmailId;
            objUserBEL.Password = Secure.Encrypt(vm.Password);
            objUserBEL.First_Name = vm.FirstName;
            objUserBEL.Last_Name = vm.LastName;
            objUserBEL.Phone_No1 = vm.Mobile;

            UserDetailsDAL objUserDAL = new UserDetailsDAL();
            List<string> OTPList = new List<string>();

            if (ModelState.IsValid)
            {

                UserLoginDetailsViewModel LDvm = new UserLoginDetailsViewModel(objUserDAL.AddUserDetails(objUserBEL));
                if (string.IsNullOrEmpty(LDvm.ErrorMessage)  && (LDvm.User_ID != "" || LDvm.User_ID != string.Empty))
                {
                    Message messages = new Message();
                    messages.message = new List<string>();
                    //FormsAuthentication.SetAuthCookie(objUserBEL.User_ID, true);
                    Session["userDetail"] = LDvm;
                   // return RedirectToAction("Index", "UserDashbaord");
                    OTPBEL OTPBel = new OTPBEL ();
                   OTPList= OTPBel.GetAndSendOTP(Convert.ToInt64(objUserBEL.Phone_No1));
                   //OTPList.Add("123456");
                   //    OTPList.Add("7030701191");
                   //    OTPList.Add("True");
                   if (OTPList != null)
                   {
                       if (Convert.ToBoolean(OTPList[2]))
                       {
                          // Session[OTPList[2]+"_OTP"] = OTPList[0];
                           Session["OTP"] = OTPList[0];
                           Mail mailSendOTP = new Mail();
                           Message messagesOTP = new Message();
                           messagesOTP.to = (((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).Email_Id).ToString();
                           if (!string.IsNullOrEmpty(messagesOTP.to))
                           {
                               messagesOTP.from = ConfigurationManager.AppSettings["UserName"];
                               messagesOTP.password = ConfigurationManager.AppSettings["Password"];
                               messagesOTP.username = ConfigurationManager.AppSettings["UserName"];
                               messagesOTP.message = new List<string>();
                               messagesOTP.subject = ConfigurationManager.AppSettings["Subject"];



                               string strHi = string.Format("Hi {0},", (((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).FullName).ToString());
                               string strWelcome = ConfigurationManager.AppSettings["Welcome"];
                               string strThanks = ConfigurationManager.AppSettings["Thanks"];
                               string strOTP = string.Format("The OTP '{0}' can be used for one time login. Please use password '{1}' to login in to our website or you can also use our App to find parking location across NCR. ", Session["OTP"], Secure.Decrypt(objUserBEL.Password));
                               string strKindly = ConfigurationManager.AppSettings["Kindly"];
                               string strThanksAgain = "Thanks again,";
                               string strTeam = "Team Truewheels";

                               StringBuilder body = new StringBuilder();
                               body.AppendFormat("<table><tr font-size='16'><td>{0}</td></tr><br><tr><td>{1}</td></tr><br><tr><td>{2}</td></tr><br><tr><td>{3}</td></tr><br><tr><td>{4}</td></tr><br><tr><td>{5}</td></tr><br><tr><td>{6}</td></tr></table>", strHi, strWelcome, strThanks, strOTP, strKindly, strThanksAgain, strTeam);
                               messagesOTP.message.Add(body.ToString());
                               mailSendOTP.Send(messagesOTP);
                           }
                       }
                   }
                       
                    return RedirectToAction("VerifyOTP", "OTP");
                  //  return RedirectToAction("Login", "TrueWheelsUser");
                }
                else
                {
                    ModelState.AddModelError("", LDvm.ErrorMessage.ToString());
                }

            }
            else
            {
               // Response.Write(ModelState.SelectMany(x => x.Value.Errors.Select(z => z.Exception)));
                ModelState.AddModelError("", "Data is incorrect!");
            }
            return View();
        }

        [HttpPost]
        public bool SignUpWithFB(string FirstName, string LastName, string EmailId, string Fb_id, string Pic_url)
        {
            UserDetailsBEL objUserBEL = new UserDetailsBEL();

            objUserBEL.Email_Id = EmailId;
            objUserBEL.Password = FirstName + "@Truewheels";
            objUserBEL.First_Name = FirstName;
            objUserBEL.Last_Name = LastName;
            objUserBEL.SignUp_Mode_ID = "FB";
            objUserBEL.Phone_No1 = "NA";
            objUserBEL.Other_ID = Fb_id;
            Session["Pic_url"] = Pic_url;
            UserDetailsDAL objUserDAL = new UserDetailsDAL();
            UserLoginDetailsViewModel LDvm = new UserLoginDetailsViewModel(objUserDAL.AddUserDetails(objUserBEL));
            if (string.IsNullOrEmpty(LDvm.ErrorMessage) && (!string.IsNullOrEmpty(LDvm.User_ID) && LDvm.User_ID != "0"))
            {
                //FormsAuthentication.SetAuthCookie(objUserBEL.User_ID, true);
                Session["userDetail"] = LDvm;
                return true;
               // return RedirectToAction("Index", "UserDashbaord");
            }
            else
            {
                ModelState.AddModelError("", LDvm.ErrorMessage.ToString());
                return false;
            }

               
        }

        public bool SignUpWithGG(string Fullname, string EmailId, string GG_id, string Pic_url)
        {
            UserDetailsBEL objUserBEL = new UserDetailsBEL();

            objUserBEL.Email_Id = EmailId;
            objUserBEL.Password = Fullname + "@Truewheels";
            objUserBEL.First_Name = Fullname;
            objUserBEL.Last_Name = "";
            objUserBEL.SignUp_Mode_ID = "GG";
            objUserBEL.Phone_No1 = "NA";
            objUserBEL.Other_ID = GG_id;            
            Session["Pic_url"] = Pic_url;

            UserDetailsDAL objUserDAL = new UserDetailsDAL();
            UserLoginDetailsViewModel LDvm = new UserLoginDetailsViewModel(objUserDAL.AddUserDetails(objUserBEL));
            if (string.IsNullOrEmpty(objUserBEL.ErrorMessage) && (objUserBEL.User_ID != "" || objUserBEL.User_ID != string.Empty))
            {
                //FormsAuthentication.SetAuthCookie(objUserBEL.User_ID, true);
                Session["userDetail"] = LDvm;
                return true;
                // return RedirectToAction("Index", "UserDashbaord");
            }
            else
            {
                ModelState.AddModelError("", objUserBEL.ErrorMessage.ToString());
                return false; 
            }


        }

        [HttpPost]
        public ActionResult Login(UserLogin ul)
       {
            
            UserDetailsBEL objUserBEL = new UserDetailsBEL();
                 
         
           
            if (ModelState.IsValid)
            {
                if (ul.userName.Contains('@'))
                    objUserBEL.Email_Id = ul.userName;
                else
                    objUserBEL.Phone_No1 = ul.userName;
                objUserBEL.Password = Secure.Encrypt(ul.password);


                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                //UserDetailsBEL tr = 
                UserLoginDetailsViewModel vm = new UserLoginDetailsViewModel(objUserDAL.FunAuthenticateUser(objUserBEL));
                //UserLoginDetailsViewModel vm = new UserLoginDetailsViewModel(objUserDAL.CheckPhoneNoExists(objUserBEL.Phone_No1));
                if (vm.ErrorMessage==string.Empty)
                {
                    FormsAuthentication.SetAuthCookie(objUserBEL.User_ID,true);
                    Session["userDetail"] = vm;
                    Session["userPhoneNo/Email"] =!string.IsNullOrWhiteSpace(vm.Phone_No1)? vm.Phone_No1: vm.Email_Id;

                    string tempdata;
                    if (TempData["RequestedUrl"] != null)
                    {
                        tempdata=Convert.ToString(TempData["RequestedUrl"]);
                        TempData["RequestedUrl"] = null;
                        string path = (String.Format("{0}://{1}:{2}/", Request.Url.Scheme, Request.Url.Host, Request.Url.Port)).ToString();
                        return Redirect(path + tempdata);
                       // return Redirect("http://localhost:21473/" + tempdata);
                       // return  RedirectToAction(tempdata, "parkingArea"); 
                    }
                    else
                        return RedirectToAction("Index", "UserDashbaord");
                }
                else
                {
                    ModelState.AddModelError("", vm.ErrorMessage.ToString());
                }
            }
            else
            {
                ModelState.AddModelError("", "Login data is incorrect!");
            }
            return View();

        }

        public ActionResult Login()
        {
            return View("Login");
        }
        [HttpPost]
        public ActionResult ForgotPassword(UpdateCred UP)
        {
            List<string> OTPList = new List<string>(); 
            OTPBEL OTPBel = new OTPBEL();
            UserDetailsDAL objUserDAL = new UserDetailsDAL();
           // OTPViewModel ovm = new OTPViewModel("ForgotPassword");
            var userDetails = objUserDAL.CheckPhoneNoExists(UP.mobile);
            if (userDetails.ErrorMessage != "No record exist for this user")
            {
                TempData["OTPSource"] = "ForgotPassword";
                Session["mobileno"] = UP.mobile;
                if (Convert.ToString(Session["mobileno"]) != Convert.ToString(Session["mobilenoOTP"]))
                {
                    TempData["OTPErrorMessage"] = null;
                    Session["OTPFP"] = null;
                    Session["VerificationTime"] = null;
                }
                if (Convert.ToString(Session["mobileno"]) == Convert.ToString(Session["mobilenoOTP"]))
                {
                    TempData["OTPErrorMessage"] = "Already sent OTP to given number.";
                    return RedirectToAction("VerifyOTP", "OTP");
                }
                if (Convert.ToString(Session["OTPFP"]) != null && Session["VerificationTime"] != null)
                {
                    DateTime renewal = DateTime.Parse(Session["VerificationTime"].ToString());
                    var diff = (DateTime.Now - renewal).Minutes;
                    if (diff < 5)
                    {
                        TempData["OTPErrorMessage"] = "OTP already messaged.";
                        //ViewBag.ErrorMessage = "Mobile No. does not exist.";
                        return RedirectToAction("VerifyOTP", "OTP");
                    }
                }
                OTPList = OTPBel.GetAndSendOTP(Convert.ToInt64(UP.mobile));
                //OTPList.Add("121212");
                //OTPList.Add("121212");
                //OTPList.Add("true");
                if (OTPList != null)
                {
                    if (Convert.ToBoolean(OTPList[2]))
                    {
                        // Session[OTPList[2]+"_OTP"] = OTPList[0];
                        Session["mobilenoOTP"] = UP.mobile;
                        Session["OTP"] = OTPList[0];
                        Session["OTPFP"] = OTPList[0];
                        Session["VerificationTime"] = DateTime.Now;

                        Mail mailSendOTP = new Mail();
                        Message messages = new Message();
                        messages.to = userDetails.Email_Id;
                        if (!string.IsNullOrEmpty(messages.to))
                        {
                            messages.from = ConfigurationManager.AppSettings["UserName"];
                            messages.password = ConfigurationManager.AppSettings["Password"];
                            messages.username = ConfigurationManager.AppSettings["UserName"];
                            messages.message = new List<string>();
                            messages.subject = MailTemplates.SubjectFP;
                            string strHi = string.Format("Hello {0},", (userDetails.First_Name + " " + userDetails.Last_Name));
                            string strThanksNeeds = MailTemplates.ThanksNeeds;
                            string strTWLoginPwd = string.Format(MailTemplates.TWLoginPwd, Session["OTP"]);
                            string strKindly = MailTemplates.Kindly;
                            string strThanksAgain = MailTemplates.ThanksAgain;
                            string strTeam = MailTemplates.Team;

                            StringBuilder body = new StringBuilder();
                            body.AppendFormat("<table><tr font-size='16'><td>{0}</td></tr><br><tr><td>{1}</td></tr><br><tr><td>{2}</td></tr><br><tr><td>{3}</td></tr><br><tr><td>{4}</td></tr><br><tr><td>{5}</td></tr><br></table>", strHi, strThanksNeeds, strTWLoginPwd, strKindly, strThanksAgain, strTeam);
                            messages.message.Add(body.ToString());
                            mailSendOTP.Send(messages);
                        }
                    }
                    Session["ForgotMobleNum"] = UP.mobile;
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Mobile No. does not exist.";
                //ViewBag.ErrorMessage = "Mobile No. does not exist.";
                return View("ForgotPassword", UP);
            }

            return RedirectToAction("VerifyOTP", "OTP");
            //return View();
        }
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View();
        }
        public ActionResult LogOff()
        {
            Session.Clear();
            //ResultExecutingContext filterContext;
            //filterContext.HttpContext.Response.RemoveOutputCacheItem();//.HttpContext.Response.RemoveOutputCacheItem();
            //Response.Cache.SetExpires(DateTime.Now);
             return RedirectToAction("Login", "TrueWheelsUser");
        }

        public ActionResult TermsCondition()
        {
            return View();
        }
        
	}
}