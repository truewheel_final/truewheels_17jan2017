﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrueWheels.BEL;
using TrueWheels.DAL;
using TrueWheels.Web.Models;
using System.IO;
using System.Web.Helpers;
using System.Web.Configuration;
using System.Reflection;

namespace TrueWheels.Web.Controllers
{
    public class UserDashBaordController : Controller
    {
        //
        // GET: /UserDashBaord/
        [HttpGet]
        public ActionResult Index()
        {
            try {
                if (Session["userDetail"] != null)
                {
                    UserMaintenanceViewModel objUserMaintenance = new UserMaintenanceViewModel();
                    //List<DashBaordViewModal> DVM = new List<DashBaordViewModal>;
                    UserDetailsDAL objUserDAL = new UserDetailsDAL();
                    List<DashBoardBEL> UserMenus = objUserDAL.GetMenuMapping((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                    PersonalinfoBEL objPersonalInfo = objUserDAL.GetProfileInfo((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                    //objPersonalInfo.profilePicPath = Server.MapPath("/Content/images/profilePicture/") + objPersonalInfo.profilePicPath;// +".png";

                    if ((objPersonalInfo.SignUp_Mode_ID == "GG" || objPersonalInfo.SignUp_Mode_ID == "FB") && (objPersonalInfo.mobileNo == "NA" || objPersonalInfo.mobileNo == null))
                    {
                        return RedirectToAction("GenerateOTP", "OTP");
                    }

                    if ((string.IsNullOrEmpty(objPersonalInfo.SignUp_Mode_ID) || objPersonalInfo.SignUp_Mode_ID == "FB"
                        || objPersonalInfo.SignUp_Mode_ID == "GG") && (objPersonalInfo.IsMobileVerified == "N" ||
                        objPersonalInfo.IsMobileVerified == null))
                    {
                        List<string> OTPList = new List<string>();
                        OTPBEL OTPBel = new OTPBEL();
                        OTPList = OTPBel.GetAndSendOTP(Convert.ToInt64((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).Phone_No1).ToString()));

                        if (OTPList != null)
                        {
                            if (Convert.ToBoolean(OTPList[2]))
                            {
                                Session["mobilenoOTP"] = Convert.ToInt64((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).Phone_No1).ToString());
                                Session["OTP"] = OTPList[0];
                                Session["OTPFP"] = OTPList[0];
                                Session["VerificationTime"] = DateTime.Now;
                            }
                        }
                        return RedirectToAction("VerifyOTP", "OTP");
                    }

                    objPersonalInfo.fullName = objPersonalInfo.firstName + " " + objPersonalInfo.lastName;
                    ViewBag.DashBoardMenu = UserMenus;
                    objUserMaintenance.Personalinfo = objPersonalInfo;

                    if (string.IsNullOrEmpty(UserMenus[0].ErrorMessage))
                    {
                        //  DVM = UserMenus;
                        return View(objUserMaintenance);
                    }
                    else
                    {

                        ModelState.AddModelError("", UserMenus[0].ErrorMessage.ToString());
                    }
                }
                 return View();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex,MethodBase.GetCurrentMethod().Name,this.GetType().Name);
            }
            return View();
        }

        [HttpGet]
        public ActionResult DashboardIndex()
        {
            if (Session["userDetail"] != null)
            {
                UserMaintenanceViewModel objUserMaintenance = new UserMaintenanceViewModel();
                //List<DashBaordViewModal> DVM = new List<DashBaordViewModal>;
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                List<DashBoardBEL> UserMenus = objUserDAL.GetMenuMapping((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                PersonalinfoBEL objPersonalInfo = objUserDAL.GetProfileInfo((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                // objPersonalInfo.profilePic_Path = Server.MapPath("/Content/images/profilePicture/") + objPersonalInfo.profilePic_Path;// +".png";


                objPersonalInfo.fullName = objPersonalInfo.firstName + " " + objPersonalInfo.lastName;
                ViewBag.DashBoardMenu = UserMenus;
                objUserMaintenance.Personalinfo = objPersonalInfo;

                if (string.IsNullOrEmpty(UserMenus[0].ErrorMessage))
                {
                    //  DVM = UserMenus;
                    return PartialView("DashboardPartial",objUserMaintenance);
                }
                else
                {

                    ModelState.AddModelError("", UserMenus[0].ErrorMessage.ToString());
                }
            }
            return PartialView("DashboardPartial");
        }

        [HttpPost]
        public ActionResult Index(PersonalinfoBEL profileInfo, HttpPostedFileBase profilePic)
         {
            
            if (ModelState.IsValid)
            {
                //if (profilePic != null)
                //{
                //    var filename = profileInfo.User_Id + ".png";
                //    var filePathOriginal = Server.MapPath("/Content/images/profilePicture");
                //    string savedFileName = Path.Combine(filePathOriginal, filename);
                //    string path = System.Configuration.ConfigurationManager.AppSettings["appPath"] + savedFileName;
                //    System.IO.File.Delete(savedFileName);
                //    profilePic.SaveAs(savedFileName);
                //}


                //List<DashBaordViewModal> DVM = new List<DashBaordViewModal>;
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                List<DashBoardBEL> UserMenus = objUserDAL.GetMenuMapping((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                if (profileInfo.newPassword == null)
                {
                    profileInfo.newPassword = profileInfo.password;
                }

                UserMaintenanceViewModel objUserMaintenance = new UserMaintenanceViewModel();

                objUserMaintenance.Personalinfo = objUserDAL.UpdateProfileInfo(profileInfo);
                //objUserMaintenance.Personalinfo.profilePicPath = objUserMaintenance.Personalinfo.userId.ToString();
                objUserMaintenance.Personalinfo.fullName = objUserMaintenance.Personalinfo.firstName + " " + objUserMaintenance.Personalinfo.lastName;
                ViewBag.DashBoardMenu = UserMenus;

                if (string.IsNullOrEmpty(UserMenus[0].ErrorMessage))
                {
                    //  DVM = UserMenus;
                    return View(objUserMaintenance);
                }
                else
                {

                    ModelState.AddModelError("", UserMenus[0].ErrorMessage.ToString());
                    return View(objUserMaintenance);
                }
            }
            else
            {
              return  RedirectToAction("Index", "UserDashBaord");

                //UserDetailsDAL objUserDAL = new UserDetailsDAL();
                //List<DashBoardBEL> UserMenus = objUserDAL.GetMenuMapping((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                //ViewBag.DashBoardMenu = UserMenus;
                //UserMaintenanceViewModel objUserMaintenance = new UserMaintenanceViewModel();
                //objUserMaintenance.Personalinfo = profileInfo;
                //return View(objUserMaintenance);
               
            }
        }

        //public ActionResult Index(string userid)
        //{


        //    return View();
        //}

        //
        // GET: /UserDashBaord/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /UserDashBaord/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /UserDashBaord/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

       

        //
        // POST: /UserDashBaord/Edit/5
        [HttpPost]
        public ActionResult EditPersonalInfo(UserMaintenanceViewModel objUserMaintenance)
        {


            return RedirectToAction("Index");
        }

        //
        // GET: /UserDashBaord/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /UserDashBaord/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult FollowUs()
        {
            return PartialView("_FollowUs");
        }

        [HttpGet]
        public ActionResult InviteFriends()
        {
            return PartialView("_InviteFriends");
        }

        [HttpGet]
        public ActionResult Inbox()
        {
            UserDetailsDAL objUserDAL = new UserDetailsDAL();
            List<UserInboxBEL> ListUserNotifications = objUserDAL.GetUserInbox((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());

            return PartialView("_Inbox", ListUserNotifications);
        }

        public ActionResult DeleteNotification(string id)
        {
            if (Session["userDetail"] != null)
            {
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                UserInboxBEL UserNotification = objUserDAL.DeleteUserNotification((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString(), id);
                if (UserNotification.Success)
                {
                    TempData["SaveSuccess"] = "Deleted Successfully";
                }
                else
                {
                    TempData["SaveSuccess"] = "An error occured";
                }
                List<UserInboxBEL> ListUserNotifications = objUserDAL.GetUserInbox((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());

                return PartialView("_Inbox", ListUserNotifications);
            }
            else
            {
                return PartialView("_Inbox");
            }

        }


        [HttpGet]
        public ActionResult ViewProfile()
        {
            if (Session["userDetail"] != null)
            {
                UserMaintenanceViewModel objUserMaintenance = new UserMaintenanceViewModel();
                //List<DashBaordViewModal> DVM = new List<DashBaordViewModal>;
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                List<DashBoardBEL> UserMenus = objUserDAL.GetMenuMapping((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                PersonalinfoBEL objPersonalInfo = objUserDAL.GetProfileInfo((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                objPersonalInfo.profilePicPath = objPersonalInfo.userId + ".png";
                objPersonalInfo.fullName = objPersonalInfo.firstName + " " + objPersonalInfo.lastName;
                ViewBag.DashBoardMenu = UserMenus;
                objUserMaintenance.Personalinfo = objPersonalInfo;

                if (string.IsNullOrEmpty(UserMenus[0].ErrorMessage))
                {
                    return PartialView("_Personalinfo", objPersonalInfo);
                }
                else
                {

                    ModelState.AddModelError("", UserMenus[0].ErrorMessage.ToString());
                }
            }

            return PartialView("_Personalinfo");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadFile()
        {
            string _imgname = string.Empty;
            try
            {
                 _imgname = string.Empty;
                string User_id = string.Empty;
                if (Session["userDetail"] != null)
                {
                    User_id = (((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString();
                    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                    {
                        var pic = System.Web.HttpContext.Current.Request.Files["MyImages"];
                        if (pic.ContentLength > 0)
                        {
                            var fileName = Path.GetFileName(User_id);
                            var _ext = Path.GetExtension(pic.FileName);


                            // _imgname = Guid.NewGuid().ToString();
                            var _comPath = Server.MapPath("/Content/images/profilePicture/") + fileName + _ext;
                            _imgname = fileName + _ext;

                            ViewBag.Msg = _comPath;
                            var path = _comPath;

                            // Saving Image in Original Mode
                            pic.SaveAs(path);
                            //pic.SaveAs(Server.MapPath("/Upload/MVC_") + "1" + _ext);

                            // resizing image
                            MemoryStream ms = new MemoryStream();
                            WebImage img = new WebImage(_comPath);

                            if (img.Width > 200)
                                img.Resize(200, 200);
                            img.Save(_comPath);

                            UserDetailsDAL userDetailsDAL = new UserDetailsDAL();
                            userDetailsDAL.SetProfilePic(User_id, _imgname);
                            // end resize
                        }
                    }
                }
                //Session expire error
                var cachbuster = DateTime.Now;
                _imgname = _imgname + "?c=@" + cachbuster;
                return Json(Convert.ToString(_imgname), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
            }
            return Json(Convert.ToString(_imgname), JsonRequestBehavior.AllowGet);
        }

        #region "Invite Gmail Contacts by Amit"
        public PartialViewResult InviteGmailContacts(List<string> emails)
        {
            DashBaordViewModal objDashBaordViewModal = new DashBaordViewModal();
            try
            {
                
                List<InviteDetails> lstInviteDetails = new List<InviteDetails>();
                objDashBaordViewModal.objInviteDetails = new List<InviteDetails>();
                if (emails.Count > 0)
                {
                    foreach (var item in emails)
                    {
                        var splitEmailName = item.Split(',');

                        lstInviteDetails.Add(new InviteDetails { EmailInvite = splitEmailName[0], InviteName = splitEmailName[1] });
                    }
                    objDashBaordViewModal.objInviteDetails = lstInviteDetails;
                }
                
            }
               
            catch (Exception ex)
            { ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name); }
            return PartialView("InviteGmailContacts", objDashBaordViewModal);
            
        }
        #endregion 

        [HttpPost]
        public JsonResult SendEmailToGmailContacts(DashBaordViewModal model)
        {
            string status = "false";
            try
            {
                var inviteGmailContacts = Convert.ToBoolean(WebConfigurationManager.AppSettings["inviteGmailContacts"]);
                if (inviteGmailContacts)
                {
                    var emailId = (((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).Email_Id).ToString(); ;
                    if (model.objInviteDetails.Count > 0)
                    {
                        var result = (from i in model.objInviteDetails
                                      where i.IsTrue == true
                                      select i.EmailInvite).ToList();
                        if (result.Count <= 0)
                        {
                            status = "NoEmailSelected";
                            return Json(status, JsonRequestBehavior.AllowGet);
                        }
                        Mail objmail = new Mail();
                        Message messages = new Message();
                        messages.bcc = new string[result.Count];
                        for (var i = 0; i < result.Count; i++)
                        {
                            messages.bcc[i] = Convert.ToString(result[i]);
                        }

                        messages.from = "info@truewheelsuat.in";
                        messages.password = "Truewheels.2016";
                        messages.subject = "Invite Gmail Contacts.";
                        messages.bcc = messages.bcc;
                        messages.to = "info2truewheels@gmail.com";
                        messages.message = new List<string>();
                        messages.message.Add("Your friend has invite to join TrueWheels.");

                        bool mailStatus = objmail.Send(messages);
                        if(mailStatus)
                        status = "true";
                    }
                }
            }
            catch (Exception ex)
            { ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name); }

            return Json(status, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult InviteFriendByEmail()
        {
            return PartialView("_InviteEmail");
        }

        public JsonResult SendEmailInviteFriend(DashBaordViewModal objDashBaordViewModal)
        {
            bool status = false;
            try
            {
                var fullName = (((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).FullName).ToString(); ;
                if (objDashBaordViewModal.FriendEmailId != null)
                {
                    Mail objmail = new Mail();
                    Message messages = new Message();
                    messages.from = "info@truewheelsuat.in";
                    messages.password = "Truewheels.2016";
                    messages.subject = "Invite To Join TrueWheels.";
                    messages.bcc = messages.bcc;
                    messages.to = objDashBaordViewModal.FriendEmailId;
                    messages.message = new List<string>();
                    messages.message.Add(string.Format("Your friend {0} has invited you to join TrueWheels.", fullName));

                    bool mailStatus = objmail.Send(messages);
                    if (mailStatus)
                    status = true;
                }

                return Json(status, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
                return Json(status, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateOTP(GenerateOTPModel GenerateOTPModel)
        {
            try
            {
                List<string> OTPList = new List<string>();
                OTPBEL OTPBel = new OTPBEL();
                UserDetailsDAL UserDetailsDAL = new UserDetailsDAL();
                string userId = ((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                var result = UserDetailsDAL.UserPhoneNoVerification(userId, GenerateOTPModel.mobileNo);
                if (!string.IsNullOrEmpty(result.ErrorMessage))
                {
                    TempData["ProfileVerified"] = "Phone No. already exist.";
                    return RedirectToAction("GenerateOTP","OTP");
                }
                else if (!result.VerifiedSuccessfully)
                {
                    TempData["ProfileVerified"] = "An error has occured while validating Phone Number. Please try after sometime.";
                    return RedirectToAction("GenerateOTP", "OTP");
                }
                OTPList = OTPBel.GetAndSendOTP(Convert.ToInt64(GenerateOTPModel.mobileNo));

                if (OTPList != null)
                {
                    if (Convert.ToBoolean(OTPList[2]))
                    {
                        Session["mobilenoOTP"] = Convert.ToInt64(GenerateOTPModel.mobileNo);
                        Session["OTP"] = OTPList[0];
                        Session["OTPFP"] = OTPList[0];
                        Session["VerificationTime"] = DateTime.Now;
                    }
                }
                return RedirectToAction("VerifyOTP", "OTP");
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
                return RedirectToAction("VerifyOTP", "OTP");
            }
        }

        public ActionResult AddPAUser()
        {
            PBDetailsBEL objPBDetailsBELList = new PBDetailsBEL();
            try
            {
                List<PBDetailsBEL> objPBDetailsBEL = new List<PBDetailsBEL>();
                objPBDetailsBELList.ParkingItemList = new List<SelectListItem>();
                ParkingAreaDAL objParkingAreaDAL = new ParkingAreaDAL();
                objPBDetailsBEL = objParkingAreaDAL.GetParkingWithId();
                foreach (var item in objPBDetailsBEL)
                {
                    objPBDetailsBELList.ParkingItemList.Add(new SelectListItem
                    {
                        Text = item.ParkingName,
                        Value = item.ParkinIds,
                        Selected = false
                    });
                }
                return PartialView("AddPAUser", objPBDetailsBELList);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendExcepToDB(ex, MethodBase.GetCurrentMethod().Name, this.GetType().Name);
            }
            return PartialView("AddPAUser", objPBDetailsBELList);
        }

        public ActionResult AddPAUserDetails(PBDetailsBEL objPBDetailsBEL)
        {
            ParkingLocationList obj = new ParkingLocationList();
            UserDetailsDAL userDetailsDAL = new UserDetailsDAL();
            PBDetailsBEL pBDetailsBEL = new PBDetailsBEL();
            pBDetailsBEL.user_Name = objPBDetailsBEL.First_Name + "" + objPBDetailsBEL.Last_Name;
            pBDetailsBEL.Address = objPBDetailsBEL.Address;
            pBDetailsBEL.dob = objPBDetailsBEL.dob;
            pBDetailsBEL.Email_Id = objPBDetailsBEL.Email_Id;
            pBDetailsBEL.Password = objPBDetailsBEL.Password;
            pBDetailsBEL.phone_No1 = objPBDetailsBEL.phone_No1;
            pBDetailsBEL.userType = "PA";
            pBDetailsBEL.parkingLocationList = new List<ParkingLocationList>();
            foreach (var item in objPBDetailsBEL.ParkinIdsList)
            {
                var values = userDetailsDAL.GetParkingIdAndNameById(item);
                obj.parking_id = values.parking_id;
                obj.Parking_Name = values.Parking_Name;
                pBDetailsBEL.parkingLocationList.Add(obj);
                obj = new ParkingLocationList();
            }
            var response = userDetailsDAL.AddPA(pBDetailsBEL);
            return RedirectToAction("Index", "UserDashBaord");
        }
    }
}
