using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrueWheels.BEL;
using TrueWheels.DAL;
using TrueWheels.Web.Models;


namespace TrueWheels.Web.Controllers
{
    public class UtilitySignUpAPIController : ApiController
    {

        public string AuthenticateToken(System.Net.Http.Headers.HttpRequestHeaders headers)
        {

            string token = string.Empty;
            string user_ID = string.Empty;
            if (headers.Contains("authToken"))
            {
                token = headers.GetValues("authToken").First();
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                user_ID = objUserDAL.AuthenticateToken(token);
            }
            else
                user_ID = "0";
            
            return user_ID;

        }

        public string AuthenticateTokenPB(System.Net.Http.Headers.HttpRequestHeaders headers)
        {

            string token = string.Empty;
            string user_ID = string.Empty;
            if (headers.Contains("authToken"))
            {
                token = headers.GetValues("authToken").First();
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                user_ID = objUserDAL.AuthenticateTokenPB(token);
            }
            else
                user_ID = "0";

            return user_ID;

        }
     public IHttpActionResult GetProduct(int id)
            {
                System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
                string token = string.Empty;
                string pwd = string.Empty;
                if (headers.Contains("username"))
                {
                    token = headers.GetValues("username").First();
                }
                if (headers.Contains("password"))
                {
                    pwd = headers.GetValues("password").First();
                }
         
              if (headers.Contains("authToken"))
                {
                    token = headers.GetValues("authToken").First();
                }
                //code to authenticate and return some thing
                //if (!Authenticated(token, pwd)
                //    return Unauthorized();
                //var product = products.FirstOrDefault((p) => p.Id == id);
                //if (product == null)
                //{
                //    return NotFound();
                //}
                return Ok();
            }

        [Route("api/SignupPB/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage SignupPB(UtilityPBSignupAPIModel vm)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
             string PA_User_id = string.Empty;
             Random ran = new Random();
             System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
             PA_User_id = AuthenticateToken(headers);
             if (PA_User_id == "0")
             {
                 jContent = new JObject(
                                new JProperty("resultCode", 4),
                                new JProperty("error", "InValid AuthToken"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", "InValid AuthToken")
                                                        )));
                 res.Content = new StringContent(jContent.ToString());
                 return res;
             }
            if (vm != null)
            {
                
               string password = "TWuser@" + Convert.ToString(ran.Next(1000, 9999));
               vm.password = Secure.Encrypt(password);
                vm.confirmPassword = vm.password;
                if (ModelState.IsValid)
                {
                    try
                    {
                        PBDetailsBEL objUserBEL = new PBDetailsBEL();

                        objUserBEL.Email_Id = vm.emailId;
                        objUserBEL.Password = vm.password;
                        objUserBEL.user_Name = vm.userName;
                        objUserBEL.phone_No1 = vm.mobile;
                        objUserBEL.Address = vm.address;
                        objUserBEL.dob = vm.dateOfBirth;
                        objUserBEL.isAdminRightsAssigned = vm.isAdminRightsAssigned;
                        objUserBEL.pA_CustId = "PA"+PA_User_id.Trim();
                        objUserBEL.working_location = vm.workingLocation;

                        UserDetailsDAL objUserDAL = new UserDetailsDAL();
                        PBDetailsBEL ulvm = objUserDAL.AddPB(objUserBEL);

                        if (ulvm.user_ID != "0")
                        {
                            //  res.StatusCode = HttpStatusCode.Created;
                            jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "Added successfully"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "User added successfully"),
                                                        new JProperty("UserID", ulvm.user_ID))));

                            OTPController SMScontrolller = new OTPController();
                            SMScontrolller.sendMessage(Convert.ToInt64(objUserBEL.phone_No1), "Greeting!! Thank you for registering with TrueWheels. Your temporary  password is" + password);
                        }
                        else
                        {
                            // res.StatusCode = HttpStatusCode.BadRequest;
                            jContent = new JObject(
                                 new JProperty("resultCode", 1),
                                new JProperty("error", ulvm.ErrorMessage),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", ulvm.ErrorMessage),
                                                        new JProperty("ErrorMessage", ulvm.ErrorMessage),
                                                        new JProperty("Transation", ulvm.user_ID))));
                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                            new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }
                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();
                    //res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 3),
                        new JProperty("error", errorList),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", errorList),
                                                   new JProperty("Transation", null))));
                }
            }
            res.Content = new StringContent(jContent.ToString());
            return res;

        }

        [Route("api/loginPAPB/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage LoginPAPB(UserLoginPAPB vm)
        {
            var res = new HttpResponseMessage();
            JObject jContent;
            if (ModelState.IsValid)
            {


                try
                {
                    UtilityLoginBEL objUserBEL = new UtilityLoginBEL();


                    objUserBEL.Password = Secure.Encrypt(vm.password);
                    //objUserBEL.Password = vm.password;
                    objUserBEL.MobileNumber = vm.mobile;
                    objUserBEL.UserType = vm.userType;
                    Guid guid ;
                    guid = Guid.NewGuid();
                    objUserBEL.AuthToken = guid.ToString();

                    UserDetailsDAL objUserDAL = new UserDetailsDAL();
                    string Errormessage = string.Empty;
                    //UserDetailsBEL lvm = objUserDAL.AuthenticatePAPB(objUserBEL);
                    List<LocationsIDName> AVP = objUserDAL.AuthenticatePAPB(objUserBEL, out Errormessage);
                    string json = string.Empty;
                    if (AVP.Count() > 0 && (Errormessage == string.Empty ||Errormessage==""))
                    {
                        json = JsonConvert.SerializeObject(AVP.ToArray());
                        var jsonArray = JArray.Parse(json);
                        //jContent = JObject.Parse(json);
                        jContent = new JObject(
                                    new JProperty("resultCode", 0),
                                    new JProperty("Success", true),
                                     
                                               new JProperty("result",
                                                  new JObject(
                                                       new JProperty("authToken", objUserBEL.AuthToken),
                                                       new JProperty("Parkings", jsonArray)
                    
                                                      )));                                         
                    
                    }
                    else
                    {

                        jContent = new JObject(
                            new JProperty("resultCode", 1),
                                new JProperty("error", Errormessage),
                                            new JProperty("result",
                                                new JObject(
                                                    new JProperty("Success", false),
                                                    new JProperty("Message", Errormessage)
                                                    )));

                        
                    }

                }
                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                                             new JProperty("resultCode", 2),
                                            new JProperty("error", "Exception Occured"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString())
                                                   )));

                }

                //return Request.CreateResponse<string>(HttpStatusCode.OK, mystring);


            }
            else
            {
                var errorList = (from item in ModelState
                                 where item.Value.Errors.Any()
                                 select item.Value.Errors[0].ErrorMessage).ToList();
                //res.StatusCode = HttpStatusCode.BadRequest;
                jContent = new JObject(
                                 new JProperty("resultCode", 3),
                                 new JProperty("error", errorList),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", errorList),
                                               new JProperty("Transation", null))));
            }
            res.Content = new StringContent(jContent.ToString());
            return res;
        }

        [Route("api/UpdateImageDocURls/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage UpdateImageDocURls(UserIMageDocUrls vm)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject(); ;
            string PA_User_id = string.Empty;
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            PA_User_id = AuthenticateToken(headers);
            if (PA_User_id == "0")
            {
                jContent = new JObject(
                               new JProperty("resultCode", 4),
                               new JProperty("error", "InValid AuthToken"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", "InValid AuthToken")
                                                       )));
                res.Content = new StringContent(jContent.ToString());
                return res;
            }
            if (vm != null)
            {
                if (ModelState.IsValid)
                {


                    try
                    {
                        PBDetailsBEL objUserBEL = new PBDetailsBEL();

                        objUserBEL.user_ID = vm.userID;
                        objUserBEL.picUrl = vm.picUrl;
                        objUserBEL.docURL = vm.docURL;

                        UserDetailsDAL objUserDAL = new UserDetailsDAL();
                        
                        //UserDetailsBEL lvm = objUserDAL.AuthenticatePAPB(objUserBEL);
                        
                        PBDetailsBEL ulvm = objUserDAL.UpdateImageDocURL(objUserBEL);

                        if (ulvm.user_ID != "0")
                        {
                            //  res.StatusCode = HttpStatusCode.Created;
                            jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "Updated successfully"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", " Updated successfully")
                                                        )));

                        }
                        else
                        {

                            jContent = new JObject(
                                new JProperty("resultCode", 1),
                                    new JProperty("error", ulvm.ErrorMessage),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", ulvm.ErrorMessage)
                                                        )));
                            
                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                                                 new JProperty("resultCode", 2),
                                                new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString())
                                                       )));

                    }

                    //return Request.CreateResponse<string>(HttpStatusCode.OK, mystring);


                }
                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();
                    //res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                                     new JProperty("resultCode", 3),
                                     new JProperty("error", errorList),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", errorList),
                                                   new JProperty("Transation", null))));
                }
            }
            res.Content = new StringContent(jContent.ToString());
            return res;
        }

        [Route("api/UpdatePBProfile/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage UpdatePBProfile(UtilityPBProfileAPIModel vm)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject(); ;
            string PA_User_id = string.Empty;
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            PA_User_id = AuthenticateToken(headers);
            if (PA_User_id == "0")
            {
                jContent = new JObject(
                               new JProperty("resultCode", 4),
                               new JProperty("error", "InValid AuthToken"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", "InValid AuthToken")
                                                       )));
                res.Content = new StringContent(jContent.ToString());
                return res;
            }
            if (vm != null)
            {
                if (ModelState.IsValid)
                {


                    try
                    {
                        PBDetailsBEL objUserBEL = new PBDetailsBEL();
                        objUserBEL.user_ID = vm.userID;
                        objUserBEL.Email_Id = vm.emailId;
                        objUserBEL.user_Name = vm.userName;
                        objUserBEL.phone_No1 = vm.mobile;
                        objUserBEL.Address = vm.address;
                        objUserBEL.dob = vm.dateOfBirth;

                        if (vm.workingLocation != null && vm.workingLocation != 0)
                        objUserBEL.working_location = vm.workingLocation;

                        UserDetailsDAL objUserDAL = new UserDetailsDAL();

                        //UserDetailsBEL lvm = objUserDAL.AuthenticatePAPB(objUserBEL);

                        PBDetailsBEL ulvm = objUserDAL.UpdatePBProfile(objUserBEL);

                        if (ulvm.user_ID != "0")
                        {
                            //  res.StatusCode = HttpStatusCode.Created;
                            jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "Updated successfully"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", " Updated successfully")
                                                        )));

                        }
                        else
                        {

                            jContent = new JObject(
                                new JProperty("resultCode", 1),
                                    new JProperty("error", ulvm.ErrorMessage),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", ulvm.ErrorMessage)
                                                        )));

                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                                                 new JProperty("resultCode", 2),
                                                new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString())
                                                       )));

                    }

                    //return Request.CreateResponse<string>(HttpStatusCode.OK, mystring);


                }
                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();
                    //res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                                     new JProperty("resultCode", 3),
                                     new JProperty("error", errorList),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", errorList),
                                                   new JProperty("Transation", null))));
                }
            }
            res.Content = new StringContent(jContent.ToString());
            return res;
        }

        [Route("api/DeactivatePB/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage DeactivatePB(string pBID)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject(); ;
            string PA_User_id = string.Empty;
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            PA_User_id = AuthenticateToken(headers);
            if (PA_User_id == "0")
            {
                jContent = new JObject(
                               new JProperty("resultCode", 4),
                               new JProperty("error", "InValid AuthToken"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", "InValid AuthToken")
                                                       )));
                res.Content = new StringContent(jContent.ToString());
                return res;
            }
             if (pBID != null || !string.IsNullOrWhiteSpace(pBID))
                {


                    try
                    {
                       UserDetailsDAL objUserDAL = new UserDetailsDAL();

                        //UserDetailsBEL lvm = objUserDAL.AuthenticatePAPB(objUserBEL);

                        PBDetailsBEL ulvm = objUserDAL.DeactivatePB(pBID,  PA_User_id);

                        if (ulvm.user_ID != "0")
                        {
                            //  res.StatusCode = HttpStatusCode.Created;
                            jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "Deactivated successfully"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", " Deactivated successfully")
                                                        )));

                        }
                        else
                        {

                            jContent = new JObject(
                                new JProperty("resultCode", 1),
                                    new JProperty("error", ulvm.ErrorMessage),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", ulvm.ErrorMessage)
                                                        )));

                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                                                 new JProperty("resultCode", 2),
                                                new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString())
                                                       )));

                    }

                    //return Request.CreateResponse<string>(HttpStatusCode.OK, mystring);


                }
                else
                {
                    
                    jContent = new JObject(
                                     new JProperty("resultCode", 3),
                                     new JProperty("error", "PBId is mendatory to pass"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", "PBId is mendatory to pass")
                                                   )));
                }
            
            res.Content = new StringContent(jContent.ToString());
            return res;
        }

        [Route("api/GetAllPBs/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetAllPBs()
        {
            var res = new HttpResponseMessage();
            JObject jContent;
            if (ModelState.IsValid)
            {


                try
                {
                                                           
                    UserDetailsDAL objUserDAL = new UserDetailsDAL();
                    string Errormessage = string.Empty;
                    string PA_User_id = string.Empty;
                    System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
                    PA_User_id = AuthenticateToken(headers);
                    if (PA_User_id == "0")
                    {
                        jContent = new JObject(
                                       new JProperty("resultCode", 4),
                                       new JProperty("error", "InValid AuthToken"),
                                                       new JProperty("result",
                                                           new JObject(
                                                               new JProperty("Success", false),
                                                               new JProperty("Message", "InValid AuthToken")
                                                               )));
                        res.Content = new StringContent(jContent.ToString());
                        return res;
                    }
                    List<PBDetailsBEL> AVP = objUserDAL.GetAllPBs(PA_User_id, out Errormessage);
                    string json = string.Empty;
                    if (AVP.Count() > 0 && (Errormessage == string.Empty || Errormessage == ""))
                    {
                        json = JsonConvert.SerializeObject(AVP.ToArray());
                        var jsonArray = JArray.Parse(json);
                        //jContent = JObject.Parse(json);
                        jContent = new JObject(
                                    new JProperty("resultCode", 0),
                                    new JProperty("Success", true),

                                               new JProperty("result",
                                                  new JObject(
                                                       new JProperty("ParkingBoy", jsonArray)

                                                      )));

                    }
                    else
                    {

                        jContent = new JObject(
                            new JProperty("resultCode", 1),
                                new JProperty("error", Errormessage),
                                            new JProperty("result",
                                                new JObject(
                                                    new JProperty("Success", false),
                                                    new JProperty("Message", Errormessage)
                                                    )));


                    }

                }
                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                                             new JProperty("resultCode", 2),
                                            new JProperty("error", "Exception Occured"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString())
                                                   )));

                }

                //return Request.CreateResponse<string>(HttpStatusCode.OK, mystring);


            }
            else
            {
                var errorList = (from item in ModelState
                                 where item.Value.Errors.Any()
                                 select item.Value.Errors[0].ErrorMessage).ToList();
                //res.StatusCode = HttpStatusCode.BadRequest;
                jContent = new JObject(
                                 new JProperty("resultCode", 3),
                                 new JProperty("error", errorList),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", errorList),
                                               new JProperty("Transation", null))));
            }
            res.Content = new StringContent(jContent.ToString());
            return res;
        }


        [Route("api/GetVehicleDetail/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetVehicleDetail(string vehicleNumber)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            string PA_User_id = string.Empty;
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            PA_User_id = AuthenticateToken(headers);
            if (PA_User_id == "0")
            {
                PA_User_id = AuthenticateTokenPB(headers);
            }
            if (PA_User_id == "0")
            {
                jContent = new JObject(
                               new JProperty("resultCode", 4),
                               new JProperty("error", "InValid AuthToken"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", "InValid AuthToken")
                                                       )));
                res.Content = new StringContent(jContent.ToString());
                return res;
            }
            if (!string.IsNullOrWhiteSpace(vehicleNumber))
            {

              try
                    {
                       
                        UserDetailsDAL objUserDAL = new UserDetailsDAL();
                        UserBookingBEL ulvm = objUserDAL.Get_VehicleDetail(vehicleNumber);

                        if (ulvm.vehicleNumber != "0" && ulvm.vehicleNumber != null)
                        {
                            //  res.StatusCode = HttpStatusCode.Created;
                            jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "successfully fetched"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "successfully fetched"),
                                                        new JProperty("VehicleNumber", ulvm.vehicleNumber),
                                                        new JProperty("CheckedIn", ulvm.CheckedIn),
                                                        new JProperty("CheckedOut", ulvm.CheckedOut),
                                                        new JProperty("ExpiryDate", ulvm.expiryDate),
                                                        new JProperty("PlanOpt", ulvm.planOpt),
                                                        new JProperty("MobileNumber", ulvm.mobileNumber)
                                                        
                                                        )));

                            // OTPController SMScontrolller = new OTPController();
                            // SMScontrolller.sendMessage(Convert.ToInt32(objUserBEL.Phone_No1), "Greeting!! Thank you for registering with TrueWheels. Your temporary  password is" + objUserBEL.Password + " . Please use this password in website login and request you to reset it. ");
                        }
                        else
                        {
                            // res.StatusCode = HttpStatusCode.BadRequest;
                            jContent = new JObject(
                                 new JProperty("resultCode", 1),
                                new JProperty("error", ulvm.ErrorMessage),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", ulvm.ErrorMessage),
                                                        new JProperty("ErrorMessage", ulvm.ErrorMessage)
                                                        )));
                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                            new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString())
                                                       )));

                    }
                }
                else
                {
                    
                    jContent = new JObject(
                        new JProperty("resultCode", 3),
                        new JProperty("error", "Vehicle Number is mendatory"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", "Vehicle Number is mendatory")
                                                   )));
                }
            
            res.Content = new StringContent(jContent.ToString());
            return res;

        }


        [Route("api/CommunicateCustomer/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage CommunicateCustomer(string vehicleNumber, string message)
        {
            string ErrorMessage = string.Empty;
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            string PA_User_id = string.Empty;
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            PA_User_id = AuthenticateToken(headers);
            if (PA_User_id == "0")
            {
                jContent = new JObject(
                               new JProperty("resultCode", 4),
                               new JProperty("error", "InValid AuthToken"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", "InValid AuthToken")
                                                       )));
                res.Content = new StringContent(jContent.ToString());
                return res;
            }
            if (!string.IsNullOrWhiteSpace(vehicleNumber))
            {

                try
                {

                    UserDetailsDAL objUserDAL = new UserDetailsDAL();
                    string Number = objUserDAL.Get_VehiclePhone(vehicleNumber, out ErrorMessage);

                    if (Number != "0" && Number != null && Number!="")
                    {
                        OTPController SMScontrolller = new OTPController();
                        if (SMScontrolller.sendMessage(Convert.ToInt64(Number), message))
                        {
                            jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "Message sent successfully"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "Message sent successfully")

                                                        )));
                        }
                        else
                        {
                            jContent = new JObject(
                                new JProperty("resultCode", 1),
                                new JProperty("error", "Sorry, Something went wrong"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", "Sorry, Something went wrong")

                                                        )));
                        }
                      
                    }
                    else
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                             new JProperty("resultCode", 1),
                            new JProperty("error", ErrorMessage),
                                            new JProperty("result",
                                                new JObject(
                                                    new JProperty("Success", false),
                                                    new JProperty("Message", ErrorMessage),
                                                    new JProperty("ErrorMessage", ErrorMessage)
                                                    )));
                    }

                }
                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 2),
                        new JProperty("error", "Exception Occured"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString())
                                                   )));

                }
            }
            else
            {

                jContent = new JObject(
                    new JProperty("resultCode", 3),
                    new JProperty("error", "Vehicle Number is mendatory"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", "Vehicle Number is mendatory")
                                               )));
            }

            res.Content = new StringContent(jContent.ToString());
            return res;

        }

        [Route("api/GetMiniReport/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetMiniReport()
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            string PA_User_id = string.Empty;
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            PA_User_id = AuthenticateToken(headers);
            if (PA_User_id == "0")
            {
                jContent = new JObject(
                               new JProperty("resultCode", 4),
                               new JProperty("error", "InValid AuthToken"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", "InValid AuthToken")
                                                       )));
                res.Content = new StringContent(jContent.ToString());
                return res;
            }
            try
            {
                string Totvehicle = string.Empty;
                string TotPBs = string.Empty;
                string ErrorMessage = string.Empty;
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                objUserDAL.Get_MiniReport(PA_User_id, out ErrorMessage, out Totvehicle, out TotPBs);

                if ((ErrorMessage == string.Empty || ErrorMessage == null || ErrorMessage == "") && Totvehicle != string.Empty && Totvehicle != null && Totvehicle != "")
                {
                    //  res.StatusCode = HttpStatusCode.Created;
                    jContent = new JObject(
                        new JProperty("resultCode", 0),
                        new JProperty("error", "successfully fetched"),
                                        new JProperty("result",
                                            new JObject(
                                                new JProperty("Success", true),
                                                new JProperty("Message", "successfully fetched"),
                                                new JProperty("totalInVehicle", Totvehicle),
                                                new JProperty("totalPBAvailable", TotPBs)
                                                )));

                    // OTPController SMScontrolller = new OTPController();
                    // SMScontrolller.sendMessage(Convert.ToInt32(objUserBEL.Phone_No1), "Greeting!! Thank you for registering with TrueWheels. Your temporary  password is" + objUserBEL.Password + " . Please use this password in website login and request you to reset it. ");
                }
                else
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                         new JProperty("resultCode", 1),
                        new JProperty("error", ErrorMessage),
                                        new JProperty("result",
                                            new JObject(
                                                new JProperty("Success", false),
                                                new JProperty("Message", ErrorMessage),
                                                new JProperty("ErrorMessage", ErrorMessage)
                                                )));
                }

            }
            catch (Exception ex)
            {
                // res.StatusCode = HttpStatusCode.BadRequest;
                jContent = new JObject(
                    new JProperty("resultCode", 2),
                    new JProperty("error", "Exception Occured"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", ex.Message),
                                               new JProperty("ErrorMessage", ex.InnerException.ToString())
                                               )));

            }


            res.Content = new StringContent(jContent.ToString());
            return res;

        }

        [Route("api/GetBriefReport/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetBriefReport(string period , string locationID)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            string PA_User_id = string.Empty;
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            PA_User_id = AuthenticateToken(headers);
            
            if (PA_User_id == "0")
            {
                jContent = new JObject(
                               new JProperty("resultCode", 4),
                               new JProperty("error", "InValid AuthToken"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", "InValid AuthToken")
                                                       )));
                res.Content = new StringContent(jContent.ToString());
                return res;
            }
            try
            {
                
                string ErrorMessage = string.Empty;
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                BookingBriefReportBEL BBR = new BookingBriefReportBEL();
                BBR = objUserDAL.Get_BriefReport(period, locationID, out  ErrorMessage);

                if ((ErrorMessage == string.Empty || ErrorMessage == null || ErrorMessage == "") )
                {
                    //  res.StatusCode = HttpStatusCode.Created;
                    jContent = new JObject(
                        new JProperty("resultCode", 0),
                        new JProperty("error", "successfully fetched"),
                                        new JProperty("result",
                                            new JObject(
                                                new JProperty("Success", true),
                                                new JProperty("Message", "successfully fetched"),
                                                new JProperty("RegularIN", BBR.RegularIN),
                                                new JProperty("RegularOut", BBR.RegularOut),
                                                new JProperty("RegularTot", BBR.RegularTot),
                                                new JProperty("MBTVehicleIn", BBR.MBTVehicleIn),
                                                new JProperty("MBTVehicleOut", BBR.MBTVehicleOut),
                                                new JProperty("MBTVehicleTot", BBR.MBTVehicleTot),
                                                new JProperty("MDTVehicleIn", BBR.MDTVehicleIn),
                                                new JProperty("MDTVehicleOut", BBR.MDTVehicleOut),
                                                new JProperty("MDTVehicleTot", BBR.MDTVehicleTot),
                                                new JProperty("MNTVehicleIn", BBR.MNTVehicleIn),
                                                new JProperty("MNTVehicleOut", BBR.MNTVehicleOut),
                                                new JProperty("MNTVehicleTot", BBR.MNTVehicleTot)
                                                )));

                    // OTPController SMScontrolller = new OTPController();
                    // SMScontrolller.sendMessage(Convert.ToInt32(objUserBEL.Phone_No1), "Greeting!! Thank you for registering with TrueWheels. Your temporary  password is" + objUserBEL.Password + " . Please use this password in website login and request you to reset it. ");
                }
                else
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                         new JProperty("resultCode", 1),
                        new JProperty("error", ErrorMessage),
                                        new JProperty("result",
                                            new JObject(
                                                new JProperty("Success", false),
                                                new JProperty("Message", ErrorMessage),
                                                new JProperty("ErrorMessage", ErrorMessage)
                                                )));
                }

            }
            catch (Exception ex)
            {
                // res.StatusCode = HttpStatusCode.BadRequest;
                jContent = new JObject(
                    new JProperty("resultCode", 2),
                    new JProperty("error", "Exception Occured"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", ex.Message),
                                               new JProperty("ErrorMessage", ex.InnerException.ToString())
                                               )));

            }


            res.Content = new StringContent(jContent.ToString());
            return res;

        }

        [Route("api/GetRevenue/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetRevenue(ReportApiModel ReportBel)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            string PA_User_id = string.Empty;
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            PA_User_id = AuthenticateToken(headers);

            if (PA_User_id == "0")
            {
                jContent = new JObject(
                               new JProperty("resultCode", 4),
                               new JProperty("error", "InValid AuthToken"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", "InValid AuthToken")
                                                       )));
                res.Content = new StringContent(jContent.ToString());
                return res;
            }
            try
            {

                string ErrorMessage = string.Empty;
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                BookingBriefReportBEL BBR = new BookingBriefReportBEL();
                BBR = objUserDAL.Get_Revenue(ReportBel.period, ReportBel.locationID, out  ErrorMessage);

                if ((ErrorMessage == string.Empty || ErrorMessage == null || ErrorMessage == ""))
                {
                    //  res.StatusCode = HttpStatusCode.Created;
                    jContent = new JObject(
                        new JProperty("resultCode", 0),
                        new JProperty("error", "successfully fetched"),
                                        new JProperty("result",
                                            new JObject(
                                                new JProperty("Success", true),
                                                new JProperty("Message", "successfully fetched"),
                                                //new JProperty("DailyCollection", "5000"),
                                                //new JProperty("WeeklyCollection", "10000"),
                                                //new JProperty("MonthlyCollection", "200000"),
                                                //new JProperty("TotalCollection", "300000")
                                                 new JProperty("DailyCollection", BBR.DailyCollection),
                                                 new JProperty("DailyHelmetCollection", BBR.DailyHelmetCollection),
                                                new JProperty("WeeklyCollection", BBR.WeeklyCollection),
                                                new JProperty("WeeklyHelmetCollection", BBR.WeeklyHelmetCollection),
                                                new JProperty("MonthlyCollection", BBR.MonthlyCollection),
                                                new JProperty("MonthlyHelmetCollection", BBR.MonthlyHelmetCollection),
                                                new JProperty("TotalCollection", BBR.TotalCollection)  ,
                                                new JProperty("TotalHelmetCollection", BBR.TotalHelmetCollection)
                                                
                                                )));

                    // OTPController SMScontrolller = new OTPController();
                    // SMScontrolller.sendMessage(Convert.ToInt32(objUserBEL.Phone_No1), "Greeting!! Thank you for registering with TrueWheels. Your temporary  password is" + objUserBEL.Password + " . Please use this password in website login and request you to reset it. ");
                }
                else
                {
                   // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                         new JProperty("resultCode", 1),
                        new JProperty("error", ErrorMessage),
                                        new JProperty("result",
                                            new JObject(
                                                new JProperty("Success", false),
                                                new JProperty("Message", ErrorMessage),
                                                new JProperty("ErrorMessage", ErrorMessage)
                                                )));
                }

            }
            catch (Exception ex)
            {
                // res.StatusCode = HttpStatusCode.BadRequest;
                jContent = new JObject(
                    new JProperty("resultCode", 2),
                    new JProperty("error", "Exception Occured"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", ex.Message),
                                               new JProperty("ErrorMessage", ex.InnerException.ToString())
                                               )));

            }


            res.Content = new StringContent(jContent.ToString());
            return res;

        }

       

        [Route("api/GetExpiringBookings/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetExpiringBookings(string ParkingID)
        {
            string Errormessage= string.Empty;
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            string PA_User_id = string.Empty;
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            PA_User_id = AuthenticateToken(headers);
            if (PA_User_id == "0")
            {
                jContent = new JObject(
                               new JProperty("resultCode", 4),
                               new JProperty("error", "InValid AuthToken"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", "InValid AuthToken")
                                                       )));
                res.Content = new StringContent(jContent.ToString());
                return res;
            }
            if (!string.IsNullOrWhiteSpace(ParkingID))
            {

                try
                {

                    UserDetailsDAL objUserDAL = new UserDetailsDAL();

                    List<UserBookingBEL> ulvm = objUserDAL.GetExpiringBookings(ParkingID, out Errormessage);
                   // UserBookingBEL ulvm = objUserDAL.Get_VehicleDetail(ParkingID);
                    string json;
                    if (ulvm.Count > 0 && Errormessage ==string.Empty)
                    {
                        

                        json = JsonConvert.SerializeObject(ulvm.ToArray());
                        var jsonArray = JArray.Parse(json);
                        //jContent = JObject.Parse(json);
                        jContent = new JObject(
                                    new JProperty("resultCode", 0),
                                    new JProperty("Success", true),

                                               new JProperty("result",
                                                  new JObject(
                                                      // new JProperty("authToken", objUserBEL.AuthToken),
                                                       new JProperty("Parkings", jsonArray)

                                                      )));  
                    }
                    else
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        json = JsonConvert.SerializeObject(ulvm.ToArray());
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject(
                             new JProperty("resultCode", 1),
                            new JProperty("error", Errormessage),
                                            new JProperty("result",
                                                new JObject(
                                                    new JProperty("Parkings", jsonArray),
                                                    new JProperty("Success", false),
                                                    new JProperty("Message", Errormessage),
                                                    new JProperty("ErrorMessage", Errormessage)
                                                    )));
                    }

                }
                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 2),
                        new JProperty("error", "Exception Occured"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString())
                                                   )));

                }
            }
            else
            {

                jContent = new JObject(
                    new JProperty("resultCode", 3),
                    new JProperty("error", "Parking ID is mendatory"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", "Parking ID is mendatory")
                                               )));
            }

            res.Content = new StringContent(jContent.ToString());
            return res;

        }


        [Route("api/TerminateExpiringBookings/")]
        [HttpPost, HttpGet]
        
        public HttpResponseMessage TerminateExpiringBookings(TerminateRequestModel ListTerminateReuest)    
        {
            string Errormessage = string.Empty;
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            string PA_User_id = string.Empty;
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            PA_User_id = AuthenticateTokenPB(headers);
            if (PA_User_id == "0")
            {
                jContent = new JObject(
                               new JProperty("resultCode", 4),
                               new JProperty("error", "InValid AuthToken"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", "InValid AuthToken")
                                                       )));
                res.Content = new StringContent(jContent.ToString());
                return res;
            }

            if (ListTerminateReuest != null)
            {
                try
                {
                    UserDetailsDAL objUserDAL = new UserDetailsDAL();
                  //  string Errormessage = string.Empty;
                    foreach (var item in ListTerminateReuest.ListTerminateRequest)
                    {

                        Errormessage = objUserDAL.TerminateExpiringBooking(item.Booking_ID, item.Action);

                        var index = ListTerminateReuest.ListTerminateRequest.IndexOf(item);
                        if (Errormessage == string.Empty)
                            ListTerminateReuest.ListTerminateRequest[index].Status = "Success";
                        else
                        {
                            ListTerminateReuest.ListTerminateRequest[index].Status = "Failed";
                            ListTerminateReuest.ListTerminateRequest[index].FailureReason = Errormessage;
                        }

                        Errormessage = string.Empty;
                    }
                    
                   // List<TerminateReuest> lis = ListTerminateReuest.ListTerminateRequest;
                   /////// string er = TerminateExpiringBookings(lis);
                   // List<UserBookingBEL> ulvm = objUserDAL.GetExpiringBookings(ParkingID, out Errormessage);
                  //  UserBookingBEL ulvm = objUserDAL.Get_VehicleDetail(ParkingID);
                    //Errormessage == string.Empty)
                    string json;
                    if (Errormessage == string.Empty)
                    {
                        json = JsonConvert.SerializeObject(ListTerminateReuest.ListTerminateRequest.ToArray());
                        var jsonArray = JArray.Parse(json);
                        //jContent = JObject.Parse(json);
                        jContent = new JObject(
                                    new JProperty("resultCode", 0),
                                    new JProperty("Success", true),

                                               new JProperty("result",
                                                  new JObject(
                                                    //   new JProperty("authToken", objUserBEL.AuthToken),
                                                       new JProperty("StatusList", jsonArray)

                                                      )));                                         
                    
                        //jContent = new JObject(
                        //        new JProperty("resultCode", 0),
                        //        new JProperty("error", "Action performed successfully"),
                        //                        new JProperty("result",
                        //                            new JObject(
                        //                                new JProperty("Success", true),
                        //                                new JProperty("Message", "All Bookings Terminated successfully")
                        //                                  //,new JProperty("UserID", ulvm.user_ID)
                        //                                )));

                       
                    }
                    else
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                             new JProperty("resultCode", 1),
                            new JProperty("error", Errormessage),
                                            new JProperty("result",
                                                new JObject(
                                                    new JProperty("Success", false),
                                                    new JProperty("Message", Errormessage),
                                                    new JProperty("ErrorMessage", Errormessage)
                                                    )));
                    }

                }
                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 2),
                        new JProperty("error", "Exception Occured"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString())
                                                   )));

                }
            }
            else
            {

                jContent = new JObject(
                    new JProperty("resultCode", 3),
                    new JProperty("error", "Parking ID is mendatory"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", "Parking ID is mendatory")
                                               )));
            }

            res.Content = new StringContent(jContent.ToString());
            return res;

        }

        [Route("api/GetBriefReport_old/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetBriefReport_old(string period, string locationID)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            string PA_User_id = string.Empty;
            System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            PA_User_id = AuthenticateToken(headers);

            if (PA_User_id == "0")
            {
                jContent = new JObject(
                               new JProperty("resultCode", 4),
                               new JProperty("error", "InValid AuthToken"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", "InValid AuthToken")
                                                       )));
                res.Content = new StringContent(jContent.ToString());
                return res;
            }
            try
            {

                string ErrorMessage = string.Empty;
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                BookingBriefReportBEL BBR = new BookingBriefReportBEL();
                BBR = objUserDAL.Get_BriefReport(period, locationID, out  ErrorMessage);

                if ((ErrorMessage == string.Empty || ErrorMessage == null || ErrorMessage == ""))
                {
                    //  res.StatusCode = HttpStatusCode.Created;
                    jContent = new JObject(
                        new JProperty("resultCode", 0),
                        new JProperty("error", "successfully fetched"),
                                        new JProperty("result",
                                            new JObject(
                                                new JProperty("Success", true),
                                                new JProperty("Message", "successfully fetched"),
                                                new JProperty("RegularIN", BBR.RegularIN),
                                                new JProperty("RegularOut", BBR.RegularOut),
                                                new JProperty("RegularTot", BBR.RegularTot),
                                                new JProperty("MBTVehicleIn", BBR.MBTVehicleIn),
                                                new JProperty("MBTVehicleOut", BBR.MBTVehicleOut),
                                                new JProperty("MBTVehicleTot", BBR.MBTVehicleTot),
                                                new JProperty("MDTVehicleIn", BBR.MDTVehicleIn),
                                                new JProperty("MDTVehicleOut", BBR.MDTVehicleOut),
                                                new JProperty("MDTVehicleTot", BBR.MDTVehicleTot),
                                                new JProperty("MNTVehicleIn", BBR.MNTVehicleIn),
                                                new JProperty("MNTVehicleOut", BBR.MNTVehicleOut),
                                                new JProperty("MNTVehicleTot", BBR.MNTVehicleTot)
                                                )));

                    // OTPController SMScontrolller = new OTPController();
                    // SMScontrolller.sendMessage(Convert.ToInt32(objUserBEL.Phone_No1), "Greeting!! Thank you for registering with TrueWheels. Your temporary  password is" + objUserBEL.Password + " . Please use this password in website login and request you to reset it. ");
                }
                else
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                         new JProperty("resultCode", 1),
                        new JProperty("error", ErrorMessage),
                                        new JProperty("result",
                                            new JObject(
                                                new JProperty("Success", false),
                                                new JProperty("Message", ErrorMessage),
                                                new JProperty("ErrorMessage", ErrorMessage)
                                                )));
                }

            }
            catch (Exception ex)
            {
                // res.StatusCode = HttpStatusCode.BadRequest;
                jContent = new JObject(
                    new JProperty("resultCode", 2),
                    new JProperty("error", "Exception Occured"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", ex.Message),
                                               new JProperty("ErrorMessage", ex.InnerException.ToString())
                                               )));

            }


            res.Content = new StringContent(jContent.ToString());
            return res;

        }
    }
}
