﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrueWheels.BEL;
using TrueWheels.DAL;
using TrueWheels.Web.Models;

namespace TrueWheels.Web.Controllers
{
    public class BookingAPIController : ApiController
    {

        [Route("api/BookThroughWorker/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage BookThroughWorker(BookingAPIModel bookingApiModel)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            Random ran = new Random();
            if (bookingApiModel != null)
            {
                
                if (ModelState.IsValid)
                {
                    try
                    {
                        BookingBEL objBookingBEL = new BookingBEL();

                        objBookingBEL.userId = bookingApiModel.userId;
                        objBookingBEL.parkingId = bookingApiModel.parkingId;
                        objBookingBEL.vehicalNo = bookingApiModel.vehicalNo;
                        objBookingBEL.mobileNo = bookingApiModel.mobileNo;
                        objBookingBEL.vehicleWheels = bookingApiModel.vehicleWheels;
                        objBookingBEL.Helmet = bookingApiModel.helmet;
                        objBookingBEL.MonthlyChargePaid = bookingApiModel.MonthlyChargePaid; //Y and N only 
                        if(string.IsNullOrWhiteSpace(bookingApiModel.MonthlySubscription))
                            objBookingBEL.MonthlySubscription = "0";
                        else
                            objBookingBEL.MonthlySubscription = bookingApiModel.MonthlySubscription; // 0,B,N,D

                        if (string.IsNullOrWhiteSpace(bookingApiModel.AutoSubscrip))
                            objBookingBEL.AutoSubscrip = "N";
                        else
                            objBookingBEL.AutoSubscrip = bookingApiModel.AutoSubscrip; // 0,B,N,D
                        
                        Random rnd = new Random();               
                         objBookingBEL.OTP = Convert.ToString(rnd.Next(100000, 999999));
                         BookingResult bookingResult = new BookingResult();
                        BookingDAL bookingDAL = new BookingDAL();
                         bookingResult = bookingDAL.BookParkingArea(objBookingBEL);

                         if (bookingResult.bookingID != 0)
                        {
                            //  res.StatusCode = HttpStatusCode.Created;
                            jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "Booked successfully"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "Booked successfully"),
                                                        new JProperty("bookingID", bookingResult.bookingID))));

                            DateTime DTCheckedIn = Convert.ToDateTime(bookingResult.checkedInDateTime.ToString());
                            string checkedInddMMyyyy = DTCheckedIn.Day + "/" + DTCheckedIn.Month + "/" + DTCheckedIn.Year + " " + DTCheckedIn.Hour + ":" + DTCheckedIn.Minute + ":" + DTCheckedIn.Second;

                            OTPController SMScontrolller = new OTPController();
                          // DateTime indianTime= DateTime.Now.AddHours(12).AddMinutes(30);
                            //SMScontrolller.sendMessage(Convert.ToInt64(objBookingBEL.mobileNo), "Your TrueWheels Parking OTP is " + objBookingBEL.OTP + " . Your vehicle number " + bookingApiModel.vehicalNo + " has been parked at " + bookingResult.parkingaddress+ " on " + bookingResult.checkedInDateTime.ToString() + ". Please show your otp when you exit till then Don't share your OTP with anyone for security purpose of your vehicle. For anytime anywhere parking ,download True Wheels App.");
                            SMScontrolller.sendMessage(Convert.ToInt64(objBookingBEL.mobileNo), "" + objBookingBEL.OTP + " is your exit OTP. Vehicle No.  " + bookingResult.vehicalNo + " has been parked at " + bookingResult.parkingaddress + " on " + checkedInddMMyyyy + " .");
                                //"Your TrueWheels Parking OTP is " + objBookingBEL.OTP + " . Your vehicle number " + bookingApiModel.vehicalNo + " has been parked at " + bookingResult.parkingaddress + " on " + bookingResult.checkedInDateTime.ToString() + ". Please show your otp when you exit till then Don't share your OTP with anyone for security purpose of your vehicle. For anytime anywhere parking ,download True Wheels App.");
                         }
                        else
                        {
                            // res.StatusCode = HttpStatusCode.BadRequest;
                            jContent = new JObject(
                                 new JProperty("resultCode", 1),
                                new JProperty("error", "Error occured"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", "Error occured"),
                                                        new JProperty("ErrorMessage", bookingResult.ErrorMessage),
                                                        new JProperty("Transation", bookingResult.bookingID))));
                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                            new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }
                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();
                    //res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 3),
                        new JProperty("error", errorList),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", errorList),
                                                   new JProperty("Transation", null))));
                }
            }
            res.Content = new StringContent(jContent.ToString());
            return res;

        }


        [Route("api/GenerateBill/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GenerateBill(RequestBillAPIModel RequestBillApiModel)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            Random ran = new Random();
            if (RequestBillApiModel != null)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        BookingBEL objBookingBEL = new BookingBEL();
                        BookingResult bookingResult = new BookingResult();
                        objBookingBEL.parkingId = RequestBillApiModel.parkingId;
                       // objBookingBEL.mobileNo = RequestBillApiModel.mobileNo;
                        objBookingBEL.OTP = RequestBillApiModel.OTP;
                        BookingDAL bookingDAL = new BookingDAL();
                         bookingResult = bookingDAL.CheckOutNGenerateBill(objBookingBEL);

                         if (bookingResult.bookingID != 0)
                        {
                            jContent = new JObject(
                           new JProperty("resultCode", 0),
                               new JProperty("error", "Success"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", true),
                                                   new JProperty("TotalAmount", bookingResult.TotalAmount),
                                                   new JProperty("VehicleNo", bookingResult.vehicalNo),
                                                   new JProperty("CheckedIN", bookingResult.checkedInDateTime),
                                                   new JProperty("CheckedOut", bookingResult.checkedOutDateTime),
                                                   new JProperty("TotalHours", bookingResult.TotalHours),
                                                   new JProperty("TotalNights", bookingResult.TotalNights),
                                                   new JProperty("NightsCharge", bookingResult.NightsCharge),
                                                   new JProperty("bookingId", bookingResult.bookingID),
                                                   new JProperty("monthlySubscription", bookingResult.MonthlySubscription)
                             )));

                            DateTime DTcheckedOutDateTime = Convert.ToDateTime(bookingResult.checkedOutDateTime.ToString());
                            string checkedOutDateTimeddMMyyyy = DTcheckedOutDateTime.Day + "/" + DTcheckedOutDateTime.Month + "/" + DTcheckedOutDateTime.Year + " " + DTcheckedOutDateTime.Hour + ":" + DTcheckedOutDateTime.Minute + ":" + DTcheckedOutDateTime.Second;
                            OTPController SMScontrolller = new OTPController();
                            if (Convert.ToInt32(bookingResult.TotalHours) < 1)
                                bookingResult.TotalHours = "Less than 1";

                            SMScontrolller.sendMessage(Convert.ToInt64(bookingResult.mobileNo), "Vehicle No " + bookingResult.vehicalNo + " is Checked out from " + bookingResult.parkingaddress + " on " + checkedOutDateTimeddMMyyyy + ".Total billable amount is Rs " + bookingResult.TotalAmount + " for " + bookingResult.TotalHours + "Hours.");
                        }
                        else
                        { 
                            // res.StatusCode = HttpStatusCode.BadRequest;
                            jContent = new JObject(
                                 new JProperty("resultCode", 1),
                                new JProperty("error", bookingResult.ErrorMessage),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", bookingResult.ErrorMessage),
                                                        new JProperty("ErrorMessage", bookingResult.ErrorMessage)
                                                        //,new JProperty("Transation", bookingResult.bookingID)
                                                        )));
                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                            new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }
                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();
                    //res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 3),
                        new JProperty("error", errorList),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", errorList),
                                                   new JProperty("Transation", null))));
                }
            }
            res.Content = new StringContent(jContent.ToString());
            return res;

        }

        [Route("api/ResendOTPForBooking/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetOTPFromDB(ResendOTPFromDBAPIModel ResendAPIModel)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            Random ran = new Random();

            string mobilenumber= string.Empty;
            string errormessage = string.Empty;
            if (ResendAPIModel != null)
            {

                if (ModelState.IsValid)
                {
                    try
                    {
                       
                        BookingDAL bookingDAL = new BookingDAL();
                        string OTP = bookingDAL.GetOTPFromDB(ResendAPIModel.vehicleNo, ResendAPIModel.parkingId, out mobilenumber,out errormessage);

                        if (OTP != "0")
                        {
                            //  res.StatusCode = HttpStatusCode.Created;
                            if (mobilenumber != "" && mobilenumber != "0")
                            {
                                OTPController SMScontrolller = new OTPController();
                                SMScontrolller.sendMessage(Convert.ToInt64(mobilenumber), "Thank you for using Truewheels. Here is Your OTP " + OTP + " . Please show it when you exit. ");

                                jContent = new JObject(
                                    new JProperty("resultCode", 0),
                                    new JProperty("error", "Sent successfully"),
                                                    new JProperty("result",
                                                        new JObject(
                                                            new JProperty("Success", true),
                                                            new JProperty("Message", "Sent successfully")
                                                            )));
                            }
                            else
                            {
                                jContent = new JObject(
                                new JProperty("resultCode", 1),
                               new JProperty("error", "Error occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", "Mobile number does not exist"),
                                                       new JProperty("ErrorMessage", "Mobile number does not exist")
                                                       )));
                            }

                            
                        }
                        else
                        {
                            // res.StatusCode = HttpStatusCode.BadRequest;
                            jContent = new JObject(
                                 new JProperty("resultCode", 1),
                                new JProperty("error", "Error occured"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", errormessage),
                                                        new JProperty("ErrorMessage", errormessage)
                                                        )));
                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                            new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }
                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();
                    
                    jContent = new JObject(
                        new JProperty("resultCode", 3),
                        new JProperty("error", errorList),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", errorList)
                                                   )));
                }
            }
            res.Content = new StringContent(jContent.ToString());
            return res;

        }


        [Route("api/PayAmount/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage PayAmount(PaymentAPIModel PaymentAPIModel)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            Random ran = new Random();
            if (PaymentAPIModel != null)
            {

                if (ModelState.IsValid)
                {
                    try
                    {

                        PaymentBEL obpaymentBEL = new PaymentBEL();
                        obpaymentBEL.bookingID = PaymentAPIModel.bookingID;
                            obpaymentBEL.paidAmount = PaymentAPIModel.paidAmount;
                            if (string.IsNullOrWhiteSpace(PaymentAPIModel.fullandFinal))
                                obpaymentBEL.fullAndFinal = "N";
                            else
                                obpaymentBEL.fullAndFinal = PaymentAPIModel.fullandFinal;

                        BookingDAL bookingDAL = new BookingDAL();
                        string result = bookingDAL.SavePayment(obpaymentBEL);

                        if (result == "TRUE")
                        {
                           
                          //  OTPController SMScontrolller = new OTPController();
                           // SMScontrolller.sendMessage(Convert.ToInt64(ResendAPIModel.mobileNo), "Thank you for using Truewheels. Here is Your OTP" + OTP + " . Please show it when you exit. ");
                            jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "Paid successfully"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "Paid successfully")
                                                        )));


                        }
                        else
                        {
                            // res.StatusCode = HttpStatusCode.BadRequest;
                            jContent = new JObject(
                                 new JProperty("resultCode", 1),
                                new JProperty("error", result),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", "Error occured"),
                                                        new JProperty("ErrorMessage", result)
                                                        )));
                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                            new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }
                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();

                    jContent = new JObject(
                        new JProperty("resultCode", 3),
                        new JProperty("error", errorList),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", errorList)
                                                   )));
                }
            }
            res.Content = new StringContent(jContent.ToString());
            return res;

        }



        [Route("api/GetBookingDetails/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetBookingDetails(RequestBookingdetails requestBookingdetails)
        {
            //HttpRequestMessage rd = new HttpRequestMessage();
            //rd.


            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            Random ran = new Random();
            if (requestBookingdetails != null)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        BookingBEL objBookingBEL = new BookingBEL();
                        BookingResult bookingResult = new BookingResult();
                        objBookingBEL.parkingId = requestBookingdetails.parkingId;
                        // objBookingBEL.mobileNo = RequestBillApiModel.mobileNo;
                        objBookingBEL.OTP = requestBookingdetails.OTP;
                        BookingDAL bookingDAL = new BookingDAL();
                        bookingResult = bookingDAL.GetBookingDetails(objBookingBEL);

                        if (bookingResult.bookingID != 0)
                        {
                            jContent = new JObject(
                           new JProperty("resultCode", 0),
                               new JProperty("error", "Success"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", true),
                                                   new JProperty("VehicleNumber", bookingResult.vehicalNo),
                                                   new JProperty("VehicleWheels", bookingResult.vehiclewheels),
                                                   new JProperty("CheckedIN", bookingResult.checkedInDateTime),
                                                   new JProperty("CheckedOut", bookingResult.checkedOutDateTime),
                                                   new JProperty("Mobile", bookingResult.mobileNo),
                                                   new JProperty("MonthlySubscription", bookingResult.MonthlySubscription),
                                                   new JProperty("MonthlyDailyCheckIN", bookingResult.MonthlyDailyCheckIN),
                                                   new JProperty("Autosubscriped", bookingResult.AutoSuscription),                                                   
                                                   new JProperty("BookingID", bookingResult.bookingID)
                                                   
                                                   )));

                            //OTPController SMScontrolller = new OTPController();
                            //if (Convert.ToInt32(bookingResult.TotalHours) < 1)
                            //    bookingResult.TotalHours = "Less than 1";

                            //SMScontrolller.sendMessage(Convert.ToInt64(bookingResult.mobileNo), "Dear Customer, Your vehicle no " + bookingResult.vehicalNo + " is Checked out from " + bookingResult.parkingaddress + " on " + bookingResult.checkedOutDateTime + ". Thank you for Visiting Truewheels. We Hope you liked our service and will visit again . Your total billable amount is Rs " + bookingResult.TotalAmount + " for " + bookingResult.TotalHours + "Hours. For anytime anywhere parking ,download True Wheels App.");
                        }
                        else
                        {
                            // res.StatusCode = HttpStatusCode.BadRequest;
                            jContent = new JObject(
                                 new JProperty("resultCode", 1),
                                new JProperty("error", bookingResult.ErrorMessage),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", bookingResult.ErrorMessage),
                                                        new JProperty("ErrorMessage", bookingResult.ErrorMessage)
                                //,new JProperty("Transation", bookingResult.bookingID)
                                                        )));
                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                            new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }
                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();
                    //res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 3),
                        new JProperty("error", errorList),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", errorList),
                                                   new JProperty("Transation", null))));
                }
            }
            res.Content = new StringContent(jContent.ToString());
            return res;

        }


        [Route("api/DailyCheckInforMonthlyCustomer/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage DailyCheckInforMonthlyCustomer(RequestBookingdetails requestBookingdetails)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            Random ran = new Random();
            if (requestBookingdetails != null)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        BookingBEL objBookingBEL = new BookingBEL();
                        BookingResult bookingResult = new BookingResult();
                        objBookingBEL.parkingId = requestBookingdetails.parkingId;
                        if (string.IsNullOrWhiteSpace(requestBookingdetails.mobileNo))
                        { objBookingBEL.mobileNo = "0";
                        }
                        else
                            objBookingBEL.mobileNo = requestBookingdetails.mobileNo;
                        objBookingBEL.OTP = requestBookingdetails.OTP;

                        Random rnd = new Random();
                        objBookingBEL.NewOTP = Convert.ToString(rnd.Next(100000, 999999));

                        BookingDAL bookingDAL = new BookingDAL();
                        bookingResult = bookingDAL.DailyMonthlyCheckIn(objBookingBEL);

                        
                        if (bookingResult.bookingID != 0)
                        {
                            
                                //  res.StatusCode = HttpStatusCode.Created;
                                jContent = new JObject(
                                    new JProperty("resultCode", 0),
                                    new JProperty("error", "Booked successfully"),
                                                    new JProperty("result",
                                                        new JObject(
                                                            new JProperty("Success", true),
                                                            new JProperty("Message", "Booked successfully"),
                                                            new JProperty("bookingID", bookingResult.bookingID)))
                                                   //         ,new JProperty("VehicleNumber", bookingResult.vehicalNo),
                                                   //new JProperty("VehicleWheels", bookingResult.vehiclewheels),
                                                   //new JProperty("CheckedIN", bookingResult.checkedInDateTime),
                                                   //new JProperty("CheckedOut", bookingResult.checkedOutDateTime),
                                                   //new JProperty("Mobile", bookingResult.mobileNo),
                                                   //new JProperty("MonthlySubscription", bookingResult.MonthlySubscription),
                                                   //new JProperty("MonthlyDailyCheckIN", bookingResult.MonthlyDailyCheckIN),
                                                   //new JProperty("BookingID", bookingResult.bookingID)
                                                            );
                            DateTime DTCheckedIn= Convert.ToDateTime(bookingResult.checkedInDateTime.ToString());
                            string checkedInddMMyyyy = DTCheckedIn.Day + "/" + DTCheckedIn.Month + "/" + DTCheckedIn.Year + " " + DTCheckedIn.Hour + ":" + DTCheckedIn.Minute + ":" + DTCheckedIn.Second;

                                if (bookingResult.Isnumberchanged == "Y")
                                {
                                    OTPController SMScontrolller = new OTPController();
                                    SMScontrolller.sendMessage(Convert.ToInt64(objBookingBEL.mobileNo), "Your Contact number has been updated. if you have not requested it please call us on " + ConfigurationManager.AppSettings["TWSupportContactNumber"].ToString());
                                }
                            OTPController SMScontrolller1 = new OTPController();
                            SMScontrolller1.sendMessage(Convert.ToInt64(bookingResult.mobileNo), "" + objBookingBEL.NewOTP + " is your exit OTP. Vehicle No. " + bookingResult.vehicalNo + " has been parked at " + bookingResult.parkingaddress + " on " + checkedInddMMyyyy + "");
                                                      
                                                                                                
                        }
                        else
                        {
                            // res.StatusCode = HttpStatusCode.BadRequest;
                            jContent = new JObject(
                                 new JProperty("resultCode", 1),
                                new JProperty("error", bookingResult.ErrorMessage),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", bookingResult.ErrorMessage),
                                                        new JProperty("ErrorMessage", bookingResult.ErrorMessage)
                                //,new JProperty("Transation", bookingResult.bookingID)
                                                        )));
                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                            new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }
                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();
                    //res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 3),
                        new JProperty("error", errorList),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", errorList),
                                                   new JProperty("Transation", null))));
                }
            }
            res.Content = new StringContent(jContent.ToString());
            return res;

        }



        [Route("api/TerminateOrChangeMonthlySubscription/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage TerminateOrChangeMonthlySubscription(RequestTerminateSubscription requestTerminateSubscription)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            Random ran = new Random();
            if (requestTerminateSubscription != null)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        BookingBEL objBookingBEL = new BookingBEL();
                        BookingResult bookingResult = new BookingResult();
                        objBookingBEL.parkingId = requestTerminateSubscription.parkingId;
                        objBookingBEL.changeOrTurminateflag = requestTerminateSubscription.changeOrTerminate;

                        if (!string.IsNullOrWhiteSpace(requestTerminateSubscription.bookingId))
                            objBookingBEL.OTP = requestTerminateSubscription.OTP;
                        else
                            objBookingBEL.OTP = "0";

                        if (!string.IsNullOrWhiteSpace(requestTerminateSubscription.bookingId))
                            objBookingBEL.bookingID = Convert.ToInt64(requestTerminateSubscription.bookingId);
                        else
                            objBookingBEL.bookingID = 0;

                        BookingDAL bookingDAL = new BookingDAL();
                        bookingResult = bookingDAL.TerminateOrChangeSubscription(objBookingBEL);

                        if (string.IsNullOrWhiteSpace(bookingResult.ErrorMessage))
                        {
                            //  res.StatusCode = HttpStatusCode.Created;
                            OTPController SMScontrolller = new OTPController();
                            if (objBookingBEL.changeOrTurminateflag == "T")
                            {
                                jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "Terminated successfully"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "Terminated successfully")
                                    //, new JProperty("bookingID", bookingResult.bookingID)
                                                        )));
                                DateTime DTUnSuscriptionDate = Convert.ToDateTime(bookingResult.UnSuscriptionDate.ToString());
                                string UnSuscriptionDateddMMyyyy = DTUnSuscriptionDate.Day + "/" + DTUnSuscriptionDate.Month + "/" + DTUnSuscriptionDate.Year + " " + DTUnSuscriptionDate.Hour + ":" + DTUnSuscriptionDate.Minute + ":" + DTUnSuscriptionDate.Second;


                                SMScontrolller.sendMessage(Convert.ToInt64(bookingResult.mobileNo), "Your vehicle number " + bookingResult.vehicalNo + " has been unsubscribed from  monthly service.  on " + UnSuscriptionDateddMMyyyy + "");
                            }
                            else
                            {
                                jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "Subscription Changed successfully"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "Subscription Changed successfully")
                                    //, new JProperty("bookingID", bookingResult.bookingID)
                                                        )));
                                //string 

                                DateTime DTUnSuscriptionDate = Convert.ToDateTime(bookingResult.UnSuscriptionDate.ToString());
                                string UnSuscriptionDateddMMyyyy = DTUnSuscriptionDate.Day + "/" + DTUnSuscriptionDate.Month + "/" + DTUnSuscriptionDate.Year + " " + DTUnSuscriptionDate.Hour + ":" + DTUnSuscriptionDate.Minute + ":" + DTUnSuscriptionDate.Second;

                                SMScontrolller.sendMessage(Convert.ToInt64(bookingResult.mobileNo), "Your vehicle number " + bookingResult.vehicalNo + " monthly subscription has been changed on " + UnSuscriptionDateddMMyyyy + "");
                            }
                        }
                        else
                        {
                            // res.StatusCode = HttpStatusCode.BadRequest;
                            jContent = new JObject(
                                 new JProperty("resultCode", 1),
                                new JProperty("error", bookingResult.ErrorMessage),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", bookingResult.ErrorMessage),
                                                        new JProperty("ErrorMessage", bookingResult.ErrorMessage)
                                //,new JProperty("Transation", bookingResult.bookingID)
                                                        )));
                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                            new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }
                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();
                    //res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 3),
                        new JProperty("error", errorList),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", errorList),
                                                   new JProperty("Transation", null))));
                }
            }
            res.Content = new StringContent(jContent.ToString());
            return res;

        }


        [Route("api/GetBookingDetailsByVehiclenumber/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetBookingDetailsByVehiclenumber(RequestBookingdetailsByVehicle requestBookingdetails)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            Random ran = new Random();
            if (requestBookingdetails != null)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        BookingBEL objBookingBEL = new BookingBEL();
                        BookingResult bookingResult = new BookingResult();
                        objBookingBEL.parkingId = requestBookingdetails.parkingId;
                        // objBookingBEL.mobileNo = RequestBillApiModel.mobileNo;
                        objBookingBEL.vehicalNo = requestBookingdetails.vehicleNumber;
                        BookingDAL bookingDAL = new BookingDAL();
                        // bookingResult = bookingDAL.GetBookingDetails(objBookingBEL);
                        //  bookingResult = bookingDAL.GetBookingDetailsByVehicle(objBookingBEL);
                        string Errormessage = string.Empty;
                        List<BookingResult> AVP = bookingDAL.GetBookingDetailsByVehicle(objBookingBEL, out Errormessage);
                        string json = string.Empty;





                        if (AVP.Count() > 0 && (Errormessage == string.Empty || Errormessage == ""))
                        {
                            json = JsonConvert.SerializeObject(AVP.ToArray());
                            var jsonArray = JArray.Parse(json);
                            jContent = new JObject(
                           new JProperty("resultCode", 0),
                               new JProperty("error", "Success"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", true),
                                                   new JProperty("bookingResult", jsonArray)
                                                  

                                                   )));

                            //OTPController SMScontrolller = new OTPController();
                            //if (Convert.ToInt32(bookingResult.TotalHours) < 1)
                            //    bookingResult.TotalHours = "Less than 1";

                            //SMScontrolller.sendMessage(Convert.ToInt64(bookingResult.mobileNo), "Dear Customer, Your vehicle no " + bookingResult.vehicalNo + " is Checked out from " + bookingResult.parkingaddress + " on " + bookingResult.checkedOutDateTime + ". Thank you for Visiting Truewheels. We Hope you liked our service and will visit again . Your total billable amount is Rs " + bookingResult.TotalAmount + " for " + bookingResult.TotalHours + "Hours. For anytime anywhere parking ,download True Wheels App.");
                        }
                        else
                        {
                            // res.StatusCode = HttpStatusCode.BadRequest;
                            jContent = new JObject(
                                 new JProperty("resultCode", 1),
                                new JProperty("error", Errormessage),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", Errormessage),
                                                        new JProperty("ErrorMessage", Errormessage)
                                //,new JProperty("Transation", bookingResult.bookingID)
                                                        )));
                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                            new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }
                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();
                    //res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 3),
                        new JProperty("error", errorList),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", errorList),
                                                   new JProperty("Transation", null))));
                }
            }
            res.Content = new StringContent(jContent.ToString());
            return res;

        }

        // GET api/bookingapi
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

     
    
    }
}
