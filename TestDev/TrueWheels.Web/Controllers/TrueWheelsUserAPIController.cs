﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrueWheels.BEL;
using TrueWheels.DAL;
using TrueWheels.Web.Models;


namespace TrueWheels.Web.Controllers
{

    public class abc
    {
        public string phone { get; set; }
    }

    public class TrueWheelsUserAPIController : ApiController
    {
        public string AuthenticateToken(System.Net.Http.Headers.HttpRequestHeaders headers)
        {

            string token = string.Empty;
            string user_ID = string.Empty;
            if (headers.Contains("authToken"))
            {
                token = headers.GetValues("authToken").First();
                UserDetailsDAL objUserDAL = new UserDetailsDAL();
                user_ID = objUserDAL.AuthenticateToken(token);
            }
            else
                user_ID = "0";

            return user_ID;

        }

        [Route("api/signup/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage Signup(SignupAPIModel vm)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            Random ran = new Random();
            if (vm != null)
            {
                vm.password = Secure.Encrypt("TWuser@" + Convert.ToString(ran.Next(1000, 9999)));
                vm.confirmPassword = vm.password;
                if (ModelState.IsValid)
                {
                    try
                    {
                        UserDetailsBEL objUserBEL = new UserDetailsBEL();

                        objUserBEL.Email_Id = vm.emailId;
                        objUserBEL.Password = vm.password;
                        objUserBEL.First_Name = vm.firstName;
                        objUserBEL.Last_Name = vm.lastName;
                        objUserBEL.Phone_No1 = vm.mobile;

                        UserDetailsDAL objUserDAL = new UserDetailsDAL();
                        UserLoginDetailsViewModel ulvm = new UserLoginDetailsViewModel(objUserDAL.AddUserDetails(objUserBEL));

                        if (ulvm.User_ID != "0")
                        {
                          //  res.StatusCode = HttpStatusCode.Created;
                            jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "User added successfully"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "User added successfully"),
                                                        new JProperty("Transation", ulvm.User_ID))));

                            OTPController SMScontrolller = new OTPController();
                            SMScontrolller.sendMessage(Convert.ToInt32(objUserBEL.Phone_No1), "Greeting!! Thank you for registering with TrueWheels. Your temporary  password is" + objUserBEL.Password + " . Please use this password in website login and request you to reset it. ");
                        }
                        else
                        {
                           // res.StatusCode = HttpStatusCode.BadRequest;
                            jContent = new JObject(
                                 new JProperty("resultCode",1),
                                new JProperty("error", ulvm.ErrorMessage),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", ulvm.ErrorMessage),
                                                        new JProperty("ErrorMessage", ulvm.ErrorMessage),
                                                        new JProperty("Transation", ulvm.User_ID))));
                        }

                    }
                    catch (Exception ex)
                    {
                       // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                            new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }
                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();
                    //res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 3),
                        new JProperty("error", errorList),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", errorList),
                                                   new JProperty("Transation", null))));
                }
            }
            res.Content = new StringContent(jContent.ToString());
            return res;

        }

        [Route("api/SignupAPIModel4FBNGG/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage SignupAPIModel4FBNGG(SignupAPIModel4FBGG SVMFBGG)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            Random ran = new Random();
            if (SVMFBGG != null)
            {
                SVMFBGG.password = Secure.Encrypt(SVMFBGG.firstName + "@Truewheels");//"TWuser@" + Convert.ToString(ran.Next(1000, 9999)));
                SVMFBGG.confirmPassword = SVMFBGG.password;
                if (ModelState.IsValid)
                {
                    try
                    {
                         UserDetailsBEL objUserBEL = new UserDetailsBEL();

                        objUserBEL.Email_Id = SVMFBGG.emailId;
                        objUserBEL.Password = SVMFBGG.firstName + "@Truewheels";
                        objUserBEL.First_Name = SVMFBGG.firstName;
                        objUserBEL.Last_Name = SVMFBGG.lastName;
                        objUserBEL.SignUp_Mode_ID = SVMFBGG.signUpModeID;
                        objUserBEL.Phone_No1 = SVMFBGG.mobile;
                        objUserBEL.Other_ID = SVMFBGG.fbId;
                       // Session["Pic_url"] = SVMFB.picUrl;
                        UserDetailsDAL objUserDAL = new UserDetailsDAL();
                        UserLoginDetailsViewModel LDvm = new UserLoginDetailsViewModel(objUserDAL.AddUserDetails(objUserBEL));
                        if (string.IsNullOrEmpty(LDvm.ErrorMessage) && (LDvm.User_ID != "" || LDvm.User_ID != string.Empty || LDvm.User_ID != "0"))
                        {
                                                   //  res.StatusCode = HttpStatusCode.Created;
                            jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "User added successfully"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "User added successfully"),
                                                        new JProperty("Transation", LDvm.User_ID))));

                         //   OTPController SMScontrolller = new OTPController();
                           // SMScontrolller.sendMessage(Convert.ToInt32(objUserBEL.Phone_No1), "Greeting!! Thank you for registering with TrueWheels. Your temporary  password is" + objUserBEL.Password + " . Please use this password in website login and request you to reset it. ");
                        }
                        else
                        {
                            // res.StatusCode = HttpStatusCode.BadRequest;
                            jContent = new JObject(
                                 new JProperty("resultCode", 1),
                                new JProperty("error", LDvm.ErrorMessage),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", LDvm.ErrorMessage),
                                                        new JProperty("ErrorMessage", LDvm.ErrorMessage),
                                                        new JProperty("Transation", LDvm.User_ID))));
                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                            new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }
                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();
                    //res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 3),
                        new JProperty("error", errorList),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", errorList),
                                                   new JProperty("Transation", null))));
                }
            }
            res.Content = new StringContent(jContent.ToString());
            return res;
            
        }

        //public bool SignUpWithGG(string Fullname, string EmailId, string GG_id, string Pic_url)
        //{
        //    UserDetailsBEL objUserBEL = new UserDetailsBEL();

        //    objUserBEL.Email_Id = EmailId;
        //    objUserBEL.Password = Fullname + "@Truewheels";
        //    objUserBEL.First_Name = Fullname;
        //    objUserBEL.Last_Name = "";
        //    objUserBEL.SignUp_Mode_ID = "GG";
        //    objUserBEL.Phone_No1 = "NA";
        //    objUserBEL.Other_ID = GG_id;
        //  //  Session["Pic_url"] = Pic_url;

        //    UserDetailsDAL objUserDAL = new UserDetailsDAL();
        //    UserLoginDetailsViewModel LDvm = new UserLoginDetailsViewModel(objUserDAL.AddUserDetails(objUserBEL));
        //    if (string.IsNullOrEmpty(objUserBEL.ErrorMessage) && (objUserBEL.User_ID != "" || objUserBEL.User_ID != string.Empty))
        //    {
        //        //FormsAuthentication.SetAuthCookie(objUserBEL.User_ID, true);
        //        Session["userDetail"] = LDvm;
        //        return true;
        //        // return RedirectToAction("Index", "UserDashbaord");
        //    }
        //    else
        //    {
        //        return false;
        //        ModelState.AddModelError("", objUserBEL.ErrorMessage.ToString());
        //    }


        //}

        [Route("api/login/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage Login(UserLogin vm)
        {
            var res = new HttpResponseMessage();
            JObject jContent;
            if (ModelState.IsValid)
            {


                try
                {
                    UserDetailsBEL objUserBEL = new UserDetailsBEL();

                    objUserBEL.Email_Id = vm.email;
                    objUserBEL.Password = vm.password;
                    objUserBEL.Phone_No1 = vm.mobile;
                    objUserBEL.User_Name = vm.userName;

                    UserDetailsDAL objUserDAL = new UserDetailsDAL();
                    UserLoginDetailsViewModel lvm = new UserLoginDetailsViewModel(objUserDAL.FunAuthenticateUser(objUserBEL));

                    if (lvm.User_ID != "0")
                    {
                       // res.StatusCode = HttpStatusCode.Accepted;
                        jContent = new JObject(
                            new JProperty("resultCode", 0),
                                new JProperty("error", "Success"),
                                            new JProperty("result",
                                                new JObject(
                                                    new JProperty("Success", true),
                                                    new JProperty("Email", lvm.Email_Id),
                                                    new JProperty("Mobile", lvm.Phone_No1),
                                                    new JProperty("Username", lvm.User_Name),
                                                    new JProperty("LastLogin", lvm.Last_Login),
                                                    new JProperty("UserId", lvm.User_ID))));
                    }
                    else
                    {
                       // res.StatusCode = HttpStatusCode.Unauthorized;
                        jContent = new JObject(
                            new JProperty("resultCode", 1),
                                new JProperty("error", lvm.ErrorMessage),
                                            new JProperty("result",
                                                new JObject(
                                                    new JProperty("Success", false),
                                                    new JProperty("Message", lvm.ErrorMessage),
                                                    new JProperty("ErrorMessage", lvm.ErrorMessage),
                                                    new JProperty("UserId", lvm.User_ID))));
                    }

                }
                catch (Exception ex)
                {
                   // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                                             new JProperty("resultCode", 2),
                                            new JProperty("error", "Exception Occured"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                   new JProperty("UserId", null))));

                }

                //return Request.CreateResponse<string>(HttpStatusCode.OK, mystring);


            }
            else
            {
                var errorList = (from item in ModelState
                                 where item.Value.Errors.Any()
                                 select item.Value.Errors[0].ErrorMessage).ToList();
                //res.StatusCode = HttpStatusCode.BadRequest;
                jContent = new JObject(
                                 new JProperty("resultCode", 3),
                                 new JProperty("error", errorList),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", errorList),
                                               new JProperty("Transation", null))));
            }
            res.Content = new StringContent(jContent.ToString());
            return res;
        }


        [Route("api/IsRegistered/")]
        [HttpPost, HttpGet]

        public HttpResponseMessage IsRegistered(string Phone_no)
        {
            // string Phone_no = av.Mobile; //this.Request.Content.ReadAsStringAsync().Result;
            var res = new HttpResponseMessage();
            JObject jContent;
            if (!string.IsNullOrEmpty(Phone_no))//(ModelState.IsValid)
            {
                try
                {
                    UserDetailsDAL objUserDAL = new UserDetailsDAL();
                    UserLoginDetailsViewModel lvm = new UserLoginDetailsViewModel(objUserDAL.CheckPhoneNoExists(Phone_no));

                    if (lvm.User_ID != "0")
                    {
                       // res.StatusCode = HttpStatusCode.Accepted;
                        jContent = new JObject(
                            new JProperty("resultCode", 0),
                                new JProperty("error", "Success"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", true),
                                                   new JProperty("Email", lvm.Email_Id),
                                                   new JProperty("Mobile", lvm.Phone_No1),
                                                   new JProperty("Username", lvm.User_Name),
                                                   new JProperty("LastLogin", lvm.Last_Login),
                                                   new JProperty("UserId", lvm.User_ID),
                                                   new JProperty("Alternate_Email_Id", lvm.Alternate_Email_Id))));

                    }
                    else
                    {
                       // res.StatusCode = HttpStatusCode.Unauthorized;
                        jContent = new JObject(
                            new JProperty("resultCode", 1),
                                new JProperty("error", lvm.ErrorMessage),
                                            new JProperty("result",
                                                new JObject(
                                                    new JProperty("Success", false),
                                                    new JProperty("Message", lvm.ErrorMessage),
                                                    new JProperty("ErrorMessage", lvm.ErrorMessage),
                                                    new JProperty("UserId", lvm.User_ID))));
                    }

                }
                catch (Exception ex)
                {
                   // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                                             new JProperty("resultCode", 2),
                                            new JProperty("error", "Exception Occured"),
                                           new JProperty("result",                        
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                   new JProperty("UserId", null))));

                }

                //return Request.CreateResponse<string>(HttpStatusCode.OK, mystring);

            }
            else
            {
                //res.StatusCode = HttpStatusCode.BadRequest;
                jContent = new JObject(
                    new JProperty("resultCode", 3),
                        new JProperty("error", "Phone_no Either null or Empty"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", "Phone_no Either null or Empty"),
                                               new JProperty("UserId", null))));
            }
            res.Content = new StringContent(jContent.ToString());
            return res;
            //throw new HttpResponseException(HttpStatusCode.BadRequest);
        }


        [Route("api/GetLocationHint/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetLocations(string Searchterm)
        {
            var res = new HttpResponseMessage();
            JObject jContent;
            string json = string.Empty;
            if (!string.IsNullOrEmpty(Searchterm))//(ModelState.IsValid)
            {
                // List<string> locations;
                ParkingAreaDAL Pdal = new ParkingAreaDAL();

                List<LocationsIDName> AVP = Pdal.GetAllLocation(Searchterm.ToUpper());
                if (AVP.Count()>0)
                {                  
                    json = JsonConvert.SerializeObject(AVP.ToArray());
                    var jsonArray = JArray.Parse(json);
                    //jContent = JObject.Parse(json);
                    jContent = new JObject(new JProperty("Success", true),
                                           new JProperty("result",
                                               jsonArray));
                    // return Json(locations, JsonRequestBehavior.AllowGet);

                }
                else
                {
                   // res.StatusCode = HttpStatusCode.Unauthorized;
                    jContent = new JObject(new JProperty("Success", false),
                                        new JProperty("result",
                                            new JObject(
                                                new JProperty("Message", "Not Found"),
                                                new JProperty("ErrorMessage", "No data found")
                                                )));
                }
            }
            else
            {
               // res.StatusCode = HttpStatusCode.BadRequest;
                jContent = new JObject(
                    new JProperty("resultCode", 3),
                        new JProperty("error", "Invalid Input"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", "Searchterm Either null or Empty"),
                                               new JProperty("UserId", null))));
            }
            res.Content = new StringContent(jContent.ToString());
            return res;
        }



        [Route("api/SearchLocation/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage SearchLocation(SearchParking Svm)
        {
            var lat = string.Empty;
            var lng = string.Empty;
            bool flag;
            string FDateTime = "0";
            string TDateTime = "0";
            string strOrderBy = "Space_Type";
            PublicParkingAreaBEL parking = new PublicParkingAreaBEL();
            string json = string.Empty;
            var res = new HttpResponseMessage();
            JObject jContent;
            if (ModelState.IsValid)
            {
                parking.PopularParking = Svm.PopularParking;
                if (string.IsNullOrEmpty(Svm.Destination) || string.IsNullOrEmpty(Svm.FromDatetime) || string.IsNullOrEmpty(Svm.ToDatetime))
                {
                    if (string.IsNullOrWhiteSpace(Svm.Destination))
                    {
                        if (!(string.IsNullOrWhiteSpace(Svm.Lat) && string.IsNullOrWhiteSpace(Svm.Long)))
                        {
                            parking.Main_Latitude = decimal.Parse(Svm.Lat);
                            parking.Main_Longitude = decimal.Parse(Svm.Long);
                        }
                        else
                        {
                            lat = "0";
                            lng = "0";
                            parking.Main_Latitude = decimal.Parse(lat);
                            parking.Main_Longitude = decimal.Parse(lng);
                        }
                        
                    }
                    else
                    {
                        LatLong.GetLatLongByLocation(Svm.Destination, ref lat, ref lng);
                        parking.Main_Latitude = decimal.Parse(lat);
                        parking.Main_Longitude = decimal.Parse(lng);
                    }
                    if (!(string.IsNullOrWhiteSpace(Svm.FromDatetime) || string.IsNullOrWhiteSpace(Svm.ToDatetime)))
                    {
                        FDateTime = Svm.FromDatetime;
                        TDateTime = Svm.ToDatetime;
                    }

                    parking.Distance = Convert.ToInt32(ConfigurationManager.AppSettings["Searchdistance"].ToString());
                    parking.FromDateTime = FDateTime;
                    parking.ToDateTime = TDateTime;
                    if (string.IsNullOrEmpty(Svm.ParkingClass))
                        parking.ParkingClass = "PC_A";
                    else
                        parking.ParkingClass = Svm.ParkingClass;// "PC_A";
                    
                    parking.OrderBy = strOrderBy;

                    ParkingAreaDAL parkingDal = new ParkingAreaDAL();
                    List<AvailablePublicParkingAreaResult> availableParkingList = parkingDal.GetAvailableParkingPublicForApp(parking);
                    if (availableParkingList.Count > 0)
                    {

                        json = JsonConvert.SerializeObject(availableParkingList.ToArray());
                       // res.StatusCode = HttpStatusCode.Accepted;
                        var jsonArray = JArray.Parse(json);
                        //jContent = JObject.Parse(json);
                        jContent = new JObject(
                            new JProperty("resultCode", 0),
                                new JProperty("error", "Success"),
                           // new JProperty("Success", true),
                                               new JProperty("result",
                                                    new JObject(
                                               new JProperty("parkingList", jsonArray))));

                    }
                    else
                    {
                       // res.StatusCode = HttpStatusCode.Unauthorized;
                        jContent = new JObject(
                             new JProperty("resultCode", 1),
                                new JProperty("error", "No Parking available at or nearby this location"),                                           
                                            new JProperty("result",
                                                new JObject(
                                                     new JProperty("Success", false),
                                                    new JProperty("Message", "Error Occured"),
                                                    new JProperty("ErrorMessage", "No Parking available at or nearby this location")
                            // ,new JProperty("UserId", "0")
                                                    )));
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(Svm.Lat) && string.IsNullOrWhiteSpace(Svm.Long))
                    {
                        parking.Main_Latitude = decimal.Parse(Svm.Lat);
                        parking.Main_Longitude = decimal.Parse(Svm.Long);
                    }
                    else
                    { LatLong.GetLatLongByLocation(Svm.Destination, ref lat, ref lng);
                    parking.Main_Latitude = decimal.Parse(lat);
                    parking.Main_Longitude = decimal.Parse(lng);
                    }
                    
                    parking.Distance = Convert.ToInt32(ConfigurationManager.AppSettings["Searchdistance"].ToString());
                    parking.FromDateTime = FDateTime;
                    parking.ToDateTime = TDateTime;
                    parking.ParkingClass = Svm.ParkingClass;// "PC_A";
                    
                    parking.OrderBy = strOrderBy;

                    ParkingAreaDAL parkingDal = new ParkingAreaDAL();
                    List<AvailablePublicParkingAreaResult> availableParkingList = parkingDal.GetAvailableParkingPublicForApp(parking);

                    if (availableParkingList.Count > 0)
                    {
                        json = JsonConvert.SerializeObject(availableParkingList);
                       // res.StatusCode = HttpStatusCode.Accepted;
                        //res.Content = new StringContent(json);
                        
                        var jsonArray = JArray.Parse(json);
                        jContent = new JObject( new JProperty("resultCode", 0),
                            new JProperty("error", "Success"),
                                //(new JProperty("Success", true),
                                              new JProperty("result",
                                                   new JObject(
                                               new JProperty("parkingList",
                                                  jsonArray))));
                        // jContent = JObject.Parse(json);
                        //jContent = new JObject(new JProperty("Success", true),
                        //                       new JProperty("result",
                        //                           new JObject(JObject.Parse(json))));

                    }
                    else
                    {
                      //  res.StatusCode = HttpStatusCode.Unauthorized;
                        jContent = new JObject(
                             new JProperty("resultCode", 1),
                                new JProperty("error", "No Parking available at or nearby this location"),                                            
                                            new JProperty("result",
                                                new JObject(
                                                    new JProperty("Success", false),
                                                    new JProperty("Message", "Error Occured"),
                                                    new JProperty("ErrorMessage", "No Parking available at or nearby this location")
                            // ,new JProperty("UserId", "0")
                                                    )));
                    }
                }
            }

            else
            {
                var errorList = (from item in ModelState
                                 where item.Value.Errors.Any()
                                 select item.Value.Errors[0].ErrorMessage).ToList();
               // res.StatusCode = HttpStatusCode.BadRequest;
                jContent = new JObject(
                     new JProperty("resultCode", 3),
                        new JProperty("error", errorList),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", errorList),
                                               new JProperty("Transation", null))));
            }

            res.Content = new StringContent(jContent.ToString());
            return res;

        }

        [Route("api/GetNotification/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage GetNotification(string UserId)
        {
            var res = new HttpResponseMessage();
            JObject jContent;
            string json = string.Empty;
            string PA_User_id = string.Empty;
            //Random ran = new Random();
            //System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
            //PA_User_id = AuthenticateToken(headers);
            //if (PA_User_id == "0")
            //{
            //    jContent = new JObject(
            //                   new JProperty("resultCode", 4),
            //                   new JProperty("error", "InValid AuthToken"),
            //                                   new JProperty("result",
            //                                       new JObject(
            //                                           new JProperty("Success", false),
            //                                           new JProperty("Message", "InValid AuthToken")
            //                                           )));
            //    res.Content = new StringContent(jContent.ToString());
            //    return res;
            //}
            if (!string.IsNullOrEmpty(UserId))//(ModelState.IsValid)
            {
                // List<string> locations;
                UserInboxBEL userInboxBEL = new UserInboxBEL();
                UserDetailsDAL userDetailsDAL = new UserDetailsDAL();
                var getNotification = userDetailsDAL.GetUserNotification(UserId);
                if (getNotification.Count() > 0)
                {
                    json = JsonConvert.SerializeObject(getNotification.ToArray());
                    var jsonArray = JArray.Parse(json);
                    //jContent = JObject.Parse(json);
                    jContent = new JObject(new JProperty("Success", true),
                                           new JProperty("result",
                                               jsonArray));
                    // return Json(locations, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    // res.StatusCode = HttpStatusCode.Unauthorized;
                    jContent = new JObject(new JProperty("Success", false),
                                        new JProperty("result",
                                            new JObject(
                                                new JProperty("Message", "Not Found"),
                                                new JProperty("ErrorMessage", "No data found")
                                                )));
                }
            }
            else
            {
                // res.StatusCode = HttpStatusCode.BadRequest;
                jContent = new JObject(
                    new JProperty("resultCode", 3),
                        new JProperty("error", "Invalid Input"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),
                                               new JProperty("ErrorMessage", "Invalid Input"),
                                               new JProperty("UserId", null))));
            }
            res.Content = new StringContent(jContent.ToString());
            return res;
        }

        [Route("api/SignUpApp/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage SignUpApp(SignUpAppModel vm)
        {
            var res = new HttpResponseMessage();
            JObject jContent = new JObject();
            Random ran = new Random();
            if (vm != null)
            {
               
                if (ModelState.IsValid)
                {
                    try
                    {
                        UserDetailsfrmAppBEL objUserBEL = new UserDetailsfrmAppBEL();

                        objUserBEL.Email_Id = vm.Email_Id;
                        //objUserBEL.Password = vm.password;
                        objUserBEL.Full_Name = vm.Full_Name;
                        objUserBEL.Alternate_No = vm.Alternate_No;
                        //objUserBEL.Last_Name = vm.lastName;
                        objUserBEL.Phone_No1 = vm.Phone_No1;
                        objUserBEL.SignUp_Mode_ID = vm.SignUp_Mode_ID;
                        objUserBEL.Other_ID = vm.Other_ID;
                        objUserBEL.User_ID = vm.user_id;

                        UserDetailsDAL objUserDAL = new UserDetailsDAL();
                        UserLoginDetailsViewModel ulvm = new UserLoginDetailsViewModel(objUserDAL.AddUserDetailsFrmAPP(objUserBEL));

                        if (ulvm.ErrorMessage == "" && string.IsNullOrEmpty(ulvm.ErrorMessage) && string.IsNullOrWhiteSpace(ulvm.ErrorMessage))//ErrorMessage == "" && string.IsNullOrEmpty(ErrorMessage) && string.IsNullOrWhiteSpace(ErrorMessage)
                        {
                            //  res.StatusCode = HttpStatusCode.Created;
                            jContent = new JObject(
                                new JProperty("resultCode", 0),
                                new JProperty("error", "User added successfully"),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", true),
                                                        new JProperty("Message", "User added successfully"),
                                                        new JProperty("User_ID", ulvm.User_ID),
                                                         new JProperty("FullName", ulvm.FullName))));

                            OTPController SMScontrolller = new OTPController();
                            SMScontrolller.sendMessage(Convert.ToInt64(objUserBEL.Phone_No1), "Greeting!! Thank you for registering with TrueWheels App.!Search Book Park!");// + objUserBEL.Password + " . Please use this password in website login and request you to reset it. ");
                        }
                        else
                        {
                            // res.StatusCode = HttpStatusCode.BadRequest;
                            jContent = new JObject(
                                 new JProperty("resultCode", 1),
                                new JProperty("error", ulvm.ErrorMessage),
                                                new JProperty("result",
                                                    new JObject(
                                                        new JProperty("Success", false),
                                                        new JProperty("Message", ulvm.ErrorMessage),
                                                        new JProperty("ErrorMessage", ulvm.ErrorMessage),
                                                        new JProperty("Transation", ulvm.User_ID))));
                        }

                    }
                    catch (Exception ex)
                    {
                        // res.StatusCode = HttpStatusCode.BadRequest;
                        jContent = new JObject(
                            new JProperty("resultCode", 2),
                            new JProperty("error", "Exception Occured"),
                                               new JProperty("result",
                                                   new JObject(
                                                       new JProperty("Success", false),
                                                       new JProperty("Message", ex.Message),
                                                       new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                       new JProperty("Transation", null))));

                    }
                }
                else
                {
                    var errorList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select item.Value.Errors[0].ErrorMessage).ToList();
                    //res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 3),
                        new JProperty("error", errorList),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", "Invalid Input"),
                                                   new JProperty("ErrorMessage", errorList),
                                                   new JProperty("Transation", null))));
                }
            }
            res.Content = new StringContent(jContent.ToString());
            return res;

        }
    }
}