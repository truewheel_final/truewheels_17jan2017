﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrueWheels.Web.Controllers
{
    public class SearchRYSParking
    {
        public string Distance { get; set; }
        public string SearchDateTime { get; set; }
        public string TimeFrom { get; set; }

        //public string ParkingClass { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public string distance { get; set; }
    }
}
