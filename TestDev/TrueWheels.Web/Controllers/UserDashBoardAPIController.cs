﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrueWheels.BEL;
using TrueWheels.DAL;
using TrueWheels.Web.Models;
using Newtonsoft.Json.Linq;
using System.Web;

namespace TrueWheels.Web.Controllers
{
    public class UserDashBoardAPIController : ApiController
    {
        [Route("api/ViewProfile/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage ViewProfile(string User_id)
        {
            string json = string.Empty;
            var res = new HttpResponseMessage();
            JObject jContent;
            if (!string.IsNullOrEmpty(User_id))
            {
                try
                {
                    UserDetailsDAL objUserDAL = new UserDetailsDAL();
                    List<DashBoardBEL> UserMenus = objUserDAL.GetMenuMapping(User_id);
                    PersonalinfoBEL objPersonalInfo = objUserDAL.GetProfileInfo(User_id);
                    objPersonalInfo.profilePicPath = objPersonalInfo.userId + ".png";
                    objPersonalInfo.fullName = objPersonalInfo.firstName + " " + objPersonalInfo.lastName;
                    objPersonalInfo.menuList = UserMenus;
                    if (objPersonalInfo.userId == 0)
                    {
                        //  res.StatusCode = HttpStatusCode.Unauthorized;
                        jContent = new JObject(
                            //new JProperty("Success", false),
                            //new JProperty("result",
                            //    new JObject(
                                             new JProperty("resultCode", 1),
                                             new JProperty("error", "Failed"),
                                             new JProperty("result",
                                                new JObject(
                                                     new JProperty("Success", false),
                                                    new JProperty("Message", ""),
                                                    new JProperty("ErrorMessage", objPersonalInfo.errorMessage == null ? "" : objPersonalInfo.errorMessage),
                                                    new JProperty("UserId", "0"))));
                    }
                    else
                    {
                        json = JsonConvert.SerializeObject(objPersonalInfo);
                        // res.StatusCode = HttpStatusCode.Accepted;
                        //res.Content = new StringContent(json);
                        jContent = JObject.Parse(json);
                        jContent = new JObject(
                            //new JProperty("Success", true),
                             new JProperty("resultCode", 0),
                                new JProperty("error", "Success"),
                                new JProperty("result",
                                                    new JObject(JObject.Parse(json))));
                    }
                    //jContent = new JObject(res.Content);
                    // return res;
                }
                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        //new JProperty("Success", false),
                        //new JProperty("result",
                        //new JObject(
                                new JProperty("resultCode", 1),
                                new JProperty("error", "Failed"),
                                            new JProperty("result",
                                                new JObject(
                                                     new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                   new JProperty("UserId", null))));

                    //  return res;
                }
            }
            //throw new HttpResponseException(HttpStatusCode.BadRequest);
            else
            {
                // res.StatusCode = HttpStatusCode.Unauthorized;
                jContent = new JObject(
                    //new JProperty("Success", false),
                    //new JProperty("result",
                    //    new JObject(
                                        new JProperty("resultCode", 3),
                            new JProperty("error", "Invalid Input"),
                                           new JProperty("result",
                                               new JObject(
                                               new JProperty("Success", false),
                                            new JProperty("Message", "Invalid Input"),
                                            new JProperty("ErrorMessage", "User_id is Either empty or null"),
                                            new JProperty("UserId", "0"))));
                // res.Content = new StringContent(jContent.ToString());
            }
            res.Content = new StringContent(jContent.ToString());
            return res;



        }



        [Route("api/upateprofile/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage UpateProfile(PersonalinfoBEL profileInfo)//, HttpPostedFileBase profilePic
        {
            string json = string.Empty;
            var res = new HttpResponseMessage();
            JObject jContent;
            if (ModelState.IsValid)
            {
                try
                {
                    UserDetailsDAL objUserDAL = new UserDetailsDAL();
                    // List<DashBoardBEL> UserMenus = objUserDAL.GetMenuMapping((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                    if (profileInfo.newPassword == null)
                    {
                        profileInfo.newPassword = profileInfo.password;
                    }
                    //if (profileInfo.MobileNo == null)
                    //{
                    //    profileInfo.NewPassword = profileInfo.Password;
                    //}

                    UserMaintenanceViewModel objUserMaintenance = new UserMaintenanceViewModel();

                    objUserMaintenance.Personalinfo = objUserDAL.UpdateProfileInfo(profileInfo);
                    objUserMaintenance.Personalinfo.profilePicPath = objUserMaintenance.Personalinfo.userId + ".png";
                    objUserMaintenance.Personalinfo.fullName = objUserMaintenance.Personalinfo.firstName + " " + objUserMaintenance.Personalinfo.lastName;
                    // ViewBag.DashBoardMenu = UserMenus;
                    if (!string.IsNullOrEmpty(objUserMaintenance.Personalinfo.errorMessage))
                    {
                        //res.StatusCode = HttpStatusCode.Unauthorized;
                        jContent = new JObject(
                                            new JProperty("resultCode", 1),
                                            new JProperty("error", "Failed"),
                                            new JProperty("result",
                                                new JObject(
                                                    new JProperty("Success", false),
                                                    new JProperty("Message", "Error Occured"),
                                                    new JProperty("ErrorMessage", objUserMaintenance.Personalinfo.errorMessage),
                                                    new JProperty("UserId", "0"))));
                    }
                    else
                    {
                        json = JsonConvert.SerializeObject(objUserMaintenance);
                        //res.StatusCode = HttpStatusCode.Accepted;
                        //res.Content = new StringContent(json);
                        jContent = JObject.Parse(json);
                        jContent = new JObject(new JProperty("resultCode", 0),
                                 new JProperty("error", "Success"),
                                                 new JProperty("result",
                                                     new JObject(JObject.Parse(json))));
                        //jContent = new JObject(
                        //        new JProperty("resultCode", 0),
                        //        new JProperty("error", "Success"),                            
                        //                        new JProperty("result",
                        //                       new JObject(
                        //                           new JProperty("Success", true),
                        //                           new JObject(JObject.Parse(json)))));
                    }


                }
                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 1),
                                new JProperty("error", "Failed"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                   new JProperty("Transation", null))));

                }
            }
            else
            {
                var errorList = (from item in ModelState
                                 where item.Value.Errors.Any()
                                 select item.Value.Errors[0].ErrorMessage).ToList();

                var exceptionList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select Convert.ToString(item.Value.Errors[0].Exception)).ToList();
                errorList.AddRange(exceptionList);
                //res.StatusCode = HttpStatusCode.BadRequest;

                List<string> g = new List<string>();
                jContent = new JObject(
                            new JProperty("resultCode", 3),
                            new JProperty("error", "Invalid Input"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),

                                               new JProperty("ErrorMessage", errorList), //g = (errorList.Count < 1) ? exceptionList : errorList
                                               new JProperty("Transation", null))));
            }
            res.Content = new StringContent(jContent.ToString());
            return res;

        }


        //Added by Neeraj for update in App 

        [Route("api/updateprofileinApp/")]
        [HttpPost, HttpGet]
        public HttpResponseMessage UpdateProfileApp(PersonalinfoAppBEL personalinfoAppBEL)//, HttpPostedFileBase profilePic
        {
            string json = string.Empty;
            var res = new HttpResponseMessage();
            JObject jContent;
            if (ModelState.IsValid)
            {
                try
                {
                    UserDetailsDAL objUserDAL = new UserDetailsDAL();
                    // List<DashBoardBEL> UserMenus = objUserDAL.GetMenuMapping((((TrueWheels.Web.Models.UserLoginDetailsViewModel)(Session["userDetail"])).User_ID).ToString());
                    if (personalinfoAppBEL.newPassword == null)
                    {
                        personalinfoAppBEL.newPassword = personalinfoAppBEL.password;
                    }
                    //if (profileInfo.MobileNo == null)
                    //{
                    //    profileInfo.NewPassword = profileInfo.Password;
                    //}

                    UserMaintenanceViewModelApp objUserMaintenanceapp = new UserMaintenanceViewModelApp();

                    objUserMaintenanceapp.PersonalinfoApp = objUserDAL.UpdateProfileInfoApp(personalinfoAppBEL);
                    //objUserMaintenanceapp.Personalinfo.profilePicPath = objUserMaintenance.Personalinfo.userId + ".png";
                    objUserMaintenanceapp.PersonalinfoApp.fullName = objUserMaintenanceapp.PersonalinfoApp.firstName + " " + objUserMaintenanceapp.PersonalinfoApp.lastName;
                    // ViewBag.DashBoardMenu = UserMenus;
                    if (!string.IsNullOrEmpty(objUserMaintenanceapp.PersonalinfoApp.errorMessage))
                    {
                        //res.StatusCode = HttpStatusCode.Unauthorized;
                        jContent = new JObject(
                                            new JProperty("resultCode", 1),
                                            new JProperty("error", "Failed"),
                                            new JProperty("result",
                                                new JObject(
                                                    new JProperty("Success", false),
                                                    new JProperty("Message", "Error Occured"),
                                                    new JProperty("ErrorMessage", objUserMaintenanceapp.PersonalinfoApp.errorMessage),
                                                    new JProperty("UserId", "0"))));
                    }
                    else
                    {
                        json = JsonConvert.SerializeObject(objUserMaintenanceapp);
                        //res.StatusCode = HttpStatusCode.Accepted;
                        //res.Content = new StringContent(json);
                        jContent = JObject.Parse(json);
                        jContent = new JObject(new JProperty("resultCode", 0),
                                 new JProperty("error", "Success"),
                                                 new JProperty("result",
                                                     new JObject(JObject.Parse(json))));
                        //jContent = new JObject(
                        //        new JProperty("resultCode", 0),
                        //        new JProperty("error", "Success"),                            
                        //                        new JProperty("result",
                        //                       new JObject(
                        //                           new JProperty("Success", true),
                        //                           new JObject(JObject.Parse(json)))));
                    }


                }
                catch (Exception ex)
                {
                    // res.StatusCode = HttpStatusCode.BadRequest;
                    jContent = new JObject(
                        new JProperty("resultCode", 1),
                                new JProperty("error", "Failed"),
                                           new JProperty("result",
                                               new JObject(
                                                   new JProperty("Success", false),
                                                   new JProperty("Message", ex.Message),
                                                   new JProperty("ErrorMessage", ex.InnerException.ToString()),
                                                   new JProperty("Transation", null))));

                }
            }
            else
            {
                var errorList = (from item in ModelState
                                 where item.Value.Errors.Any()
                                 select item.Value.Errors[0].ErrorMessage).ToList();

                var exceptionList = (from item in ModelState
                                     where item.Value.Errors.Any()
                                     select Convert.ToString(item.Value.Errors[0].Exception)).ToList();
                errorList.AddRange(exceptionList);
                //res.StatusCode = HttpStatusCode.BadRequest;

                List<string> g = new List<string>();
                jContent = new JObject(
                            new JProperty("resultCode", 3),
                            new JProperty("error", "Invalid Input"),
                                       new JProperty("result",
                                           new JObject(
                                               new JProperty("Success", false),
                                               new JProperty("Message", "Invalid Input"),

                                               new JProperty("ErrorMessage", errorList), //g = (errorList.Count < 1) ? exceptionList : errorList
                                               new JProperty("Transation", null))));
            }
            res.Content = new StringContent(jContent.ToString());
            return res;

        }

    }
}

