﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrueWheels.Web.Models
{
    public class SignupAPIModel
    {
        [Required(ErrorMessage = "Please Enter Your First Name")]
        [Display(Name = "First Name")]
        public string firstName { get; set; }

        [Display(Name = "Last Name")]
        public string lastName { get; set; }

        [Display(Name = "Email Id")]
        public string emailId { get; set; }

        [Display(Name = "Mobile No.")]
        [Required(ErrorMessage = "Your must provide a PhoneNumber")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string mobile { get; set; }

        
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [DataType(DataType.Password)]
         public string confirmPassword { get; set; }

    }

    public class SignupAPIModel4FBGG
    {
       // string FirstName, string LastName, string EmailId, string Fb_id, string Pic_url;

        
        [Display(Name = "First Name")]
        public string firstName { get; set; }

        [Display(Name = "Last Name")]
        public string lastName { get; set; }

        [Display(Name = "Email Id")]
        public string emailId { get; set; }

        [Display(Name = "Mobile No.")]
        [Required(ErrorMessage = "Your must provide a PhoneNumber")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string mobile { get; set; }

        [Display(Name = "Fb_id")]
        public string fbId { get; set; }

        [Display(Name = "Pic_url")]
        public string picUrl { get; set; }

        [Display(Name = "Password")]
        public string password { get; set; }

        [Display(Name = "ConfirmPassword")]
        public string confirmPassword { get; set; }

        [Required(ErrorMessage = "Please Enter SignUpMode (Possible value can be GG for google and FB for facebook")]
        [Display(Name = "SignUpModeID")]
        public string signUpModeID { get; set; }

        
       
    }

    public class UtilityPBSignupAPIModel
    {

        [Required(ErrorMessage = "Please Enter Your Name")]
        [Display(Name = "UserName")]
        public string userName { get; set; }


        [Display(Name = "Email Id")]
        public string emailId { get; set; }

        [Display(Name = "Mobile No.")]
        [Required(ErrorMessage = "Your must provide a PhoneNumber")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string mobile { get; set; }


        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [DataType(DataType.Password)]
        public string confirmPassword { get; set; }

        public string dateOfBirth { get; set; }

        public string address { get; set; }

        public string isAdminRightsAssigned { get; set; }

        public string pACustomerId { get; set; }

        public Int64 workingLocation { get; set; }


        
        

    }

    public class UtilityPBProfileAPIModel
    {
        [Required(ErrorMessage = "Your must provide PB UserID")]
        public string userID { get; set; }
        public string userName { get; set; }


        [Display(Name = "Email Id")]
        public string emailId { get; set; }

        [Display(Name = "Mobile No.")]
        
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string mobile { get; set; }


        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [DataType(DataType.Password)]
        public string confirmPassword { get; set; }

        public string dateOfBirth { get; set; }

        public string address { get; set; }

        public string isAdminRightsAssigned { get; set; }

        public string pACustomerId { get; set; }

        public Int64 workingLocation { get; set; }





    }
}