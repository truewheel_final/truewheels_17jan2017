﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrueWheels.Web.Models
{
    public class GenerateOTPModel
    {
        [Display(Name = "Mobile")]
        [Required(ErrorMessage = "Please enter the mobile no.")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Number should be 10 digit long.")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string mobileNo { get; set; }

    }
}