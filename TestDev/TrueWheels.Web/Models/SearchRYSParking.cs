﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrueWheels.Web.Models
{
    public class SearchRYSParking
    {
        public string Distance { get; set; }
        public string SearchDateTime { get; set; }
        public string TimeFrom { get; set; }

        //public string ParkingClass { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }

        //[Required(ErrorMessage = "Please Enter the value of PopularParking flag")]
        //public bool PopularParking { get; set; }
    }
}