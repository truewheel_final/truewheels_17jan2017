﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrueWheels.Web.Models
{
    public class DashBaordViewModal
    {
       public string Menu_id { get; set; }

       public string Menu_description { get; set; }

       public string Menu_Url { get; set; }

       public string User_id { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name is Required.")]
       public string Name { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email is Required.")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                            @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
                            ErrorMessage = "Email is not valid")]
       public string Email { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Subject is Required.")]
       public string Subject { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Message is Required.")]
       public string Message { get; set; }

        //By Amit
       public List<InviteDetails> objInviteDetails { get; set; }

     //  public string UserPicLink { get; set; }
       //[Required(AllowEmptyStrings = false, ErrorMessage = "Email is Required.")]
       //[RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
       //                    @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
       //                    @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
       //                    ErrorMessage = "Email is not valid")]
       public string FriendEmailId { get; set; }
    }

    //by Amit
    public class InviteDetails
    {
        public string EmailInvite { get; set; }
        public bool IsTrue { get; set; }
        public string InviteName { get; set; }

    }
}