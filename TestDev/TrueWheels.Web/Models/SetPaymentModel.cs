﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TrueWheels.BEL;

namespace TrueWheels.Web.Models
{
    public class SetPaymentModel // : RYSBookSpaceRequest
    {
        [Required(ErrorMessage = "Not a Valid User")]
        [Display(Name = "User ID")]
        public Int64 login_userId { get; set; }

        //[Required(ErrorMessage = "Not Valid Parking Owner")]
        //[Display(Name = "User ID")]
        //public Int64 owner_userId { get; set; }

        [Required(ErrorMessage = "Not Valid Parking")]
        [Display(Name = "ParkingID")]
        public string parkingId { get; set; }

                //public string Bank_Name { get; set; }

        
        //public string Card_Type { get; set; }

        [Required(ErrorMessage = "Please Select Vehicle")]
        [Display(Name = "vehicalNo")]
        public string vehicalNo { get; set; }


        
        //public string mobileNo { get; set; }

        //[Required(ErrorMessage = "Please Enter In Time")]
        //[Display(Name = "IN Time")]
        public string bookedInTime { get; set; }

        [Required(ErrorMessage = "Please Enter Out time ")]
        [Display(Name = "Out Time")]
        public string bookedOutDatetime { get; set; }

       
        public string checkedInDateTime { get; set; }

       
        public string OTP { get; set; }

       
        public string vehicleType { get; set; }

        [Required(ErrorMessage = "Please Enter paymentMode")]
        [Display(Name = "paymentMode")]
        public string paymentMode { get; set; }



        
        public string paymentAmount { get; set; }

        //[Required(ErrorMessage = "Incorrect bookingID")]
        //[Display(Name = "IN Time")]
        //public Int64 bookingID { get; set; }

        //[Required(ErrorMessage = "Please Enter extraHours")]
        //[Display(Name = "extraHours")]
        //public string extraHours { get; set; }

        //[Required(ErrorMessage = "Please Enter In time ")]
        //[Display(Name = "IN Time")]

        //[Required(ErrorMessage = "Please Enter In time ")]
        //[Display(Name = "IN Time")]
        //public string Helmet { get; set; }

        
        public bool services { get; set; }

        //[Required(ErrorMessage = "Please Enter In time ")]
        //[Display(Name = "IN Time")]
        //public string Error { get; set; }

        
        public string vehicleWheel { get; set; }

        
        public string monthlySubscription { get; set; }

        
        public string hourBaseParking { get; set; }

        
        public string BookingStatus { get; set; }

       
        public bool Status { get; set; }

        public string Transaction_Id { get; set; }

        public string BaseAmount { get; set; }
        public string PayableToRYSAmount { get; set; }
        public string BookingTDSAmount { get; set; }
        public string TotalAmount { get; set; }
        public string TWServiceCharge { get; set; }
        public string RYSTDSAmount { get; set; }

    }
}