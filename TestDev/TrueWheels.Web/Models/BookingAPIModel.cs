﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrueWheels.Web.Models
{
    public class BookingAPIModel
    {
       
	    [Required(ErrorMessage = "userId is Mandatory")]
        [Display(Name = "User ID")]
        public Int64 userId { get; set; }

        [Required(ErrorMessage = "parkingId is Mandatory")]
        [Display(Name = "parkingId")]
        public string parkingId { get; set; }

        [Required(ErrorMessage = "Vehical Number is Mandatory")]
        [Display(Name = "Vehical Number")]
        public string vehicalNo { get; set; }

        
        [Display(Name = "Mobile No.")]
        [Required(ErrorMessage = "Your must provide a PhoneNumber")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string mobileNo { get; set; }

        [Display(Name = "BookedInTime")]
        public string bookedInTime { get; set; }

         [Display(Name = "BookedOutDatetime")]
        public string bookedOutDatetime { get; set; }

          [Display(Name = "CheckedInDateTime")]
        public string checkedInDateTime { get; set; }

         [Display(Name = "AllotedSeat")]
        public string allotedSeat { get; set; }

         [Required(ErrorMessage = "Vehical Wheels is Mandatory")]
         [Display(Name = "Vehical Wheels")]
         public string vehicleWheels { get; set; }

        [Display(Name = "MonthlySubscription")]
         public string MonthlySubscription { get; set; }

        [Display(Name = "AutoSubscrip")]
         public string AutoSubscrip { get; set; }

        [Display(Name = "helmet")]
        public string helmet { get; set; }

        [Display(Name = "MonthlyChargePaid")]
        public string MonthlyChargePaid { get; set; }
	 
	 
	}

    public class ResendOTPFromDBAPIModel
    {
              

        [Required(ErrorMessage = "parkingId is Mandatory")]
        [Display(Name = "parkingId")]
        public string parkingId { get; set; }
        
        [Display(Name = "Mobile No.")]
       // [Required(ErrorMessage = "Your must provide a PhoneNumber")]
        [DataType(DataType.PhoneNumber)]
       // [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string mobileNo { get; set; }

        [Required(ErrorMessage = "vehicleNo is Mandatory")]
        [Display(Name = "vehicleNo")]
        public string vehicleNo { get; set; }
    }

    public class PaymentAPIModel
    {
        
        [Required(ErrorMessage = "paidAmount is Mandatory")]
        [Display(Name = "paidAmount")]
        public Int32 paidAmount { get; set; }


        [Required(ErrorMessage = "bookingID is Mandatory")]
        [Display(Name = "bookingID")]
        public Int64 bookingID{ get; set; }

        public string fullandFinal { get; set; }
    }
    
}