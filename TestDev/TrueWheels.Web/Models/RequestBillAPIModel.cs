﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrueWheels.Web.Models
{
    public class RequestBillAPIModel
    {
        
        [Required(ErrorMessage = "parkingId is Mandatory")]
        [Display(Name = "parkingId")]
        public string parkingId { get; set; }
                
        [Display(Name = "Mobile No.")]
       // [Required(ErrorMessage = "Your must provide a PhoneNumber")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string mobileNo { get; set; }
        
        [Display(Name = "CheckedInDateTime")]
        public string checkedInDateTime { get; set; }

        [Required(ErrorMessage = "OTP is Mandatory")]
        [Display(Name = "OTP")]
        public string OTP { get; set; }

	 
	 
    }

    public class RequestBookingdetails
    {

        [Required(ErrorMessage = "parkingId is Mandatory")]
        [Display(Name = "parkingId")]
        public string parkingId { get; set; }

            
        [Required(ErrorMessage = "OTP is Mandatory")]
        [Display(Name = "OTP")]
        public string OTP { get; set; }

        [Display(Name = "Mobile No.")]
        // [Required(ErrorMessage = "Your must provide a PhoneNumber")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string mobileNo { get; set; }

        

    }

    public class RequestTerminateSubscription
    {
       
        [Required(ErrorMessage = "parkingId is Mandatory")]
        [Display(Name = "parkingId")]
        public string parkingId { get; set; }


        [Required(ErrorMessage = "OTP is Mandatory")]
        [Display(Name = "OTP")]
        public string OTP { get; set; }
        
       // [Required(ErrorMessage = "BookingId is Mandatory")]
        [Display(Name = "BookingId")]
        public string bookingId { get; set; }

        [Required(ErrorMessage = "ChangeOrTerminate is Mandatory. Possible values are T,C.")]
        [Display(Name = "ChangeOrTerminate")]
        public string changeOrTerminate { get; set; }

        
    }

    public class RequestBookingdetailsByVehicle
    {

        [Required(ErrorMessage = "parkingId is Mandatory")]
        [Display(Name = "parkingId")]
        public string parkingId { get; set; }



        public string OTP { get; set; }

        [Required(ErrorMessage = "vehicleNumber is Mandatory")]
        [Display(Name = "vehicleNumber")]
        public string vehicleNumber { get; set; }

        [Display(Name = "Mobile No.")]
        // [Required(ErrorMessage = "Your must provide a PhoneNumber")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string mobileNo { get; set; }



    }
}