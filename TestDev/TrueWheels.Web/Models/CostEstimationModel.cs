﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrueWheels.Web.Models
{
    public class CostEstimationModel
    {
        [Required(ErrorMessage = "Please Enter In time ")]
        [Display(Name = "IN Time")]
        public string BookedIntime { get; set; }


        [Required(ErrorMessage = "Please Enter Out Time ")]
        [Display(Name = "Out Time")]
        public string BookedOutTime { get; set; }


        [Required(ErrorMessage = "Please Enter Parking ID ")]
        [Display(Name = "Parking ID")]
        public string Parking_ID { get; set; }

        [Required(ErrorMessage = "Please Enter Vehicle Type ")]
        [Display(Name = "Vehicle Type")]
        public string Vehicle_Type { get; set; }

                public string MonthlySuscription { get; set; }
    }
}