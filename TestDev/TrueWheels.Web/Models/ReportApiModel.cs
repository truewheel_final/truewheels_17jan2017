﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrueWheels.Web.Models
{
    public class ReportApiModel
    {
            [Required(ErrorMessage = "Please Enter period ")]
            [Display(Name = "Period")]
            public string period { get; set; }


            [Required(ErrorMessage = "Please Enter locationID ")]
            [Display(Name = "locationID")]
            public string locationID { get; set; }
                      

            public List<string> locationIDTestList { get; set; }
        
    }
}