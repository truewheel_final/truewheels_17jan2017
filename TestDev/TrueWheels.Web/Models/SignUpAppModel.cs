﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrueWheels.Web.Models
{
    public class SignUpAppModel
    {
        [Required(ErrorMessage = "Please Enter Your Full Name")]
        [Display(Name = "Full Name")]
        public string Full_Name { get; set; }


        public string user_id { get; set; }

        [Display(Name = "Email_Id")]
        public string Email_Id { get; set; }

        [Display(Name = "Mobile No.")]
        [Required(ErrorMessage = "Your must provide a PhoneNumber")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string Phone_No1 { get; set; }

        [Display(Name = "Alternate Phone Number.")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string Alternate_No { get; set; }

        public string SignUp_Mode_ID { get; set; }

        public string Other_ID { get; set; }
        
        

        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        //[DataType(DataType.Password)]
        //public string password { get; set; }

        //[DataType(DataType.Password)]
        //public string confirmPassword { get; set; }

    }
}