﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrueWheels.Web.Models
{
    public class UserLogin
    {
        [Display(Name="Username")]
        public string userName { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }
        [Display(Name = "Mobile")]
        public string mobile { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string password { get; set; }
    }

    public class UserLoginPAPB
    {
        [Display(Name = "Username")]
        public string userName { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "Mobile")]
        [Required]
        public string mobile { get; set; }

        [Required]
        public string password { get; set; }

        public string @userType { get; set; }

    }

    public class UserIMageDocUrls
    {
        [Required]
        [Display(Name = "userID")]
        public string userID { get; set; }

        [Display(Name = "picUrl")]
        public string picUrl { get; set; }

        [Display(Name = "docURL")]
        public string docURL { get; set; }

        

    }
}