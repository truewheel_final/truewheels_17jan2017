﻿using System.Web;
using System.Web.Optimization;

namespace TrueWheels.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));
            bundles.Add(new ScriptBundle("~/bundles/ScrriptsJQuery").Include(
                     "~/Scripts/jquery-ui.min.js"
                     ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                      "~/Content/js/vendor/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/Content/js").Include(
               "~/Content/js/vendor/jquery-1.10.2.min.js",
                "~/Content/js/vendor/jquery-ui.js",
                "~/Content/js/vendor/jquery.unobtrusive-ajax.js",
               "~/Content/js/vendor/modernizr-2.6.2.min.js", 
              "~/Content/js/bootstrap.min.js",
              "~/Content/js/owl.carousel.min.js",
              "~/Content/js/wow.js",
            "~/Content/js/main.js",
            "~/Content/js1/jquery-1.11.1.min.js",
            "~/Content/js1/jquery.ba-cond.min.js",
            "~/Content/js1/jquery.easing.min.js",
            "~/Content/js1/jquery.fancybox.pack.js",
             "~/Content/js1/jquery.parallax-1.1.3.js",
              "~/Content/js1/jquery.singlePageNav.min.js",
              "~/Content/js1/jquery.slitslider.js",
              "~/Content/js1/main.js",
              "~/Content/js1/modernizr-2.6.2.min.js",
              "~/Content/js1/owl.carousel.min.js",
               "~/Content/js1/wow.min.js",
               "~/Content/js/jquery-1.8.3.js",
               "~/Content/js/jquery-ui.js"
           ));

            bundles.Add(new StyleBundle("~/Content/css/stylebundle").Include(
                   
                    "~/Content/css/truewheels.css",
                    "~/Content/css/animate.css ",
                    "~/Content/css/bootstrap-theme.css ",
                    "~/Content/css/bootstrap.min.css",
                    "~/Content/css/font-awesome.min.css",
                    "~/Content/css/fontello.css",
                    "~/Content/css/normalize.css",
                    "~/Content/css/owl.carousel.css",
                    "~/Content/css/owl.theme.css",                    
                    "~/Content/css/jquery-ui.css",
                    "~/Content/css/theme.css"
                    
                    ));
            bundles.Add(new StyleBundle("~/Content/bundles").Include(
                    "~/Content/style.css",
                    "~/Content/responsive.css",
                    "~/Content/jquery-ui.css",
                    "~/Content/main.css",
                    "~/Content/DashBoard.css",
                    "~/Content/Site.css",
                    "~/Content/Wrapper.css",
                    "~/Content/font-awesome-4.7.0/css/font-awesome.css",
                    "~/Content/font-awesome-4.7.0/css/font-awesome.min.css"
                    ));

            bundles.Add(new StyleBundle("~/Content/css/Homestyle").Include(
                  "~/Content/css/main.css"
                   ));

              bundles.Add(new ScriptBundle("~/bundles/HomeIndexjs").Include(
                      "~/Content/js1/jquery.singlePageNav.min.js",
                      "~/Content/js1/owl.carousel.min.js"
                      ,"~/Content/js1/jquery.slitslider.js" 
                      , "~/Content/js1/main.js"));

              bundles.Add(new ScriptBundle("~/bundles/DashBoardjs").Include(
                       "~/Content/js1/jquery.singlePageNav.min.js",
                       "~/Content/js1/owl.carousel.min.js"
                       , "~/Content/js1/jquery.slitslider.js"
                       , "~/Content/js1/main.js"));

              bundles.Add(new ScriptBundle("~/bundles/otpjs").Include(
                  "~/Content/js/vendor/jquery-1.10.2.min.js"
                        ));

              bundles.Add(new ScriptBundle("~/bundles/Commonjs").Include(
                   "~/Content/js/vendor/jquery-1.10.2.min.js"
                         ));

              bundles.Add(new StyleBundle("~/Content/css/ParkingIndexstyle").Include(
                             "~/Content/css/main.css"
                             ));
            bundles.Add(new ScriptBundle("~/bundles/Locatejs").Include(
                   "~/Content/js/jquery-1.8.3.js",
                   "~/Content/js/jquery-ui.js"
                         ));

            bundles.Add(new StyleBundle("~/Content/css/Locatestyle").Include(
                             "~/Content/css/jquery-ui.css"
                             ));
            bundles.Add(new ScriptBundle("~/bundles/Forgotpassjs").Include(
                      "~/Scripts/jquery-1.8.2.min.js",
                      "~/Scripts/jquery.validate.min.js",
                      "~/Scripts/jquery.validate.unobtrusive.min.js",
                      "~/Content/js/vendor/jquery-1.10.2.min.js"
                            ));
            bundles.Add(new StyleBundle("~/Content/Login/css/Signupstyle").Include(
                             "~/Content/Login/css/fontcss.css",
                             "~/Content/Login/css/style.css"
                             ));
            bundles.Add(new StyleBundle("~/Content/css/Signupstyle2").Include(
                             "~/Content/css/main.css"
                             ));

            bundles.Add(new ScriptBundle("~/bundles/Signupjs").Include(
                      "~/Scripts/jquery-1.8.2.min.js",
                      "~/Scripts/jquery.validate.min.js",
                      "~/Scripts/jquery.validate.unobtrusive.min.js",
                      "~/Content/js/vendor/jquery-1.10.2.min.js"
                            ));
              bundles.Add(new StyleBundle("~/Content/Login/css/DashBoardIndexstyle").Include(
                             "~/Content/Login/css/fontcss.css",
                             "~/Content/Login/css/style.css"
                             ));
            bundles.Add(new StyleBundle("~/Content/DashBoardIndexstyle2").Include(
                             "~/Content/DashBoard.css",
                             "~/Content/font-awesome-4.7.0/css/font-awesome.css",
                             "~/Content/font-awesome-4.7.0/css/font-awesome.min.css"

                             ));

            bundles.Add(new StyleBundle("~/Content/Login/css/DashBoardInboxStyle").Include(
                          "~/Content/Login/css/style.css"
                            ));
     
    
    

        }
    }
}

