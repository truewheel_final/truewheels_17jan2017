﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrueWheels.Web.MailClass
{
    public class MailTemplates
    {
        //Contact Us
        public const string SubjectContactUs = "Thanks for contacting us.";
        public const string ThanksContactUs = "Thanks for contacting us. Your reference no. is {0}. Please use the reference number for any further communication with us. ";
        public const string GetBack = "We will get back to you within 48hrs.";
        public const string Kindly = "Kindly connect us at truwheels.2016@gmail.com for any assistance.";
        public const string ThanksAgain = "Thanks again,";
        public const string Team = "Team Truewheels";

        //Forgot Password
        public const string SubjectFP = "Truewheels Password Change Notification";
        public const string ThanksNeeds = "Thanks for choosing us to solve all your parking needs.";
        public const string TWLoginPwd = "Your Forget Password OTP is {0}";
       
    
    }
}