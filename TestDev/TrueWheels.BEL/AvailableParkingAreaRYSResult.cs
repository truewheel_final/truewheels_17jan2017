﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class AvailableParkingAreaRYSResult
    {
        //**  neeraj changes for daily , weeklyand monthly rates of bike and car on SYS
        public string CarDailyCharge { get; set; }
        //public string DateTimeTo { get; set; }
        //public string DateTimeFrom { get; set; }
        //public string Error { get; set; }
        //public string Detail_ID { get; set; }
        //public string DescriptionSelected { get; set; }
        //public string ParkingClass { get; set; }
        //public string PopularParking { get; set; }
        //public string Active { get; set; }
        //public string FacilitySelected { get; set; }
        //public string No_Of_Space_Avaiable_bike { get; set; }
        //public string Open24Hour { get; set; }
        //public string DaysSelected { get; set; }

        //public string TimeTo { get; set; }
        //public string TimeFrom { get; set; }

        //public string Hourly_Pricing { get; set; }
        //public string Parking_Available_Date { get; set; }

        public string bikedailycharge { get; set; }
        //public string carweeklycharge { get; set; }
        //public string bikeweeklycharge { get; set; }
        public string carmonthlycharge { get; set; }
        public string bikemonthlycharge { get; set; }

        //public string vechileType { get; set; }
        //public string Status { get; set; }
        //public string owner_id { get; set; }
        //public string Space_type { get; set; }
        //public string Property_type { get; set; }
        //public string PropertyVerifiedStatus { get; set; }
        //public string PropertyPinCode { get; set; }
        //public string PropertyLandMark { get; set; }
        public string lattitude { get; set; }
        public string longitude { get; set; }

        //public string ParkingRating { get; set; }
        //public string Description { get; set; }

        //**done changes
        public Int64 Parking_Id { get; set; }

        public string ErrorMessage { get; set; }
        public bool Success { get; set; }

        // Added by neeraj for select SYS

        /// public string TransactionSuccess { get; set; }
        //public string Parking_Owner_Comment { get; set; }
        //public string Park_Space_Name { get; set; }
        //public string PropertyAddress { get; set; }
        //public string OwnerComments { get; set; }
        //public string parking_address { get; set; }
        //public string street { get; set; }
        //public string city { get; set; }

        //public string GeoLoc { get; set; }
        //// public int Error { get; set; }
        // public int Parking_id { get; set; }

        public string Distance { get; set; }


        //public Int64 Rating { get; set; }
        //public string No_Of_Space_Avaiable { get; set; }
        //public string state { get; set; }
        public string CarBasicCharge { get; set; }
        public string BikeBasicCharge { get; set; }

        //public Int64 TWAttendantId { get; set; }
        //// public string DescriptionType { get; set; }
        //public List<string> DescriptionType { get; set; }

        //public List<string> FacilityType { get; set; }
        //// public string FacilityType { get; set; }

        //public List<string> Space_Availablility { get; set; }
        ////public string Space_Availablility { get; set; }


        //public static int Count { get; set; }

        //public static object ToArray()
        //{
        //    throw new NotImplementedException();
        //}
    }
}
