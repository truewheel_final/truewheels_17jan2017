﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class PublicParkingAreaBEL
    {
        public decimal Main_Latitude { get; set; }


        public decimal Main_Longitude { get; set; }


        public int Distance { get; set; }


        public string FromDateTime { get; set; }

       public string ToDateTime { get; set; }

        public string ParkingClass { get; set; }

        public string OrderBy { get; set; }

        //public string Description { get; set; }

        public bool PopularParking { get; set; }



        //public string SearchDateTime { get; set; }
        //public string TimeTo { get; set; }

        //public string ParkingClass { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
