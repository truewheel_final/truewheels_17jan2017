﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
   public class OTPBEL
    {
       public bool sendMessage(long number, string message)
       {
           Stream data = null;
           StreamReader reader = null;
           try
           {
               long _number = number;
               string _message = message;
               WebClient client = new WebClient();
               long phno = _number;
               string massage = _message;
               //string baseurl = "http://bhashsms.com/api/sendmsg.php?user=lardeal&pass=123456&sender=LRDEAL&phone=" + _number + "&text=" + _message + "&priority=ndnd&stype=normal";
               string baseurl = "http://bhashsms.com/api/sendmsg.php?user=Truewheels&pass=Truewheels_2017&sender=TruWhl&phone=" + _number + "&text=" + _message + "&priority=ndnd&stype=normal";
               data = client.OpenRead(baseurl);
               reader = new StreamReader(data); string s = reader.ReadToEnd();
               data.Close();
               reader.Close();
               return true;
           }
           catch (Exception ex)
           {
               return false;
           }
           finally
           {
              // data.Close();
               //reader.Close();

           }

       }

       public List<string> GetAndSendOTP(long number)
       {
           
               List<string> OTP = new List<string>();
               Random rnd = new Random();
               int _OTPNO = rnd.Next(100000, 999999);
               long _number = Convert.ToInt64(number);
               bool MessageSent = false;
               // for later ---string _message = Convert.ToString(_OTPNO) + " is your Registration OTP. Treat this  as confidential. Sharing it with anyone gives them full access to your TrueWheels Wallet. TrueWheels never calls to verify your OTP.";
               string _message = Convert.ToString(_OTPNO)+ " is your Registration OTP. Treat this  as confidential. Sharing it with anyone gives them full access to your TrueWheels Account. TrueWheels never calls to verify your OTP.";
               OTP.Add(_OTPNO.ToString());
               OTP.Add(_number.ToString());
               
               //Session["OTP"] = OTP;
               if (sendMessage(_number, _message))
               {
                   MessageSent=true;
                   OTP.Add(Convert.ToString(MessageSent));
                   return OTP;
               }
               else
               {
                   OTP.Add(Convert.ToString(MessageSent));
                   return OTP;
                   // = string.Format("OTP sinding failed try again, Make sure your phone must be in network coverage\n OR try another phone");
               }
               
           
       }

       // send booking message to Vehicle owner

       //public List<string> GetAndSendOTPBooking(long number,long bookingNo)
       //{

       //    List<string> OTP = new List<string>();
       //    Random rnd = new Random();
       //    int _OTPNO = rnd.Next(100000, 999999);
       //    long _number = Convert.ToInt64(number);
       //    bool MessageSent = false;
       //    // for later ---string _message = Convert.ToString(_OTPNO) + " is your Registration OTP. Treat this  as confidential. Sharing it with anyone gives them full access to your TrueWheels Wallet. TrueWheels never calls to verify your OTP.";
       //    string _message = Convert.ToString(_OTPNO) + " is your Booking OTP. Treat this  as confidential. Please show your OTP to attendent to park your vehicle.TrueWheels never calls to verify your OTP.";
       //    OTP.Add(_OTPNO.ToString());
       //    OTP.Add(_number.ToString());

       //    //Session["OTP"] = OTP;
       //    if (sendMessage(_number, _message))
       //    {
       //        MessageSent = true;
       //        OTP.Add(Convert.ToString(MessageSent));
       //        return OTP;
       //    }
       //    else
       //    {
       //        OTP.Add(Convert.ToString(MessageSent));
       //        return OTP;
       //        // = string.Format("OTP sinding failed try again, Make sure your phone must be in network coverage\n OR try another phone");
       //    }


       //}

       //// Added by neeraj to send the message of booking details to Parking owner

       //public List<string> BookingDetailstoPO(long number, long bookingNo)
       //{

       //    List<string> OTP = new List<string>();
       //    Random rnd = new Random();
       //    int _OTPNO = rnd.Next(100000, 999999);
       //    long _number = Convert.ToInt64(number);
       //    long _bookingNo = Convert.ToInt64(bookingNo);
       //    bool MessageSent = false;
       //    // for later ---string _message = Convert.ToString(_OTPNO) + " is your Registration OTP. Treat this  as confidential. Sharing it with anyone gives them full access to your TrueWheels Wallet. TrueWheels never calls to verify your OTP.";
       //    string _message = Convert.ToString(_bookingNo) + "Booking ID has been cancelled , for more details reach to us through TrueWheel App";
       //    OTP.Add(_OTPNO.ToString());
       //    OTP.Add(_number.ToString());

       //    //Session["OTP"] = OTP;
       //    if (sendMessage(_number, _message))
       //    {
       //        MessageSent = true;
       //        OTP.Add(Convert.ToString(MessageSent));
       //        return OTP;
       //    }
       //    else
       //    {
       //        OTP.Add(Convert.ToString(MessageSent));
       //        return OTP;
       //        // = string.Format("OTP sinding failed try again, Make sure your phone must be in network coverage\n OR try another phone");
       //    }


       //}

       //// Added by neeraj to send the message of booking details to TW attendant 

       //public List<string> BookingDetailstoTWA(long number, long bookingNo)
       //{

       //    List<string> OTP = new List<string>();
       //    Random rnd = new Random();
       //    int _OTPNO = rnd.Next(100000, 999999);
       //    long _number = Convert.ToInt64(number);
       //    long _bookingNo = Convert.ToInt64(bookingNo);
       //    bool MessageSent = false;
       //    // for later ---string _message = Convert.ToString(_OTPNO) + " is your Registration OTP. Treat this  as confidential. Sharing it with anyone gives them full access to your TrueWheels Wallet. TrueWheels never calls to verify your OTP.";
       //    string _message = Convert.ToString(_bookingNo) + "Booking ID has been cancelled , for more details reach to us through TrueWheel App";
       //    OTP.Add(_OTPNO.ToString());
       //    OTP.Add(_number.ToString());

       //    //Session["OTP"] = OTP;
       //    if (sendMessage(_number, _message))
       //    {
       //        MessageSent = true;
       //        OTP.Add(Convert.ToString(MessageSent));
       //        return OTP;
       //    }
       //    else
       //    {
       //        OTP.Add(Convert.ToString(MessageSent));
       //        return OTP;
       //        // = string.Format("OTP sinding failed try again, Make sure your phone must be in network coverage\n OR try another phone");
       //    }


       //}

        
       //Added by neeraj to message Booking details to PO, VO and attendent

        
       public string RYSBookingMessage(long VOPhoneNo, long POphoneNo, string bookedby, string vehicleno, string BookedInTime, string BookedOutDatetime, string parkname, long AttendentNo, long SNphoneNo,long bookingNo)
       {


           long _bookingNo = Convert.ToInt64(bookingNo);
           string MessageNOtSentTo = string.Empty;
           long _number;
           string _message;



           if (bookedby == "VO")
           {
               _number = Convert.ToInt64(VOPhoneNo);
               _message = "Booking has been confirmed for parking " + Convert.ToString(parkname) + " , be on time at parking place , calculatar will start exact on your BookIn time, KIndly checkout while leaving the Parking.";
               if (!sendMessage(_number, _message))
                   MessageNOtSentTo = "VO";
               _number = Convert.ToInt64(POphoneNo);
               _message = "Booking of " + Convert.ToString(vehicleno) + " has been confirmed BookIn time is " + Convert.ToString(BookedInTime) + " and BookOut time is " + Convert.ToString(BookedOutDatetime) + " , for Parking " + Convert.ToString(parkname) + " ,Kindly available on Parking at BookIN time ";
               if (!sendMessage(_number, _message))
               { MessageNOtSentTo = MessageNOtSentTo + "PO"; }
               _number = Convert.ToInt64(AttendentNo);
               _message = "Booking of " + Convert.ToString(vehicleno) + " has been confirmed BookIn time is " + Convert.ToString(BookedInTime) + " and BookOut time is " + Convert.ToString(BookedOutDatetime) + " , for Parking " + Convert.ToString(parkname) + " ,Kindly confirm once vehicle Parked IN ";
               if (!sendMessage(_number, _message))
               { MessageNOtSentTo = MessageNOtSentTo + "Attendent"; }
               _number = Convert.ToInt64(SNphoneNo);
               _message = "Booking of " + Convert.ToString(vehicleno) + " has been confirmed BookIn time is " + Convert.ToString(BookedInTime) + " and BookOut time is " + Convert.ToString(BookedOutDatetime) + " , for Parking " + Convert.ToString(parkname) + " ,Kindly check the timing of vehicle when Parked IN ";
               if (!sendMessage(_number, _message))
               { MessageNOtSentTo = MessageNOtSentTo + "Attendent"; }

           }
           //else if (CancelledBy == "PO")
           //{
           //    _number = Convert.ToInt64(VOPhoneNo);
           //    _message = "Booking of " + Convert.ToString(vehicleno) + " has been cancelled by Parking Owner,cancellation reason is " + Convert.ToString(Canrsn) + " .";
           //    if (sendMessage(_number, _message))
           //        MessageNOtSentTo = "PO";
           //    _number = Convert.ToInt64(POphoneNo);
           //    _message = "Booking of " + Convert.ToString(vehicleno) + " has been cancelled , Parking is active to recieve other booking"; ;
           //    if (!sendMessage(_number, _message))
           //        MessageNOtSentTo = MessageNOtSentTo + "VO";
           //    _number = Convert.ToInt64(AttendentNo);
           //    _message = "Booking of " + Convert.ToString(vehicleno) + " has been cancelled , Wait for other booking";
           //    if (!sendMessage(_number, _message))
           //    { MessageNOtSentTo = MessageNOtSentTo + "Attendent"; }

           //}

           return MessageNOtSentTo;
       }
      

       // Added by neeraj to message cancel details to PO, VO and attendent
       public string RYSSendCancellationMessage(long VOPhoneNo, long POphoneNo, string CancelledBy, string vehicleno,string Canrsn,long AttendentNo,long SNphoneNo, long bookingNo)
       {

          
           long _bookingNo = Convert.ToInt64(bookingNo);
           string MessageNOtSentTo = string.Empty;
           long _number;
           string _message;
           
           

           if (CancelledBy == "VO")
           {
                _number = Convert.ToInt64(VOPhoneNo);
                _message = "Booking of " + Convert.ToString(vehicleno) + " has been cancelled , Book another parking to park your vehicle.";
               if (!sendMessage(_number, _message))
                   MessageNOtSentTo = "VO";
                _number = Convert.ToInt64(POphoneNo);
                _message = "Booking of " + Convert.ToString(vehicleno) + " has been cancelled by Vehicle Owner,cancellation reason is " + Convert.ToString(Canrsn) + " .";
                if (!sendMessage(_number, _message))
                { MessageNOtSentTo = MessageNOtSentTo + "PO"; }
               _number = Convert.ToInt64(AttendentNo);
               _message = "Booking of " + Convert.ToString(vehicleno) + " has been cancelled , Wait for other booking";
               if (!sendMessage(_number, _message))
               { MessageNOtSentTo = MessageNOtSentTo + "Attendent"; }
               _number = Convert.ToInt64(SNphoneNo);
               _message = "Booking of " + Convert.ToString(vehicleno) + " has been cancelled , Wait for other booking";
               if (!sendMessage(_number, _message))
               { MessageNOtSentTo = MessageNOtSentTo + "Support Number"; }
               
           }
           else if (CancelledBy == "PO")
           {
               _number = Convert.ToInt64(VOPhoneNo);
               _message = "Booking of " + Convert.ToString(vehicleno) + " has been cancelled by Parking Owner,cancellation reason is " + Convert.ToString(Canrsn) + " .";
               if (sendMessage(_number, _message))
                   MessageNOtSentTo = "PO";
               _number = Convert.ToInt64(POphoneNo);
               _message = "Booking of " + Convert.ToString(vehicleno) + " has been cancelled , Parking is active to recieve other booking"; ;
               if (!sendMessage(_number, _message))
                   MessageNOtSentTo = MessageNOtSentTo + "VO";
               _number = Convert.ToInt64(AttendentNo);
               _message = "Booking of " + Convert.ToString(vehicleno) + " has been cancelled , Wait for other booking";
               if (!sendMessage(_number, _message))
               { MessageNOtSentTo = MessageNOtSentTo + "Attendent"; }
               _number = Convert.ToInt64(SNphoneNo);
               _message = "Booking of " + Convert.ToString(vehicleno) + " has been cancelled , Wait for other booking";
               if (!sendMessage(_number, _message))
               { MessageNOtSentTo = MessageNOtSentTo + "Support Number"; }


           }

           return MessageNOtSentTo;
       }

      
      // Added by neeraj to send the message of support to who raised the support


       public List<string> SendSupportConfirmationtoID(long number, long bookingNo)
       {

           List<string> OTP = new List<string>();
           Random rnd = new Random();
           int _OTPNO = rnd.Next(100000, 999999);
           long _number = Convert.ToInt64(number);
           long _bookingNo = Convert.ToInt64(bookingNo);
           bool MessageSent = false;
           // for later ---string _message = Convert.ToString(_OTPNO) + " is your Registration OTP. Treat this  as confidential. Sharing it with anyone gives them full access to your TrueWheels Wallet. TrueWheels never calls to verify your OTP.";
           string _message =  "Thanks for choosing True Wheel,we have recieved your support request, We are looking into the issue, you will get response with in 24 hour";
           OTP.Add(_OTPNO.ToString());
           OTP.Add(_number.ToString());

           //Session["OTP"] = OTP;
           if (sendMessage(_number, _message))
           {
               MessageSent = true;
               OTP.Add(Convert.ToString(MessageSent));
               return OTP;
           }
           else
           {
               OTP.Add(Convert.ToString(MessageSent));
               return OTP;
               // = string.Format("OTP sinding failed try again, Make sure your phone must be in network coverage\n OR try another phone");
           }


       }

      // Added by neeraj to trigger checkout message
       public string RYSSendCheckoutMessage(long VOPhoneNo, long POphoneNo, string checkedoutBy, string vehicleno, string parkname, long AttendentNo,long SNphoneNo, long bookingNo)
       {


           long _bookingNo = Convert.ToInt64(bookingNo);
           string MessageNOtSentTo = string.Empty;
           long _number;
           string _message;



           if (checkedoutBy == "VO")
           {
               _number = Convert.ToInt64(VOPhoneNo);
               _message = " Vehicle " + Convert.ToString(vehicleno) + "has been checkedout from parking " + Convert.ToString(parkname) + " please pay cash.";
               if (!sendMessage(_number, _message))
                   MessageNOtSentTo = "VO";
               _number = Convert.ToInt64(POphoneNo);
               _message = " Vehicle " + Convert.ToString(vehicleno) + " has been checked out by Vehicle owner from parking " + Convert.ToString(parkname) + " Kindly collect the cash";
               if (!sendMessage(_number, _message))
               { MessageNOtSentTo = MessageNOtSentTo + "PO"; }
               _number = Convert.ToInt64(AttendentNo);
               _message = " Vehicle " + Convert.ToString(vehicleno) + " has been checkedout from parking " + Convert.ToString(parkname);
               if (!sendMessage(_number, _message))
               { MessageNOtSentTo = MessageNOtSentTo + "Attendent"; }
               _number = Convert.ToInt64(SNphoneNo);
               _message = " Vehicle " + Convert.ToString(vehicleno) + " has been checkedout from parking " + Convert.ToString(parkname) + " Kindly collect the cash ";
               if (!sendMessage(_number, _message))
               { MessageNOtSentTo = MessageNOtSentTo + "Attendent"; }

           }
           else if (checkedoutBy == "PO")
           {
               _number = Convert.ToInt64(VOPhoneNo);
               _message = " Vehicle " + Convert.ToString(vehicleno) + " has been checked out by Parking owner from parking " + Convert.ToString(parkname) + " Please pay the cash and contact us in case any support needed";
               if (sendMessage(_number, _message))
                   MessageNOtSentTo = "PO";
               _number = Convert.ToInt64(POphoneNo);
               _message = " Vehicle " + Convert.ToString(vehicleno) + "has been checkedout from parking " + Convert.ToString(parkname) + " please collect the cash.";
               if (!sendMessage(_number, _message))
                   MessageNOtSentTo = MessageNOtSentTo + "VO";
               _number = Convert.ToInt64(AttendentNo);
               _message = " Vehicle " + Convert.ToString(vehicleno) + " has been checkedout from parking " + Convert.ToString(parkname);
               if (!sendMessage(_number, _message))
               { MessageNOtSentTo = MessageNOtSentTo + "Attendent"; }
               _number = Convert.ToInt64(SNphoneNo);
               _message = " Vehicle " + Convert.ToString(vehicleno) + " has been checkedout from parking " + Convert.ToString(parkname) + " Kindly collect the cash ";
               if (!sendMessage(_number, _message))
               { MessageNOtSentTo = MessageNOtSentTo + "Attendent"; }
               


           }

           return MessageNOtSentTo;
       }

    }
}
//CheckoutDetailstoPO
//CheckoutDetailstoTWA
//CheckoutDetailstoVO
//SendSupportConfirmationtoID
//RYSSendCheckoutMessage
//RYSBookingMessage