﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class StandardCodesResponse
    {
        private string category = string.Empty;
        private List<SystemCodes> categoryProperty;

        public List<SystemCodes> CategoryProperty
        {
            get { return categoryProperty; }
            set { categoryProperty = value; }
        }

        public string  Category
        {
            get { return category; }
            set { category = value; }
        }
    }
}
