﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class OnProfileClick
    {
       // public string owner_id { get; set; }

        public Int64 User_id { get; set; }

        // public int Vehicle_Id { get; set; }
        public string Full_Name { get; set; }

        public string DOB { get; set; }


        public string Phone_No1 { get; set; }

        public string Phone_No2 { get; set; }

        //public string Phone_No1 { get; set; }

        public string Driving_License { get; set; }

        public string PAN { get; set; }

        public string Email_id { get; set; }

      

        private List<Vehicles> ListVehicle;

        public List<Vehicles> listVehicle
        {
            get { return ListVehicle; }
            set { ListVehicle = value; }
        }
        //public string Vehicle_No1 { get; set; }
        //public string Vehicle_No2 { get; set; }
        //public string Vehicle_No3 { get; set; }
        //public string Vehicle_No4 { get; set; }
        //public string Vehicle_No5 { get; set; }
        //public string Vehicle_Model1 { get; set; }
        //public string Vehicle_Model2 { get; set; }
        //public string Vehicle_Model3 { get; set; }
        //public string Vehicle_Model4 { get; set; }
        //public string Vehicle_Model5 { get; set; }
        //public string Vehicle_Type1 { get; set; }
        //public string Vehicle_Type2 { get; set; }
        //public string Vehicle_Type3 { get; set; }
        //public string Vehicle_Type4 { get; set; }
        //public string Vehicle_Type5 { get; set; }

    }

    public class Vehicles
    {
        public string Vehicle_No { get; set; }

        public string Vehicle_Model { get; set; }

        public string Vehicle_Type { get; set; }
    }
}
