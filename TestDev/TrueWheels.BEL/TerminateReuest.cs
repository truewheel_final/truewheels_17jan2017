﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
   public class TerminateReuest
    {
       public string Booking_ID { get; set; }
       public string Action { get; set; }

       public string Status { get; set; }

       public string FailureReason { get; set; }
    }
}
