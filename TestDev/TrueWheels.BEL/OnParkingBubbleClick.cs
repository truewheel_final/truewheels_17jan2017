﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class OnParkingBubbleClick
    {

        public Int64 Parking_id { get; set; }

        public string Parking_Space_Name { get; set; }
        
        public string Parking_Available_Date { get; set; }
        public string parking_address { get; set; }
        public string CarBasicCharge { get; set; }
        public string BikeBasicCharge { get; set; }
        public string CarDailyCharge { get; set; }
        public string bikedailycharge { get; set; }
        public string bikemonthycharge { get; set; }
        public string carmonthlycharge { get; set; }
        public string No_Of_Space_Avaiable { get; set; }
        public string No_Of_Space_Avaiable_bike { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public string DaysSelected { get; set; }

        public string DescriptionSelected { get; set; }

        public string FacilitySelected { get; set; }
        public string Parking_Owner_Comment { get; set; }


    }
}
