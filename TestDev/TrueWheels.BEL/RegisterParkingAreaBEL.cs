using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class RegisterParkingAreaBEL
    {
        public int owner_id { get; set; }
        public string Space_type { get; set; }
        public string Property_type { get; set; }

        public string No_of_space { get; set; }


        public string state { get; set; }
        public string VechileType { get; set; }
        public string PropertyVerifiedStatus { get; set; }
        public string FacilityGroupId { get; set; }
        public string PropertyAddress { get; set; }
        public int PropertyPinCode { get; set; }
        public string PropertyZone { get; set; }
        public string PropertyLandMark { get; set; }
        public string OwnerComments { get; set; }
        public string lattitude { get; set; }
        public string longitude { get; set; }

        //amit changes for SYS to set properties

        public string datetimeFrom { get; set; }
        public string datetimeTo { get; set; }
        public string parkingAvailable { get; set; }

        public string open24hours { get; set; }
        public string description { get; set; }
        public string BikeBasicCharge { get; set; }
        public string CarBasicCharge { get; set; }
        public string parkingSpaceName { get; set; }
        public string Park_Space_Name { get; set; }


        public string Status { get; set; }

        public string parking_Id { get; set; }

        public string facilitySelected { get; set; }

        public string daysSelected { get; set; }

        public string descriptionSelected { get; set; }
        public string Parking_support_no { get; set; }


        public List<string> missingcolumn()
        {
            List<string> clmns = new List<string>();
            return clmns;
        }

        // neeraj changes for daily , weeklyand monthly rates of bike and car on SYS

        public string CarDailyCharge { get; set; }

        public string bikedailycharge { get; set; }
        public string carweeklycharge { get; set; }
        public string bikeweeklycharge { get; set; }
        public string carmonthlycharge { get; set; }
        public string bikemonthycharge { get; set; }

        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string No_Of_Space_Avaiable_bike { get; set; }
        public string Parking_Available_Date { get; set; }
        public string Hourly_Pricing { get; set; }

        //Added by neeraj for all RYS parking on MAP

        //public string Latitude { get; set; }
        //public string Longitude { get; set; }
        //public string distance { get; set; }
        //public string SearchDateTime { get; set; }
        ////public string TimeFrom { get; set; }
        //public string Error { get; set; }
        //public string Status { get; set; }


        // done changes
    }
    //public class SelectedDays
    //{
    //    public string daysSelected { get; set; }
    //}

    //public class DetailDescription
    // {
    //     public string DescriptionType {get;set;}
    // }

    // public class FacilityType { 
    //     public string FacilityTypeDes { get; set; } 
    // }



}




