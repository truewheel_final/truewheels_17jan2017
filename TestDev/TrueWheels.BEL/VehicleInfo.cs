﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class VehicleInfo
    {
        public Int64 User_id { get; set; }

        private List<UsersVehicles> ListVehicle;

        public List<UsersVehicles> listVehicle
        {
            get { return ListVehicle; }
            set { ListVehicle = value; }
        }
    }
    public class UsersVehicles
        {
            public string Vehicle_No { get; set; }

            public string Vehicle_Model { get; set; }

            public string Vehicle_Type { get; set; }
        }
    }

