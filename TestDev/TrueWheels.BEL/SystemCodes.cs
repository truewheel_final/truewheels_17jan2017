﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
   public class SystemCodes
    {

       private string propertyName = string.Empty;
       private string propertyCode = string.Empty;
       private string propertyDescription = string.Empty;
       public string PropertyName
       {
           get { return propertyName; }
           set { propertyName = value; }
       }
       public string PropertyCode
       {
           get { return propertyCode; }
           set { propertyCode = value; }
       }
       public string PropertyDescription
       {
           get { return propertyDescription; }
           set { propertyDescription = value; }
       }

    }
}
