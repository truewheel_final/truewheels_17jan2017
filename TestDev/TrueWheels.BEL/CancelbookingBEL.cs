﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class CancelbookingBEL
    {
        public int BookedID { get; set; }

        public string CancellationReason { get; set; }

        public string ErrorMessage { get; set; }
        public bool Success { get; set; }
    }
}
