﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
//using System.Web.Mvc;

namespace TrueWheels.DAL
{
    public class UserDetailsBEL
    {
        
        public string User_ID
        {
            get;
            set;
        }


        public string User_Name
        {
            get;
            set;
        }


        public string Phone_No1
        {
            get;
            set;
        }


        public string Password
        {
            get;
            set;
        }


        public string Email_Id
        {
            get;
            set;
        }


        public string First_Name
        {
            get;
            set;
        }


        public string Last_Name
        {
            get;
            set;
        }


        public string Owner_Address
        {
            get;
            set;
        }


        public string Phone_No2
        {
            get;
            set;
        }


        public string Alternate_Email_Id
        {
            get;
            set;
        }


        public string SignUp_Mode_ID
        {
            get;
            set;
        }

        public string Last_Login
        {
            get;
            set;
        }

        public string ErrorMessage
        {
            get;
            set;
        }

        public string Other_ID
        {
            get;
            set;
        }


        public string Name { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        
    }

    public class PBDetailsBEL
    {

        public string user_ID
        {
            get;
            set;
        }


        public string user_Name
        {
            get;
            set;
        }


        public string phone_No1
        {
            get;
            set;
        }


        public string Password
        {
            get;
            set;
        }


        public string Email_Id
        {
            get;
            set;
        }


        public string First_Name
        {
            get;
            set;
        }


        public string Last_Name
        {
            get;
            set;
        }


        public string Address
        {
            get;
            set;
        }


        public string Phone_No2
        {
            get;
            set;
        }


        public string Alternate_Email_Id
        {
            get;
            set;
        }


        public string SignUp_Mode_ID
        {
            get;
            set;
        }

        public string Last_Login
        {
            get;
            set;
        }

        public string ErrorMessage
        {
            get;
            set;
        }

        public string Other_ID
        {
            get;
            set;
        }


        public string name { get; set; }
        public string email { get; set; }

        public string dob { get; set; }

        public string isAdminRightsAssigned { get; set; }

        public string pA_CustId { get; set; }

        public Int64 working_location { get; set; }

        public string picUrl { get; set; }

        
        public string docURL { get; set; }

        public string workLocationName { get; set; }

        public string isMobileVerified { get; set; }

        public string status { get; set; }
        public string userType { get; set; }
        public Int32 parking_id { get; set; }
        public string Parking_Name { get; set; }
        public List<ParkingLocationList> parkingLocationList { get; set; }
        public List<SelectListItem> ParkingItemList { get; set; }
        public string ParkingName { get; set; }
        public string[] ParkingNames { get; set; }
        public string ParkinIds { get; set; }
        public Int32[] ParkinIdsList { get; set; }

    }

    public class ParkingLocationList
    {
        public Int32 parking_id { get; set; }
        public string Parking_Name { get; set; }

        public string ErrorMessage { get; set; }
    }

    public class UtilityLoginBEL
    {

        public string MobileNumber
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public string UserType
        {
            get;
            set;
        }

        public string AuthToken
        {
            get;
            set;
        }


    }

    public class UserBookingBEL
    {
        public string BookingID
        {
            get;
            set;
        }
        public string ErrorMessage
        {
            get;
            set;
        }

        public string OTP
        {
            get;
            set;
        }

        public string mobileNumber
        {
            get;
            set;
        }

        public string vehicleNumber
        {
            get;
            set;
        }

        public string planOpt
        {
            get;
            set;
        }

        public string expiryDate
        {
            get;
            set;
        }

        public string message
        {
            get;
            set;
        }

        public string CheckedIn
        {
            get;
            set;
        }

        public string CheckedOut
        {
            get;
            set;
        }

        public string BookedInOn
        {
            get;
            set;
        }
    }
                                            
                                                
    public class BookingBriefReportBEL
    {        
        public string RegularIN { get; set; }
        public string RegularOut { get; set; }
        public string RegularTot { get; set; }
        public string MBTVehicleIn { get; set; }
        public string MBTVehicleOut { get; set; }
        public string MBTVehicleTot { get; set; }
        public string MDTVehicleIn { get; set; }
        public string MDTVehicleOut { get; set; }
        public string MDTVehicleTot { get; set; }
        public string MNTVehicleIn { get; set; }
        public string MNTVehicleOut { get; set; }
        public string MNTVehicleTot { get; set; }

        public string DailyCollection { get; set; }
        public string WeeklyCollection { get; set; }
        public string MonthlyCollection { get; set; }
        public string TotalCollection { get; set; }

        public string DailyHelmetCollection { get; set; }
        public string WeeklyHelmetCollection { get; set; }
        public string MonthlyHelmetCollection { get; set; }
        public string TotalHelmetCollection { get; set; }




    }

    public class ReportBEL
    {
        public string period { get; set; }
        public List<string> locationID { get; set; }
    }
}
