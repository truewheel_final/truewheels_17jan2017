﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class ProfileOTPPhoneNoVerified
    {
        public string UserId { get; set;}
        public string IsMobileVerified { get; set; }

        public bool VerifiedSuccessfully { get; set; }

        public string PhoneNo { get; set; }

        public string ErrorMessage { get; set; }
    }
}
