﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class OnPublicParkingBubbleClick
    {
        public string lattitude { get; set; }
        public Int64 parking_id { get; set; }
        public string longitude { get; set; }

        public string propertyaddress { get; set; }
        public string No_of_space { get; set; }
        public string No_of_space_bike { get; set; }
        public string Space_type { get; set; }
        public string Property_type { get; set; }
        public string Description { get; set; }
        public string BookingAvailableFlag { get; set; }
        public string CarBasicCharge { get; set; }
        public string BikeBasicCharge { get; set; }
        public string BikeNightCharge { get; set; }
        public string CarNightCharge { get; set; }
        public string BikeMonthlyDayCharge { get; set; }
        public string CarMonthlyDayCharge { get; set; }
        public string BikeMonthlyNightCharge { get; set; }
        public string CarMonthlyNightCharge { get; set; }
        public string BikeMonthlyCharge { get; set; }
        public string CarMonthlyCharge { get; set; }


    }
}
