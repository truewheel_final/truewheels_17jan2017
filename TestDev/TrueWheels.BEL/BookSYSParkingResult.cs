﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class BookSYSParkingResult
    {
    //    public Int64 login_userId { get; set; }

    //    public Int64 owner_userId { get; set; }
    //    public string parkingId { get; set; }

    //    public string vehicalNo { get; set; }

    //    public string mobileNo { get; set; }

    //    public string bookedInTime { get; set; }

    //    public string bookedOutDatetime { get; set; }

    //    public string checkedInDateTime { get; set; }

        public string OTP { get; set; }
        //public string vehicleType { get; set; }

        //public string paymentMode { get; set; }
       //public string paymentAmount { get; set; }
        public Int64 bookingID { get; set; }

        //public string extraHours { get; set; }
        //public string Helmet { get; set; }

        //public bool services { get; set; }

        public string Error { get; set; }

        public bool Status { get; set; }

        //** Added by Neeraj to display details to the owner who booked parking

        public Int64 Booked_Id { get; set; }
        public string VehicalNumber { get; set; }
        public string MobileNo { get; set; }
        public string BookingDateTime { get; set; }
        public string BookedIntime { get; set; }
        public string BookedOutTime { get; set; }
        public string PaymentMode { get; set; }
        public string parking_address { get; set; }
        public string OwnerComments { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public Int64 PropertyPinCode { get; set; }
        public string Park_Space_Name { get; set; }
        //public Int64 OTP { get; set; }
        public Int64 TWAttendantId { get; set; }

        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Phone_No1 { get; set; }

        public string Attendant_Phone_No { get; set; }
        public string Attendant_Name { get; set; }

        public string BookedOutTimeMonthly { get; set; }
        public string BookedInTimeMonthly { get; set; }

        public string Transaction_Id { get; set; }





       // public string Error { get; set; }
       // public string Error { get; set; }



    }
}
