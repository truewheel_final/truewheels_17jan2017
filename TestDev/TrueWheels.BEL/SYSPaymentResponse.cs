﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class SYSPaymentResponse
    {
        public bool Status { get; set; }
        public string Error { get; set; }

        //  public string Cost { get; set; }

        public Int64 Attendant_Phone_No { get; set; }

        public string Attendant_Name { get; set; }

        public string First_Name { get; set; }

        public string Full_Name { get; set; }

        public string Last_Name { get; set; }

        public string Number { get; set; }

        public string Parking_support_no { get; set; }

        public string Owner_Address { get; set; }

        public string PaidAmount { get; set; }

        public string Booking_ID { get; set; }

        public string Payment_Transaction_ID { get; set; }

        public string Transaction_ID { get; set; }
        public string TotalAmount { get; set; }

       // public string vehicleno { get; set; }



    }
}
