﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class BookingResult
    {
        public string parkingId { get; set; } 

        public string vehicalNo { get; set; }

        public string mobileNo { get; set; }

        public string bookedInTime { get; set; }

        public string bookedOutDatetime { get; set; }

        public string checkedInDateTime { get; set; }

        public string checkedOutDateTime { get; set; }

        public string allotedSeat { get; set; }

        public string OTP { get; set; }

        public string TotalAmount { get; set; }

        public string TotalHours { get; set; }

        public string TotalNights { get; set; }

        public string NightsCharge { get; set; }

        public string TotalMinutes { get; set; }

        public Int64 bookingID { get; set; }

        public string parkingaddress { get; set; }

        public string ErrorMessage { get; set; }

        public string MonthlySubscription { get; set; }

        public string vehiclewheels { get; set; }

        public string MonthlyDailyCheckIN { get; set; }

        public string UnSuscriptionDate { get; set; }

        public string AutoSuscription { get; set; }

        public string Isnumberchanged { get; set; }

          	 
    }
}
