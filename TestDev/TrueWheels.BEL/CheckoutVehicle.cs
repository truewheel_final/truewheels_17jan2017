﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
   public class CheckoutVehicle
    {
        public bool Status { get; set; }
        public string Error { get; set; }

        public string TotalAmount { get; set; }
        public string Total_Days { get; set; }
        public string Total_Hour { get; set; }
        public string Total_Min { get; set; }
        public string ExtraAmount { get; set; }
        public string Extra_Hour { get; set; }
        public string Extra_Min { get; set; }

        public string Extra_month { get; set; }

        public string Final_amount { get; set; }
    }
}
