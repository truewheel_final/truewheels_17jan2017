using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class BookSYSParking
    {

       public Int64 login_userId { get; set; }

        public Int64 owner_userId { get; set; }
        public string parkingId { get; set; }

        public string Bank_Name { get; set; }
        public string Card_Type { get; set; }


        public string vehicalNo { get; set; }

        public string mobileNo { get; set; }

        public string bookedInTime { get; set; }

        public string bookedOutDatetime { get; set; }

        public string checkedInDateTime { get; set; }

        public string OTP { get; set; }
        public string vehicleType { get; set; }

        public string paymentMode { get; set; }
        public string paymentAmount { get; set; }
        public Int64 bookingID { get; set; }

        public string extraHours { get; set; }
        public string Helmet { get; set; }

        public bool services { get; set; }
        public string Error { get; set; }

        public string vehicleWheel { get; set; }
        public string monthlySubscription { get; set; }
        public string hourBaseParking { get; set; }
        public string BookingStatus { get; set; }

        public string BookedInTimeMonthly { get; set; }
        public string BookedOutTimeMonthly { get; set; }

        public bool Status { get; set; }

        public string Transaction_ID { get; set; }

        public string BaseAmount { get; set; }
        public string PayableToRYSAmount { get; set; }
        public string BookingTDSAmount { get; set; }
        public string TotalAmount { get; set; }
        public string TWServiceCharge { get; set; }
        public string RYSTDSAmount { get; set; }


    }
}
