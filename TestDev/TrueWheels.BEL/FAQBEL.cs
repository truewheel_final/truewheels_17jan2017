﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
     public class FAQBEL
    {
        public List<string> QuestionsList { get; set; }

        public List<string> AnswersList { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        public string Id { get; set; }

        public string QuestionDesc { get; set; }

        public int id { get; set; }

        public int QuestionId { get; set; }

        public string AnswerDesc { get; set; }
    }
}
