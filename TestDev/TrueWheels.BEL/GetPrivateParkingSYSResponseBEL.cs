﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class GetPrivateParkingSYSResponseBEL
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string distance { get; set; }
        public string SearchDateTime { get; set; }
        
        public string TimeFrom { get; set; }
        public string Error { get; set; }
        public string Status { get; set; }
    }
}
