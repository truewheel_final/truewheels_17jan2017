﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class PersonalinfoAppBEL
    {
        public int userId { get; set; }

        public int Vehicle_Id { get; set; }
        public string fullName { get; set; }

        public string Full_Name { get; set; }
        //[Required(AllowEmptyStrings = false, ErrorMessage = "First Name Required.")]
        public string firstName { get; set; }
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Last Name Required.")]
        public string lastName { get; set; }

        public string displayName { get; set; }

        //[Required(AllowEmptyStrings = false, ErrorMessage = "DOB Required.")]
        //[DisplayFormat(DataFormatString = "{0:dd MMM, yyyy}")]
        public DateTime? dob { get; set; }


        public string mobileNo { get; set; }

        // [Required(ErrorMessage = "Your must provide a PhoneNumber")]
        //[Display(Name = "PhoneNo")]
        //[DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public string alternateNo { get; set; }

        public string Phone_No1 { get; set; }

        public string IsMobileVerified { get; set; }

        public string SignUp_Mode_ID { get; set; }
        public string drivingLicense { get; set; }

        public string panNo { get; set; }

        //[Required(ErrorMessage = "Email is Required")]
        //[DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string emailID { get; set; }

        //[DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string alternateEmailId { get; set; }

        public string lastLogin { get; set; }

        public string ownerAddress { get; set; }

        public char ownerVerificationStatus { get; set; }

        //[DataType(DataType.Password)]
        //[Required(ErrorMessage = "Password is Required")]
        public string password { get; set; }


        //[DataType(DataType.Password)]
        //[Display(Name = "Old password")]
        public string matchPassword { get; set; }


        //[StringLength(100, ErrorMessage = "The password must be at least 6 characters long.")]
        //[DataType(DataType.Password)]
        //[Display(Name = "New Password")]
        public string newPassword { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm password")]
        //[Compare("newPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string confirmNewPassword { get; set; }

        public string profilePicPath { get; set; }

        //public HttpPostedFileBase profilePic { get; set; }

        public string errorMessage { get; set; }
        public bool success { get; set; }

        public List<DashBoardBEL> menuList { get; set; }

        public string Vehicle_No1 { get; set; }
        public string Vehicle_No2 { get; set; }
        public string Vehicle_No3 { get; set; }
        public string Vehicle_No4 { get; set; }
        public string Vehicle_No5 { get; set; }
        public string Vehicle_Model1 { get; set; }
        public string Vehicle_Model2 { get; set; }
        public string Vehicle_Model3 { get; set; }
        public string Vehicle_Model4 { get; set; }
        public string Vehicle_Model5 { get; set; }
        public string Vehicle_Type1 { get; set; }
        public string Vehicle_Type2 { get; set; }
        public string Vehicle_Type3 { get; set; }
        public string Vehicle_Type4 { get; set; }
        public string Vehicle_Type5 { get; set; }


    }
}
