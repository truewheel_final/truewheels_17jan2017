﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueWheels.BEL
{
    public class BookingRecievedSYS
    {

        public string Full_Name { get; set; }
        public string Parking_Space_Name { get; set; }
        public  string BookingDateTime { get; set; }
        public string CheckedOutDateTime { get; set; }

        public string Total_month { get; set; }
        public  string BookedIntime { get; set; }

        public  string BookedOutTime { get; set; }

       // public  string PayableToRYSAmount { get; set; }

       // public  string Last_Name { get; set; }

        public  string VehicalNumber { get; set; }

        //public  string VehicleWheel { get; set; }

        public  string VehicleType { get; set; }

       // public  string RYSTDSAmount { get; set; }

        public  string MonthlySubscription { get; set; }

        public Int64 Booked_Id { get; set; }
        public string Total_Days { get; set; }
        public string Total_Hour { get; set; }
        public string Total_Min { get; set; }
        //public string Extra_Days { get; set; }
        //public string Extra_Hour { get; set; }
        //public string Extra_Min { get; set; }
        //public string Extra_month { get; set; }
        //public string PayableToRYSAmountExtra { get; set; }
        //public string RYSTDSAmountExtra { get; set; }
       // public Int64 TWAttendantId { get; set; }
        public Int64 Phone_No1 { get; set; }

        public string parking_address { get; set; }
        public string BookingStatus { get; set; }
        public string Base_Amount { get; set; }
        //public string Total_Hour { get; set; }


        public  bool Success { get; set; }
    }
}
